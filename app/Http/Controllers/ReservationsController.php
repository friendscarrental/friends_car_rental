<?php namespace App\Http\Controllers;

use App\Paypal\PaypalPayment;

use App\Models\Reservation;
use App\Models\Payment;
use App\Models\Location;
use App\Models\Driver;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ReservationsController extends AdminController
{
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reservations = Reservation::orderby('pickup_datetime', 'ASC')
						->paginate(25);
		
		return view('admin.reservations', ['resourceName'=>'reservations', 'records' => $reservations]);
    }
	
	/**
     * Finished/Old Reservations
	 * Passed their due date
     */
    public function past($vehicle_id=null)
    {
		$viewParameters = ['resourceName'=>'reservations'];
		if($vehicle_id != null)
		{
			$reservations = Reservation::where('status', 2)
							->orWhereRaw('reservations.return_datetime < NOW() - INTERVAL 1 DAY')
							->where('id_vehicle', $vehicle_id)
							->orderby('return_datetime', 'DESC')
							->paginate(25);
			
			$viewParameters['filtered'] = true;
		}
		else
		{
			$reservations = Reservation::where('status', 2)
							->orWhereRaw('reservations.return_datetime < NOW() - INTERVAL 1 DAY')
							->orderby('return_datetime', 'DESC')
							->paginate(25);
			
			$viewParameters['filtered'] = false;
		}
		$viewParameters['records'] = $reservations;
		
		return view('admin.reservationsPast', $viewParameters);
    }
	
	/**
     * change status: approve or cancel/denie
     */
    public function status($id, $status)//status=> 0=cancel, 1=approve, 2=try approving
    {
		$reservation = Reservation::find($id);
		
		if($status == 2)
		{			
			$query = "SELECT COUNT(1) AS r_count
						FROM reservations AS r 
						WHERE r.id != {$reservation->id}
						  AND r.id_vehicle = {$reservation->id_vehicle}
						  AND r.status = 1
						  AND r.pickup_datetime < '{$reservation->return_datetime}' 
						  AND r.return_datetime > '{$reservation->pickup_datetime}' ";						  
			$result = \DB::select($query);
			
			if($result[0]->r_count > 0)
			{
				$validator = \Validator::make([], 
					['Record_not_found'=>'required'], 
					['Record_not_found.required'=>"Reservation can NOT be recovered because <b>{$reservation->vehicle_title}</b> has been reserved by someone else in the same period."]
				);
				$validator->fails();
				return back()->withErrors($validator);
			}
			else{
				$status = 1;
			}
		}
		
		if($status == 1)
		{
			if($reservation->pay_later_request == 1)
				$reservation->pay_later_request = 2;//request approved
				
			$reservation->status = 1;
			$reservation->approved = 1;
			$currentUser = \Auth::user();			
			$reservation->id_user_approved = $currentUser->id;
			$reservation->approved_user_name = $currentUser->firstname.' '.$currentUser->lastname;
			$reservation->approved_at = date('Y-m-d H:i:s');
			
			$reservation->save();
		}
		else
		{	
			if($reservation->pay_later_request == 1)
				$reservation->pay_later_request = 3;//request refused
				
			$reservation->status = 0;
			$reservation->approved = 3;//canceled
			$reservation->id_user_approved = -1;
			$reservation->approved_user_name = null;
			$reservation->approved_at = null;
			$reservation->save();
		}
				
		return redirect('reservations/'.$reservation->id);
    }
	
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $query = " SELECT reservations.id
						, reservations.id_vehicle
						, reservations.vehicle_title
						, CONCAT(users.firstname, ' ', users.lastname) AS user_fullname
						, reservations.primary_driver_name
						, reservations.pickup_datetime
						, reservations.pickup_location
						, reservations.return_datetime
						, reservations.return_location
						, reservations.duration_days
						, reservations.created_at
						, reservations.approved
						, reservations.id_user_approved
						, reservations.approved_user_name
						, reservations.approved_at
						, reservations.total_price
						, reservations.payment_completed
						, reservations.security_deposit
						, reservations.id_payment
						, reservations.pay_later_request
						, reservations.request_description
						, reservations.status
					 FROM reservations
					INNER JOIN users 
					   ON (reservations.id_user = users.id)
					WHERE reservations.id = :reservationID";
		$reservation = \DB::select($query, [':reservationID'=> $id]);
		$reservation = $reservation[0];
		
        $query = " SELECT payments.id
						, payments.amount
						, payments.id_paymentmethod
						, paymentmethod.name AS paymentmethod
						, payments.id_paymenttype
						, paymenttype.name AS paymenttype
						, payments.id_user_verified
						, payments.verified_by
						, payments.status
						, IF(payments.id_paymentmethod = 1, CONCAT('Paypal ID: ', payments.paypal_payment_id), payments.details) AS details
						, IFNULL(payments.paid_at, payments.created_at) AS paid_at
					 FROM payments
					INNER JOIN paymenttype 
					   ON (payments.id_paymenttype = paymenttype.id)
					INNER JOIN paymentmethod 
					   ON (payments.id_paymentmethod = paymentmethod.id)
					WHERE payments.id_reservation = :reservationID";
		$payments = \DB::select($query, [':reservationID'=> $id]);
		
		$entries = \DB::table('reservation_entries')
							->where('id_reservation', $id)
							->where('deleted', 0)
							->get();
		
		$query = " SELECT drivers.id
						, drivers.firstname
						, drivers.lastname
						, drivers.passport
						, drivers.idcard
						, DATE_FORMAT(drivers.birthdate,'%d-%b-%Y') AS birthdate
						, drivers.birthplace
						, b_country.name AS birth_country
						, drivers.license_number
						, DATE_FORMAT(drivers.license_expiration,'%d-%b-%Y') AS license_expiration
						, drivers.email
						, address.address
						, address.zip_code
						, address.city
						, address.phone_1
						, address.phone_2
						, a_country.name AS address_country
						, rd.primary
					FROM
						drivers
						INNER JOIN address 
							ON (drivers.id = address.id_driver)
						INNER JOIN countries AS b_country
							ON (drivers.id_country_birth = b_country.id)
						INNER JOIN countries AS a_country
							ON (address.id_country = a_country.id)
						INNER JOIN reservation_drivers AS rd
							ON (rd.id_driver = drivers.id)
					WHERE rd.id_reservation = :reservationID
					  AND drivers.deleted = 0
					  AND address.type_home = 1						  
					ORDER BY rd.primary DESC";
		$drivers = \DB::select($query, [':reservationID'=> $id]);
		
		$reservation_status = "";
		if($reservation->status==0)
		{
			$reservation_status = "Canceled";
			if($reservation->approved == 2)
				$reservation_status = "Expired";
		}else{
			if($reservation->approved == 1)
				$reservation_status = "Approved";
			else
				$reservation_status = "Pending";
		}
		
		$data = [
			'model' => $reservation, 
			'payments' => $payments, 
			'entries' => $entries, 
			'drivers' => $drivers, 
			'reservation_status' => $reservation_status
		];
		
        return view('admin.reservationsView', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		$modelData = Reservation::find($id);
		return view('admin.reservationsForm')->with('model', $modelData);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		try {
			$model = Reservation::find($id);
			$model->deleted = 1;
			$model->save();
		}
		catch (\Exception $e){}//skip errors
		
		return response()->json('ok', 200);
    }
}
