<?php namespace App\Http\Controllers;

use App\Paypal\PaypalPayment;

use App\Models\Payment;
use App\Models\Reservation;

use App\Models\Driver;
use App\Models\Address;
use App\Models\Country;
use App\Models\User;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;

use Auth;

class PublicController extends Controller 
{
	public function index()
	{
		
	}
	
	public function registerStepOne()
	{		
		return view('public.registerSignup');
	}
	
	public function registerStepOneSave(Request $request)
	{	
        $input = $request->all();
		try {
			
			$fieldLabelNames = array(
		        'firstname' => 'Firstname',
		        'lastname' => 'Lastname',
		        'email' => 'Email',
		        'password' => 'Password',
			);
	        $requiredFields = array(
		        'firstname' => 'required',
		        'lastname' => 'required',
		        'email' => 'required|email|unique:users',
		        'password' => 'required|min:4',
	        );
			
	        $validator = \Validator::make($input, $requiredFields);
	        $validator->setAttributeNames($fieldLabelNames);

	        if($validator->fails())
		        throw new \Exception('Validation Failed.');
			
			$sessionData = [
				'email' 	=> $request->get('email'),
				'firstname' => $request->get('firstname'),
				'lastname'  => $request->get('lastname'),
				'password'  => $request->get('password')
			];
			$request->session()->put('register_step_one_data', $sessionData);
			
			return redirect('register-driver');
		}
		catch (\Exception $e)
		{
			info($e->getMessage(), [$e->getLine()]);
		}
		
		return back()->withErrors($validator)->withInput()->withInput($input);
	}
	
	public function registerStepTwo(Request $request)
	{		
		if( session()->has('register_step_one_data') )
		{
			$sessionData = session()->get('register_step_one_data');

			$countries = Country::all();
			$stepTwoData = [
				'init_registration'	=> true,
				'email' 	=> $sessionData['email'],
				'firstname' => $sessionData['firstname'],
				'lastname'  => $sessionData['lastname'],
				'countries' => $countries
			];
			
			return view('public.registerDriver', $stepTwoData);
		}else
			return view('public.sessionExpired', ['redirect_from' => 'none']);			
	}
	
	public function registerStepThree(Request $request)
	{	
        $input = $request->all();
		
		\DB::beginTransaction();
		try {
			$fieldLabelNames = array(
		        'firstname' => 'Firstname',
		        'lastname' => 'Lastname',
				
		        'address' => 'Address',
		        //'zip_code' => 'Zip Code',
				'city' => 'City',
				'id_country' => 'Country',
				'phone_1' => 'Phone 1',
				
				'birth_day' => 'Birth Day',
				'birth_month' => 'Birth Month',
				'birth_year' => 'Birth Year',				
		        'birthplace' => 'Birth Place',
		        'id_country_birth' => 'Country of Birth',
				
		        'license_number' => 'Drivers License',
				'license_exp_day' => 'License Expiration Day',
				'license_exp_month' => 'License Expiration Month',
				'license_exp_year' => 'License Expiration Year',	
				'id_country_license' => 'Country',
				
				'id_country_license' => 'Country',
				'id_country_license' => 'Country',
				
		        'passport' => 'Passport Number',
		        'idcard' => 'ID Card Number',
				
		        'email' => 'Email',
		        'password' => 'Password',
			);
	        $requiredFields = array(
		        'firstname' => 'required',
		        'lastname' => 'required',
				
		        'address' => 'required',
		        //'zip_code' => 'required',
				'city' => 'required',
				'id_country' => 'required',
				'phone_1' => 'required',
				
		        'birth_day' => 'required',
		        'birth_month' => 'required',
		        'birth_year' => 'required|integer|digits:4',
		        'birthplace' => 'required',
		        'id_country_birth' => 'required',
				
		        'license_number' => 'required',
		        'license_exp_day' => 'required',
		        'license_exp_month' => 'required',
		        'license_exp_year' => 'required|integer|digits:4',
		        'id_country_license' => 'required',
				
				'passport' => 'required_without:idcard',
				'idcard' => 'required_without:passport',
	
		        'email' => 'required|email|unique:users',
		        'password' => 'required|min:4',
	        );
			
	        $validator = \Validator::make($input, $requiredFields);
	        $validator->setAttributeNames($fieldLabelNames);
			
	        if($validator->fails())
		        throw new \Exception('Validation Failed.');
			
			$user = new User();
			$user->created_at = date('Y-m-d H:i:s');
			$user->email = $request->get('email');
			$user->password = $request->get('password');
			$user->firstname = $request->get('users_firstname');
			$user->lastname = $request->get('users_lastname');
			$user->id_role = 9;
			$user->status = 1;
			$user->save();
			
			//save driver
			$driver = new Driver();
			$driver->id_user = $user->id;
			$driver->email = $request->get('email');
			$driver->firstname = $request->get('firstname');
			$driver->lastname = $request->get('lastname');
			$bday = $request->get('birth_year').'-'.$request->get('birth_month').'-'.$request->get('birth_day');
			$driver->birthdate = $bday;
			$driver->birthplace = $request->get('birthplace');
			$driver->id_country_birth = $request->get('id_country_birth');
			$driver->license_number = $request->get('license_number');
			$exp_date = $request->get('license_exp_year').'-'.$request->get('license_exp_month').'-'.$request->get('license_exp_day');
			$driver->license_expiration = $exp_date;
			$driver->id_country_license = $request->get('id_country_license');
			$driver->passport = $request->get('passport');
			$driver->idcard = $request->get('idcard');
			$driver->created_at = date('Y-m-d H:i:s');
			$driver->active = 1;
			$driver->save();
			
			//save address			
			$address = new Address();
			$address->id_driver = $driver->id;
			$address->type_home = 1;
			$address->firstname = $request->get('firstname');
			$address->lastname = $request->get('lastname');			
			$address->address = $request->get('address');
			$address->zip_code = $request->get('zip_code');
			$address->city = $request->get('city');
			$address->id_country = $request->get('id_country');
			$address->phone_1 = $request->get('phone_1');
			$address->phone_2 = $request->get('phone_2');			
			$address->created_at = date('Y-m-d H:i:s');
			$address->deleted = 0;
			$address->save();
			
			\DB::commit();
			
			//perform login
			Auth::login($user);
			//Auth::loginUsingId($user->id);
			
			return redirect('/reservations/complete/signup');
		}
		catch (\Exception $e)
		{
			\DB::rollback();
			info($e->getMessage(), [$e->getLine()]);
			if($e instanceof \Illuminate\Database\Eloquent\ModelNotFoundException)
			{
				$validator = \Validator::make($input, ['Record_not_found'=>'required'], ['Record_not_found.required'=>'The record you are trying to edit does not exits!']);
				$validator->fails();
			}
		}
		
		return back()->withErrors($validator)->withInput();
	}
	
	/**
     * PAYPAL PAYMENT RESPONSE RETURN 
     */
    public function paypalResponse(Request $request, $payment_id=null, $status='canceled')//status: 'ok' or 'canceled'
	{
		if($payment_id != null)
		{
			try { 
			
				$payment = Payment::findOrFail($payment_id);
				
				if($status='ok')
				{
					if($request->exists("paymentId") && $request->exists("PayerID"))
					{						
						$paypal = new PaypalPayment();
						$paymentResponse = $paypal->executePayment($request->get("paymentId"), $request->get("PayerID"));
						
						if($paymentResponse != null && $paymentResponse->getState() == "approved")
						{	
							$payment->id_user_verified = 0;//paypal=0 or user_id
							$payment->verified_by = 'Paypal';//or user_fullname
							$payment->status = 1;//verified
							$payment->paypal_payment_id = $paymentResponse->getId();
							
							//$paymentResponse->transactions[0]->related_resources[0]->sale->id;
							$transactions = $paymentResponse->getTransactions();
							$resources = $transactions[0]->getRelatedResources();
							$sale = $resources[0]->getSale();
							$payment->paypal_sale_id = $sale->getId();
							
							$payment->paid_at = date('Y-m-d H:i:s');
							$payment->details = $paymentResponse->toJSON();
							$payment->save();
							
							$reservation = Reservation::findOrFail($payment->id_reservation);
							$reservation->approved = 1;			//0=pending 1=approved 2=expired
							$reservation->status = 1;			//0=canceled 1=pending/reserved
							$reservation->id_user_approved = 0;	//-1=pending 0=paypal else >1 user approved
							$reservation->approved_user_name = "PayPal";
							$reservation->approved_at = date('Y-m-d H:i:s');
							$reservation->payment_completed = 1;
							$reservation->save();
						}
						
						return redirect('reservations/'.$reservation->id);
					}
					else 
					{
						return view('public.paypalPaymentCanceled', ['reservation_id' => $payment->id_reservation, 'reason' => 'missing_paypal_params']);
					}
				}
				else if($status='canceled')
				{
					$payment->status = -1;//canceled by paypal
					$payment->save();
					$reservation = Reservation::findOrFail($payment->id_reservation);
					$reservation->payment_completed = 2;//canceled by paypal
					$reservation->save();
					
					return view('public.paypalPaymentCanceled', ['reservation_id' => $payment->id_reservation, 'reason' => 'canceled']);
				}
			}
			catch (\Exception $e)
			{
				info($e->getMessage(), [$e->getLine()]);
			}
		}
		
		return view('public.paypalResponseError');
	}
	
	public function pdf()
	{
		$labels = [
			'TIRANA_OFFICE' => 		'TIRANA OFFICE',
			'FIER_OFFICE' => 		'FIER OFFICE',
			
			'PHONE' => 				'PHONE',
			'MOB' => 				'MOB',
			'EMAIL' => 				'EMAIL',
			'WEB' => 				'WEB',
			
			'COMPANY_NAME' =>		'FRIENDS TRAVEL  AGENCY Sh.p.k.',
			'COMPANY_REG_NUMBER' => 'NIPT: L32522402A',
			'COMPANY_ADDRESS' => 	'Main office: Lagja "15 Tetori ", Str "Kastriot Muco ", Fier, Albania',
			
			'RENTAL_AGREEMENT' => 	'RENTAL AGREEMENT',
			'NO' => 				'NO',
			'DATE' => 				'DATE',
			
			'RENTER' => 			'THE RENTER',
			'ADDITIONAL_DRIVER' => 	'ADDITIONAL DRIVER',
			'LEASE_DURATION' => 	'DURATION OF LEASE',
			'PAYMENT_DEPOSIT' => 	'PAYMENT & DEPOSIT',
			
			'NAME' => 				'NAME',
			'SURNAME' => 			'SURNAME',
			'BIRTHDAY' => 			'DATE OF BIRTH',
			'BIRTH_PLACE' => 		'PLACE OF BIRTH',
			'HOME_ADDRESS' =>  		'PERMANENT ADDRESS',
			'TEMPORARY_ADDRESS' => 	'TEMPORARY ADDRESS',
			'DRIVING_LICENSE' => 	'DRIVING LICENSE',
			'LICENSE_VALIDITY' => 	'VALIDITY',
			
			'DURATION' => 			'DURATION',
			'RETURN_DATE' => 		'RETURN DATE',
			'RETURN_TIME' => 		'TIME OF RETURN',
			'RETURN_PLACE' => 		'PLACE OF RETURN',
			
			'CHECK_OUT' => 			'CHECK OUT',
			'CHECK_IN' => 			'CHECK IN',
			'RENTAL_PRICE' => 		'RENTAL PRICE',
			'TRANSFER_SERVICE' => 	'TRANSFER SERVICE',
			'VAT' => 				'VAT (20%)',
			'FUEL' => 				'FUEL',
			'FUEL_FULL' => 			'Full',
			'DAMAGES' => 			'DAMAGES',
			'DELAY_EXTENSION' => 	'DELAY/EXTENSION',
			'CLEANING_WASH' => 		'CLEANING & WASH',
			'FINES' => 				'FINES',
			'TOTAL' => 				'TOTAL',
			'N_A' => 				'N/A',
			'NONE' => 				'None',
			
			'THE_RENTER_DECLARES'=> 'THE RENTER DECLARES',
			'RENTERS_DECLARATION'=> 'I hereby declare to have carefully the genereal Terms and Conditions of this Rental Agreement, and to completely agree with, Accepting to rent the contracted vehicle, I undertake under my responsibility, to use it properly and to fully respect the General Terms & Conditions of this Rental Agreement.',
			
			'AUTHORISATION_TITLE'=> 'AUTHORISATION',
			'AUTHORISATION_PART1'=> ' authorises the Renter  Mr/Mrs. ',
			'AUTHORISATION_PART2'=> 'to use the below described vehicle, from ',
			'AUTHORISATION_PART2'=> '. This Authorisation is valid for the territory of the Republic of Albania.',
			
			'VEHICLE_REPORT'=> 		'CHECK OUT & CHECK IN VEHICLE REPORT',
			'VEHICLE_FEATURES'=> 	'VEHICLE FEATURES',
			'MAKE'=> 				'MAKE',
			'VIN_NO'=> 				'VIN NO.',
			'PLATE_NO'=> 			'PLATE NO.',
			'VEHICLE_CHECK_OUT'=> 	'VEHICLE CHECK OUT',
			'VEHICLE_CHECK_IN'=> 	'VEHICLE CHECK IN',
			'PICKUP_PLACE'=> 		'PICKUP PLACE',
			'PICKUP_DATE'=> 		'PICKUP DATE',
			'PICKUP_TIME'=> 		'PICKUP TIME',
			'RETURN_PLACE'=> 		'RETURN PLACE',
			'RETURN_DATE'=> 		'RETURN DATE',
			'RETURN_TIME'=> 		'RETURN TIME',
			'LESSOR_SIGN'=> 		'LESSOR SIGNITURE',
			'RENTER_SIGN'=> 		'RENTER SIGNITURE',
			'NOTES'=> 				'NOTES',
			'BLUE'=> 				'BLUE',
			'RED'=> 				'RED',
			'EQUIPMENTS_FUEL'=> 	'EQUIPMENTS & FUEL',
		];
		
		$data = [];
		//return view('pdf.contractTemplate', ['lbl'=>$labels, 'data'=> $data]);
		$pdf = \PDF::loadView('pdf.contractTemplate', ['lbl'=>$labels, 'data'=> $data]);
		return $pdf->download('invoice.pdf');
	}
	
	public function about()
	{
		return view('about');
	}
	
}
