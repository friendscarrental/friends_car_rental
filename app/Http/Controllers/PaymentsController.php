<?php namespace App\Http\Controllers;

use App\Paypal\PaypalPayment;

use App\Models\Payment;
use App\Models\PaymentRefunds;
use App\Models\Reservation;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class PaymentsController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $payments = Payment::all();
		
		return view('admin.payments', ['resourceName'=>'payments', 'records' => $payments]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($reservationID=0)
    {		
		$paymentmethods = \DB::table('paymentmethod')
							->get();
		$paymenttypes = \DB::table('paymenttype')
							->where('manual', 1)
							->get();
				
		return view('admin.paymentsForm', ['reservation_id' => $reservationID, 'paymentmethods' => $paymentmethods, 'paymenttypes' => $paymenttypes]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
		try {
		
			$fieldLabelNames = array(
		        'amount' => 'Amount',
			);
	        $requiredFields = array(
		        'amount' => 'required|numeric',
	        );
	        $validator = \Validator::make($input, $requiredFields);
	        $validator->setAttributeNames($fieldLabelNames);

	        if($validator->fails())
		        throw new \Exception('Validation Failed.');
			
			$currentUser = \Auth::user();
			$reservation = Reservation::find($request->get('reservation_id'));
			
			if($request->exists('id'))
			{
				$payment = Payment::findOrFail($request->get('id'));
				if($payment->status == 0 && $payment->id_user_verified == -1 && $request->get('status') == 1)
				{
					$payment->id_user_verified = $currentUser->id;
					$payment->verified_by = $currentUser->firstname.' '.$currentUser->lastname;
					$payment->paid_at = date('Y-m-d H:i:s');
				}
			}
			else
			{
				$payment = new Payment();
				$payment->id_user = $currentUser->id;
				$payment->id_reservation = $reservation->id;
				$payment->created_at = date('Y-m-d H:i:s');
				if($request->get('status') == 1)
				{
					$payment->id_user_verified = $currentUser->id;
					$payment->verified_by = $currentUser->firstname.' '.$currentUser->lastname;
					$payment->paid_at = date('Y-m-d H:i:s');
				}
			}
			
			$payment->amount = $request->get('amount');
			$payment->id_paymentmethod = $request->get('id_paymentmethod');
			$payment->id_paymenttype = $request->get('id_paymenttype');
			$payment->details = $request->get('details');
			$payment->status = $request->get('status');
			$payment->save();
			
			if($payment->status == 1 && $payment->amount >= $reservation->total_price)
			{
				$reservation->payment_completed = 1;
				$reservation->save();
			}
			else
			{
				$query = "SELECT SUM(payments.amount) AS total 
							FROM payments
						   WHERE payments.id_reservation = :reservationID
						   AND payments.status = 1";
				$paymentsSUM = \DB::select($query, [':reservationID'=>$reservation->id]);
				
				if($paymentsSUM[0]->total >= $reservation->total_price)
				{
					$reservation->payment_completed = 1;
					$reservation->save();
				}
			}
			
				
			if($request->exists('pay_now'))
			{
				$paymentDescription = $reservation->duration.($reservation->duration == 1? ' day': ' days');
				$paymentDescription .= ' Reservation for '.$reservation->vehicle_title;
				if($payment->details != null && trim($payment->details) != "")
					$paymentDescription .= ' - '.$payment->details;
					
				$paypalEntries = array([
					'name' => 'Payment',
					'quantity' => 1,
					'price' => $payment->amount
				]);
				
				$paypal = new PaypalPayment();
				$paymentResponse = $paypal->makePayment($payment->id, $payment->amount, 'EUR', $paymentDescription, $paypalEntries);
				
				if($paymentResponse != null && $paymentResponse->getState() == "created")
				{
					$payment->paypal_payment_id = $paymentResponse->getId();
					$payment->save();
					
					return redirect($paypal->getLink($paymentResponse->getLinks(), "approval_url") );					
					/* USING header() DOES NOT CLEAR THE SESSION AS NEEDED
					header("Location: " . $paypal->getLink($paymentResponse->getLinks(), "approval_url") );exit;*/
				}
				else
				{
					return view('public.paypalPaymentCreationError', ['reservation_id' => $reservationID]);
				}
			}
			
			return redirect('reservations/'.$reservation->id);
		}
		catch (\Exception $e)
		{
			info($e->getMessage(), [$e->getLine()]);
			if($e instanceof \Illuminate\Database\Eloquent\ModelNotFoundException)
			{
				$validator = \Validator::make($input, ['Record_not_found'=>'required'], ['Record_not_found.required'=>'The record you are trying to edit does not exits!']);
				$validator->fails();
			}
		}
		
		return back()->withErrors($validator)->withInput()->with('model', $input);
    }

    /**
     * REFUND Payments
     */	
    public function refund($id, $deposit="")
    {
		$modelData = Payment::find($id);
		$data = [];
		
		if($deposit != "")
		{
			$reservation = Reservation::find($modelData->id_reservation);
			$modelData->amount = $reservation->security_deposit;
			$data['security_deposit'] = true;
		}
			
		$data['paymenttypes'] = \DB::table('paymenttype')
									->where('manual', 0)
									->get();
		
		return view('admin.paymentsRefund', $data)->with('model', $modelData);
    }
	
	public function refundStore(Request $request)
    {
        $input = $request->all();
		try {
		
			$payment = Payment::find($request->get('id'));
			$reservation = Reservation::find($payment->id_reservation);
			
			$remaining_amount = $payment->amount - $payment->refunded_amount;
			$refund_type = $request->get('refund_type');
			
			
			if($request->exists('security_deposit'))
			{
				$reservation->security_deposit_refunded = 1;
				$reservation->save();
				$refund_type = 7;
			}
			
			if($refund_type == 7)//Partial Refund
			{
				$fieldLabelNames = array(
					'amount' => 'Amount',
				);
				$requiredFields = array(
					'amount' => 'required|numeric|between:1,'.$remaining_amount,
				);
				$validator = \Validator::make($input, $requiredFields);
				$validator->setAttributeNames($fieldLabelNames);

				if($validator->fails())
					throw new \Exception('Validation Failed.');
					
				$amount = $request->get('amount');
				$refund_amount = $amount;
				
				if($remaining_amount - $amount < 1)
					$refund_amount = 0;//full refund
			}
			else
			{
				$refund_amount = 0;//full refund
				$amount = $remaining_amount;
			}
			
			$paypal = new PaypalPayment();			
			$paymentResponse = $paypal->refundSale($payment->paypal_sale_id, $refund_amount);
			
			if($paymentResponse != null && $paymentResponse->getState() == "completed")
			{
				$refund = new Payment();
				$refund->amount = $amount;
				$refund->id_paymentmethod = 1;
				$refund->id_paymenttype = $refund_type;//6=Full, 7=Partial
				$refund->details = $paymentResponse->toJSON();
				$refund->status = 3;//refunded
				$refund->paypal_refund_id = $paymentResponse->getId();
				
				$refund->id_payment_related = $payment->id;
				$refund->id_reservation = $payment->id_reservation;
				$refund->paypal_payment_id = $payment->paypal_payment_id;
				$refund->paypal_sale_id = $payment->paypal_sale_id;
				
				$currentUser = \Auth::user();
				$refund->id_user = $currentUser->id;
				$refund->id_user_verified = $currentUser->id;
				$refund->verified_by = $currentUser->firstname.' '.$currentUser->lastname;
				
				$refund->paid_at = date('Y-m-d H:i:s');
				$refund->created_at = date('Y-m-d H:i:s');
				$refund->save();
				
				if($refund_amount==0)
				{
					$payment->refunded_amount = $payment->amount;
					$payment->status = 2;//full-refund
				}
				else
				{
					$payment->refunded_amount = $remaining_amount - $amount;
					$payment->status = 3;//partial-refund
				}
				
				$payment->save();
			}
			else 
			{
				$validator = \Validator::make([], 
					['Record_not_found'=>'required'], 
					['Record_not_found.required'=>"Refund Failed, Please try again later."]
				);
				$validator->fails();
				
				return redirect('reservations/'.$payment->id_reservation)->withErrors($validator);
			}
			
			return redirect('reservations/'.$payment->id_reservation);
		}
		catch (\Exception $e)
		{
			info($e->getMessage(), [$e->getLine()]);
			if($e instanceof \Illuminate\Database\Eloquent\ModelNotFoundException)
			{
				$validator = \Validator::make($input, ['Record_not_found'=>'required'], ['Record_not_found.required'=>'The record you are trying to edit does not exits!']);
				$validator->fails();
			}
		}
		
		return back()->withErrors($validator)->withInput()->with('model', $input);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		$modelData = Payment::find($id);
		
		$paymentmethods = \DB::table('paymentmethod')
							->get();
		$paymenttypes = \DB::table('paymenttype')
							->where('manual', 1)
							->get();
				
		return view('admin.paymentsForm', ['reservation_id' => $modelData->id_reservation, 'paymentmethods' => $paymentmethods, 'paymenttypes' => $paymenttypes])->with('model', $modelData);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		try {
			$model = Payment::find($id);
			$model->deleted = 1;
			$model->save();
		}
		catch (\Exception $e){}//skip errors
		
		return response()->json('ok', 200);
    }
}
