<?php namespace App\Http\Controllers;

use App\Models\Vehicle;
use App\Models\Vehicletype;
use App\Models\Vehiclefueltype;
use App\Models\Vehicleseasonalprice;
use App\Models\Vehiclelocationcost;
use App\Models\Vehicleimage;
use App\Models\Durationdiscount;
use App\Models\Reservation;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class VehiclesController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$query = "SELECT vehicles.*,
						 vehicletypes.name AS vehicletypes_name,
						 vehiclefueltypes.name AS fuel_type
					FROM vehicles 
					  INNER JOIN vehicletypes 
						ON (vehicles.id_vehicletype = vehicletypes.id) 
					  INNER JOIN vehiclefueltypes 
						ON (vehicles.id_vehiclefueltype = vehiclefueltypes.id) 
					WHERE vehicles.deleted = 0";
		$vehicles = \DB::select($query);
		
		return view('admin.vehicles', ['resourceName'=>'vehicles', 'records' => $vehicles]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$vehicletypes = Vehicletype::where('deleted', 0)
									->get();
		$fueltypes = Vehiclefueltype::where('deleted', 0)
									->get();
									
        return view('admin.vehiclesForm', ['vehicletypes' => $vehicletypes, 'fueltypes' => $fueltypes]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $input = $request->all();
		try {
		
			$fieldLabelNames = array(
		        'title' => 'Title',
		        'make' => 'Make',
		        'model' => 'Model',
		        'year' => 'Year',
		        'seats' => 'Seats',
		        'doors' => 'Doors',
		        'baggage_size' => 'Bagage Size',
				'standard_price' => 'Standard Price',
				'security_deposit' => 'Security Deposit',
			);
	        $requiredFields = array(
		        'title' => 'required',
		        'make' => 'required',
		        'model' => 'required',
		        'year' => 'required',
		        'seats' => 'required',
		        'doors' => 'required',
		        'baggage_size' => 'required',
				'standard_price' => 'required|numeric',
				'security_deposit' => 'required|numeric',
	        );	
			
		    $validator = \Validator::make($input, $requiredFields);
	        $validator->setAttributeNames($fieldLabelNames);

	        if($validator->fails())
		        throw new \Exception('Validation Failed.');

			if($request->exists('id'))
			{				
				$vehicle = Vehicle::findOrFail($request->get('id'));
				$vehicle->updated_at = date('Y-m-d H:i:s');			
			}
			else
			{
				$vehicle = new Vehicle();
				$vehicle->created_at = date('Y-m-d H:i:s');
				$vehicle->updated_at = date('Y-m-d H:i:s');				
			}
			
			$resp = $this->uploadFiles($request, 'icon_image_path', '450x300', '270x180', '60x40');
			if($resp['code'] == 200)
			{	
				//remove old image from gallery
				Vehicleimage::where('id_vehicle', $vehicle->id)
							->where('img_url', $vehicle->icon_image_path)
							->delete();
							
				$vehicle->icon_image_path = $resp['filename'];
			}
			else if($resp['code'] == 500)
				throw new \Exception('Error_uploading_file');
			
			$vehicle->title = $request->get('title');
			$vehicle->id_vehicletype = $request->get('id_vehicletype');
			$vehicle->standard_price = $request->get('standard_price');
			$vehicle->security_deposit = $request->get('security_deposit');
			$vehicle->make = $request->get('make');
			$vehicle->model = $request->get('model');
			$vehicle->year = $request->get('year');
			$vehicle->vin_number = $request->get('vin_number');
			$vehicle->plate_number = $request->get('plate_number');
			$vehicle->description = $request->get('description');
			$vehicle->transmission = $request->get('transmission');
			$vehicle->seats = $request->get('seats');
			$vehicle->doors = $request->get('doors');
			$vehicle->id_vehiclefueltype = $request->get('id_vehiclefueltype');
			$vehicle->baggage_size = $request->get('baggage_size');
			$vehicle->airconditioning = $request->get('airconditioning');
			$vehicle->active = $request->get('active');
			$vehicle->save();
			
			if($resp['code'] == 200)
			{
				$vehicleimage = new  Vehicleimage();				
				$vehicleimage->img_url = $resp['filename'];
				$vehicleimage->id_vehicle = $vehicle->id;
				$vehicleimage->description = $request->get('description');
				$vehicleimage->created_at = date('Y-m-d H:i:s');
				$vehicleimage->save();
			}
			
			return redirect('vehicles');
		}
		catch (\Exception $e)
		{
			info($e->getMessage(), [$e->getLine()]);
			if($e instanceof \Illuminate\Database\Eloquent\ModelNotFoundException)
			{
				$validator = \Validator::make($input, ['Record_not_found'=>'required'], ['Record_not_found.required'=>'The record you are trying to edit does not exits!']);
				$validator->fails();
			}
			
			if($e->getMessage() == 'Error_uploading_file')
			{
				$validator = \Validator::make($input, ['Record_not_found'=>'required'], ['Record_not_found.required'=>'There was an error uploading the image file, please try again!']);
				$validator->fails();
			}
		}
		
		return back()->withErrors($validator)->withInput()->with('model', $input);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $query = "SELECT vehicles.*,
						 vehicletypes.name AS vehicletypes_name,
						 vehiclefueltypes.name AS fuel_type
					FROM vehicles 
					  INNER JOIN vehicletypes 
						ON (vehicles.id_vehicletype = vehicletypes.id) 
					  INNER JOIN vehiclefueltypes
						ON (vehicles.id_vehiclefueltype = vehiclefueltypes.id) 
					WHERE vehicles.id = :vehicleID";
		$vehicle = \DB::select($query, [':vehicleID'=>$id]);
		
		$reservations = Reservation::where('status', '<', 2)
						->where('id_vehicle', $vehicle[0]->id)
						->orderby('pickup_datetime', 'ASC')
						->paginate(25);
		
        return view('admin.vehiclesView', ['records' => $reservations])->with('model', $vehicle[0]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $modelData = Vehicle::find($id);
		$vehicletypes = Vehicletype::where('deleted', 0)
									->get();
		$fueltypes = Vehiclefueltype::where('deleted', 0)
									->get();
									
        return view('admin.vehiclesForm', ['vehicletypes' => $vehicletypes, 'fueltypes' => $fueltypes])->with('model', $modelData);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
     {
		try {
			$model = Vehicle::find($id);
			$model->deleted = 1;
			$model->save();
		}
		catch (\Exception $e){}//skip errors
		
		return response()->json('ok', 200);
    }
	
	
    /*******************************************************************************
     * CUSTOM actions for Duration Discount
     *******************************************************************************/
	 
    public function durationDiscounts($id)
    {
        $modelData = Vehicle::find($id);
		$vehicledurationdiscounts = Durationdiscount::where('id_vehicle', $id)->get();
		
        return view('admin.vehicleDurationDiscounts', ['records' => $vehicledurationdiscounts])->with('vehicle', $modelData);
    }
	
	public function durationDiscountsStore(Request $request)
    {
        $input = $request->all();
		try {
		
			$fieldLabelNames = array(
		        'duration' => 'Duration',
		        'discount' => 'Discount',		       
			);
	        $requiredFields = array(
		        'duration' => 'required|numeric',
		        'discount' => 'required|numeric',
	        );	
			
		    $validator = \Validator::make($input, $requiredFields);
	        $validator->setAttributeNames($fieldLabelNames);

	        if($validator->fails())
		        throw new \Exception('Validation Failed.');
				
			$ddiscount = new  Durationdiscount();
			$ddiscount->duration = $request->get('duration');
			$ddiscount->percentage = $request->get('percentage');
			if($ddiscount->percentage==1)
				$ddiscount->discount = $request->get('discount')/100;//percentage %
			else
				$ddiscount->discount = $request->get('discount');//fixed amount per day
			$ddiscount->id_vehicle = $request->get('vehicle_id');
			$ddiscount->created_at = date('Y-m-d H:i:s');
			$ddiscount->save();
			
			return redirect('vehicles/'.$request->get('vehicle_id'));
		}
		catch (\Exception $e)
		{
			info($e->getMessage(), [$e->getLine()]);
			if($e instanceof \Illuminate\Database\Eloquent\ModelNotFoundException)
			{
				$validator = \Validator::make($input, ['Record_not_found'=>'required'], ['Record_not_found.required'=>'The record you are trying to edit does not exits!']);
				$validator->fails();
			}
		}
		
		return back()->withErrors($validator)->withInput()->with('model', $input);
    }

    public function durationDiscountsDestroy($id)
    {
        try {
			Durationdiscount::destroy($id);
		}
		catch (\Exception $e){}//skip errors
		
		return response()->json('ok', 200);
    }
	
    /*******************************************************************************
     * CUSTOM actions for Seasonal Prices
     *******************************************************************************/
	 
    public function seasonalPrices($id)
    {
        $modelData = Vehicle::find($id);
		$vehicleseasonalprices = Vehicleseasonalprice::where('id_vehicle', $id)
													->orderby('start_season_month', 'ASC')
													->orderby('start_season_day', 'ASC')
													->get();
		
        return view('admin.vehicleSeasonalPrices', ['records' => $vehicleseasonalprices])->with('vehicle', $modelData);
    }
	
	public function seasonalPricesStore(Request $request)
    {
        $input = $request->all();
		try {
		
			$fieldLabelNames = array(
		        'name' => 'Season Name',
		        'price' => 'Price',		       
			);
	        $requiredFields = array(
		        'name' => 'required',
		        'price' => 'required',
	        );	
			
		    $validator = \Validator::make($input, $requiredFields);
	        $validator->setAttributeNames($fieldLabelNames);

	        if($validator->fails())
		        throw new \Exception('Validation Failed.');
				
			$seasonalprice = new  Vehicleseasonalprice();
			$seasonalprice->name = $request->get('name');
			$seasonalprice->start_season_month = $request->get('start_date_month');
			$seasonalprice->start_season_day = $request->get('start_date_day');
			$seasonalprice->end_season_month = $request->get('end_date_month');
			$seasonalprice->end_season_day = $request->get('end_date_day');
			$seasonalprice->price = $request->get('price');
			$seasonalprice->id_vehicle = $request->get('vehicle_id');
			$seasonalprice->save();
			
			return redirect('vehicles/'.$request->get('vehicle_id'));
		}
		catch (\Exception $e)
		{
			info($e->getMessage(), [$e->getLine()]);
			if($e instanceof \Illuminate\Database\Eloquent\ModelNotFoundException)
			{
				$validator = \Validator::make($input, ['Record_not_found'=>'required'], ['Record_not_found.required'=>'The record you are trying to edit does not exits!']);
				$validator->fails();
			}
		}
		
		return back()->withErrors($validator)->withInput()->with('model', $input);
    }

    public function seasonalPricesDestroy($id)
    {
        try {
			Vehicleseasonalprice::destroy($id);
		}
		catch (\Exception $e){}//skip errors
		
		return response()->json('ok', 200);
    }
	
    /*******************************************************************************
     * CUSTOM actions Vehicle Images					     *******   2   *********
     *******************************************************************************/
	 
    public function images($id)
    {
        $modelData = Vehicle::find($id);
		$vehicleimages = Vehicleimage::where('id_vehicle', $id)->get();
		
        return view('admin.vehicleImages', ['records' => $vehicleimages])->with('vehicle', $modelData);
    }
	
	public function imagesStore(Request $request)
    {
        $input = $request->all();
		try {
		
	        $requiredFields = array();	
			
		    $validator = \Validator::make($input, $requiredFields);

	        if($validator->fails())
		        throw new \Exception('Validation Failed.');
				
			$vehicleimage = new  Vehicleimage();
			
			$resp = $this->uploadFiles($request, 'image', '450x300', '', '60x40');
			if($resp['code'] == 200)
				$vehicleimage->img_url = $resp['filename'];
			else if($resp['code'] == 500)
				throw new \Exception('Error_uploading_file');
			
			$vehicleimage->id_vehicle = $request->get('vehicle_id');
			$vehicleimage->description = $request->get('description');
			$vehicleimage->created_at = date('Y-m-d H:i:s');
			$vehicleimage->save();
			
			return redirect('vehicles/'.$request->get('vehicle_id'));
		}
		catch (\Exception $e)
		{
			info($e->getMessage(), [$e->getLine()]);
			if($e instanceof \Illuminate\Database\Eloquent\ModelNotFoundException)
			{
				$validator = \Validator::make($input, ['Record_not_found'=>'required'], ['Record_not_found.required'=>'The record you are trying to edit does not exits!']);
				$validator->fails();
			}
			
			if($e->getMessage() == 'Error_uploading_file')
			{
				$validator = \Validator::make($input, ['Error_uploading_file'=>'required'], ['Error_uploading_file.required'=>'There was an error uploading the image file, please try again!']);
				$validator->fails();
			}
		}
		
		return back()->withErrors($validator)->withInput()->with('model', $input);
    }

    public function imagesDestroy($id)
    {
        try {
			Vehicleimage::destroy($id);
		}
		catch (\Exception $e){}//skip errors
		
		return response()->json('ok', 200);
    }
	
    /*******************************************************************************
     * CUSTOM actions Location Costs					     *******   3   *********
     *******************************************************************************/
	 
    public function locationCosts($id)
    {
        $modelData = Vehicle::find($id);
        $query = "SELECT locations.id, 
						 locations.name,
						 IFNULL(vehiclelocationcosts.cost, 0) AS cost
					FROM locations
					LEFT JOIN vehiclelocationcosts
					ON (locations.id = vehiclelocationcosts.id_location 
						AND vehiclelocationcosts.id_vehicle = :vehicleID)
					WHERE locations.deleted = 0
					ORDER BY locations.name";
		$vehiclelocationcosts = \DB::select($query, [':vehicleID'=>$id]);
		
        return view('admin.vehicleLocationCosts', ['records' => $vehiclelocationcosts])->with('vehicle', $modelData);
    }
	
	public function locationCostsStore(Request $request)
    {
        $input = $request->except('_token');
		
		\DB::beginTransaction();
		try {	
		
			$fieldLabelNames = [];
			$requiredFields = [];
			
			$tableRows = [];
			foreach($input as $key=>$value)
			{
				if($value >= 0)
					$tableRows[$key] = $value;
			}
			$vehicleID = $request->get('vehicle_id');
			//delete all
			Vehiclelocationcost::where('id_vehicle', $vehicleID)->delete();
			foreach($tableRows as $locationID=>$costValue)
			{	
				$locationcost = new  Vehiclelocationcost();
				$locationcost->id_vehicle = $vehicleID;
				$locationcost->id_location = $locationID;
				$locationcost->cost = $costValue;
				$locationcost->created_at = date('Y-m-d H:i:s');
				$locationcost->save();
			}
			
			\DB::commit();
			return redirect('vehicles/'.$vehicleID);
		}
		catch (\Exception $e)
		{	
			\DB::rollback();
			info($e->getMessage(), [$e->getLine()]);
		}
		
		return back();
    }

}
