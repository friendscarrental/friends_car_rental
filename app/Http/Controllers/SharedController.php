<?php namespace App\Http\Controllers;

use App\Paypal\PaypalPayment;

use App\Models\Reservation;
use App\Models\Payment;
use App\Models\Location;
use App\Models\Driver;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class SharedController extends Controller 
{
	public function __construct()
	{
		$this->middleware('auth');
	}
	
    public function cancelReservation(Request $request)
	{
		//CLEAR SESSION DATA
		$request->session()->forget('reservation_data');
		
		return redirect('/');
    }
	
    public function completeReservation(Request $request, $redirectFrom='none', $newDriverID=null)
    {
		if( $request->session()->has('reservation_data') )
		{
			$reservationData = $request->session()->get('reservation_data');
			$reservationData['pickup_location'] = Location::find($reservationData['pickup_location_id']);
			$reservationData['return_location'] = Location::find($reservationData['return_location_id']);
			
			info('redirectFrom', [$redirectFrom, $newDriverID]);
			
			if($redirectFrom == "new-driver" && $newDriverID != null)
			{
				$driver = Driver::find($newDriverID);
				
				$driver->text = $driver->firstname.' '.$driver->lastname;
				$reservationData['driver'] = $driver;
			}
			else 
			{
				$currentUser = \Auth::user();
				if($currentUser->id_role >= 5)//The public that registers as client to make an order/reservation
				{
					$driver = Driver::where('deleted', 0)
									->where('id_user', $currentUser->id)
									->first();
					if($driver)
					{
						$driver->text = $driver->firstname.' '.$driver->lastname;
						$reservationData['driver'] = $driver;
					}
				}
			}
			
			return view('admin.reservationsComplete', ['reservation_data' => $reservationData]);
		}else
			return view('public.sessionExpired', ['redirect_from' => $redirectFrom]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function saveReservation(Request $request)
    {
        $input = $request->all();
		
		//info('inputs', [$input]);
		
		if( !$request->session()->has('reservation_data') )
			return view('public.sessionExpired', ['redirect_from' => 'none']);
		
		$reservationData = $request->session()->get('reservation_data');		
			
		\DB::beginTransaction();
		try {
			
			$fieldLabelNames = array(
		        'primary_driver' => 'Primary Diver',
			);
	        $requiredFields = array(
		        'primary_driver' => 'required',
	        );
			
			$currentUser = \Auth::user();
			
			$reservation = new Reservation();
			$payment = new Payment();
			
			if($request->exists('pay_later'))
			{
				$reservation->pay_later_request = 1;
				$fieldLabelNames['request_description'] = 'Special Requests';
				$requiredFields['request_description'] = 'required';
			}
			
	        $validator = \Validator::make($input, $requiredFields);
	        $validator->setAttributeNames($fieldLabelNames);

	        if($validator->fails())
		        throw new \Exception('Validation Failed.');
			
			$special_discount = 0;
			if($request->exists('special_discount') && $request->get('special_discount') > 0)
			{
				$special_discount = ($reservationData['total_price'] * $request->get('special_discount') / 100);
			}
			
			$reservationTotalPrice = ($reservationData['total_price'] - $special_discount);
			$reservationFinalPayment = $reservationTotalPrice + $reservationData['vehicle']->security_deposit;
			
			$payment->status = 0;
			$payment->id_paymentmethod = ($request->exists('pay_later')?0:1);//none or paypal
			
			if($request->exists('approved_reservation') && $currentUser->id_role <= 5)//only admin or agency roles
			{
				$reservation->approved = 1;	//0=pending 1=approved 2=expired					
				$reservation->id_user_approved = $currentUser->id;
				$reservation->approved_user_name = $currentUser->firstname.' '.$currentUser->lastname;
				$reservation->approved_at = date('Y-m-d H:i:s');
				
				if($request->get('approved_reservation') == 'approved_with_payment')
				{
					$reservation->payment_completed = 1;	//0=pending 1=approved 2=expired	
					$payment->id_paymentmethod = 2;//cash
					$payment->id_user_verified = $currentUser->id;
					$payment->verified_by = $currentUser->firstname.' '.$currentUser->lastname;
					$payment->paid_at = date('Y-m-d H:i:s');
					$payment->status = 1;//verified
				}
				else 
					$payment->id_paymentmethod = 0;//none
			}
			else 
			{
				$reservation->approved = 0;	//0=pending 1=approved 2=expired
			}
			
			$primaryDriver = Driver::find($request->get('primary_driver'));
			
			$reservation->id_vehicle = $reservationData['vehicle']->id;
			$reservation->vehicle_title = $reservationData['vehicle']->title;
			$reservation->id_user = $currentUser->id;
			$reservation->primary_driver_name = $primaryDriver->firstname.' '.$primaryDriver->lastname;
			$reservation->pickup_datetime = $reservationData['pickup_date'];
			$reservation->id_location_pickup = $reservationData['pickup_location_id'];
			$reservation->pickup_location = Location::find($reservationData['pickup_location_id'])->name;
			$reservation->return_datetime = $reservationData['return_date'];
			$reservation->id_location_return = $reservationData['return_location_id'];
			$reservation->return_location = Location::find($reservationData['return_location_id'])->name;
			$reservation->duration_days = $reservationData['duration'];
			$reservation->total_price = $reservationTotalPrice;
			$reservation->security_deposit = $reservationData['vehicle']->security_deposit;
			$reservation->request_description = $request->get('request_description');
			$reservation->status = 1;	//0=canceled 1=pending/reserved	
			$reservation->created_at = date('Y-m-d H:i:s');			
			$reservation->save();
			
			$reservationID = $reservation->id;
			
			//continue payment details
			$payment->id_user = $currentUser->id;
			$payment->amount = $reservationFinalPayment;
			$payment->id_paymenttype = 1;
			$payment->id_reservation = $reservationID;
			$payment->created_at = date('Y-m-d H:i:s');
			$payment->save();
			
			//update again adding the payment ID
			$reservation->id_payment = $payment->id;;			
			$reservation->save();
			
			\DB::table('reservation_drivers')->insert([
				'id_reservation' => $reservationID, 
				'id_driver' => $primaryDriver->id,
				'primary' => 1
			]);
			
			if($request->exists('secondary_driver'))
			{	
				$secondaryDriver = Driver::find($request->get('secondary_driver'));
				\DB::table('reservation_drivers')->insert([
					'id_reservation' => $reservationID, 
					'id_driver' => $secondaryDriver->id,
					'primary' => 0
				]);
			}
			
			$reservationEntries = [];
			$paypalEntries = [];
			
			foreach($reservationData['vehicle']->receipt_entries as $entry)
			{	
				$reservationEntries[] = [
					'id_reservation' => $reservationID, 
					'entry_name' => $entry['entry_name'],
					'quantity' => $entry['quantity'],
					'price' => $entry['price'],
					//'debit_credit' => ($entry['price']==-1?1:0),
					'id_accessory' => 0,
					'details' => json_encode($entry['details'])
				];
				
				$paypalEntries[] = [
					'name' => $entry['entry_name'],
					'quantity' => $entry['quantity'],
					'price' => $entry['price']
				];
			}
			
			foreach($reservationData['reservation_receipt_entries'] as $entry)
			{
				$reservationEntries[] = [
					'id_reservation' => $reservationID, 
					'entry_name' => $entry['entry_name'],
					'quantity' => $entry['quantity'],
					'price' => $entry['price'],
					'id_accessory' => 0,
					'details' => json_encode($entry['details'])
				];
				
				$paypalEntries[] = [
					'name' => $entry['entry_name'],
					'quantity' => $entry['quantity'],
					'price' => $entry['price']
				];
			}
			
			$accessoryReservations = [];
			foreach($reservationData['accessories_receipt_entries'] as $entry)
			{
				$reservationEntries[] = [
					'id_reservation' => $reservationID, 
					'entry_name' => $entry['entry_name'],
					'quantity' => $entry['quantity'],
					'price' => $entry['price'],
					'id_accessory' => $entry['id'],
					'details' => ''
				];
				
				$paypalEntries[] = [
					'name' => $entry['entry_name'],
					'quantity' => $entry['quantity'],
					'price' => $entry['price']
				];
				
				if($entry['trackable'] == 1)
				{
					$accessoryReservations[] = [
						'id_reservation' => $reservationID, 
						'id_accessory' => $entry['id'],
						'pickup_datetime' => $reservationData['pickup_date'],
						'return_datetime' => $reservationData['return_date'],
					];
				}
			}
			
			if($special_discount > 0)
			{
				$reservationEntries[] = [
					'id_reservation' => $reservationID, 
					'entry_name' => 'Special Discount (-'.$request->get('special_discount').'%)',
					'quantity' => 1,
					'price' => $special_discount * -1,
					'id_accessory' => 0,
					'details' => 'Special Discount: -'.$request->get('special_discount').'% of '.number_format($reservationData['total_price'], 2, '.', ' ')
				];
				
				$paypalEntries[] = [
					'name' => 'Special Discount (-'.$request->get('special_discount').'%)',
					'quantity' => 1,
					'price' => $special_discount * -1
				];
			}
		
			//SECURITY DEPOSIT paypal entry
			$paypalEntries[] = [
				'name' => 'Refundable SECURITY DEPOSIT',
				'quantity' => 1,
				'price' => $reservationData['vehicle']->security_deposit
			];
			
			
			if(count($accessoryReservations) == 0)
				\DB::table('accessory_reservations')->insert($accessoryReservations);
			
			\DB::table('reservation_entries')->insert($reservationEntries);
			
			
			\DB::commit();
			
			//CLEAR SESSION DATA
			$request->session()->forget('reservation_data');
			
			if($request->exists('pay_now_with_paypal'))
			{
				$paymentDescription = $reservation->duration.($reservation->duration == 1? ' day': ' days');
				$paymentDescription .= ' for '.$reservation->vehicle_title;
				
				$paypal = new PaypalPayment();
				$paymentResponse = $paypal->makePayment($payment->id, $reservationFinalPayment, 'EUR', $paymentDescription, $paypalEntries);
				
				if($paymentResponse != null && $paymentResponse->getState() == "created")
				{
					$payment->paypal_payment_id = $paymentResponse->getId();
					$payment->save();
					
					return redirect($paypal->getLink($paymentResponse->getLinks(), "approval_url") );					
					/* USING header() DOES NOT CLEAR THE SESSION AS NEEDED
					header("Location: " . $paypal->getLink($paymentResponse->getLinks(), "approval_url") );exit;*/
				}
				else
				{
					return view('public.paypalPaymentCreationError', ['reservation_id' => $reservationID]);
				}
			}
			
			return redirect('reservations');
		}
		catch (\Exception $e)
		{
			\DB::rollback();
			info($e->getMessage(), [$e->getLine()]);
			if($e instanceof \Illuminate\Database\Eloquent\ModelNotFoundException)
			{
				$validator = \Validator::make($input, ['Record_not_found'=>'required'], ['Record_not_found.required'=>'The record you are trying to edit does not exits!']);
				$validator->fails();
			}
		}
		
		return back()->withErrors($validator)->withInput()->with('model', $input);
    }
	
    /*{ 
		"id": "PAY-72076239CT8721229K3TK46Y", 
		"state": "created", 
		"create_time": "2016-03-14T12:28:43Z", 
		"redirect_urls": { "return_url": "http://rental.arcuus.com/payments/completion/10/ok", "cancel_url": "http://rental.arcuus.com/payments/completion/10/canceled" }, 
		"intent": "sale", 
		"payer": { "payment_method": "paypal" }, 
		"transactions": [ 
			{ "amount": { "total": "465.00", "currency": "EUR" }, "description": "Payment Description", "invoice_number": "10", "related_resources": [ ] } 
		], 
		"links": [ 
			{ "href": "https://api.sandbox.paypal.com/v1/payments/payment/PAY-72076239CT8721229K3TK46Y", "rel": "self", "method": "GET" }, 
			{ "href": "https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=EC-25W37286KN981321R", "rel": "approval_url", "method": "REDIRECT" }, 
			{ "href": "https://api.sandbox.paypal.com/v1/payments/payment/PAY-72076239CT8721229K3TK46Y/execute", "rel": "execute", "method": "POST" } 
		] 
	}*/

	
	public function documents($id, $document='agreement', $language='en')//$document='agreement|terms'
	{	
		$language = strtolower ($language);
		if($language == 'en' || $language == 'sq')
		{
			if($document == 'agreement')
			{	
				return $this->agreement($id, $language);
			}
			if($document == 'terms')
			{	
				$file = base_path('resources/views/pdf/GENERAL_TERMS_'.$language.'.docx');
				return response()->download($file);
			}
		}
		return view('public.notFound');
	}
	
	private function agreement($id, $language='en')
	{
		$labels = array(
		'en'=>[
			'TIRANA_OFFICE' => 		'TIRANA OFFICE',
			'FIER_OFFICE' => 		'FIER OFFICE',
			
			'PHONE' => 				'PHONE',
			'MOB' => 				'MOB',
			'EMAIL' => 				'EMAIL',
			'WEB' => 				'WEB',
			'TEL_MOB' => 			'PHONE',
			
			'COMPANY_NAME' =>		'FRIENDS TRAVEL  AGENCY Sh.p.k.',
			'COMPANY_REG_NUMBER' => 'NIPT: L32522402A',
			'COMPANY_ADDRESS' => 	'Main office: Lagja "15 Tetori ", Str "Kastriot Muco ", Fier, Albania',
			
			'RENTAL_AGREEMENT' => 	'RENTAL AGREEMENT',
			'RESERVATION_NO' => 	'NO',
			'DOC_DATE' => 			'DATE',
			
			'RENTER' => 			'THE RENTER',
			'ADDITIONAL_DRIVER' => 	'ADDITIONAL DRIVER',
			'LEASE_DURATION' => 	'DURATION OF LEASE',
			'PAYMENT_DEPOSIT' => 	'PAYMENT & DEPOSIT',
			
			'NAME' => 				'NAME',
			'SURNAME' => 			'SURNAME',
			'PASSPORT_ID' =>		'PASSPORT/ID',
			'BIRTHDAY' => 			'DATE OF BIRTH',
			'BIRTH_PLACE' => 		'PLACE OF BIRTH',
			'ADDRESS_LINE1' =>  	'ADDRESS',
			'ADDRESS_LINE2' => 		'CITY/COUNTRY',
			'DRIVING_LICENSE' => 	'DRIVING LICENSE',
			'LICENSE_VALIDITY' => 	'VALIDITY',
			
			'DAYS' => 				'Days',
			'DURATION' => 			'DURATION',
			'RETURN_DATE' => 		'RETURN DATE',
			'RETURN_TIME' => 		'TIME OF RETURN',
			'RETURN_PLACE' => 		'PLACE OF RETURN',
			
			'CHECK_OUT' => 			'CHECK OUT',
			'CHECK_IN' => 			'CHECK IN',
			'RENTAL_PRICE' => 		'RENTAL PRICE',
			'TRANSFER_SERVICE' => 	'TRANSFER SERVICE',
			'VAT' => 				'VAT (20%)',
			'FUEL' => 				'FUEL',
			'FUEL_FULL' => 			'Full',
			'DAMAGES' => 			'DAMAGES',
			'DELAY_EXTENSION' => 	'DELAY/EXTENSION',
			'CLEANING_WASH' => 		'CLEANING & WASH',
			'FINES' => 				'FINES',
			'N_A' => 				'N/A',
			'SECURITY_DEPOSIT' => 	'DEPOSIT',
			'TOTAL' => 				'TOTAL',
			
			'THE_RENTER_DECLARES'=> 'THE RENTER DECLARES',
			'RENTERS_DECLARATION'=> 'I hereby declare to have carefully the genereal Terms and Conditions of this Rental Agreement, and to completely agree with, Accepting to rent the contracted vehicle, I undertake under my responsibility, to use it properly and to fully respect the General Terms & Conditions of this Rental Agreement.',
			
			'AUTHORISATION_TITLE'=> 'AUTHORISATION',
			'AUTHORISATION_PART1'=> 'authorises the Renter  Mr/Mrs.',
			'AUTHORISATION_PART2'=> 'to use the below described vehicle, from',
			'AUTHORISATION_PART3'=> 'to',
			'AUTHORISATION_PART4'=> 'This Authorisation is valid for the territory of the Republic of Albania.',
			
			'VEHICLE_REPORT'=> 		'CHECK OUT & CHECK IN VEHICLE REPORT',
			'VEHICLE_FEATURES'=> 	'VEHICLE FEATURES',
			'MAKE'=> 				'MAKE',
			'VIN_NO'=> 				'VIN NO.',
			'PLATE_NO'=> 			'PLATE NO.',
			'VEHICLE_CHECK_OUT'=> 	'VEHICLE CHECK OUT',
			'VEHICLE_CHECK_IN'=> 	'VEHICLE CHECK IN',
			'PICKUP_PLACE'=> 		'PICKUP PLACE',
			'PICKUP_DATE'=> 		'PICKUP DATE',
			'PICKUP_TIME'=> 		'PICKUP TIME',
			'RETURN_PLACE'=> 		'RETURN PLACE',
			'RETURN_DATE'=> 		'RETURN DATE',
			'RETURN_TIME'=> 		'RETURN TIME',
			'LESSOR_SIGN'=> 		'LESSOR SIGNITURE',
			'RENTER_SIGN'=> 		'RENTER SIGNITURE',
			'NOTES'=> 				'NOTES',
			'BLUE'=> 				'BLUE',
			'RED'=> 				'RED',
			'EQUIPMENTS_FUEL'=> 	'EQUIPMENTS & FUEL',
		],
		'sq'=>[
			'TIRANA_OFFICE' => 		'ZYRA TIRANE',
			'FIER_OFFICE' => 		'ZYRA FIER',
			
			'PHONE' => 				'Tel',
			'MOB' => 				'Mob',
			'EMAIL' => 				'Email',
			'WEB' => 				'Web',
			'TEL_MOB' => 			'TEL/MOB',

			'COMPANY_NAME' =>		'FRIENDS TRAVEL  AGENCY Sh.p.k.',
			'COMPANY_REG_NUMBER' => 'NIPT: L32522402A',
			'COMPANY_ADDRESS' => 	'Zyra Qendore: Lagja "15 Tetori ", Rr "Kastriot Muco ", Fier, Albania',
									
			'RENTAL_AGREEMENT' => 	'KONTRATE QIRAJE',
			'RESERVATION_NO' =>		'NR',
			'DOC_DATE' => 			'Data',
			
			'RENTER' => 			'QIRAMARRESI',
			'ADDITIONAL_DRIVER' => 	'DREJTUES AUTOMJETI SHTESE',
			'LEASE_DURATION' => 	'KOHEZGJATJA E QIRASE',
			'PAYMENT_DEPOSIT' => 	'PAGESA DHE DEPOZITA',
			
			'NAME' => 				'EMRI',
			'SURNAME' => 			'MBIEMRI',
			'PASSPORT_ID' =>		'PASAPORTA/ID',
			'BIRTHDAY' => 			'DATELINDJA',
			'BIRTH_PLACE' => 		'VENDLINDJA',
			'ADDRESS_LINE1' =>  	'ADRESA',
			'ADDRESS_LINE2' => 		'QYTET/SHTET',
			'DRIVING_LICENSE' => 	'LEJE DREJTIMI',
			'LICENSE_VALIDITY' => 	'VLEFSHMERIA',
			
			'DAYS' => 				'Dite',
			'DURATION' => 			'KOHEZGJATJA',
			'RETURN_DATE' => 		'DATA E KTHIMIT',
			'RETURN_TIME' => 		'ORA E KTHIMI',
			'RETURN_PLACE' => 		'VENDI I KTHIMIT',
			
			'CHECK_OUT' => 			'MARRJE',
			'CHECK_IN' => 			'KTHIM',
			'RENTAL_PRICE' => 		'&Ccedil;MIMI I QIRASE',
			'TRANSFER_SERVICE' => 	'TRANSFERIM SHERBIMI',
			'VAT' => 				'TVSH (20%)',
			'FUEL' => 				'KARBURANT',
			'FUEL_FULL' => 			'Plot',
			'DAMAGES' => 			'DEMTIME',
			'DELAY_EXTENSION' => 	'VONESA, SHTYRJE AFATI',
			'CLEANING_WASH' => 		'LARJE, PASTRIM',
			'FINES' => 				'GJOBA',
			'N_A' => 				'Nuk Ka',
			'SECURITY_DEPOSIT' => 	'GARANCI',
			'TOTAL' => 				'GJITHSEJ',
			
			'THE_RENTER_DECLARES'=> 'DEKLARIM I QIRAMARRËSIT',
			'RENTERS_DECLARATION'=> 'Pasi lexova me kujdes Percaktimet dhe Kushtet e Pergjithshme te kesaj Kontrate Qiraje, deklaroj se bie plotesisht dakord me to dhe duke pranuar te marr me qira automjetin e kontraktuar, marr persiper ne pergjegjesine time, ta perdor me kujdes dhe te respektoj Percaktimet dhe Kushtet e Pergjithshme te kesaj Kontrate.',
			
			'AUTHORISATION_TITLE'=> 'AUTORIZIM',
			'AUTHORISATION_PART1'=> 'autorizon Z/Znj',
			'AUTHORISATION_PART2'=> 'ta perdore automjetin me te dhenat e meposhtme, nga data',
			'AUTHORISATION_PART3'=> 'deri ne daten',
			'AUTHORISATION_PART4'=> 'Ky autorizim eshte i vlefshem vetem brenda territorit te Republikes se Shqiperise.',
			
			'VEHICLE_REPORT'=> 		'PROCES VERBAL MARRJE NE DOREZIM AUTOMJETI',
			'VEHICLE_FEATURES'=> 	'TE DHENAT E AUTOMJETIT',
			'MAKE'=> 				'MARKA',
			'VIN_NO'=> 				'SHASIA',
			'PLATE_NO'=> 			'TARGA',
			'VEHICLE_CHECK_OUT'=> 	'MARRJA E AUTOMJETIT',
			'VEHICLE_CHECK_IN'=> 	'KTHIMI I AUTOMJETIT',
			'PICKUP_PLACE'=> 		'VENDI MARRJES',
			'PICKUP_DATE'=> 		'DATA E MARRJES',
			'PICKUP_TIME'=> 		'ORA MARRJES',
			'RETURN_PLACE'=> 		'VENDI KTHIMIT',
			'RETURN_DATE'=> 		'DATA E KTHIMIT',
			'RETURN_TIME'=> 		'ORA KTHIMIT',
			'LESSOR_SIGN'=> 		'Qiradhenesi',
			'RENTER_SIGN'=> 		'Qiramarresi',
			'NOTES'=> 				'SHENIME',
			'BLUE'=> 				'me blu',
			'RED'=> 				'me te kuqe',
			'EQUIPMENTS_FUEL'=> 	'PAJISJE DHE KARBURANTI',
		]);
		
		$templateData = [ 'lbl' => $labels[$language] ];
		
		$query = " SELECT r.id
						, r.vehicle_title
						, r.duration_days
						, DATE_FORMAT(r.pickup_datetime,'%d-%b-%y') AS pickup_date
						, DATE_FORMAT(r.pickup_datetime,'%H:%i') AS pickup_time
						, r.pickup_location
						, DATE_FORMAT(r.return_datetime,'%d-%b-%y') AS return_date
						, DATE_FORMAT(r.return_datetime,'%H:%i') AS return_time
						, r.return_location
						, r.security_deposit
						, r.total_price
						, v.vin_number
						, v.plate_number
						, fuel.name AS fuel_type
					 FROM reservations AS r
					INNER JOIN vehicles AS v
					   ON (r.id_vehicle = v.id)
					INNER JOIN vehiclefueltypes AS fuel
					   ON (v.id_vehiclefueltype = fuel.id) 
					WHERE r.id = :reservationID";
		$reservation = \DB::select($query, [':reservationID'=> $id]);
		$templateData['model'] = $reservation[0];
		
		$query = " SELECT drivers.firstname
						, drivers.lastname
						, drivers.passport
						, drivers.idcard
						, DATE_FORMAT(drivers.birthdate,'%d-%b-%Y') AS birthdate
						, drivers.birthplace
						, b_country.name AS birth_country
						, drivers.license_number
						, DATE_FORMAT(drivers.license_expiration,'%d-%b-%Y') AS license_expiration
						, drivers.email
						, address.address
						, address.zip_code
						, address.city
						, address.phone_1
						, address.phone_2
						, a_country.name AS address_country
					FROM
						drivers
						INNER JOIN address 
							ON (drivers.id = address.id_driver)
						INNER JOIN countries AS b_country
							ON (drivers.id_country_birth = b_country.id)
						INNER JOIN countries AS a_country
							ON (address.id_country = a_country.id)
						INNER JOIN reservation_drivers AS rd
							ON (rd.id_driver = drivers.id)
					WHERE rd.id_reservation = :reservationID
					  AND rd.primary = 1
					  AND address.type_home = 1	";
		$primary = \DB::select($query, [':reservationID'=> $id]);
		$templateData['primary'] = $primary[0];
		
		$query = " SELECT drivers.firstname
						, drivers.lastname
						, drivers.license_number
						, DATE_FORMAT(drivers.license_expiration,'%d-%b-%Y') AS license_expiration
						, address.phone_1
						, address.phone_2
					FROM
						drivers
						INNER JOIN address 
							ON (drivers.id = address.id_driver)
						INNER JOIN reservation_drivers AS rd
							ON (rd.id_driver = drivers.id)
					WHERE rd.id_reservation = :reservationID
					  AND rd.primary = 0
					  AND address.type_home = 1	
					LIMIT 1";
		$secondary = \DB::select($query, [':reservationID'=> $id]);
		if(count($secondary) > 0)
			$templateData['secondary'] = $secondary[0];
		
		//return view('pdf.contractTemplate', ['lbl'=>$labels, 'data'=> $data]);
		$pdf = \PDF::loadView('pdf.agreementTemplate', $templateData);
		return $pdf->download('invoice.pdf');
	}
	
}
