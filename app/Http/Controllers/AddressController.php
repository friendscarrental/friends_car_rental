<?php namespace App\Http\Controllers;

use App\Models\Address;
use App\Models\Country;
use App\Models\Driver;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class AddressController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($driver_id=null)
    {	
		if($driver_id != null)
		{	
			$driver = Driver::find($driver_id);	
			$modelData = [
				'id_driver' => $driver->id,
				'firstname' => $driver->firstname,
				'lastname' => $driver->lastname,
				'id_country' => $driver->id_country_birth
			];
		}
		$countries = Country::all();
		return view('admin.addressForm', ['countries' => $countries])->with('model', $modelData);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $input = $request->all();
		try {
		
			$fieldLabelNames = array(
		        'firstname' => 'Firstname',
		        'lastname' => 'Lastname',
		        'address' => 'Address',
		        //'zip_code' => 'Zip Code',
				'city' => 'City',
				'id_country' => 'Country',
				'phone_1' => 'Main Phone 1',		 
			);
	        $requiredFields = array(
		        'firstname' => 'required',
		        'lastname' => 'required',
		        'address' => 'required',
		        //'zip_code' => 'required',
				'city' => 'required',
				'id_country' => 'required',
				'phone_1' => 'required',		 
	        );	
			
		    $validator = \Validator::make($input, $requiredFields);
	        $validator->setAttributeNames($fieldLabelNames);

	        if($validator->fails())
		        throw new \Exception('Validation Failed.');

			if($request->exists('id'))
			{				
				$address = Address::findOrFail($request->get('id'));
			}
			else
			{
				//create
				$address = new  Address();	
				$address->created_at = date('Y-m-d H:i:s');
				$address->type_home = 0;
			}
			
			$address->id_driver = $request->get('id_driver');
			$address->firstname = $request->get('firstname');
			$address->lastname = $request->get('lastname');
			$address->address = $request->get('address');
			$address->zip_code = $request->get('zip_code');
			$address->city = $request->get('city');
			$address->id_country = $request->get('id_country');
			$address->phone_1 = $request->get('phone_1');
			$address->phone_2 = $request->get('phone_2');
			$address->save();
			
			return redirect('drivers/'.$address->id_driver.'/edit');
		}
		catch (\Exception $e)
		{
			info($e->getMessage(), [$e->getLine()]);
			if($e instanceof \Illuminate\Database\Eloquent\ModelNotFoundException)
			{
				$validator = \Validator::make($input, ['Record_not_found'=>'required'], ['Record_not_found.required'=>'The record you are trying to edit does not exits!']);
				$validator->fails();
			}
		}
		
		return back()->withErrors($validator)->withInput()->with('model', $input);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		$modelData = Address::find($id);
		$countries = Country::all();
		
		return view('admin.addressForm', ['countries' => $countries])->with('model', $modelData);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Set as Primary Address.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function primary($id)
    {
		try {
			
			$address = Address::find($id);
			
			Address::where('id_driver', $address->id_driver)
				  ->update(['type_home' => 0]);
				  
			$address->type_home = 1;
			$address->save();
		}
		catch (\Exception $e){}//skip errors
		
		return back();
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
			$model = Address::find($id);
			$model->deleted = 1;
			$model->save();
		}
		catch (\Exception $e){}//skip errors
		
		return response()->json('ok', 200);
    }
}
