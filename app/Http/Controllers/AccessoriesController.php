<?php namespace App\Http\Controllers;

use App\Models\Accessory;
use App\Models\Vehicle;
use App\Models\Vehicleaccessory;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class AccessoriesController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $accessories = Accessory::where('deleted', 0)
								->get();
		
		return view('admin.accessories', ['resourceName'=>'accessories', 'records' => $accessories]);
		
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$availabilities = \DB::select("SELECT * FROM accessory_availability");
		
        return view('admin.accessoriesForm', ['availabilities' => $availabilities]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
		try {
		
			$fieldLabelNames = array(
		        'name' => 'Name',
		        'price' => 'Price',
			);
	        $requiredFields = array(
		        'name' => 'required',
		        'price' => 'required',
	        );	
			
		    $validator = \Validator::make($input, $requiredFields);
	        $validator->setAttributeNames($fieldLabelNames);

	        if($validator->fails())
		        throw new \Exception('Validation Failed.');

			if($request->exists('id'))
			{
				$accessory = Accessory::findOrFail($request->get('id'));
			}
			else
			{
				//create
				$accessory = new Accessory();
				$accessory->created_at = date('Y-m-d H:i:s');
			}
			
			$resp = $this->uploadFiles($request, 'img_url', '100x120');
			if($resp['code'] == 200)
			{	
				$accessory->img_url = $resp['filename'];
			}
			else if($resp['code'] == 500)
				throw new \Exception('Error_uploading_file');
			
			$accessory->name = $request->get('name');
			$accessory->description = $request->get('description');
			$accessory->price = $request->get('price');
			$accessory->one_time_fee = $request->get('one_time_fee');
			$accessory->availability = $request->get('availability');
			$accessory->active = $request->get('active');
			$accessory->save();
			
			if($request->exists('apply_to_all') && $request->get('apply_to_all') == 1)
			{
				$vehicles = Vehicle::where('deleted', 0)->get();
				foreach($vehicles as $vehicle)
				{
					$vAcc = new Vehicleaccessory();
					$vAcc->id_vehicle = $vehicle->id;
					$vAcc->id_accessorie = $accessory->id;
					$vAcc->price = $accessory->price;
					$vAcc->created_at = date('Y-m-d H:i:s');
					$vAcc->save();
				}
			}
			
			return redirect('accessories');
		}
		catch (\Exception $e)
		{
			info($e->getMessage(), [$e->getLine()]);
			if($e instanceof \Illuminate\Database\Eloquent\ModelNotFoundException)
			{
				$validator = \Validator::make($input, ['Record_not_found'=>'required'], ['Record_not_found.required'=>'The record you are trying to edit does not exits!']);
				$validator->fails();
			}
			
			if($e->getMessage() == 'Error_uploading_file')
			{
				$validator = \Validator::make($input, ['Error_uploading_file'=>'required'], ['Error_uploading_file.required'=>'There was an error uploading the image file, please try again!']);
				$validator->fails();
			}
		}
		
		return back()->withErrors($validator)->withInput()->with('model', $input);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		$modelData = Accessory::find($id);
		$availabilities = \DB::select("SELECT * FROM accessory_availability");
		
		return view('admin.accessoriesForm', ['availabilities' => $availabilities])->with('model', $modelData);    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		try {
			$model = Accessory::find($id);
			$model->deleted = 1;
			$model->save();
		}
		catch (\Exception $e){}//skip errors
		
		return response()->json('ok', 200);
    }
	
	
    /*******************************************************************************
     * CUSTOM actions ACCESSORIES PER VEHICLE			     				********
     *******************************************************************************/
	 
    public function perVehicle($id)
    {
        $modelData = Vehicle::find($id);
        $query = "SELECT accessories.id, 
						 accessories.name,
						 accessories.img_url,
						 accessories.one_time_fee,
						 IFNULL(vehicleaccessories.price, accessories.price) AS price,
						 IF(vehicleaccessories.id IS NULL, 0, 1) AS applicable
					FROM accessories
					LEFT JOIN vehicleaccessories
						ON (accessories.id = vehicleaccessories.id_accessory 
							AND vehicleaccessories.id_vehicle = :vehicleID)
					WHERE accessories.deleted = 0
					ORDER BY accessories.name";
		$vehicleaccessories = \DB::select($query, [':vehicleID'=>$id]);
		
        return view('admin.accessoriesPerVehicle', ['records' => $vehicleaccessories])->with('vehicle', $modelData);
    }
	
	public function perVehicleStore(Request $request)
    {
        $input = $request->except('_token');
		
		\DB::beginTransaction();
		try {	
			
			$fieldLabelNames = [];
			$requiredFields = [];
			
			$tableRows = [];
			foreach($input as $key=>$value)
			{
				$split = explode('-', $key);
				if(count($split)==2)
				{
					$rowID = $split[0];
					$colName = $split[1];
					
					$tableRows[$rowID][$colName] = $value;
				}
			}
			$vehicleID = $request->get('vehicle_id');
			//delete all
			Vehicleaccessory::where('id_vehicle', $vehicleID)->delete();
			foreach($tableRows as $accessoryID=>$row)
			{	
				$vehicleaccessory = new  Vehicleaccessory();
				$vehicleaccessory->id_vehicle = $vehicleID;
				$vehicleaccessory->id_accessory = $accessoryID;
				$vehicleaccessory->price = $row['price'];
				$vehicleaccessory->created_at = date('Y-m-d H:i:s');
				$vehicleaccessory->save();
			}
			
			\DB::commit();
			return redirect('vehicles/'.$vehicleID);
		}
		catch (\Exception $e)
		{	
			\DB::rollback();
			info($e->getMessage(), [$e->getLine()]);
		}
		
		return back();
    }

}
