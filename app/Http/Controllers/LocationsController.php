<?php namespace App\Http\Controllers;

use App\Models\Location;
use App\Models\Locationtype;
use App\Models\Operationalhours;
use App\Models\Vehiclelocationcost;
use App\Models\Locationtolocationcosts;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class LocationsController extends AdminController
{
	private $weekDaysArray = [
		'1'=>'Monday',
		'2'=>'Tuesday',
		'3'=>'Wednesday',
		'4'=>'Thursday',
		'5'=>'Friday',
		'6'=>'Saturday',
		'7'=>'Sunday',
	];
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		/*$locations = Location::join('locationtypes', 'locationtypes.id', '=', 'locations.id_locationtype')
							->select('locations.*', 'locationtypes.name AS locationtypes_name')
							->where('locations.deleted', 0)
							->get();*/
							
		$query = "SELECT locations.*,
						 locationtypes.name AS locationtypes_name
					FROM locations 
					  INNER JOIN locationtypes 
						ON (locations.id_locationtype = locationtypes.id) 
					WHERE locations.deleted = 0
					ORDER BY locations.active ASC";
		$locations = \DB::select($query);
		
		return view('admin.locations', ['resourceName'=>'locations', 'records' => $locations]);
		
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $locationtypes = Locationtype::where('deleted', 0)->get();
        return view('admin.locationsForm', ['locationtypes' => $locationtypes]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $input = $request->all();
		try {
		
			$fieldLabelNames = array(
		        'name' => 'Name',
		        'city' => 'City',
		        'lat' => 'Map Coordinates',
		        'address' => 'Address',
		        'tel' => 'Tel',
		        'after_hours_price' => 'After Hours Price',
			);
	        $requiredFields = array(
		        'name' => 'required',
		        'city' => 'required',
		        'address' => 'required',
		        'lat' => 'required',
		        'tel' => 'required',
		        'after_hours_price' => 'required|numeric',
	        );	
			
			if($request->exists('id'))
			{				
				$location = Location::findOrFail($request->get('id'));
				$location->updated_at = date('Y-m-d H:i:s');
			}
			else
			{
				$location = new Location();
				$location->created_at = date('Y-m-d H:i:s');
				$location->updated_at = date('Y-m-d H:i:s');
				
				$requiredFields['open_time'] = 'required|date_format:H:i';
				$fieldLabelNames['open_time'] = 'Open Time';
				$requiredFields['close_time'] = 'required|date_format:H:i';
				$fieldLabelNames['close_time'] = 'Close Time';
			}
			
		    $validator = \Validator::make($input, $requiredFields);
	        $validator->setAttributeNames($fieldLabelNames);

	        if($validator->fails())
		        throw new \Exception('Validation Failed.');

			$location->name = $request->get('name');
			$location->city = $request->get('city');
			$location->address = $request->get('address');
			$location->lat = $request->get('lat');
			$location->lng = $request->get('lng');
			$location->tel = $request->get('tel');
			$location->tel2 = $request->get('tel2');
			$location->fax = $request->get('fax');
			$location->email = $request->get('email');
			$location->id_locationtype = $request->get('id_locationtype');
			$location->after_hours_price = $request->get('after_hours_price');
			$location->active = $request->get('active');
			$location->save();
			
			//only on location creation
			if(!$request->exists('id'))
			{	
				$operationalhours = new Operationalhours();
				$operationalhours->id_location = $location->id;
				$operationalhours->start_day = 1;
				$operationalhours->end_day = 7;
				$operationalhours->open_time = $request->get('open_time');
				$operationalhours->close_time = $request->get('close_time');
				$operationalhours->not_operational = 0;
				$operationalhours->save();
			}
			
			\Cache::forget('locations');
			return redirect('locations');
		}
		catch (\Exception $e)
		{
			info($e->getMessage(), [$e->getLine()]);
			if($e instanceof \Illuminate\Database\Eloquent\ModelNotFoundException)
			{
				$validator = \Validator::make($input, ['Record_not_found'=>'required'], ['Record_not_found.required'=>'The record you are trying to edit does not exits!']);
				$validator->fails();
			}
		}
		
		return back()->withErrors($validator)->withInput()->with('model', $input);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		$modelData = Location::find($id);	   
        $locationtypes = Locationtype::where('deleted', 0)
									->get();
		return view('admin.locationsForm', ['locationtypes' => $locationtypes])->with('model', $modelData);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
   {
		try {
			$model = Location::find($id);
			$model->deleted = 1;
			$model->save();
		}
		catch (\Exception $e){}//skip errors
		
		return response()->json('ok', 200);
    }
	
	
    /**
     * Edit and Update Open Hours per Location
     */
	public function openHours($locationID)
    {
		$openhours = Operationalhours::where('id_location', $locationID)
									 ->orderBy('start_day', 'asc')
									 ->get();
									 
		$location = Location::find($locationID);
		
		return view('admin.operationalhours', ['location' => $location, 'weekDaysArray' => $this->weekDaysArray, 'records'=>$openhours]);		
    }
		
	public function openHoursStore(Request $request)
    {
        $input = $request->all();
		try {
		
			$operationalhours = new Operationalhours();
			$operationalhours->start_day = $request->get('start_day');
			$operationalhours->end_day = $request->get('end_day');
			if($request->get('not_operational') == 1)
			{	
				$operationalhours->open_time = 0;
				$operationalhours->close_time = 0;
				$operationalhours->not_operational = 1;
			}
			else 
			{	
				$fieldLabelNames = array(
					'open_time' => 'Open Time',
					'close_time' => 'Close Time'
				);
				$requiredFields = array(
					'open_time' => 'required|date_format:H:i',
					'close_time' => 'required|date_format:H:i'
				);
				$validator = \Validator::make($input, $requiredFields);
				$validator->setAttributeNames($fieldLabelNames);

				if($validator->fails())
					throw new \Exception('Validation Failed.');
				
				$operationalhours->open_time = $request->get('open_time');
				$operationalhours->close_time = $request->get('close_time');
				$operationalhours->not_operational = 0;
			}
			$operationalhours->id_location = $request->get('location_id');
			$operationalhours->save();
			
			\Cache::forget('locations');			
			return redirect('locations');
		}
		catch (\Exception $e)
		{
			info($e->getMessage(), [$e->getLine()]);
			if($e instanceof \Illuminate\Database\Eloquent\ModelNotFoundException)
			{
				$validator = \Validator::make($input, ['Record_not_found'=>'required'], ['Record_not_found.required'=>'The record you are trying to edit does not exits!']);
				$validator->fails();
			}
		}
		
		return back()->withErrors($validator)->withInput()->with('model', $input);
    }

    public function openHoursDestroy($id)
    {
        try {
			Operationalhours::destroy($id);
		}
		catch (\Exception $e){}//skip errors
		
		return response()->json('ok', 200);
    }
	
	
	/*******************************************************************************
     * CUSTOM actions for Return to Different Location Costs					     
     *******************************************************************************/
	 
    public function toLocationCosts($id)
    {
        $modelData = Location::find($id);
        $query = "SELECT locations.id, 
						 locations.name,
						 IFNULL(l2l.cost, 0) AS cost
					FROM locations
					LEFT JOIN location_to_location_costs AS l2l
					ON (locations.id = l2l.id_location_from
						AND l2l.id_location_to = {$modelData->id})
					WHERE locations.deleted = 0
					  AND locations.id <> {$modelData->id}
					GROUP BY locations.id
					ORDER BY locations.name";
		$locationtolocationcosts = \DB::select($query);
		
        return view('admin.locationToLocationCosts', ['records' => $locationtolocationcosts])->with('location', $modelData);
    }
	
	public function toLocationCostsStore(Request $request)
    {
        $input = $request->except('_token', 'location_id');
		
		\DB::beginTransaction();
		try {	
			
			$fieldLabelNames = [];
			$requiredFields = [];
			
			$tableRows = [];
			foreach($input as $key=>$value)
			{
				if($value >= 0)
					$tableRows[$key] = $value;
			}
			$toLocationID = $request->get('location_id');
			//delete all
			Locationtolocationcosts::where('id_location_to', $toLocationID)->delete();
			foreach($tableRows as $fromLocationID=>$costValue)
			{	
				$locationcost = new  Locationtolocationcosts();
				$locationcost->id_location_from = $fromLocationID;
				$locationcost->id_location_to = $toLocationID;
				$locationcost->cost = $costValue;
				$locationcost->created_at = date('Y-m-d H:i:s');
				$locationcost->save();
			}
			
			\DB::commit();
			return redirect('locations');
		}
		catch (\Exception $e)
		{	
			\DB::rollback();
			info($e->getMessage(), [$e->getLine()]);
		}
		
		return back();
    }

	/*******************************************************************************
     * CUSTOM actions for Return to Different Location Costs					     
     *******************************************************************************/
	 
    public function perVehicleCosts($id)
    {
        $modelData = Location::find($id);
		$query = "SELECT vehicles.id, 
						 vehicles.title,
						 IFNULL(vehiclelocationcosts.cost, 0) AS cost
					FROM vehicles
					LEFT JOIN vehiclelocationcosts
					ON (vehicles.id = vehiclelocationcosts.id_vehicle
						AND vehiclelocationcosts.id_location  = :locationID)
					WHERE vehicles.deleted = 0
					ORDER BY vehicles.title";
		$vehiclelocationcosts = \DB::select($query, [':locationID'=>$id]);
		
        return view('admin.locationPerVehicleCosts', ['records' => $vehiclelocationcosts])->with('location', $modelData);
    }
	
	public function perVehicleCostsStore(Request $request)
    {
        $input = $request->except('_token');
		
		\DB::beginTransaction();		
		try {	
		
			$fieldLabelNames = [];
			$requiredFields = [];
			
			$tableRows = [];
			foreach($input as $key=>$value)
			{
				if($value >= 0)
					$tableRows[$key] = $value;
			}
			$locationID = $request->get('location_id');
			//delete all
			Vehiclelocationcost::where('id_location', $locationID)->delete();
			foreach($tableRows as $vehicleID=>$costValue)
			{	
				$locationcost = new  Vehiclelocationcost();
				$locationcost->id_vehicle = $vehicleID;
				$locationcost->id_location = $locationID;
				$locationcost->cost = $costValue;
				$locationcost->created_at = date('Y-m-d H:i:s');
				$locationcost->save();
			}
			
			\DB::commit();
			return redirect('locations');
		}
		catch (\Exception $e)
		{	
			\DB::rollback();
			info($e->getMessage(), [$e->getLine()]);
		}
		
		return back();
    }

}
