<?php namespace App\Http\Controllers;

use App\Models\Feature;
use App\Models\Vehiclefeature;
use App\Models\Vehicle;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class FeaturesController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $features = Feature::where('deleted', 0)
						->get();
		return view('admin.features', ['resourceName'=>'features', 'records' => $features]);
	
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.featuresForm');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
		
		try {
		
			$fieldLabelNames = array(
		        'name' => 'Name',
			);
	        $requiredFields = array(
		        'name' => 'required',
	        );	
			
		    $validator = \Validator::make($input, $requiredFields);
	        $validator->setAttributeNames($fieldLabelNames);

	        if($validator->fails())
		        throw new \Exception('Validation Failed.');

			if($request->exists('id'))
			{				
				$feature = Feature::findOrFail($request->get('id'));
			}
			else
			{
				//create
				$feature = new Feature();
				$feature->created_at = date('Y-m-d H:i:s');
			}
			
			$icon_value = '';
			$icon_type = $request->get('icon_type');
			if($icon_type == 0)
			{
				$feature->icon_type = 0;
				$feature->icon_value = $request->get('icon_label');
			}
			else if($icon_type == 1)
			{
				$feature->icon_type = 1;
				$feature->icon_value = $request->get('icon_code');
			}
			else if($icon_type == 2)
			{
				$resp = $this->uploadFiles($request, 'icon_image', '30x30');
				if($resp['code'] == 200)
				{	
					$feature->icon_type = 2;
					$feature->icon_value = $resp['filename'];
				}
				else if($resp['code'] == 500)
					throw new \Exception('Error_uploading_file');
			}
			
			$feature->name = $request->get('name');
			$feature->description = $request->get('description');
			$feature->active = $request->get('active');
			
			$feature->save();
			
			return redirect('features');
		}
		catch (\Exception $e)
		{
			info($e->getMessage(), [$e->getLine()]);
			if($e instanceof \Illuminate\Database\Eloquent\ModelNotFoundException)
			{
				$validator = \Validator::make($input, ['Record_not_found'=>'required'], ['Record_not_found.required'=>'The record you are trying to edit does not exits!']);
				$validator->fails();
			}
			
			if($e->getMessage() == 'Error_uploading_file')
			{
				$validator = \Validator::make($input, ['Error_uploading_file'=>'required'], ['Error_uploading_file.required'=>'There was an error uploading the image file, please try again!']);
				$validator->fails();
			}
		}
		
		return back()->withErrors($validator)->withInput()->with('model', $input);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		$modelData = Feature::find($id);
		return view('admin.featuresForm')->with('model', $modelData);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		try {
			$model = Feature::find($id);
			$model->deleted = 1;
			$model->save();
		}
		catch (\Exception $e){}//skip errors
		
		return response()->json('ok', 200);
    }



	/*******************************************************************************
     * CUSTOM actions FEATURES PER VEHICLE			     				********
     *******************************************************************************/
	 
    public function perVehicle($id)
    {
        $modelData = Vehicle::find($id);
        $query = "SELECT features.id, 
						 features.name,
						 features.icon_type,
						 features.icon_value,
						 vehiclefeatures.value,
						 IF(vehiclefeatures.id IS NULL, 0, 1) AS applicable
				FROM features
				LEFT JOIN vehiclefeatures
				ON (features.id = vehiclefeatures.id_feature 
				AND vehiclefeatures.id_vehicle = :vehicleID)
				WHERE features.deleted = 0
				ORDER BY features.name";
		$vehiclefeatures = \DB::select($query, [':vehicleID'=>$id]);
				
        return view('admin.featuresPerVehicle', ['records' => $vehiclefeatures])->with('vehicle', $modelData);
    }
	
	public function perVehicleStore(Request $request)
    {
        $input = $request->except('_token');
		
		\DB::beginTransaction();
		try {	
			
			$fieldLabelNames = [];
			$requiredFields = [];
			
			$tableRows = [];
			foreach($input as $key=>$value)
			{
				$split = explode('-', $key);
				if(count($split)==2)
				{
					$rowID = $split[0];
					$colName = $split[1];
					
					$tableRows[$rowID][$colName] = $value;
				}
			}
			$vehicleID = $request->get('vehicle_id');
			//delete all
			Vehiclefeature::where('id_vehicle', $vehicleID)->delete();
			foreach($tableRows as $featureID=>$row)
			{	
				$vehiclefeature = new  Vehiclefeature();
				$vehiclefeature->id_vehicle = $vehicleID;
				$vehiclefeature->id_feature = $featureID;
				$vehiclefeature->value = $row['value'];
				$vehiclefeature->created_at = date('Y-m-d H:i:s');
				$vehiclefeature->save();
			}
			
			\DB::commit();
			return redirect('vehicles/'.$vehicleID);
		}
		catch (\Exception $e)
		{	
			\DB::rollback();
			info($e->getMessage(), [$e->getLine()]);
		}
		
		return back();
    }

}
