<?php namespace App\Http\Controllers;

use App\Models\User;

use App\Paypal\PaypalPayment;

use App\Models\Payment;
use App\Models\Reservation;
use App\Models\Location;
use App\Models\Driver;

use Illuminate\Support\Str;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;

class ClientsController extends Controller 
{
	public function __construct()
	{
		$this->middleware('auth.clients');
	}
	
	public function index()
	{
		$data = [];
		$data['user'] = User::find(Auth::id());
		
		$data['records'] = Reservation::where('id_user', Auth::id())
									->orderby('pickup_datetime', 'ASC')
									->paginate(25);
		
		return view('clients.home', $data);
	}
	
    public function viewReservation($id)
    {
		$query = " SELECT reservations.id
						, reservations.id_vehicle
						, reservations.vehicle_title
						, CONCAT(users.firstname, ' ', users.lastname) AS user_fullname
						, reservations.primary_driver_name
						, reservations.pickup_datetime
						, reservations.pickup_location
						, reservations.return_datetime
						, reservations.return_location
						, reservations.duration_days
						, reservations.created_at
						, reservations.approved
						, reservations.id_user_approved
						, reservations.approved_user_name
						, reservations.approved_at
						, reservations.total_price
						, reservations.payment_completed
						, reservations.security_deposit
						, reservations.id_payment
						, reservations.pay_later_request
						, reservations.request_description
						, reservations.status
					 FROM reservations
					INNER JOIN users 
					   ON (reservations.id_user = users.id)
					WHERE reservations.id = :reservationID";
		$reservation = \DB::select($query, [':reservationID'=> $id]);
		$reservation = $reservation[0];
		
        $query = " SELECT payments.id
						, payments.amount
						, payments.id_paymentmethod
						, paymentmethod.name AS paymentmethod
						, payments.id_paymenttype
						, paymenttype.name AS paymenttype
						, payments.id_user_verified
						, payments.verified_by
						, payments.status
						, IF(payments.id_paymentmethod = 1, CONCAT('Paypal ID: ', payments.paypal_payment_id), payments.details) AS details
						, IFNULL(payments.paid_at, payments.created_at) AS paid_at
					 FROM payments
					INNER JOIN paymenttype 
					   ON (payments.id_paymenttype = paymenttype.id)
					INNER JOIN paymentmethod 
					   ON (payments.id_paymentmethod = paymentmethod.id)
					WHERE payments.id_reservation = :reservationID";
		$payments = \DB::select($query, [':reservationID'=> $id]);
		
		$entries = \DB::table('reservation_entries')
							->where('id_reservation', $id)
							->where('deleted', 0)
							->get();
		
		$query = " SELECT drivers.id
						, drivers.firstname
						, drivers.lastname
						, drivers.passport
						, drivers.idcard
						, DATE_FORMAT(drivers.birthdate,'%d-%b-%Y') AS birthdate
						, drivers.birthplace
						, b_country.name AS birth_country
						, drivers.license_number
						, DATE_FORMAT(drivers.license_expiration,'%d-%b-%Y') AS license_expiration
						, drivers.email
						, address.address
						, address.zip_code
						, address.city
						, address.phone_1
						, address.phone_2
						, a_country.name AS address_country
						, rd.primary
					FROM
						drivers
						INNER JOIN address 
							ON (drivers.id = address.id_driver)
						INNER JOIN countries AS b_country
							ON (drivers.id_country_birth = b_country.id)
						INNER JOIN countries AS a_country
							ON (address.id_country = a_country.id)
						INNER JOIN reservation_drivers AS rd
							ON (rd.id_driver = drivers.id)
					WHERE rd.id_reservation = :reservationID
					  AND drivers.deleted = 0
					  AND address.type_home = 1						  
					ORDER BY rd.primary DESC";
		$drivers = \DB::select($query, [':reservationID'=> $id]);
		
		$reservation_status = "";
		if($reservation->status==0)
		{
			$reservation_status = "Canceled";
			if($reservation->approved == 2)
				$reservation_status = "Expired";
		}else{
			if($reservation->approved == 1)
				$reservation_status = "Approved";
			else
				$reservation_status = "Pending";
		}
				
        $query = "SELECT vehicles.*,
						 vehicletypes.name AS vehicletypes_name,
						 vehiclefueltypes.name AS fuel_type
					FROM vehicles 
					  INNER JOIN vehicletypes 
						ON (vehicles.id_vehicletype = vehicletypes.id) 
					  INNER JOIN vehiclefueltypes
						ON (vehicles.id_vehiclefueltype = vehiclefueltypes.id) 
					WHERE vehicles.id = :vehicleID";
		$vehicle = \DB::select($query, [':vehicleID'=>$reservation->id_vehicle]);
				
		$data = [
			'model' => $reservation, 
			'vehicle' => $vehicle[0], 
			'payments' => $payments, 
			'entries' => $entries, 
			'drivers' => $drivers, 
			'reservation_status' => $reservation_status
		];
		
        return view('clients.reservationDetails', $data);
    }

    public function cancelReservation($id)
    {
		$reservation = Reservation::find($id);
		
		if($reservation->id_user != Auth::id())
			return view('public.notFound');
		
		$reservation->status = 0;
		$reservation->approved = 3;//canceled
		$reservation->id_user_approved = -1;
		$reservation->approved_user_name = null;
		$reservation->approved_at = null;
		$reservation->save();
		
		return redirect('view-reservation/'.$reservation->id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function paynowWithPaypal($id)
{
		$payment = Payment::find($id);
		$reservation = Reservation::find($payment->id_reservation);
		
	
		$paymentDescription = $reservation->duration.($reservation->duration == 1? ' day': ' days');
		$paymentDescription .= ' Reservation for '.$reservation->vehicle_title;
			
		$paypalEntries = array([
			'name' => 'Payment',
			'quantity' => 1,
			'price' => $payment->amount
		]);
		
		$paypal = new PaypalPayment();
		$paymentResponse = $paypal->makePayment($payment->id, $payment->amount, 'EUR', $paymentDescription, $paypalEntries);
		
		if($paymentResponse != null && $paymentResponse->getState() == "created")
		{
			$payment->paypal_payment_id = $paymentResponse->getId();
			$payment->save();
			
			return redirect($paypal->getLink($paymentResponse->getLinks(), "approval_url") );
		}
		else
		{
			return view('public.paypalPaymentCreationError', ['reservation_id' => $reservationID]);
		}
    }
	
    public function userEdit()
    {
		$userData = User::find(Auth::id());
		
		return view('admin.usersForm', ['client' => true])->with('model', $userData);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function userEditStore(Request $request)
    {
		$input = $request->all();
		$validator;
		$emailTaken = false;
		
		try {
		
			$fieldLabelNames = array(
		        'email' => 'Email',
		        'firstname' => 'Firstname',
		        'lastname' => 'Lastname',
			);
	        $requiredFields = array(
		        'email' => 'required|email',
		        'firstname' => 'required',
		        'lastname' => 'required',
	        );
			
			$user = User::findOrFail(Auth::id());
				
			//check if it was changed
			if(Str::lower($user->email) != Str::lower($request->get('email')))
			{   //now check if someone else has it already
				$emailTaken = User::where('email', $request->get('email'))->exists();
			}
			
		    if ($emailTaken)
				throw new Exception('Email_taken');
				
	        $validator = \Validator::make($input, $requiredFields);
	        $validator->setAttributeNames($fieldLabelNames);

	        if($validator->fails())
		        throw new \Exception('Validation Failed.');

			$user->email = $request->get('email');
			$user->firstname = $request->get('firstname');
			$user->lastname = $request->get('lastname');			
			$user->tel = $request->get('tel');
			$user->status = 1;
			$user->save();
			
			return redirect('my-account');
		}
		catch (\Exception $e)
		{
			info($e->getMessage(), [$e->getLine()]);
			if($e instanceof \Illuminate\Database\Eloquent\ModelNotFoundException)
			{
				$validator = \Validator::make($input, ['Record_not_found'=>'required'], ['Record_not_found.required'=>'The record you are trying to edit does not exits!']);
				$validator->fails();
			}
			if($e->getMessage() == 'Email_taken')
			{
				$validator = \Validator::make($input, ['Record_not_found'=>'required'], ['Record_not_found.required'=>'Email is already in use, please try another one.']);
				$validator->fails();
			}
		}
		
		return back()->withErrors($validator)->withInput()->with('model', $input);
    }
	
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function newPassword()
    {
		$userData = User::find(Auth::id());
		return view('admin.usersPassword', ['client' => true])->with('model', $userData);
    }
	
    /**
     * Update/Change Password
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function changePassword(Request $request)
    {
		$input = $request->all();
		
		try {
		
	        $requiredFields = array(
		        'password' => 'required|min:4',
	        );
	        $validator = \Validator::make($input, $requiredFields);

	        if($validator->fails())
		        throw new \Exception('Validation Failed.');
			
			$user = User::findOrFail(Auth::id());
			$user->password = $request->get('password');
			$user->save();
			
			return redirect('my-account');
		}
		catch (\Exception $e)
		{
			info($e->getMessage(), [$e->getLine()]);
			if($e instanceof \Illuminate\Database\Eloquent\ModelNotFoundException)
			{
				$validator = \Validator::make($input, ['Record_not_found'=>'required'], ['Record_not_found.required'=>'The record you are trying to edit does not exits!']);
				$validator->fails();
			}
		}
		
		return back()->withErrors($validator)->with('model', $input);
    }
	
}
