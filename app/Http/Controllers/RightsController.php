<?php

namespace App\Http\Controllers;

use App\Models\Right;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class RightsController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $rights = Right::where('deleted', 0)
					->get();
		
		return view('admin.rights', ['rights' => $rights]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.rightsForm');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $input = $request->all();
		try {
		
			$fieldLabelNames = array(
		        'id_rightsection' => 'Id Rights Section',
		        'shadow_rights' => 'Shadow Rights',
		        'right_label' => 'Right label',
		        'virtual' => 'Virtual',
		       
			);
	        $requiredFields = array(
		        'id_rightsection' => 'required',
		        'shadow_rights' => 'required',
		        'right_label' => 'required',
		        'virtual' => 'required',
		        
	        );	
			
		    $validator = \Validator::make($input, $requiredFields);
	        $validator->setAttributeNames($fieldLabelNames);

	        if($validator->fails())
		        throw new \Exception('Validation Failed.');

			if($request->exists('id'))
			{
				
				$right = Right::findOrFail($request->get('id'));
			}
			else
			{
				//create
				$right = new Right();
				
			}
			
			$right->id_rightsection = $request->get('id_rightsection');
			$right->shadow_rights = $request->get('shadow_rights');
			$right->right_label = $request->get('right_label');
			$right->virtual = $request->get('virtual');
			$right>save();
			return redirect('rights');
		}
		catch (\Exception $e)
		{
			info($e->getMessage(), [$e->getLine()]);
			if($e instanceof \Illuminate\Database\Eloquent\ModelNotFoundException)
			{
				$validator = \Validator::make($input, ['Record_not_found'=>'required'], ['Record_not_found.required'=>'The record you are trying to edit does not exits!']);
				$validator->fails();
			}
		}
		
		return back()->withErrors($validator)->withInput()->with('model', $input);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $modelData =Right::find($id);
		return view('admin.rightsForm')->with('model', $modelData);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
