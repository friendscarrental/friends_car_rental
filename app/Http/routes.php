<?php

//SEARCH
Route::get('/',									'SearchController@index');
Route::post('search/vehicles',					'SearchController@vehiclesList');
Route::get('search/vehicles/{id}',				'SearchController@vehicleDetails');
Route::post('search/accessories',				'SearchController@accessoriesList');
Route::post('search/checkout',					'SearchController@checkout');

//PUBLIC REGISTRATION
Route::get('pdf',								'PublicController@pdf');
Route::get('register',							'PublicController@registerStepOne');
Route::post('register-save',					'PublicController@registerStepOneSave');
Route::get('register-driver',					'PublicController@registerStepTwo');
Route::post('registration-save',				'PublicController@registerStepThree');

//RETURN RESPONSE FROM PAYPAL
Route::get('paypal-response/{id}/{status}/',	'PublicController@paypalResponse');

Route::get('login',								'AuthController@loginForm');
Route::post('login',							'AuthController@login');
Route::get('logout',							'AuthController@logout');

//CLIENTS SECTION
Route::get('my-account',						'ClientsController@index');
Route::get('view-reservation/{id}',				'ClientsController@viewReservation');
Route::get('cancel-reservation/{id}',			'ClientsController@cancelReservation');
Route::get('paynow-paypal/{id}',				'ClientsController@paynowWithPaypal');
Route::get('edit-account',						'ClientsController@userEdit');
Route::post('edit-account',						'ClientsController@userEditStore');
Route::get('change-pass',						'ClientsController@newPassword');
Route::post('change-pass',						'ClientsController@changePassword');


Route::get('reservations/cancel',				'SharedController@cancelReservation');
Route::get('reservations/complete/{redirect_from?}/{driver_id?}',	'SharedController@completeReservation');//param redirect from 
Route::post('reservations/save',				'SharedController@saveReservation');
Route::get('reservations/{id}/docs/{doc}/{lang}','SharedController@documents');

//RESERVED SECTION
Route::get('home',								'UsersController@home');
Route::get('admin',								'UsersController@home');

Route::get('accessories/{id}/per-vehicle',		'AccessoriesController@perVehicle');
Route::post('accessories/per-vehicle',			'AccessoriesController@perVehicleStore');
Route::resource('accessories',    	            'AccessoriesController');

Route::get('address/{id}/primary',        		'AddressController@primary');
Route::get('address/create/{driver_id}',        'AddressController@create');
Route::resource('address',    	                'AddressController');

Route::get('drivers/search',					'DriversController@searchDrivers');
Route::get('drivers/{id}/details',				'DriversController@driversDetails');
Route::get('drivers/{user_id?}/list',			'DriversController@index');
Route::get('drivers/select/{reservation_id?}',	'DriversController@selectDriver');
Route::post('drivers/select',					'DriversController@selectDriverStore');
Route::get('drivers/create/{reservation_id?}',	'DriversController@create');//saved on /store/new
Route::post('drivers/store/new',				'DriversController@storeNewDriver');//form opened on create()
Route::get('drivers/{id}/primary/{reservation_id}',		'DriversController@primary');
Route::delete('drivers/{id}/remove/{reservation_id}',	'DriversController@removeFrom');
Route::resource('drivers',    	                'DriversController');

Route::get('features/{id}/per-vehicle',			'FeaturesController@perVehicle');
Route::post('features/per-vehicle',			    'FeaturesController@perVehicleStore');
Route::resource('features',    	                'FeaturesController');

Route::get('locations/{id}/to-location-costs',	'LocationsController@toLocationCosts');
Route::post('locations/to-location-costs',		'LocationsController@toLocationCostsStore');

Route::get('locations/{id}/per-vehicle-costs',	'LocationsController@perVehicleCosts');
Route::post('locations/per-vehicle-costs',		'LocationsController@perVehicleCostsStore');

Route::get('locations/{id}/open-hours',			'LocationsController@openHours');
Route::post('locations/open-hours',				'LocationsController@openHoursStore');
Route::delete('locations/open-hours/{id}',		'LocationsController@openHoursDestroy');
Route::resource('locations',    	            'LocationsController');
Route::resource('locationtypes',                'LocationtypesController');

Route::get('payments/{id}/refund/{deposit?}',	'PaymentsController@refund');
Route::post('payments/refund',					'PaymentsController@refundStore');
Route::get('payments/create/{reservation_id}',	'PaymentsController@create');
Route::resource('payments',    	                'PaymentsController');

Route::get('reservations/{id}/status/{value}',	'ReservationsController@status');
Route::get('reservations/past/{v_id?}',			'ReservationsController@past');
Route::resource('reservations',    	            'ReservationsController');
Route::resource('rights',    	                'RightsController');
Route::resource('rightsections',    	        'RightsectionsController');
Route::resource('roles',                        'RolesController');
Route::resource('rolerights',    	            'RolerightsController');

Route::get('users/change-pass/{id}',			'UsersController@newPassword');
Route::post('users/change-pass',				'UsersController@changePassword');
Route::resource('users',	                    'UsersController');

Route::get('vehicles/{id}/discounts',			'VehiclesController@durationDiscounts');
Route::post('vehicles/discounts',				'VehiclesController@durationDiscountsStore');
Route::delete('vehicles/discounts/{id}',		'VehiclesController@durationDiscountsDestroy');

Route::get('vehicles/{id}/images',				'VehiclesController@images');
Route::post('vehicles/images',					'VehiclesController@imagesStore');
Route::delete('vehicles/images/{id}',			'VehiclesController@imagesDestroy');

Route::get('vehicles/{id}/location-costs',		'VehiclesController@locationCosts');
Route::post('vehicles/location-costs',			'VehiclesController@locationCostsStore');

Route::get('vehicles/{id}/seasonal-prices',		'VehiclesController@seasonalPrices');
Route::post('vehicles/seasonal-prices',			'VehiclesController@seasonalPricesStore');
Route::delete('vehicles/seasonal-prices/{id}',	'VehiclesController@seasonalPricesDestroy');

Route::resource('vehicles',    	                'VehiclesController');
Route::resource('vehiclefueltypes',    	    	'VehiclefueltypesController');
Route::resource('vehiclelocationcosts',    	    'VehiclelocationcostsController');
Route::resource('vehicletypes',              	'VehicletypesController');




// CATCH ALL ROUTE =============================  
// all routes that are not home or api will be redirected to the frontend 
// this allows angular to route them 
//App::missing(function($exception) { 
//    return Redirect::to('/');
//});


//fall-back url if missed typed the address
//this must be placed last
Route::any('{all}', function($uri)
{
	info('WRONG ROUTE: '.$uri);
	return view('public.notFound');
})->where('all', '.*');



