<?php

class Document extends Eloquent {

	protected $table = 'document';

	public $timestamps = false; //added after timestamps error log.

	protected $guarded = array('id');

}