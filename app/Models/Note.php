<?php

class Note extends Eloquent {

	protected $table = 'notes';

	public $timestamps = false; //added after timestamps error log.

	protected $guarded = array('id');


	public function addNote($note, $tbl, $tbj_id, $files, $fileType=0, $related=null)//file type => 1=interest certificate
	{
		$this->comment = $note;
		$this->filetype = $fileType;
		$this->subject_tbl = $tbl;
		$this->id_subject = $tbj_id;
		if($related != null)
		{
			$this->related_tbl = $related['tbl'];
			$this->id_related = $related['id'];
		}
		$this->id_users = Auth::id();
		$this->date_created = $now = date("Y-m-d H:i:s");
		$this->save();

		foreach ($files as $doc) {
			$document = new Document;
			$document->id_notes = $this->id;
			$document->file_name = $doc['file_name'];
			$document->file_label = $doc['file_label'];
			$document->id_users = Auth::id();
			$document->date_created = $now;
			$document->save();
		}
	}
}