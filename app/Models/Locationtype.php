<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Locationtype extends Model {

	protected $table = 'locationtypes';
	public $timestamps = false;

}
