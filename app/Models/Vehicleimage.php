<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Vehicleimage extends Model {

	protected $table = 'vehicleimages';
	public $timestamps = false;

}
