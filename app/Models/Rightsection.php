<?php

class Rightsection extends Eloquent {

    protected $table = 'rightsections';
    protected $guarded = array('id');
    public $timestamps = false; //added after timestamps error log.

}
