<?php

class Right extends Eloquent {

    protected $table = 'rights';
    public $timestamps = false;

    protected $guarded = array('id');

}