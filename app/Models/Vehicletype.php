<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Vehicletype extends Model {

	protected $table = 'vehicletypes';
	public $timestamps = false;

}
