<?php namespace App\Paypal;

use PayPal\Api\Amount;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Refund;
use PayPal\Api\Sale;

use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;

class PaypalPayment{

    private $_apiContext;
	
	public function __construct()
    {
		/* SANDBOX*/
		$this->_apiContext = new ApiContext(new OAuthTokenCredential(
			'AVGjJnvqIlNi0vhTEe0OkUK2PrVAkAp-xxktBokWNgdQfeebvWN6UyzvqrmtTQagGzK8jUknD8VQwHBb',
			'EO-JwyJRUfuONQgjU9OYTp_AxpcMrMB2ezTslhMCl3jt_BV2D0xkPPnXe02bVbWDHhhp051LWry8TjLQ'
		));
		
		/* LIVE		
		$this->_apiContext = new ApiContext(new OAuthTokenCredential(
			'AX8JOTCgRMYmLJniil5D0XB7oJXhH5LpvDR7ERVyQ9_QgvlZAV717ITmaolF_tHqOqbbRtUyRULZi_z5',
			'EHYeAFBiuzwzZfVlKMi7aS8NOQpPDYgazPH6EEf4dzqRk9h2R8LO0NfxoyrruJEEGurGDIBvO9R66uGo'
		));*/

		$this->_apiContext->setConfig(array(
			'http.ConnectionTimeOut' => 60,//30
			'http.Retry' => 1,
			 
			'mode' => 'sandbox',
			'service.EndPoint' => 'https://api.sandbox.paypal.com',			
			/*
			'mode' => 'live',
			'service.EndPoint' => 'https://api.paypal.com',
			*/
			'log.LogEnabled' => true,
			'log.FileName' => storage_path('logs/paypal.log'),
			'log.LogLevel' => 'FINE' //FINE, INFO, WARN or ERROR
		));
	}
	
	public function getLink(array $links, $type) {
		foreach($links as $link) {
			if($link->getRel() == $type) {
				return $link->getHref();
			}
		}
		return "";
	}
	
    public function makePayment($payment_id, $total, $currency, $paymentDescription, $entries=null, $returnUrl='', $cancelUrl='') 
    {  
		try {
			// Specify the payment amount.
			$amount = new Amount();
			$amount->setCurrency($currency);
			$amount->setTotal($total);
			
			$transaction = new Transaction();
			$transaction->setInvoiceNumber($payment_id);
			$transaction->setAmount($amount);		
			$transaction->setDescription($paymentDescription);
			
			$calcSubtotal = 0;
			if($entries != null)
			{
				$itemsList = [];
				foreach($entries as $entry)
				{
					$item = new Item();
					$item->setName($entry['name']);
					if(isset($entry['description']))
						$item->setDescription($entry['description']);
					if(isset($entry['quantity']))
					{
						$item->setQuantity($entry['quantity']);//price must divided by quantity to add up correctly
						$item->setPrice($entry['price']/$entry['quantity']);
					}
					else {
						$item->setQuantity(1);
						$item->setPrice($entry['price']);
					}
					
					$item->setCurrency($currency);
					$itemsList[] = $item;
					
					$calcSubtotal += $entry['price'];
				}
				
				$itemList = new ItemList();
				$itemList->setItems($itemsList);

				$transaction->setItemList($itemList);
			}
					
			if($returnUrl == '')
				$returnUrl = url('paypal-response', [$payment_id, 'ok']);//success
			if($cancelUrl == '')
				$cancelUrl = url('paypal-response', [$payment_id, 'canceled']);
			
			$redirectUrls = new RedirectUrls();
			$redirectUrls->setReturnUrl($returnUrl);
			$redirectUrls->setCancelUrl($cancelUrl);
			
			$payer = new Payer();
			$payer->setPaymentMethod("paypal");
			
			$payment = new Payment();
			$payment->setRedirectUrls($redirectUrls);
			$payment->setIntent("sale");
			$payment->setPayer($payer);
			$payment->setTransactions(array($transaction));
			
			$payment->create($this->_apiContext);
			
			return $payment;
		} 
		catch (\PayPal\Exception\PPConnectionException $ex) 
		{
			info($ex->getMessage(), [$ex->getData()]);
		} 
		catch (\Exception $e) 
		{
			info($e->getMessage(), [$e->getLine()]);
		}
		
		return null;
    }
	
    public function executePayment($paypal_payment_id, $paypal_payer_id) 
    {  
		try {
			
			$payment = Payment::get($paypal_payment_id, $this->_apiContext);

			$paymentExecution = new PaymentExecution();
			$paymentExecution->setPayerId($paypal_payer_id);

			$payment = $payment->execute($paymentExecution, $this->_apiContext);
			
			return $payment;
		} 
		catch (\PayPal\Exception\PPConnectionException $ex) 
		{
			info($ex->getMessage(), [$ex->getData()]);
		} 
		catch (\Exception $e) 
		{
			info($e->getMessage(), [$e->getLine()]);
		}
		
		return null;		
	}
	
    public function refundSale($sale_id, $amount, $currency='EUR') 
    {  
		try {			
			//$sale = Sale::get($sale_id, $this->_apiContext);

			$amt = new Amount();
			if($amount > 0)
			{
				$amt->setCurrency($currency);
				$amt->setTotal($amount);
			}
			$refund = new Refund();
			$refund->setAmount($amt);
			
			$sale = new Sale();
			$sale->setId($sale_id);
			$refund = $sale->refund($refund, $this->_apiContext);

			return $refund;
		}
		catch (\PayPal\Exception\PPConnectionException $ex) 
		{
			info($ex->getMessage(), [$ex->getData()]);
		} 
		catch (\Exception $e) 
		{
			info($e->getMessage(), [$e->getLine()]);
		}
		
		return null;		
	}
}
