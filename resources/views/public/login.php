<?php echo View::make('partials.public_header') ?>

<div class="container">
<br>
<br>
<br>
<div class="row">
	<form class="form-horizontal col-md-6" action="/login" method="post" style="background-color: #fff; border: 1px solid #ccc;">
		<h2>Login to your account</h2></br>
		
		<?php if (count($errors) > 0) { ?>
			<div class="alert alert-danger">
				<ul>
					<?php foreach ($errors->all() as $error){ ?>
						<li><?php echo $error; ?></li>
					<?php } ?>
				</ul>
			</div>
		<?php } ?>
		
		<div class="form-group">
			<label for="email" class="col-md-3 control-label">Email</label>
			<div class="col-md-8">
				<input type="email" name="email" id="email" class="form-control input-lg">
			</div>
		</div>
		
		<div class="form-group">
			<label for="password" class="col-md-3 control-label">Password</label>
			<div class="col-md-8">
				<input type="password" name="password" id="password" class="form-control input-lg">
			</div>
		</div>
		<div class="form-group">
			<div class="col-md-offset-3 col-md-9">
				<div class="checkbox">
					<label>
						<input type="checkbox" name="remember" id="remember" value="1"> Remember me
					</label>
				</div>
			</div>
		</div>
		<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
		<hr>
		<div class="form-group">
			<div class="col-md-offset-3 col-md-4">
				<button type="submit" style="width: 100%; max-width: 150px;" class="btn btn-yellow">Login <span class="glyphicon glyphicon-chevron-right"></span></button>
			</div>
			<div class="col-md-5">
				<a class="pull-right" style="margin: 5px;" href="/forgot-password">Forgot your password?</a>
			</div>
		</div>
	</form>
	<div class="col-md-6">
		<div class="row" style="background-color: #fff; border: 1px solid #ccc; margin: 0 10px; padding: 0 15px 15px;">
			<h2 style="margin-bottom: 28px;">New Customer Account?</h2>
			</br>
			</br>
			</br>
			<p>Create your account and benefit from faster booking and pick-up processes</p>
			</br>
			</br>
			</br>
			<hr>
			<a href="/register" class="btn btn-yellow pull-right">Create New Account <span class="glyphicon glyphicon-chevron-right"></span>
			</a>
		</div>
	</div>
</div>

<?php echo View::make('partials.footer') ?>