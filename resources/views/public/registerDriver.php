<?php 

	$headerPartial = 'partials.public_header';
	$formAction = '/registration-save';
	$mainTitle = 'Account Registeration <small>(Address and Driver Details)</small>';
	
	if (Auth::check())
	{
		$headerPartial = 'partials.header';
		$formAction = '/drivers/store/new';
		$mainTitle = 'Register New Driver and Address';
	}
	
	echo View::make($headerPartial);
?>

<div class="container">
<br>

<div class="row">
	<form class="col-md-offset-2 col-md-8" action="<?php echo $formAction; ?>" method="post"
		style="background-color: #fff; border: 1px solid #ccc; padding: 0 50px 35px;">
				
		<h2><?php echo $mainTitle; ?></h2>
		<hr>
		
		<?php if (count($errors) > 0) { ?>
			<div class="alert alert-danger">
				<ul>
					<?php foreach ($errors->all() as $error){ ?>
						<li><?php echo $error; ?></li>
					<?php } ?>
				</ul>
			</div>
		<?php } ?>
		
		<div class="row">
			<div class="col-md-6 form-group required">
				<label for="firstname" class="control-label">Firstname</label>
				<input type="input" name="firstname" id="firstname" value="<?php echo isset($firstname)?$firstname:old('firstname'); ?>" class="form-control" placeholder="Firstname">
			</div>
			<div class="col-md-6 form-group required">
				<label for="lastname" class="control-label">Lastname</label>
				<input type="input" name="lastname" id="lastname" value="<?php echo isset($lastname)?$lastname:old('lastname'); ?>" class="form-control" placeholder="Lastname">
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-12 form-group required">
				<label for="address" class="control-label">Address</label>
				<input type="input" name="address" id="address" value="<?php echo old('address'); ?>" class="form-control" placeholder="Address">
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-2 form-group">
				<label for="zip_code" class="control-label">Postcode</label>
				<input type="input" name="zip_code" id="zip_code" value="<?php echo old('zip_code'); ?>" class="form-control" placeholder="Postcode">
			</div>
			
			<div class="col-md-4 form-group required">
				<label for="city" class="control-label">City</label>
				<input type="input" name="city" id="city" value="<?php echo old('city'); ?>" class="form-control" placeholder="City/Town">
			</div>
			
			<div class="col-md-6 form-group required">
				<label for="id_country" class="control-label">Country</label>
				<select name="id_country" id="id_country" class="form-control">
					<option value="" selected="selected">Select a Country</option>
					<?php foreach ($countries as $country){ ?>
						<option value="<?php echo $country->id; ?>" <?php if($country->id == old('id_country')) echo'selected="selected"'; ?> ><?php echo $country->name; ?></option>
					<?php } ?>
				</select>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 form-group" style="border-bottom: 1px solid #ddd; margin: 20px 0 10px; font-style: italic; color: #aaa; padding-left: 0px;" >
				Contact
			</div>
		</div>
		<?php if(!isset($init_registration)){ ?>
			<div class="row">
				<div class="col-md-12 form-group required">
					<label for="email" class="control-label">Email</label>
					<input type="email" name="email" id="email" value="<?php echo old('email'); ?>" class="form-control" placeholder="Email">
				</div>
			</div>
		<?php } ?>
		<div class="row">
			<div class="col-md-6 form-group required">
				<label for="phone_1" class="control-label">Phone 1</label>
				<input type="input" name="phone_1" id="phone_1" value="<?php echo old('phone_1'); ?>" class="form-control" placeholder="Main Phone Number">
			</div>
			<div class="col-md-6 form-group">
				<label for="phone_2" class="control-label">Phone 2</label>
				<input type="input" name="phone_2" id="phone_2" value="<?php echo old('phone_2'); ?>" class="form-control" placeholder="Secondary Phone Number">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 form-group" style="border-bottom: 1px solid #ddd; margin: 20px 0 10px; font-style: italic; color: #aaa; padding-left: 0px;" >
				Driver Details
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 form-group required">
				<label for="birth_year" class="control-label">Birth Date</label>
				<div class="form-inline">
					<select name="birth_day" id="birth_day" class="form-control">
						<option value="" selected="selected">Day</option>
						<?php for($i=1; $i <=31; $i++){ 
								$value = str_pad($i, 2, '0', STR_PAD_LEFT); ?>
							<option value="<?php echo $value; ?>" <?php if($value == old('birth_day')) echo'selected="selected"'; ?> ><?php echo $i; ?></option>
						<?php } ?>
					</select>
					<select name="birth_month" id="birth_month" class="form-control">
						<option value="" selected="selected">Month</option>
						<?php for($i=1; $i <=12; $i++){ 
								$value = str_pad($i, 2, '0', STR_PAD_LEFT); ?>
							<option value="<?php echo $value; ?>" <?php if($value == old('birth_month')) echo'selected="selected"'; ?> ><?php echo $i; ?></option>
						<?php } ?>
					</select>
							
					<input type="input" name="birth_year" id="birth_year" value="<?php echo old('birth_year'); ?>" class="form-control" style="max-width: 150px;" placeholder="Year">					
				</div>
			</div>
		</div>		
		<div class="row">
			<div class="col-md-6 form-group required">
				<label for="birthplace" class="control-label">Birth Place</label>
				<input type="input" name="birthplace" id="birthplace" value="<?php echo old('birthplace'); ?>" class="form-control" placeholder="Birth Place">
			</div>
			<div class="col-md-6 form-group required">
				<label for="id_country_birth" class="control-label">Country of Birth</label>
				<select name="id_country_birth" id="id_country_birth" class="form-control">
					<option value="" selected="selected">Select a Country</option>
					<?php foreach ($countries as $country){ ?>
						<option value="<?php echo $country->id; ?>" <?php if($country->id == old('id_country_birth')) echo'selected="selected"'; ?> ><?php echo $country->name; ?></option>
					<?php } ?>
				</select>
			</div>			
		</div>
		
		<div class="row">
			<div class="col-md-12 form-group" style="border-bottom: 1px solid #ddd; margin: 20px 0 10px; font-style: italic; color: #aaa; padding-left: 0px;" >
				Documents
			</div>
		</div>
		<div class="row">
			<div class="col-md-6 form-group required">
				<label for="license_number" class="control-label">License Number</label>
				<input type="input" name="license_number" id="license_number" value="<?php echo old('license_number'); ?>" class="form-control" placeholder="License Number">
			</div>
			<div class="col-md-6 form-group required">
				<label for="license_year" class="control-label">License Expiration Date</label>
				<div class="form-inline">
					<select name="license_exp_day" id="license_exp_day" class="form-control">
						<option value="" selected="selected">Day</option>
						<?php for($i=1; $i <=31; $i++){ 
								$value = str_pad($i, 2, '0', STR_PAD_LEFT); ?>
							<option value="<?php echo $value; ?>" <?php if($value == old('license_exp_day')) echo'selected="selected"'; ?> ><?php echo $i; ?></option>
						<?php } ?>
					</select>
					<select name="license_exp_month" id="license_exp_month" class="form-control">
						<option value="" selected="selected">Month</option>
						<?php for($i=1; $i <=12; $i++){ 
								$value = str_pad($i, 2, '0', STR_PAD_LEFT); ?>
							<option value="<?php echo $value; ?>" <?php if($value == old('license_exp_month')) echo'selected="selected"'; ?> ><?php echo $i; ?></option>
						<?php } ?>
					</select>
					
					<input type="input" name="license_exp_year" id="license_exp_year" value="<?php echo old('license_exp_year'); ?>" class="form-control" style="max-width: 150px;" placeholder="Year">					
				</div>
			</div>		
		</div>
		<div class="row">			
			<div class="col-md-6 form-group required">
				<label for="id_country_license" class="control-label">Country</label>
				<select name="id_country_license" id="id_country_license" class="form-control">
					<option value="" selected="selected">Select a Country</option>
					<?php foreach ($countries as $country){ ?>
						<option value="<?php echo $country->id; ?>" <?php if($country->id == old('id_country_license')) echo'selected="selected"'; ?> ><?php echo $country->name; ?></option>
					<?php } ?>
				</select>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6 form-group required">
				<label for="passport" class="control-label">Passport Number</label>
				<input type="input" name="passport" id="passport" value="<?php echo old('passport'); ?>" class="form-control" placeholder="Passport Number">
			</div>
			<div class="col-md-6 form-group required">
				<label for="idcard" class="control-label">Identity Card Number</label>
				<input type="input" name="idcard" id="idcard" value="<?php echo old('idcard'); ?>" class="form-control" placeholder="ID Card Number">
			</div>
		</div>
		
		
		<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
		<?php if(isset($init_registration)){ ?>
			<div class="row">
				<div class="col-md-12 form-group" style="border-bottom: 1px solid #ddd; margin: 20px 0 10px; font-style: italic; color: #aaa; padding-left: 0px;" >
					Confirm Email and Password
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 form-group required">
					<label for="email" class="control-label">Confirm Email</label>
					<input type="email" name="email" id="email" value="<?php echo isset($email)?$email:old('email'); ?>" class="form-control" placeholder="Login Email">
				</div>
				<div class="col-md-6 form-group required">
					<label for="password" class="control-label">Confirm Password</label>
					<input type="password" name="password" id="password" class="form-control" placeholder="Password">
				</div>
			</div>
			<input type="hidden" name="users_firstname" value="<?php echo isset($firstname)?$firstname:old('users_firstname'); ?>">
			<input type="hidden" name="users_lastname" value="<?php echo isset($lastname)?$lastname:old('users_lastname'); ?>">
			
			<hr>
			<div class="row">
				<button type="submit" class="btn btn-yellow btn-lg center-block">Save and Proceed to Reservation <span class="glyphicon glyphicon-chevron-right"></span></button>
			</div>
			
		<?php }else{ ?>	
			
			<?php if(isset($reservation_id) || old('reservation_id') != null): ?>
				<input type="hidden" name="reservation_id" value="<?php echo isset($reservation_id)?$reservation_id:old('reservation_id'); ?>">
			<?php endif; ?>
			<hr>
			<div class="row">
				<button type="submit" class="btn btn-primary btn-lg center-block">Save Driver</span></button>
			</div>
		<?php } ?>
	
	</form>
</div>

<script>

	$(document).ready(function ()
	{
		$('#id_country').on('change', function() 
		{
			if($("#id_country_birth").val() == "")
				$("#id_country_birth").val($(this).val());
			
			if($("#id_country_license").val() == "")
				$("#id_country_license").val($(this).val());
		});
	});

</script>

<?php echo View::make('partials.footer') ?>