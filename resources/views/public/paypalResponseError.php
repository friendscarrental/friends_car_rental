<?php echo View::make('partials.public_header') ?>

<div class="container">
<br>

<div class="row">
	<div class="col-md-offset-2 col-md-8" style="background-color: #fff; border: 1px solid #ccc;">
		
		<h2>Unable to Proccess Paypal Response <br><small>(unexpected redirection error)</small></h2>
		<hr>
		<ul style="font-size: 18px; margin: 50px;">
			<li><a href="/">Start Over - Search Again <span class="glyphicon glyphicon-chevron-right"></span></a></li>
			<br>
			<li><a href="/home">Go to your account <span class="glyphicon glyphicon-chevron-right"></span></a></li>
		</ul>
	</div>
</div>

<?php echo View::make('partials.footer') ?>