
<script type="text/javascript">
	
	function selectVehicle(index)
	{
		console.log('selected');
		vehicleSelectedIndex = index;
		
		History.pushState({step: 3}, "Select Extras", "?step=3");//this takes it to the next page
	}

	function searchAccessories()
	{
		console.log('searchAccessories');
		if(vehicleSelectedIndex == -1)
			return;
		
		var postData = {_token: Utils.CSRF_TOKEN,
			vehicle_id: reservationData.vehicles[vehicleSelectedIndex].id,
			pickup_date: Date.convertToMysqlNoTimeZone(reservationData['pickup_date']),
			return_date: Date.convertToMysqlNoTimeZone(reservationData['return_date'])
		};
		
		$.ajax({
			type: "POST", 
			url: '/search/accessories', 
			dataType: 'json', 
			data: postData, 
			success: searchAccessoriesResult
		});
	}
	
	function searchVehiclesResult()
	{	
		var resultCount = reservationData.vehicles.length;
		if(resultCount == 0)
			return;
		
		//SET THE SECOND STEP
		History.pushState({step: 2}, resultCount+" Vehicles Found", "?step=2");
		
		var content = '';
		if(reservationData.reservation_receipt_entries.length > 0)
		{
			content += '<div class="alert alert-warning" role="alert"> <ul>';
			for(var i=0; i < reservationData.reservation_receipt_entries.length; i++)
				content += '<li>'+reservationData.reservation_receipt_entries[i].details+'</li>';
				
			content += '</ul></div>';
		}
		
		content += '<h4>We have found '+resultCount+' vehicles</h4><hr>';
		var rowTemplate = _.template($("#vehicleRowTemplate").html());
		var durationDays = (reservationData.duration==1?"1 day":reservationData.duration+" days");
		for(var i=0; i<resultCount; i++)
		{
			content += rowTemplate({vehicle: reservationData.vehicles[i], index: i, duration: durationDays});
		}
		
		$("#contentBody").html(content);
		$('html, body').animate({
			scrollTop: $("#contentBody").offset().top - 20
		}, 1000);
	}

	
	function vehicleDetails(index)
	{
		if(index==null)
			index = vehicleSelectedIndex;
			
		var vehicle = reservationData.vehicles[index];
				
		$.ajax({
			type: "GET", 
			url: '/search/vehicles/'+vehicle['id'], 
			dataType: 'json', 
			success: function(response)
			{	
				var discountContent = "";
				var discountsCount = response['discounts'].length;
				for(var i=0; i< discountsCount; i++)
				{
					discountContent += '<tr><td>' + (i > 0 ? response['discounts'][i-1].duration+1 : '1') +' - '+ response['discounts'][i].duration + ( i == discountsCount-1 ? '+' : '') +'</td>'+
							               '<td class="text-right">' + response['discounts'][i].discount +' '+ (response['discounts'][i].percentage == 1 ? '%' : 'Euro/day') +'</td></tr>';					
				}
				
				var templateData = {
					vehicle: vehicle,
					images: response['images'],
					discountContent: discountContent
				};
				
				var vehicleDetailsTemplate = _.template($("#vehicleDetailsTemplate").html());
				var vehicleDetailsContent = vehicleDetailsTemplate(templateData);
		
				bootbox.dialog({
					size: "large",
					title: '<span class="vehicle-title">'+vehicle.title+'</span> &nbsp; or similar',
					message: vehicleDetailsContent,
					buttons: { ok: {label: "OK, close", className: "btn-primary"} },
					backdrop: true
				})
				.find("div.modal-dialog").addClass("details-large-modal")
				.ready(function(){
					$('.image-gallery-thumb').click(function(e){
						var imgSource = $(this).attr("src").replace("/thumb", "");
						$(this).parent().find(".image-gallery-view").attr("src", imgSource);
					});
				});
			}
		});
		
	}
	
	
</script>

<script type="text/template" id="vehicleDetailsTemplate">

<div class="container-fluid">
	<div class="row">
		<div class="col-md-5">
			<img class="image-gallery-view" src="/images/uploads/<%- data.vehicle.icon_image_path %>">
			<% for(var i=0; i < data.images.length; ++i) { %>
				<img class="image-gallery-thumb" src="/images/uploads/thumb/<%- data.images[i].img_url %>">
			<% } %>
		</div>
		<div class="col-md-7">			
			<div class="col-md-6" style="padding-right: 40px;">
				<ul class="list-unstyled">
					<li><b><%= data.vehicle.vehicletypes_name %></b></li>
					<li><h5>Unlimited Mileage</h5></li>
					<li><small>Minimum Age 18 years</small></li>
				</ul>
				<ul class="list-unstyled features">
					<li><span class="feature-icons seats"></span> <%= data.vehicle.seats %></li>
					<li><span class="feature-icons baggage"></span> <%= data.vehicle.baggage_size %></li>
					<li><span class="feature-icons doors"></span> <%= data.vehicle.doors %></li>
					<li><span class="feature-icons engine_fuel"></span> <%= data.vehicle.fuel_type %></li>
					<li>
					<% if(data.vehicle.transmission==1){ %>
						<span class="feature-icons transmission_m"></span> Manual
					<% }else{  %>
						<span class="feature-icons transmission_a"></span> Automatic
					<% } %>						
					</li>
					<% if(data.vehicle.airconditioning==1){ %>
					<li><span class="feature-icons temperature_ac"></span> Air Conditioning</li>
					<% } %>
				</ul>
				<h4>Reservation Duration Discount</h4>
				<table class="table table-striped table-hover table-bordered">
					<thead>
						<tr>
							<th class="col-md-6">Days Range</th>
							<th class="col-md-6 text-right">Discount</th>
						</tr>
					</thead>
					<tbody>	
						<%= data.discountContent %>
					</tbody>
				</table>
			</div>
			<div class="col-md-6 price-include-exclude">
				<h4>Price includes</h4>
				<ul>
					<li>Damage and collision protection (Excess reduced to EUR 950.00)</li>
					<li>Winter equipment</li>
					<li>Airport/Railway Station Surcharge</li>
					<li>VAT/Tax</li>
				</ul>
				<h4>Price excludes</h4>
				<ul>
					<li>Super Collision Damage Waiver (SCDW) EUR 11.73 per rental</li>
					<li>Super Theft Waiver (STHW) EUR 3.70 per rental</li>
					<li>Personal accident protection (driver & passengers) EUR 12.34 per rental</li>
					<li>Windscreen, lights and tire protection EUR 4.94 per rental</li>
					<li>Additional Mileage: EUR 0.19/km</li>
					<li>Any fuel that you use in the vehicle during your rental.</li>
					<li>A deposit on your credit card will be requested at your arrival in station.</li>
				</ul>
			</div>
		</div>
	</div>
</div>

</script>
 
<script type="text/template" id="vehicleRowTemplate">

<div class="search-item"> 
	<div style="padding-bottom: 5px; margin: 10px 0; border-bottom: 1px solid #ccc; font-size: 18px;">
		<a href="javascript:vehicleDetails(<%= data.index %>)" class="vehicle-title"><%= data.vehicle.title %></a> &nbsp; or similar
		<a href="javascript:vehicleDetails(<%= data.index %>)" class="pull-right" style="padding-top: 4px;">More Details</a>
	</div>
	
	<div class="row">
		<div class="col-md-4 col-sm-6">
			<a href="javascript:vehicleDetails(<%= data.index %>)">
				<img src="/images/uploads/small/<%- data.vehicle.icon_image_path %>" class="vehicle-list-image">
			</a>
		</div>
		<div class="col-md-5 col-sm-6" style="padding-left: 30px;">
			<ul class="list-unstyled">
				<li><b><%= data.vehicle.vehicletypes_name %></b></li>
				<li><h5>Unlimited Mileage</h5></li>
			</ul>
			<ul class="col-md-4 list-unstyled features">
				<li><span class="feature-icons seats"></span> <%= data.vehicle.seats %></li>
				<li><span class="feature-icons baggage"></span> <%= data.vehicle.baggage_size %></li>
				<li><span class="feature-icons doors"></span> <%= data.vehicle.doors %></li>
			</ul>
			<ul class="col-md-8 list-unstyled features">
				<li><span class="feature-icons engine_fuel"></span> <%= data.vehicle.fuel_type %></li>
				<li>
				<% if(data.vehicle.transmission==1){ %>
					<span class="feature-icons transmission_m"></span> Manual
				<% }else{  %>
					<span class="feature-icons transmission_a"></span> Automatic
				<% } %>
				</li>
				<% if(data.vehicle.airconditioning==1){ %>
				<li><span class="feature-icons temperature_ac"></span> Air Conditioning</li>
				<% } %>
			</ul>
		</div>
		<div class="col-md-3">
			<a href="javascript:selectVehicle(<%= data.index %>)" class="pull-right btn btn-white vehicle-list-price">
				<span class="discount-txt <% if(data.vehicle.discount==0){ %> hidden <% } %>">Discount: EUR <%= Utils.priceFormat(data.vehicle.discount) %></span><br>
				<span>Price for <%= data.duration %></span><br>
				<span class="total-price-txt">EUR <%= Utils.priceFormat(data.vehicle.final_price) %></span><br><br>
				<span class="total-price-txt" style="color: #000;">Continue <span class="glyphicon glyphicon-chevron-right"></span></span>
			</a>
		</div>
	</div>
</div>

</script>