<?php echo View::make('partials.public_header') ?>

<div class="container">
<br>

<div class="row">
	<div class="col-md-offset-2 col-md-8" style="background-color: #fff; border: 1px solid #ccc;">
		
		<?php  if(isset($reason) && $reason == 'missing_paypal_params'){ ?>
		<h2>Unable to Proccess Paypal Response, Session may have expired.</h2>
		<?php  } else { ?>
		<h2>Paypal Payment Canceled</h2>
		<?php } ?>
		<hr>
		<ul style="font-size: 16px; margin: 50px;">
			<li>Your Reservation is not guaranted and it will be permanently removed soon if you don't complete the payment</li>
			<li>Your Reservation information has been temporarily saved and 
				<br>you can try again to 
				<a href="/view-reservation/<?php echo $reservation_id; ?>">make the payment by clicking here<span class="glyphicon glyphicon-chevron-right"></span></a>
			</li>
			<br>
			<li>or report it as an issue to the administrator by sending an email at 
				<a href="mailto:admin@mail.com?subject=Paypal Problem, Reservation No. <?php echo $reservation_id; ?> 
						 &body=Hello, I am having issues paying with Paypal. Reservation details: <?php echo url('view-reservation', [$reservation_id]); ?>" target="_top">admin@mail.com</a>
			</li>
			<li>To follow this issue your Reservation number is <b><?php echo $reservation_id; ?></b> <small> (which should be included in the email)<small></li>
		</ul>
	</div>
</div>

<?php echo View::make('partials.footer') ?>