
<script type="text/javascript">
	
	var accessoriesReceiptEntries = [];
	var accessories = [];
	
	function searchAccessoriesResult(response)
	{
		accessories = response['accessories'];
		var resultCount = accessories.length;
		
		var extrasContent = '<div class="col-md-8 extras-panel"><h3>Recommended Extras</h3>';
				
		var rowTemplate = _.template($("#accessoryRowTemplate").html());
		var durationDays = (reservationData.duration==1?"1 day":reservationData.duration+" days");
		var templateData;//reuse the same variable in loop
		for(var i=0; i<resultCount; i++)
		{
			accessories[i]['total_price'] = Utils.priceFormat(parseFloat(accessories[i].price) * (accessories[i].one_time_fee == 1 ? 1 : reservationData.duration));
			
			templateData = {accessory: accessories[i], index: i, duration: durationDays};
			templateData['stepper_limit'] = (accessories[i].availability < 3?'single-unit':'multi-units');			
			extrasContent += rowTemplate(templateData);
		}
		extrasContent += '</div>';//close col-md-8
		
		var sidebarTemplate = _.template($("#reservationDetailsTemplate").html());
		
		templateData = {
			vehicle: reservationData.vehicles[vehicleSelectedIndex],
			duration: durationDays,
			pickup_location: {
				name: locationsListData[selectedLocations['pickupIndex']].name,
				datetime: moment(reservationData['pickup_date']).format('dddd, MMM D, YYYY - hh:mm A')
			},
			return_location: {
				name: locationsListData[selectedLocations['returnIndex']].name,
				datetime: moment(reservationData['return_date']).format('dddd, MMM D, YYYY - hh:mm A')
			}
		};
		var sideBarContent = sidebarTemplate(templateData);
		
		$("#contentBody").html('<div class="row">'+extrasContent+sideBarContent+'</div>');
		
		//http://www.virtuosoft.eu/code/bootstrap-touchspin/
		var stepperConfig = {
			min: 0,
			max: 1,
			step: 1,
			buttondown_class: "btn btn-yellow",
			buttonup_class: "btn btn-yellow",
			buttondown_txt: '<i class="fa fa-minus"></i>',
			buttonup_txt: '<i class="fa fa-plus"></i>'
		};
		
		$(".extras-panel").ready(function()
		{
			$(".accessories-stepper.single-unit").TouchSpin( _.clone(stepperConfig) );//config cloned to remove reference
			stepperConfig['max'] = 10;//multi-units limit
			$(".accessories-stepper.multi-units").TouchSpin(stepperConfig);
			
			$(".accessories-stepper").on("change", onExtrasStepperChange);
			
			updateReceiptEntries();
		});
		
		$('html, body').animate({
			scrollTop: $("#contentBody").offset().top - 20
		}, 1000);
	}
	
	function onExtrasStepperChange()
	{
		accessoriesReceiptEntries = [];
		$(".accessories-stepper").each(function (index, value)
		{
			var quantity = $(this).val();
			var accessory = accessories[$(this).data("index")];
			if(quantity > 0)
			{
				accessoriesReceiptEntries.push({ 
					entry_name: accessory.name, 
					price: parseFloat(accessory.total_price) * quantity, 
					quantity: quantity,
					accessory_id: accessory.id
				});
			}
		});
		
		//rebuild receipt table 
		updateReceiptEntries();
	}
	
	function updateReceiptEntries()
	{
		var i = 0;
		var receiptTotalPrice = reservationData.vehicles[vehicleSelectedIndex].final_price;
		
		var tableContent = '<table class="table table-hover" style="background-color: #fff; border: 1px solid #ccc;">';
		
		var entries = reservationData.vehicles[vehicleSelectedIndex].receipt_entries;			
		for(i=0; i < entries.length; i++)
		{
			tableContent += '<tr><td>'+entries[i].entry_name+
							(entries[i].quantity != null && entries[i].quantity > 1?'<br><small>Duration: &nbsp; '+entries[i].quantity+' days</small>':'')+
							'</td><td class="text-right">EUR '+Utils.priceFormat(entries[i].price)+'</td></tr>';
			
			//tableContent += '<tr><td>'+entries[i].entry_name+'</td><td class="text-right">EUR '+Utils.priceFormat(entries[i].price)+'</td></tr>';
		}
		tableContent += '<tr><td>Mileage unlimited</td><td class="text-right" style="min-width: 100px;">included</td></tr>';
		
		entries = reservationData.reservation_receipt_entries;			
		for(i=0; i < entries.length; i++)
		{
			receiptTotalPrice += parseFloat(entries[i].price);
			tableContent += '<tr><td>'+entries[i].entry_name+'</td><td class="text-right">EUR '+Utils.priceFormat(entries[i].price)+'</td></tr>';
		}
		
		entries = accessoriesReceiptEntries;
		for(i=0; i < entries.length; i++)
		{
			receiptTotalPrice += parseFloat(entries[i].price);
			tableContent += '<tr><td>'+entries[i].entry_name+
							(entries[i].quantity > 1 ?'<br><small>Quantity: &nbsp; '+entries[i].quantity+'</small>':'')+
							'</td><td class="text-right">EUR '+Utils.priceFormat(entries[i].price)+'</td></tr>';
		}
		
		$("#receipt_entries_table").html( tableContent+'</table>' );
		$("#receipt_total_price").html( 'EUR '+Utils.priceFormat(receiptTotalPrice) );
	}
	
	
	function checkout()
	{	
		var postData = {_token: Utils.CSRF_TOKEN,
			vehicle_id: reservationData.vehicles[vehicleSelectedIndex].id,
			accessories_entries: accessoriesReceiptEntries
		};
		
		$.ajax({
			type: "POST", 
			url: '/search/checkout', 
			dataType: 'json', 
			data: postData, 
			success: function(response)
			{
				if(response.code == 440)
				{
					bootbox.dialog({
						title: "Expired Session",
						message: "<br>Your session has expired, you need to start over.<br><br>",
						buttons: { ok: {label: "OK, close", className: "btn-primary"} },
						backdrop: true,
						callback: function() {
							var currentIndex = History.getCurrentIndex();   
							// Return at the beginning
							if( currentIndex > 0 )
								History.go( -currentIndex ); 
							//pushState will clear forward history
							History.pushState({});
							window.location.replace('/');
						}
					});
				}
				else
				{
					redirectRequest('/reservations/complete');
				}
			}
		});
	}
	
</script>

<script type="text/template" id="accessoryRowTemplate">

	<div class="col-md-6" style="padding: 15px 0 5px; border: 1px solid #ccc; background-color: #fff; margin: -1px 0 0 -1px">
		<div class="col-md-4">
			<img src="/images/uploads/<%= data.accessory.img_url %>" style="max-width:120; max-height:100px;">
		</div>
		<div class="col-md-8" style="padding-right: 5px;">
			<ul class="list-unstyled">
				<li style="height: 85px; overflow: hidden; line-height: 1.2em;"><div style="font-size: 16px; font-weight: 700;"><%= data.accessory.name %></div>
					<span style="font-size: 11px; line-height: .5em;"><%= data.accessory.description %>
				</li>
				<li><span style="font-size: 18px; font-weight: 800;">EUR <%= data.accessory.total_price %> </span>&nbsp;<small>for <%= data.duration %></small></li>
				<li style="max-width: 125px;">
					<input class="accessories-stepper <%= data.stepper_limit %>" value="0" data-index="<%= data.index %>">
				</li>
			</ul>
		</div>
	</div>
	
</script>

<script type="text/template" id="reservationDetailsTemplate">

	<div class="col-md-4 reservation-details-panel">
		<div class="row">
			<div class="col-md-6">
				<h4 style="padding-top: 10px;">Your selection</h4>
			</div>
			<div class="col-md-6">
				<a href="javascript:checkout()" class="btn btn-default pull-right" style="margin: 10px 0; color: green;"><b>SKIP THIS STEP</b>
					<span class="glyphicon glyphicon-chevron-right"></span>
				</a>
			</div>
		</div>
		<div class="row" style="border: 4px solid #ffc547; margin: 10px 0; background-color: #fff;">
			<div class="col-md-5" style="padding: 7px;">
				<a href="javascript:vehicleDetails()">
					<img src="/images/uploads/small/<%- data.vehicle.icon_image_path %>" style="max-width: 100%;">
				</a>
			</div>
			<div class="col-md-7">
				<h5 style="min-height: 35px;"><a href="javascript:vehicleDetails()"><%= data.vehicle.title %></a></h5>
				<a class="btn btn-default" href="javascript:goBack()" style="margin-bottom: 7px;">
					<span class="glyphicon glyphicon-chevron-left"></span> change
				</a>
			</div>
		</div>
		<h4>Rental Locations</h4>
		<address style="font-size: 1.2em;">
			Pickup:<br>
			<strong><%= data.pickup_location.name %></strong><br>
			<%= data.pickup_location.datetime %><br><br>
			Return:<br>
			<strong><%= data.return_location.name %></strong><br>
			<%= data.return_location.datetime %>
		</address>
		
		<div id="receipt_entries_table"></div>
		
		<div class="row" style="font-size: 22px; font-weight: 900; border: 5px solid #038C00; margin: 10px 0; background-color: #fff; padding: 10px 15px 0 15px;">				
			<p style="color: #888;">Total price <span style="font-size: 16px; font-weight: 500;">for <%= data.duration %></span></p>
			<p class="text-right"><span id="receipt_total_price"></span></br>
			<span style="font-size: 14px; font-style: italic; font-weight: 400;">(tax included, excluding fuel costs)</span></p>
		</div>
		<a href="javascript:checkout()" class="btn btn-lg btn-yellow pull-right" style="margin: 10px 0;"><b>CHECK OUT</b>
			<span class="glyphicon glyphicon-chevron-right"></span>
		</a>	
	</div>
	
</script>
