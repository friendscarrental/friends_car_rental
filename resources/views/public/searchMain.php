<?php echo View::make('partials.public_header') ?>

<?php echo View::make('public.searchVehicles') ?>
<?php echo View::make('public.searchAccessories') ?>

<script src="/js/searchFormInputs.js"></script>

<script type="text/javascript">
	
	var locationsListData = jQuery.parseJSON(<?php echo var_export(json_encode($locations)); ?>);
	var weekdays = {'1':'Mon','2':'Tue','3':'Wed','4':'Thu','5':'Fri','6':'Sat','7':'Sun'};
	var selectedLocations = {pickupIndex: -1, returnIndex: -1};
	var reservationData = {vehicles: [], duration: 0 };
	var vehicleSelectedIndex = -1;

	function goBack() {
		History.back();
	}
		
	$(document).ready(function()
	{	
		$('.dropdown-menu').click(function(event){
			event.stopPropagation();
		});
		
		//RUN WHEN PAGE IS READY
		buildLocationsLists();
		
		
		$('#pickuplocationsList .list-group-item').click(function(e){
			$("#pickuplocationsList").children(".active").removeClass('active'); // PREVIOUS LIST-ITEM
			$(this).addClass('active'); // ACTIVATED LIST-ITEM
			locationDetails('pickup', $(this).data("row_index") );
		});
		$('#returnlocationsList .list-group-item').click(function(e){
			$("#returnlocationsList").children(".active").removeClass('active'); // PREVIOUS LIST-ITEM
			$(this).addClass('active'); // ACTIVATED LIST-ITEM
			locationDetails('return', $(this).data("row_index") );
		});
		
		//SET THE FIRST STEP
		History.pushState({step: 1}, "Search Vehicles", "?step=1");
		
		// Bind to StateChange Event
		History.Adapter.bind(window,'statechange',function(){
			var State = History.getState();
			//console.log('State: ', State);
			
			if(State.data.step == 2)//'vehicles'
				searchVehiclesResult();
			else if(State.data.step == 3){//'accessories'
				searchAccessories();
			}else {
				$("#contentBody").html('<h4>Search above for vehicles.</h4>');
			}
		});
		
	});
	
	function resetOnChange()
	{
		$("#contentBody").html('<h4>Click Search to see the results.</h4>');
	}
	
	function search()
	{
		if(selectedLocations['pickupIndex'] === -1)
		{
			bootbox.dialog({
				title: "No Locations Selected",
				message: "<br>Please select the pickup and return locations.<br><br>",
				buttons: { ok: {label: "OK, close", className: "btn-primary"} },
				backdrop: true
			});
		}
		else
		{
			var pickupDate = new Date($('#pickup_datepicker').data("date"));
			var returnDate = new Date($('#return_datepicker').data("date"));
			
			var postData = {_token: Utils.CSRF_TOKEN,
				pickup_location_id: locationsListData[selectedLocations['pickupIndex']].id,
				return_location_id: locationsListData[selectedLocations['returnIndex']].id,
				pickup_date: Date.convertToMysqlNoTimeZone(pickupDate),
				return_date: Date.convertToMysqlNoTimeZone(returnDate)
			};
			
			$.ajax({type: "POST", 
				url: 'search/vehicles', 
				dataType: 'json', 
				data: postData, 
				success: function(response) 
				{
					reservationData = response;
					reservationData['pickup_date'] = pickupDate;
					reservationData['return_date'] = returnDate;
					searchVehiclesResult();
				}
			});
		}
	}
	
	
</script>

<style>
	body {
		background-color: #f6f6f6;
	}
</style>

<div class="search-form-box">

<div class="container">
<div class="col-md-offset-1 col-md-10">

<div class="row">
	<h1 class="col-md-8 pull-left">Friends Car Rental</h1>
	<div class="col-md-4">
		<?php if(Auth::check()) { ?>
		<a href="/home" class="btn btn-link pull-right" style="margin: 10px 0 0; color: #fff; fonts-weight: 900;">
			<span class="glyphicon glyphicon-user"></span> Hello, <?php echo Auth::user()->firstname; ?>
		</a>
		<?php }else{ ?>
		<a href="/login" class="btn btn-primary pull-right" style="margin: 10px 0 0;"><span class="glyphicon glyphicon-user"></span> Login</a>		
		<?php } ?>
	</div>
</div>
<hr>
<div class="form-group">
	<label for="pickuplocation">Pickup</label>
	<div class="btn-group btn-block">
		<button type="button" id="pickuplocation" data-toggle="dropdown" 
			class="btn btn-default btn-lg btn-block dropdown-toggle">
			<span class="pull-left dropdown-btn-label">Select Pickup Location</span>
			<span class="caret pull-right"></span>
		</button>
		<div class="dropdown-menu location-dropdown btn-block">
			<div id="pickuplocationsList" class="list-group locations-list col-md-4"></div>
			<div class="col-md-8">
				<div id="pickuplocationDetailsPanel" class="row" style="padding-left: 15px"></div>
				<div id="pickupMap" class="row dropdown-gmap"></div>
			</div>
		</div>
	</div>
</div>
<div class="form-group">
	<label for="returnlocation">Return</label>
	<div class="btn-group btn-block">
		<button type="button" id="returnlocation" data-toggle="dropdown" 
			class="btn btn-default btn-lg btn-block dropdown-toggle">
			<span class="pull-left dropdown-btn-label">Select Return Location</span>
			<span class="caret pull-right"></span>
		</button>
		<div class="dropdown-menu location-dropdown btn-block">
			<div id="returnlocationsList" class="list-group locations-list col-md-4"></div>
			<div class="col-md-8">
				<div id="returnlocationDetailsPanel" class="row" style="padding-left: 15px"></div>
				<div id="returnMap" class="row dropdown-gmap"></div>
			</div>
		</div>
	</div>
</div>

<div class="row">		
	<div class="col-md-8">
		<div class="row">
			<label style="margin-left: 15px;">Date &amp; Time</label>
		</div>
		<div class="row form-inline">
			<div class="col-md-6"> 
				<span class="search-icons pickup-date"></span>
				<div class="form-group">
					<div class='input-group date' id='pickup_datepicker' style="min-width: 220px;">
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-calendar"></span>
						</span>
						<input type='text' class="form-control" />
					</div>
				</div>
			</div>
			<div class="col-md-6"> 
				<span class="search-icons return-date"></span>
				<div class="form-group">
					<div class='input-group date' id='return_datepicker' style="min-width: 220px;">
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-calendar"></span>
						</span>
						<input type='text' class="form-control" />
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-4"> 
		<a href="javascript:search()" class="btn btn-yellow btn-lg pull-right"
		   style="min-width: 80%; margin-top: 15px; font-size: 1.8em; font-weight: 900;">
			Search <span class="glyphicon glyphicon-chevron-right"></span>
		</a>
	</div>
</div>
	
</div>
</div>
</div>

</div>

<div class="container">
	<br>
	<div id="contentBody">
		<h4>Search above for vehicles.</h4>
	</div>

	
<?php echo View::make('partials.footer') ?>