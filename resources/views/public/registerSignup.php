<?php echo View::make('partials.public_header') ?>

<div class="container">
<br>

<div class="row">
	<form class="form-horizontal col-md-offset-2 col-md-8" action="/register-save" method="post"
		style="background-color: #fff; border: 1px solid #ccc;">
				
		<h2>Register/Create New Account</h2>
		<hr>
		
		<?php if (count($errors) > 0) { ?>
			<div class="alert alert-danger">
				<ul>
					<?php foreach ($errors->all() as $error){ ?>
						<li><?php echo $error; ?></li>
					<?php } ?>
				</ul>
			</div>
		<?php } ?>
		
		<div class="form-group required">
			<label for="firstname" class="col-md-3 control-label">Firstname</label>
			<div class="col-md-5">
				<input type="input" name="firstname" id="firstname" value="<?php echo old('firstname'); ?>" class="form-control" placeholder="Firstname">
			</div>
		</div>
		<div class="form-group required">
			<label for="lastname" class="col-md-3 control-label">Lastname</label>
			<div class="col-md-5">
				<input type="input" name="lastname" id="lastname" value="<?php echo old('lastname'); ?>" class="form-control" placeholder="Lastname">
			</div>
		</div>
		<div class="row">
			<div class="col-md-offset-1 col-md-8" style="border-bottom: 1px solid #ddd; margin-bottom: 10px; font-style: italic; color: #aaa; padding-left: 15px;" >
				Login Credentials
			</div>
		</div>
		
		<div class="form-group required">
			<label for="email" class="col-md-3 control-label">Email</label>
			<div class="col-md-6">
				<input type="email" name="email" id="email" value="<?php echo old('email'); ?>" class="form-control" placeholder="Email">
			</div>
		</div>
		
		<div class="form-group required">
			<label for="password" class="col-md-3 control-label">Password</label>
			<div class="col-md-6">
				<input type="password" name="password" id="password" class="form-control" placeholder="Password">
				<span style="color: #aaa; font-size: 11px; font-style: italic; padding-left: 10px;">
					Password must be at least 6 characters long.
				</span>
			</div>
		</div>
		<hr>
		<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">		
		<div class="form-group">
			<div class="col-md-offset-3 col-md-8">
				<button type="submit" class="btn btn-lg btn-yellow">Continue <span class="glyphicon glyphicon-chevron-right"></span></button>
			</div>
		</div>
	</form>
</div>

<?php echo View::make('partials.footer') ?>