<?php echo View::make('partials.header') ?>
<br>

<?php if (count($errors) > 0) { ?>
	<div class="alert alert-danger">
		<ul>
			<?php foreach ($errors->all() as $error){ ?>
				<li><?php echo $error; ?></li>
			<?php } ?>
		</ul>
	</div>
	<br>
<?php } ?>
		
<div class="row" <?php if($model->status==0){ ?> style="background-color: #eeaaaa;"<?php } ?> >
	<h3 class="col-md-8 pull-left">Reservation No. <strong><?php echo $model->id; ?></strong>
		<strong style="margin: 0 20px 0 50px;"><?php echo $model->vehicle_title; ?></strong> 
		<small>( <?php echo $model->duration_days.($model->duration_days>1?' Days ':' Day '); ?> )</small>
	</h3>
	<br>
	<div class="col-md-4" >		
		<?php if($model->status == 0){ ?>
			<label style="font-size: 20px; background-color: #ffe3e3; padding: 4px 10px; border-radius: 5px; float: right;"><?php echo $reservation_status; ?></label>
		<?php }else{ ?>	
			<a href="/cancel-reservation/<?php echo $model->id; ?>" class="btn btn-danger pull-right">Cancel Reservation</a>
		<?php } ?>
	</div>
</div>	

<style>
	.reservations-view {
		padding-bottom: 5px;
	}
	
	.reservations-view .form-group{
		margin-bottom: 5px;
	}
	.reservations-view .control-label{
		padding-right: 0;
	}
	
	#pickup_return_box {
		margin: 0; 
		padding: 10px 0; 
		background-color: #f3f3f3; 
		border: 1px solid #ccc; 
		border-radius: 5px; 
	}
		
	#pickup_return_box .form-control-static {
		font-size: 16px;
		margin-top: -2px;
	}
	
</style>

<?php if($model->pay_later_request == 1) { ?>
<div class="row" style="padding: 10px 25px 25px; background-color: #FFF9E6;">
	<h4>Special Request to Pay Later</h4>
	<div>
		<?php echo $model->request_description; ?>
	</div>
</div>
<?php } ?>
<hr>
<div class="row" style="margin: 0;">
	<div class="col-md-7 well form-horizontal reservations-view">
		<div id="pickup_return_box" class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="col-md-5 control-label">Pickup Date:</label>
					<div class="col-md-7">
						<p class="form-control-static"><?php echo date("j-M-y <b>H:i</b>", strtotime($model->pickup_datetime)); ?></p>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-5 control-label">Pickup From:</label>
					<div class="col-md-7">
						<p class="form-control-static"><?php echo $model->pickup_location; ?></p>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="col-md-5 control-label">Return Date:</label>
					<div class="col-md-7">
						<p class="form-control-static"><?php echo date("j-M-y <b>H:i</b>", strtotime($model->return_datetime)); ?></p>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-5 control-label">Return To:</label>
					<div class="col-md-7">
						<p class="form-control-static"><?php echo $model->return_location; ?></p>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row" style="margin-top: 20px;">
			<div class="col-md-6">
				<div class="form-group">
					<label class="col-md-5 control-label">Payment:</label>
					<div class="col-md-7">
						<p class="form-control-static"><?php echo ($model->payment_completed == 1?"Completed":"Pending"); ?></p>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-5 control-label">Reserved By:</label>
					<div class="col-md-7">
						<p class="form-control-static"><?php echo $model->user_fullname; ?></p>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-5 control-label">Primary Driver:</label>
					<div class="col-md-7">
						<p class="form-control-static"><?php echo $model->primary_driver_name; ?></p>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="col-md-6 control-label">Status:</label>
					<div class="col-md-6">
					  <p class="form-control-static"><?php echo $reservation_status; ?></p>
					</div>
				</div>				
				<div class="form-group">
					<label class="col-md-6 control-label">Created:</label>
					<div class="col-md-6">
						<p class="form-control-static"><?php echo date("j-M-y", strtotime($model->created_at)); ?></p>
					</div>
				</div>
			</div>
		</div>		
	</div>

	<div class="col-md-5" style="padding-right: 0px;">	
		<h4>Entries/Details</h4>
		<table class="table table-striped table-bordered">
			<thead>
				<tr>
					<th>Name</th>
					<th>Quantity</th>
					<th style="text-align: right;">Price</th>
				</tr>
			</thead>
			<tbody>
			<?php 
			$recordsCount = count($entries);	
			for($i=0; $i<$recordsCount; $i++) { ?>	
				<tr>
					<td><?php echo $entries[$i]->entry_name; ?></td>
					<td><?php echo $entries[$i]->quantity; ?></td>
					<td style="text-align: right;"><?php echo number_format($entries[$i]->price, 2, '.', ' '); ?></td>
				</tr>
			<?php } ?>
			</tbody>
			<tfoot>				
				<tr>
					<td colspan="3" style="font-size: 18px; text-align: right;">
						<span style="font-size: 16px; font-weight: 600; margin-right: 40px;">Total Price: </span> 
						EUR &nbsp; <span style="min-width: 70px; float: right;font-weight: 600; " ><?php echo number_format($model->total_price, 2, '.', ' '); ?></span> 
					</td>
				</tr>
				<tr>
					<td colspan="3" style="font-size: 18px; text-align: right;">
						<span style="font-size: 16px; font-weight: 600; margin-right: 40px;">Security Deposit: </span>
						EUR &nbsp; <span style="min-width: 70px; float: right;font-weight: 600; " ><?php echo number_format($model->security_deposit, 2, '.', ' '); ?></span> 
					</td>
				</tr>
			</tfoot>
		</table>
	</div>
	
</div>	

<div class="row">
<div class="row" style="background-color: #f7f7f7; border: 1px solid #ddd; margin:0 15px; padding: 10px 0;">
	
	<div class="col-md-3">
		<img src="/images/uploads/<?php echo $vehicle->icon_image_path; ?>" class="thumb lg">
	</div>
	<div class="col-md-9">
		
		<div class="row">
			<h3 style="margin: 10px 0 0 15px;"><?php echo $model->vehicle_title; ?></h3>
		</div>
		<hr>			
		<div class="row form-inline">
			<div class="form-group">
				<label style="margin-left: 15px;">Year:</label>
				<p class="form-control-static" style="margin-right: 40px;"><?php echo $vehicle->year; ?></p>
			</div>
			<div class="form-group">
				<label style="margin-left: 15px;">Type:</label>
				<p class="form-control-static" style="margin-right: 40px;"><?php echo $vehicle->vehicletypes_name; ?></p>
			</div>
			<div class="form-group">
				<label style="margin-left: 15px;">Transmission:</label>
				<p class="form-control-static" style="margin-right: 40px;"><?php echo $vehicle->transmission==1?'Manual':'Automatic'; ?></p>
			</div>
			<div class="form-group">
				<label style="margin-left: 15px;">Fuel:</label>
				<p class="form-control-static" style="margin-right: 40px;"><?php echo $vehicle->fuel_type; ?></p>
			</div>
			<?php if($vehicle->airconditioning == 1) { ?>
				<div class="form-group">
					<label style="margin-left: 15px;">Air Conditioning:</label>
					<p class="form-control-static" style="margin-right: 40px;">Included</p>
				</div>
			<?php } ?>
			<br>
			<div class="form-group">
				<label style="margin-left: 15px;">Plate No:</label>
				<p class="form-control-static" style="margin-right: 40px;"><?php echo $vehicle->plate_number; ?></p>
			</div>
			<div class="form-group">
				<label style="margin-left: 15px;">Vin No:</label>
				<p class="form-control-static" style="margin-right: 40px;"><?php echo $vehicle->vin_number; ?></p>
			</div>
		</div>
	</div>
</div>
</div>

<hr>
<div class="row">
	<h4 class="col-md-6 pull-left">Drivers</h4>
	<div class="col-md-6">
		<a href="/drivers/create/<?php echo $model->id; ?>" class="btn btn-success pull-right"><span class="glyphicon glyphicon-plus"></span> Add New Driver</a>
		<a href="/drivers/select/<?php echo $model->id; ?>" class="btn btn-default pull-right" style="margin-right: 20px;">Add Existing Driver</a>
	</div>
</div>
<table class="table table-striped table-bordered">
    <thead>
        <tr>
			<th>Driver Name</th>
			<th>License</th>
			<th>Passport/ID</th>
			<th>Born</th>
			<th>Email</th>
			<th>Phone</th>
			<th>Address</th>
			<th>&nbsp;</th>
        </tr>
    </thead>
    <tbody>
	<?php 
	$recordsCount = count($drivers);	
	for($i=0; $i<$recordsCount; $i++) { ?>	
        <tr>
            <td style="text-align: center;" >
				<a class="btn btn-link btn-block" style="padding: 3px 3px 8px 3px;" href="/drivers/<?php echo $drivers[$i]->id; ?>/edit">
					<?php echo $drivers[$i]->firstname.' '.$drivers[$i]->lastname; ?> &nbsp; <span class="glyphicon glyphicon-edit"></span>
				</a>
				<?php if($drivers[$i]->primary == 0) { ?>	
					<a class="btn btn-default btn-sm" href="/drivers/<?php echo $drivers[$i]->id.'/primary/'.$model->id; ?>">
						Set As Primary
					</a>
				<?php } else {?>
					<label>Primary Driver</label>
				<?php } ?>	
			</td>
		    <td><?php echo $drivers[$i]->license_number; ?><br><small>Exp: <?php echo $drivers[$i]->license_expiration; ?></small></td>
		    <td><?php 
					$str = "";
					if($drivers[$i]->passport != "")
						$str = 'Pass: '.$drivers[$i]->passport;
					
					if($drivers[$i]->idcard != ""){
						if($str != "")
							$str .= '<br>'; 					
						$str .= 'ID: '.$drivers[$i]->idcard;
					}
					echo $str;
			?></td>
		    <td><?php echo $drivers[$i]->birthdate; ?><br><small><?php echo $drivers[$i]->birthplace.', '.$drivers[$i]->birth_country; ?></small></td>
			<td><?php echo $drivers[$i]->email; ?></td>
		    <td><?php echo $drivers[$i]->phone_1.($drivers[$i]->phone_2!=''?'<br>'.$drivers[$i]->phone_2:''); ?></td>
			<td><?php echo $drivers[$i]->address.'<br>'.$drivers[$i]->zip_code.' '.$drivers[$i]->city.', '.$drivers[$i]->address_country; ?></td>
			<td>
				<?php if($drivers[$i]->primary == 0) { ?>	
					<button class="btn btn-warning btn-sm btn-block" style="margin-bottom: 5px; min-width: 80px;"
							onclick="Utils.confirmDeletion('drivers','<?php echo $drivers[$i]->id."/remove/".$model->id."','You are about to remove <b>".$drivers[$i]->firstname." ".$drivers[$i]->lastname; ?></b> from this reservation which you still may added it later. It will not be deleted from the system. Do you want to continue', '', 'Yes, Remove Driver');">
						<span class="glyphicon glyphicon-remove"></span> Remove
					</button>
					<button class="btn btn-danger btn-sm btn-block" 
							onclick="Utils.confirmDeletion('drivers','<?php echo $drivers[$i]->id."','Are you sure you want to PERMANENTLY delete <b>".$drivers[$i]->firstname." ".$drivers[$i]->lastname; ?></b> from the system', '');">
						Delete <span class="glyphicon glyphicon-trash"></span>
					</button>
				<?php } else {?>
					<em>Primary<br>Driver</em>
				<?php } ?>	
			</td>
        </tr>
	<?php } ?>
    </tbody>
</table>

<hr>

<div class="row">
	<h4 class="col-md-6 pull-left">Payments</h4>
</div>
<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th>Price</th>
			<th>Method</th>
			<th>Type</th>
			<th>Status</th>
			<th colspan="2">Created</th>
		</tr>
	</thead>
	<tbody>
	<?php 
	$paymentsCount = count($payments);	
	for($i=0; $i<$paymentsCount; $i++) { ?>	
		<tr>
			<td>EUR <?php echo number_format($payments[$i]->amount, 2, '.', ' '); ?></td>
			<td><?php echo $payments[$i]->paymentmethod; ?></td>
			<td><?php echo $payments[$i]->paymenttype; ?></td>
			<td><?php echo $payments[$i]->status == 1?'Verified':'Pending'; ?></td>
			<td><?php echo date("j-M-y H:i", strtotime($payments[$i]->paid_at)); ?></td>
			<td>
			<?php if($payments[$i]->status <= 0 && $payments[$i]->id_paymentmethod==1 && $payments[$i]->id_user_verified==-1){ ?>
					<a class="btn btn-yellow pull-right" href="/paynow-paypal/<?php echo $payments[$i]->id; ?>">Pay Now with Paypal</a>
				<?php } ?>		
			</td>
		</tr>
	<?php } ?>
	</tbody>
</table>

<?php echo View::make('partials.footer') ?>