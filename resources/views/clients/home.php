<?php echo View::make('partials.header') ?>
<br>
<br>
<div class="row">
	<h2 class="col-md-8 pull-left">My Reservations</h2>
	<br>
	<a href="/" class="btn btn-warning pull-right">New Reservation Search</a>
</div>
<br>

<table class="table table-striped table-hover">
    <thead>
        <tr>
			<th>Code</th>
			<th class="col-md-2">Vehicle</th>
			<th>Duration</th>
			<th>Locations</th>
			<th>Pickup Date</th>
			<th>Return Date</th>
			<th>Driver</th>
			<th>Payment</th>
			<th style="text-align: right;">Price</th>
        </tr>
    </thead>
    <tbody>
	<?php 
	$recordsCount = count($records);	
	for($i=0; $i<$recordsCount; $i++) { ?>	
        <tr>
			<th scope="row"><?php echo $records[$i]->id; ?></th>
			<th><a href="<?php echo "/view-reservation/".$records[$i]->id; ?>"><?php echo $records[$i]->vehicle_title; ?></a></td>
			<td><?php echo $records[$i]->duration_days; ?> Days</td>
			<td><div style="max-width: 130px;"> <?php echo $records[$i]->pickup_location.($records[$i]->id_location_pickup != $records[$i]->id_location_return?' - '.$records[$i]->return_location:''); ?></div></td>
			<td><?php echo date("j-M-y <b>H:i</b>", strtotime($records[$i]->pickup_datetime)); ?></td>
			<td><?php echo date("j-M-y <b>H:i</b>", strtotime($records[$i]->return_datetime)); ?></td>
			<td><?php echo $records[$i]->primary_driver_name; ?></td>
			<td><?php echo $records[$i]->payment_completed == 1?"Completed":"Pending"; ?></td>
			<td style="text-align: right;"><?php echo number_format($records[$i]->total_price, 2, '.', ' '); ?></td>
        </tr>
	<?php } ?>
    </tbody>
</table>
<?php echo $records->render(); ?>
<br>

<?php echo View::make('partials.footer') ?>