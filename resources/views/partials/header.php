<?php 
	if(Auth::user()->id_role >= 5)//clients
	{
		$page_title = 'Your Account';
		$headerPartial = 'partials.clients_header';
	}
	else //admin
	{
		$page_title = 'Admin Panel';
		$headerPartial = 'partials.admin_header';
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title><?php echo $page_title; ?></title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link href="/css/font-awesome-4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="/css/bootstrap.min.css" rel="stylesheet"type="text/css">
	<link href="/css/bootstrap-theme.min.css" rel="stylesheet"type="text/css">
	
	<link href="/css/select2.min.css" rel="stylesheet"type="text/css">
	<link href="/css/select2-bootstrap.min.css" rel="stylesheet"type="text/css">

	<link href="/css/main.css" rel="stylesheet" type="text/css">

	<script src="/js/jquery.js"></script>
	<script src="/js/bootstrap.js"></script>
	<script src="/js/underscore.min.js"></script>
	
    <script src="/js/bootbox.min.js"></script>
	
    <script src="/js/select2.min.js"></script>
	
    <script src="/js/helper.js"></script>
	
	<script>
		//GLOBAL CONSTANT
		Utils.CSRF_TOKEN = '<?php echo csrf_token(); ?>';
		
		//UNDERSCORE GLOBAL CONFIG FOR LOCAL SCOPE VARIABLE NAME
		_.templateSettings.variable = "data";
		
		$(document).ready(function(){
			//open clicked image in a new tab
			$('img.thumb').click(function() {
				var path = $(this).attr("src");
				window.open(path, '_blank');
			});
			
		});
	</script>

</head>
<body>
<div class="container">

  <!-- Static navbar -->
  <nav class="navbar navbar-default">
	<div class="container-fluid">
	  <div class="navbar-header">
		<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
		  <span class="sr-only">Toggle navigation</span>
		  <span class="icon-bar"></span>
		  <span class="icon-bar"></span>
		  <span class="icon-bar"></span>
		</button>
	  
<?php echo View::make($headerPartial); ?>