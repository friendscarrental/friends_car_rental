	  
		<a class="navbar-brand" href="/"><i class="fa fa-search"></i> Search</a>
	  </div>
	  <div id="navbar" class="navbar-collapse collapse">
		<ul class="nav navbar-nav"><?php $urlPath = Route::getCurrentRoute()->getPath(); ?>
		  <li <?php echo $urlPath=='vehicles'?'class="active"':''; ?> ><a href="/vehicles">Vehicles</a></li>
		  <li <?php echo $urlPath=='locations'?'class="active"':''; ?> ><a href="/locations">Locations</a></li>
		  <li <?php echo $urlPath=='accessories'?'class="active"':''; ?> ><a href="/accessories">Accessories</a></li>
		  <li <?php echo $urlPath=='reservations'||$urlPath=='admin'?'class="active"':''; ?> ><a href="/reservations">Reservations</a></li>
		  <li <?php echo $urlPath=='drivers'?'class="active"':''; ?> ><a href="/drivers">Drivers</a></li>
		  <li <?php echo $urlPath=='users'?'class="active"':''; ?> ><a href="/users">Users</a></li>
		  <li class="dropdown">
			<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
			other <span class="caret"></span></a>
			<ul class="dropdown-menu">
			  <li <?php echo $urlPath=='features'?'class="active"':''; ?> ><a href="/features">Features</a></li>
			  <li role="separator" class="divider"></li>
			  <li <?php echo $urlPath=='vehicletypes'?'class="active"':''; ?> ><a href="/vehicletypes">Vehicle Types</a></li>
			  <li <?php echo $urlPath=='vehiclefueltypes'?'class="active"':''; ?> ><a href="/vehiclefueltypes">Fuel Types</a></li>
			  <!-- li role="separator" class="divider"></li>
			  <li < ?php echo $urlPath=='roles'?'class="active"':''; ?> ><a href="/roles">User Roles</a></li -->
			</ul>
		  </li>
		</ul>
		<ul class="nav navbar-nav navbar-right">
			<?php if(session()->has('reservation_data')){ ?>
				<li><a href="/reservations/complete" style="margin-right: 10px; font-weight: bold;"><i style="color: #ff9900" class="fa fa-exclamation-triangle"></i> 1 Pending</a></li>
			<?php } ?>
		  <li><a href="/users/<?php echo Auth::id(); ?>/edit">Hello, <?php echo Auth::user()->firstname; ?></a></li>
		  <li><a href="/logout">Logout</a></li>
		</ul>
	  </div><!--/.nav-collapse -->
	</div><!--/.container-fluid -->
  </nav>