<!DOCTYPE html>
<html lang="en">
<head>
	<title>Friends Car Rental</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link href="/css/font-awesome-4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="/css/bootstrap.min.css" rel="stylesheet"type="text/css">
	<link href="/css/bootstrap-theme.min.css" rel="stylesheet"type="text/css">
	<link href="/css/bootstrap-datetimepicker.min.css" rel="stylesheet"type="text/css">
	
	<link href="/css/select2.min.css" rel="stylesheet"type="text/css">
	<link href="/css/select2-bootstrap.min.css" rel="stylesheet"type="text/css">

	<link href="/css/main.css" rel="stylesheet" type="text/css">

	<script src="/js/jquery.js"></script>
    <script src="/js/moment.min.js"></script>
	<script src="/js/bootstrap.js"></script>
	<script src="/js/jquery.history.js"></script>
	<script src="/js/underscore.min.js"></script>
	
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC9OmqRIYeF1EW2OO0BgHc8XnQVOHfBPgc" async defer></script>
  
	<script src="/js/jquery.bootstrap-touchspin.min.js"></script>
	<script src="/js/bootstrap-datetimepicker.min.js"></script>
    <script src="/js/bootbox.min.js"></script>
	
    <script src="/js/select2.min.js"></script>
	
    <script src="/js/helper.js"></script>
	
	<script>
		//GLOBAL CONSTANT
		Utils.CSRF_TOKEN = '<?php echo csrf_token(); ?>';
			
		//UNDERSCORE GLOBAL CONFIG FOR LOCAL SCOPE VARIABLE NAME
		_.templateSettings.variable = "data";
	</script>
	
	<style>
		body {
			background-color: #f6f6f6;
		}
	</style>
	
</head>
<body>
