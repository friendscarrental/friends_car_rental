
		<a class="navbar-brand" href="/"><i class="fa fa-search"></i> Search Vehicles</a>
	  </div>
	  <div id="navbar" class="navbar-collapse collapse">
		<ul class="nav navbar-nav"><?php $urlPath = Route::getCurrentRoute()->getPath(); ?>
		  <li <?php echo $urlPath=='my-account'||$urlPath=='admin'?'class="active"':''; ?> ><a href="/my-account">Account Reservations</a></li>
		  <li <?php echo $urlPath=='drivers'?'class="active"':''; ?> ><a href="/drivers">Drivers</a></li>
		  <li <?php echo $urlPath=='edit-account'?'class="active"':''; ?> ><a href="/edit-account">Personal Information</a></li>
		</ul>
		<ul class="nav navbar-nav navbar-right">
			<?php if(session()->has('reservation_data')){ ?>
				<li><a href="/reservations/complete" style="margin-right: 20px; font-weight: bold;"><i style="color: #ff9900" class="fa fa-exclamation-triangle"></i> 1 Pending Reservation</a></li>
			<?php } ?>
			<li><a href="/edit-account">Hello, <?php echo Auth::user()->firstname; ?></a></li>
			<li><a href="/logout">Logout</a></li>
		</ul>
	  </div><!--/.nav-collapse -->
	</div><!--/.container-fluid -->
  </nav>