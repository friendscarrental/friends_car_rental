﻿<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<style>
	@page { margin: 10px;}
	body { margin: 10px; padding: 0px;}

	.page-break {
		page-break-after: always;
	}
	.no-page-break { 
		page-break-after:avoid;
	}

	* {
		word-wrap: break-word;
	}
	
	table {
		table-layout:fixed;
		width:100%;
	    border-collapse: collapse;
	}
	
	td {		
	    border: thin solid black;
	}
	
	tr.no-border > td,
	td.no-border	{
		border: none;
	}
		
	.data-txt {
		color: #DD0000;
		font-size: 12px;
	}
	
	td.data-txt {
		text-align: center;
		padding: 3px;
	}
	
	td.data-txt.numberic {
		text-align: right;
		padding: 3px 7px 3px 0;
	}
	
	span.data-txt {
		font-weight: bold;
	}
	
	.footer td.data-txt {
		font-weight: bold;
	}
	
	.col-title {
		width: 15%;
	}
	.col-data-lg {
		width: 20%;
	}
	.col-data-md {
		width: 15%;
	}
	.col-data-sm {
		width: 10%;
	}	
	.col-spacer {
		width: 2%;
	}
	
	.col-eur {
		width: 4%;
	}
	.row {
		width: 100%;
	}
	.row:before,
	.row:after {
		  display: table;
		  content: " ";
	}
	.row:after {
		clear: both;
	}
	
	.col {
		position: relative;
		min-height: 1px;
		padding: 0;
		float: left;
		display: inline-block;
		word-wrap: break-word;
	}
		
	.address-panel {		
		width: 180px;
		font-size: 11px;
		line-height: 1.3;
	}
	
	.address-panel .lbl {
		float: left;
		display: inline-block;
		width: 40px;
		font-weight: 600;
		font-size: 0.9em;
		line-height: 1.8;
	}
		
	hr.thin-line {
		border-top: none; 
		border-bottom: thin solid black;
	}
	
	td.eur-lbl {
		font-size: 10px;
		text-align: center;
	}
	
</style>
<div style="font-size: 11px">

<!--
	
-->
<div class="row">
	<img src="<?php echo config_path().'/pdf/friends-logo.png'; ?>" style="margin-right: 100px;"/>
	<div class="col address-panel">
		<span class="lbl">&nbsp;</span><b>ZYRA TIRANE</b><br>
		<span class="lbl">Tel:</span>+355 4 481 9333<br>
		<span class="lbl">Mob:</span>+355 69 99 11 010<br>
		<span class="lbl">Email:</span><a href="mailto:explore@friendstravel.al">explore@yahoo.com</a><br>
		<span class="lbl">Web:</span><a href="http://www.friendstravel.al/">www.friendstravel.al</a><br>
	</div>	
	<div class="col address-panel">
		<span class="lbl">&nbsp;</span><b>ZYRA FIER</b><br>
		<span class="lbl">Tel:</span>+355 34 22 1666<br>
		<span class="lbl">Mob:</span>+355 69 25 52 723<br>
		<span class="lbl">Email:</span><a href="mailto:friends_travel@friendstravel.al">friends_travel@yahoo.com</a><br>
		<span class="lbl">Web:</span><a href="http://www.friendstravel.al/">www.friendstravel.al</a><br>
	</div>	
</div>

<table class="no-page-break">
<thead>
	<tr>
		<th class="col-title">&nbsp;</th>
		<th class="col-data-lg">&nbsp;</th>
		<th class="col-spacer">&nbsp;</th>
		<th class="col-title">&nbsp;</th>
		<th class="col-data-md">&nbsp;</th>
		<th class="col-spacer">&nbsp;</th>
		<th class="col-title">&nbsp;</th>
		<th class="col-eur">&nbsp;</th>
		<th class="col-data-sm">&nbsp;</th>
		<th class="col-data-sm">&nbsp;</th>
	</tr>
</thead>
<tbody>
	<tr class="no-border">
		<td colspan="10"><hr/></td>
	</tr>
	<tr class="no-border">
		<td colspan="6"><strong>FRIENDS TRAVEL AGENCY Sh.p.k.</strong></td>
		<td colspan="4"><strong>KONTRATE QIRAJE</strong></td>
	</tr>
	<tr class="no-border">
		<td colspan="10"><strong>NIPT: L32522402A </strong></td>
	</tr>
	<tr class="no-border">
		<td colspan="6"><strong>Lagja "15 Tetori", Rruga "Kastriot Muco", Fier, Shqiperi </strong></td>
		<td colspan="2"><strong>NR: &nbsp; <span class="data-txt">17</span></strong></td>
		<td colspan="2"><strong>Data: &nbsp; </strong><span class="data-txt">16/12/2015</span></td>
	</tr>
	<tr class="no-border">
		<td colspan="10">
			<hr class="thin-line" />
		</td>
	</tr>
	<tr class="no-border">
		<td colspan="2"><strong><?php echo $lbl['RENTER']; ?></strong></td>
		<td class="no-border">&nbsp;</td>
		<td colspan="2"><strong>KOHEZGJATJA E QIRASE</strong></td>
		<td class="no-border">&nbsp;</td>
		<td colspan="4"><strong>PAGESA DHE DEPOZITA</strong></td>
	</tr>
	<tr>
		<td>EMRI</td>
		<td class="data-txt">ALEKSANDER</td>
		<td class="no-border">&nbsp;</td>
		<td>KOHEZGJATJA</td>
		<td class="data-txt">6 Dite</td>
		<td class="no-border">&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>MARRJE</td>
		<td>KTHIM</td>
	</tr>
	<tr>
		<td>MBIEMRI</td>
		<td class="data-txt">BOGDANI</td>
		<td class="no-border">&nbsp;</td>
		<td>DATA E KTHIMIT</td>
		<td class="data-txt">22-Dec-15</td>
		<td class="no-border">&nbsp;</td>
		<td>&Ccedil;MIMI I QIRASE</td>
		<td class="eur-lbl">EUR</td>
		<td class="data-txt numberic">180.00</td>
		<td class="data-txt">&nbsp;</td>
	</tr>
	<tr>
		<td>PASAPORTA/ID</td>
		<td class="data-txt">BI9759951</td>
		<td class="no-border">&nbsp;</td>
		<td>ORA E KTHIMI</td>
		<td class="data-txt">15:00</td>
		<td class="no-border">&nbsp;</td>
		<td>TRANSFERIM SHERBIMI</td>
		<td class="eur-lbl">EUR</td>
		<td class="data-txt numberic">&nbsp;</td>
		<td class="data-txt">&nbsp;</td>
	</tr>
	<tr>
		<td>DATELINDJA</td>
		<td class="data-txt">9-Apr-79</td>
		<td class="no-border">&nbsp;</td>
		<td>VENDI I KTHIMIT</td>
		<td class="data-txt">Zyre Fier</td>
		<td class="no-border">&nbsp;</td>
		<td>TVSH (20%)</td>
		<td class="eur-lbl">EUR</td>
		<td class="data-txt numberic"> 30.00</td>
		<td class="data-txt">&nbsp;</td>
	</tr>
	<tr>
		<td>VENDLINDJA</td>
		<td class="data-txt">FIER</td>
		<td class="no-border">&nbsp;</td>
		<td colspan="2" class="no-border">&nbsp;</td>
		<td class="no-border">&nbsp;</td>
		<td>KARBURANT</td>
		<td class="eur-lbl">EUR</td>
		<td class="data-txt">PLOT</td>
		<td class="data-txt">PLOT</td>
	</tr>
	<tr>
		<td>ADRESA E PERHERSHME</td>
		<td class="data-txt">Kryengritja e Fierit</td>
		<td class="no-border">&nbsp;</td>
		<td colspan="2" class="no-border"><strong>DREJTUES AUTOMJETI SHTESE</strong></td>
		<td class="no-border">&nbsp;</td>
		<td>DEMTIME</td>
		<td class="eur-lbl">EUR</td>
		<td class="data-txt">&nbsp;</td>
		<td class="data-txt">&nbsp;</td>
	</tr>
	<tr>
		<td>ADRESA E PERKOHSHME</td>
		<td class="data-txt">Kryengritja e Fierit</td>
		<td class="no-border">&nbsp;</td>
		<td>EMRI</td>
		<td class="data-txt">AFRON</td>
		<td class="no-border">&nbsp;</td>
		<td>VONESA, SHTYRJE AFATI</td>
		<td class="eur-lbl">EUR</td>
		<td class="data-txt">&nbsp;</td>
		<td class="data-txt">&nbsp;</td>
	</tr>
	<tr>
		<td>TEL/MOB</td>
		<td class="data-txt">0692507262</td>
		<td class="no-border">&nbsp;</td>
		<td>MBIEMRI</td>
		<td class="data-txt">VRUSHA</td>
		<td class="no-border">&nbsp;</td>
		<td>LARJE, PASTRIM</td>
		<td class="eur-lbl">EUR</td>
		<td class="data-txt">&nbsp;</td>
		<td class="data-txt">&nbsp;</td>
	</tr>
	<tr>
		<td>EMAIL</td>
		<td class="data-txt">bogdanifier@hotmail.com</td>
		<td class="no-border">&nbsp;</td>
		<td>LEJE DREJTIMI</td>
		<td class="data-txt">032538134</td>
		<td class="no-border">&nbsp;</td>
		<td>GJOBA</td>
		<td class="eur-lbl">EUR</td>
		<td class="data-txt">Nuk Ka</td>
		<td class="data-txt">&nbsp;</td>
	</tr>
	<tr>
		<td>LEJE DREJTIMI</td>
		<td class="data-txt">06262180 43</td>
		<td class="no-border">&nbsp;</td>
		<td>VLEFSHMERIA</td>
		<td class="data-txt">45420</td>
		<td class="no-border">&nbsp;</td>
		<td>GARANCI</td>
		<td class="eur-lbl">EUR</td>
		<td class="data-txt numberic"> 100.00</td>
		<td class="data-txt">&nbsp;</td>
	</tr>
	<tr>
		<td>VLEFSHMERIA</td>
		<td class="data-txt">7-Jun-20</td>
		<td class="no-border">&nbsp;</td>
		<td>TEL/MOB</td>
		<td class="data-txt">0692507262</td>
		<td class="no-border">&nbsp;</td>
		<td><strong>GJITHSEJ</strong></td>
		<td class="eur-lbl"><strong>EUR</strong></td>
		<td><strong>GJITHSEJ</strong></td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td colspan="6" class="no-border">&nbsp;</td>
		<td>&nbsp;</td>
		<td class="eur-lbl"><strong>ALL</strong></td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr class="no-border">
		<td colspan="10">
			<strong>DEKLARIM I QIRAMARRËSIT</strong>
		</td>
	</tr>
	<tr>
		<td colspan="10" style="padding: 5px;">
			Pasi lexova me kujdes Percaktimet dhe Kushtet e Pergjithshme te kesaj Kontrate Qiraje, deklaroj se bie plotesisht dakord me to dhe duke pranuar te marr me qira automjetin e kontraktuar, marr persiper ne pergjegjesine time, ta perdor me kujdes dhe te respektoj Percaktimet dhe Kushtet e Pergjithshme te kesaj Kontrate
		</td>
	</tr>	
	<tr class="no-border">
		<td colspan="10" style="padding-top: 10px;">
			<strong>AUTORIZIM</strong>
		</td>
	</tr>
	<tr>
		<td colspan="10" style="padding: 5px; font-size: 12px;">
			Friends Travel Agency Sh.p.k. autorizon 
			<span class="data-txt">Z. ALEKSANDER BOGDANI / Z.AFRON VRUSHA </span>ta perdore automjetin me te dhenat e meposhtme, nga data 
			<span class="data-txt">16/12/2015</span> deri ne daten 
			<span class="data-txt">22/12/2015</span> . Ky autorizim eshte i vlefshem vetem brenda territorit te Republikes se Shqiperise.
		</td>
	</tr>
	<tr class="no-border">
		<td colspan="10" style="text-align: center; padding: 25px 0 10px;">
			<strong>PROCES VERBAL MARRJE NE DOREZIM AUTOMJETI</strong>
		</td>
	</tr>
  </tbody>
</table>

<div class="row no-page-break footer">
	<div class="col no-page-break" style="width: 55%;">
		<table class="no-page-break">	
			<tr>
				<td colspan="4"><center><strong>TE DHENAT E AUTOMJETIT</strong></center></td>
			</tr>
			<tr>
				<td>MARKA</td>
				<td colspan="3" class="data-txt">CITROEN C4</td>
			</tr>
			<tr>
				<td>TARGA</td>
				<td class="data-txt">AA 409 HT</td>
				<td>KARBURANTI</td>
				<td class="data-txt">NAFTE</td>
			</tr>
			<tr>
				<td>SHASIA</td>
				<td colspan="3" class="data-txt">VF7LC9HZH74821726</td>
			</tr>
			<tr class="no-border">		
				<td colspan="4" style="padding: 5px;">&nbsp;</td>
			</tr>
			<tr>		
				<td colspan="4"><center><strong>MARRJA E AUTOMJETIT</strong></center></td>
			</tr>
			<tr>
				<td>VENDI MARRJES</td>
				<td class="data-txt">Zyra Fier</td>
				<td>VENDI KTHIMIT</td>
				<td class="data-txt">Zyra Fier</td>
			</tr>
			<tr>
				<td>DATA MARRJES</td>
				<td class="data-txt">16-Dec-15</td>
				<td>DATA E KTHIMIT</td>
				<td class="data-txt">22-Dec-15</td>
			</tr>
			<tr>
				<td>ORA MARRJES</td>
				<td class="data-txt">14:20</td>
				<td>ORA KTHIMIT</td>
				<td class="data-txt">15:00</td>
			</tr>
			<tr>
				<td>SHENIME</td>
				<td colspan="3">&nbsp;<br></td>
			</tr>
			<tr>
				<td colspan="2">
					<center>
					<strong>Qiradhenesi</strong><br><br><br>
					<hr class="thin-line" style="width: 90%;"/>
					</center>
				</td>
				<td colspan="2">
					<center>
					<strong>Qiramarresi</strong><br><br><br>
					<hr class="thin-line" style="width: 90%;"/>
					</center>
				</td>
			</tr>
			<tr class="no-border">		
				<td colspan="4" style="padding: 5px;">&nbsp;</td>
			</tr>
			<tr>	
				<td colspan="4"><center><strong>KTHIMI I AUTOMJETIT</strong></center></td>
			</tr>
			<tr>
				<td><strong>DATA KTHIMIT</strong></td>
				<td style="padding: 5px;">&nbsp;</td>
				<td><strong>ORA KTHIMIT</strong></td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td><strong>SHENIME</strong></td>
				<td colspan="3">&nbsp;<br><br><br></td>
			</tr>
			<tr>
				<td colspan="2">
					<center>
					<strong>Qiradhenesi</strong><br><br><br>
					<hr class="thin-line" style="width: 90%;"/>
					</center>
				</td>
				<td colspan="2">
					<center>
					<strong>Qiramarresi</strong><br><br><br>
					<hr class="thin-line" style="width: 90%;"/>
					</center>
				</td>
			</tr>
		</table>
	</div>
	<div class="col no-page-break" style="width: 44%; margin-left: 2px;">				
		<table class="no-page-break">
			<tr>
				<td>Marrje (<span style="color: blue;">me ngjyre blu</span>)</td>
				<td>Kthim (<span style="color: #ee0000;">me te kuqe</span>)</td>
				<td><strong>PAJISJE DHE KARBURANTI</strong></td>
			</tr>
			<tr>				
				<td colspan="2" style="padding: 3px;">Shenime</td>
				<td>Dokumentacioni</td>
			</tr>
			<tr>
				<td colspan="2" style="padding: 3px;">1.</td>
				<td>Boria</td>
			</tr>
			<tr>
				<td colspan="2" style="padding: 3px;">2.</td>
				<td>Celesi me pult</td>
			</tr>
			<tr>
				<td colspan="2" style="padding: 3px;">3.</td>
				<td>Goma rezerve</td>
			</tr>
			<tr>
				<td colspan="2" rowspan="13" style="padding: 0">
					<center><img src="<?php echo config_path().'/pdf/vehicle_check.png'; ?>" style="max-width: 204px; margin: 0px 0; padding: 0;"/></center>
				</td>
				<td>Kriku</td>
			</tr>
			<tr>
				<td>Celesi i gomes</td>
			</tr>
			<tr>
				<td>Trekendeshi</td>
			</tr>
			<tr>
				<td>Kutia e ndihmes se shpejte</td>
			</tr>
			<tr>
				<td>Tapetet</td>
			</tr>
			<tr>
				<td>Tasat</td>
			</tr>
			<tr>
				<td>Ganxha terheqese</td>
			</tr>
			<tr>
				<td>Literatura e bordit</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
		</table>

	</div>
</div>

</div>