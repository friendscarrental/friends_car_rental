﻿<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<style>
	@page { margin: 10px;}
	body { margin: 10px; padding: 0px;}

	.page-break {
		page-break-after: always;
	}
	.no-page-break { 
		page-break-after:avoid;
	}

	* {
		word-wrap: break-word;
	}
	
	table {
		table-layout:fixed;
		width:100%;
	    border-collapse: collapse;
	}
	
	td {		
	    border: thin solid black;
	}
	
	tr.no-border > td,
	td.no-border	{
		border: none;
	}
		
	.data-txt {
		color: #DD0000;
		font-size: 12px;
	}
	
	td.data-txt {
		text-align: center;
		padding: 3px;
	}
	
	td.data-txt.numberic {
		text-align: right;
		padding: 3px 7px 3px 0;
	}
	
	span.data-txt {
		font-weight: bold;
	}
	
	.footer td.data-txt {
		font-weight: bold;
	}
	
	.col-title {
		width: 15%;
	}
	.col-data-lg {
		width: 20%;
	}
	.col-data-md {
		width: 15%;
	}
	.col-data-sm {
		width: 10%;
	}	
	.col-spacer {
		width: 2%;
	}
	
	.col-eur {
		width: 4%;
	}
	.row {
		width: 100%;
	}
	.row:before,
	.row:after {
		  display: table;
		  content: " ";
	}
	.row:after {
		clear: both;
	}
	
	.col {
		position: relative;
		min-height: 1px;
		padding: 0;
		float: left;
		display: inline-block;
		word-wrap: break-word;
	}
		
	.address-panel {		
		width: 180px;
		font-size: 11px;
		line-height: 1.3;
	}
	
	.address-panel .lbl {
		float: left;
		display: inline-block;
		width: 40px;
		font-weight: 600;
		font-size: 0.9em;
		line-height: 1.8;
	}
		
	hr.thin-line {
		border-top: none; 
		border-bottom: thin solid black;
	}
	
	td.eur-lbl {
		font-size: 10px;
		text-align: center;
	}
	
</style>
<div style="font-size: 11px">

<!--
	
-->
<div class="row">
	<img src="<?php echo config_path().'/pdf/friends-logo.png'; ?>" style="margin-right: 100px;"/>
	<div class="col address-panel">
		<span class="lbl">&nbsp;</span><b><?php echo $lbl['TIRANA_OFFICE']; ?></b><br>
		<span class="lbl"><?php echo $lbl['PHONE']; ?>:	</span>+355 4 481 9333<br>
		<span class="lbl"><?php echo $lbl['MOB']; ?>:	</span>+355 69 99 11 010<br>
		<span class="lbl"><?php echo $lbl['EMAIL']; ?>:	</span><a href="mailto:explore@friendstravel.al">explore@yahoo.com</a><br>
		<span class="lbl"><?php echo $lbl['WEB']; ?>:	</span><a href="http://www.friendstravel.al/">www.friendstravel.al</a><br>
	</div>	
	<div class="col address-panel">
		<span class="lbl">&nbsp;</span><b><?php echo $lbl['FIER_OFFICE']; ?></b><br>
		<span class="lbl"><?php echo $lbl['PHONE']; ?>:	</span>+355 34 22 1666<br>
		<span class="lbl"><?php echo $lbl['MOB']; ?>:	</span>+355 69 25 52 723<br>
		<span class="lbl"><?php echo $lbl['EMAIL']; ?>:	</span><a href="mailto:friends_travel@friendstravel.al">friends_travel@yahoo.com</a><br>
		<span class="lbl"><?php echo $lbl['WEB']; ?>:	</span><a href="http://www.friendstravel.al/">www.friendstravel.al</a><br>
	</div>	
</div>

<table class="no-page-break">
<thead>
	<tr>
		<th class="col-title">&nbsp;</th>
		<th class="col-data-lg">&nbsp;</th>
		<th class="col-spacer">&nbsp;</th>
		<th class="col-title">&nbsp;</th>
		<th class="col-data-md">&nbsp;</th>
		<th class="col-spacer">&nbsp;</th>
		<th class="col-title">&nbsp;</th>
		<th class="col-eur">&nbsp;</th>
		<th class="col-data-sm">&nbsp;</th>
		<th class="col-data-sm">&nbsp;</th>
	</tr>
</thead>
<tbody>
	<tr class="no-border">
		<td colspan="10"><hr/></td>
	</tr>
	<tr class="no-border">
		<td colspan="6"><strong><?php echo $lbl['COMPANY_NAME']; ?></strong></td>
		<td colspan="4"><strong><?php echo $lbl['RENTAL_AGREEMENT']; ?></strong></td>
	</tr>
	<tr class="no-border">
		<td colspan="10"><strong><?php echo $lbl['COMPANY_REG_NUMBER']; ?> </strong></td>
	</tr>
	<tr class="no-border">
		<td colspan="6"><strong><?php echo $lbl['COMPANY_ADDRESS']; ?> </strong></td>
		<td colspan="2"><strong><?php echo $lbl['RESERVATION_NO']; ?>: &nbsp; <span class="data-txt"><?php echo $model->id; ?></span></strong></td>
		<td colspan="2"><strong><?php echo $lbl['DOC_DATE']; ?>: &nbsp; </strong><span class="data-txt"><?php echo date('d/m/Y'); ?></span></td>
	</tr>
	<tr class="no-border">
		<td colspan="10">
			<hr class="thin-line" />
		</td>
	</tr>
	<tr class="no-border">
		<td colspan="2"><strong><?php echo $lbl['RENTER']; ?></strong></td>
		<td class="no-border">&nbsp;</td>
		<td colspan="2"><strong><?php echo $lbl['LEASE_DURATION']; ?></strong></td>
		<td class="no-border">&nbsp;</td>
		<td colspan="4"><strong><?php echo $lbl['PAYMENT_DEPOSIT']; ?></strong></td>
	</tr>
	<tr>
		<td><?php echo $lbl['NAME']; ?></td>
		<td class="data-txt"><?php echo $primary->firstname; ?></td>
		<td class="no-border">&nbsp;</td>
		<td><?php echo $lbl['DURATION']; ?></td>
		<td class="data-txt"><?php echo $model->duration_days; ?> <?php echo $lbl['DAYS']; ?></td>
		<td class="no-border">&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td><?php echo $lbl['CHECK_OUT']; ?></td>
		<td><?php echo $lbl['CHECK_IN']; ?></td>
	</tr>
	<tr>
		<td><?php echo $lbl['SURNAME']; ?></td>
		<td class="data-txt"><?php echo $primary->lastname; ?></td>
		<td class="no-border">&nbsp;</td>
		<td><?php echo $lbl['RETURN_DATE']; ?></td>
		<td class="data-txt"><?php echo $model->return_date; ?></td>
		<td class="no-border">&nbsp;</td>
		<td><?php echo $lbl['RENTAL_PRICE']; ?></td>
		<td class="eur-lbl">EUR</td>
		<td class="data-txt numberic"><?php echo number_format($model->total_price, 2, '.', ' '); ?></td>
		<td class="data-txt">&nbsp;</td>
	</tr>
	<tr>
		<td><?php echo $lbl['PASSPORT_ID']; ?></td>
		<td class="data-txt"><?php 
			$str = "";
			if($primary->passport != "")
				$str = $primary->passport;
			
			if($primary->idcard != ""){
				if($str != "")
					$str .= '/'; 					
				$str .= $primary->idcard;
			}
			echo $str;
		?></td>
		<td class="no-border">&nbsp;</td>
		<td><?php echo $lbl['RETURN_TIME']; ?></td>
		<td class="data-txt"><?php echo $model->return_time; ?></td>
		<td class="no-border">&nbsp;</td>
		<td><?php echo $lbl['TRANSFER_SERVICE']; ?></td>
		<td class="eur-lbl">EUR</td>
		<td class="data-txt numberic">&nbsp;</td>
		<td class="data-txt">&nbsp;</td>
	</tr>
	<tr>
		<td><?php echo $lbl['BIRTHDAY']; ?></td>
		<td class="data-txt"><?php echo $primary->birthdate; ?></td>
		<td class="no-border">&nbsp;</td>
		<td><?php echo $lbl['RETURN_PLACE']; ?></td>
		<td class="data-txt"><?php echo $model->return_location; ?></td>
		<td class="no-border">&nbsp;</td>
		<td><?php echo $lbl['VAT']; ?></td>
		<td class="eur-lbl">EUR</td>
		<td class="data-txt numberic"><?php echo number_format($model->total_price/6, 2, '.', ' '); ?></td>
		<td class="data-txt">&nbsp;</td>
	</tr>
	<tr>
		<td><?php echo $lbl['BIRTH_PLACE']; ?></td>
		<td class="data-txt"><?php echo $primary->birthplace.', '.$primary->birth_country; ?></td>
		<td class="no-border">&nbsp;</td>
		<td colspan="2" class="no-border">&nbsp;</td>
		<td class="no-border">&nbsp;</td>
		<td><?php echo $lbl['FUEL']; ?></td>
		<td class="eur-lbl">EUR</td>
		<td class="data-txt"><?php echo $lbl['FUEL_FULL']; ?></td>
		<td class="data-txt"><?php echo $lbl['FUEL_FULL']; ?></td>
	</tr>
	<tr>
		<td><?php echo $lbl['ADDRESS_LINE1']; ?></td>
		<td class="data-txt"><?php echo $primary->address; ?></td>
		<td class="no-border">&nbsp;</td>
		<td colspan="2" class="no-border"><strong><?php echo $lbl['ADDITIONAL_DRIVER']; ?></strong></td>
		<td class="no-border">&nbsp;</td>
		<td><?php echo $lbl['DAMAGES']; ?></td>
		<td class="eur-lbl">EUR</td>
		<td class="data-txt">&nbsp;</td>
		<td class="data-txt">&nbsp;</td>
	</tr>
	<tr>
		<td><?php echo $lbl['ADDRESS_LINE2']; ?></td>
		<td class="data-txt"><?php echo $primary->zip_code.' '.$primary->city.', '.$primary->address_country; ?></td>
		<td class="no-border">&nbsp;</td>
		<td><?php echo $lbl['NAME']; ?></td>
		<td class="data-txt"><?php echo !isset($secondary)?'':$secondary->firstname; ?></td>
		<td class="no-border">&nbsp;</td>
		<td><?php echo $lbl['DELAY_EXTENSION']; ?></td>
		<td class="eur-lbl">EUR</td>
		<td class="data-txt">&nbsp;</td>
		<td class="data-txt">&nbsp;</td>
	</tr>
	<tr>
		<td><?php echo $lbl['TEL_MOB']; ?></td>
		<td class="data-txt"><?php 
			$str = "";
			if($primary->phone_1 != "")
				$str = $primary->phone_1;
			
			if($primary->phone_2 != ""){
				if($str != "")
					$str .= '/'; 					
				$str .= $primary->phone_2;
			}
			echo $str;
		?></td>
		<td class="no-border">&nbsp;</td>
		<td><?php echo $lbl['SURNAME']; ?></td>
		<td class="data-txt"><?php echo !isset($secondary)?'':$secondary->lastname; ?></td>
		<td class="no-border">&nbsp;</td>
		<td><?php echo $lbl['CLEANING_WASH']; ?></td>
		<td class="eur-lbl">EUR</td>
		<td class="data-txt">&nbsp;</td>
		<td class="data-txt">&nbsp;</td>
	</tr>
	<tr>
		<td><?php echo $lbl['EMAIL']; ?></td>
		<td class="data-txt"><?php echo $primary->email; ?></td>
		<td class="no-border">&nbsp;</td>
		<td><?php echo $lbl['DRIVING_LICENSE']; ?></td>
		<td class="data-txt"><?php echo !isset($secondary)?'':$secondary->license_number; ?></td>
		<td class="no-border">&nbsp;</td>
		<td><?php echo $lbl['FINES']; ?></td>
		<td class="eur-lbl">EUR</td>
		<td class="data-txt"><?php echo $lbl['N_A']; ?></td>
		<td class="data-txt">&nbsp;</td>
	</tr>
	<tr>
		<td><?php echo $lbl['DRIVING_LICENSE']; ?></td>
		<td class="data-txt"><?php echo $primary->license_number; ?></td>
		<td class="no-border">&nbsp;</td>
		<td><?php echo $lbl['LICENSE_VALIDITY']; ?></td>
		<td class="data-txt"><?php echo !isset($secondary)?'':$secondary->license_expiration; ?></td>
		<td class="no-border">&nbsp;</td>
		<td><?php echo $lbl['SECURITY_DEPOSIT']; ?></td>
		<td class="eur-lbl">EUR</td>
		<td class="data-txt numberic"><?php echo number_format($model->security_deposit, 2, '.', ' '); ?></td>
		<td class="data-txt">&nbsp;</td>
	</tr>
	<tr>
		<td><?php echo $lbl['LICENSE_VALIDITY']; ?></td>
		<td class="data-txt"><?php echo $primary->license_expiration; ?></td>
		<td class="no-border">&nbsp;</td>
		<td><?php echo $lbl['TEL_MOB']; ?></td>
		<td class="data-txt"><?php 
			if(isset($secondary)){
				$str = "";
				if($secondary->phone_1 != "")
					$str = $secondary->phone_1;
				
				if($secondary->phone_2 != ""){
					if($str != "")
						$str .= '/'; 					
					$str .= $secondary->phone_2;
				}
				echo $str;
			}
		?></td>
		<td class="no-border">&nbsp;</td>
		<td><strong><?php echo $lbl['TOTAL']; ?></strong></td>
		<td class="eur-lbl"><strong>EUR</strong></td>
		<td><strong><?php echo $lbl['TOTAL']; ?></strong></td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td colspan="6" class="no-border">&nbsp;</td>
		<td>&nbsp;</td>
		<td class="eur-lbl"><strong>ALL</strong></td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr class="no-border">
		<td colspan="10">
			<strong><?php echo $lbl['THE_RENTER_DECLARES']; ?></strong>
		</td>
	</tr>
	<tr>
		<td colspan="10" style="padding: 5px;">
			<?php echo $lbl['RENTERS_DECLARATION']; ?>
		</td>
	</tr>	
	<tr class="no-border">
		<td colspan="10" style="padding-top: 10px;">
			<strong><?php echo $lbl['AUTHORISATION_TITLE']; ?></strong>
		</td>
	</tr>
	<tr>
		<td colspan="10" style="padding: 5px; font-size: 12px;">
			Friends Travel Agency Sh.p.k. <?php echo $lbl['AUTHORISATION_PART1']; ?> 
			<span class="data-txt"><?php 
				$str = $primary->firstname.' '.$primary->lastname;				
				if(isset($secondary))
					$str .= '/'.$secondary->firstname.' '.$secondary->lastname;
				echo $str;
			?></span> <?php echo $lbl['AUTHORISATION_PART2']; ?>
			<span class="data-txt"><?php echo $model->pickup_date; ?></span> <?php echo $lbl['AUTHORISATION_PART3']; ?>
			<span class="data-txt"><?php echo $model->return_date; ?></span> . <?php echo $lbl['AUTHORISATION_PART4']; ?>
		</td>
	</tr>
	<tr class="no-border">
		<td colspan="10" style="text-align: center; padding: 25px 0 10px;">
			<strong><?php echo $lbl['VEHICLE_REPORT']; ?></strong>
		</td>
	</tr>
  </tbody>
</table>

<div class="row no-page-break footer">
	<div class="col no-page-break" style="width: 55%;">
		<table class="no-page-break">	
			<tr>
				<td colspan="4"><center><strong><?php echo $lbl['VEHICLE_FEATURES']; ?></strong></center></td>
			</tr>
			<tr>
				<td><?php echo $lbl['MAKE']; ?></td>
				<td colspan="3" class="data-txt"><?php echo $model->vehicle_title; ?></td>
			</tr>
			<tr>
				<td><?php echo $lbl['PLATE_NO']; ?></td>
				<td class="data-txt"><?php echo $model->plate_number; ?></td>
				<td style="text-align: right; padding: 3px 7px 3px 0;"><?php echo $lbl['FUEL']; ?></td>
				<td class="data-txt"><?php echo $model->fuel_type; ?></td>
			</tr>
			<tr>
				<td><?php echo $lbl['VIN_NO']; ?></td>
				<td colspan="3" class="data-txt"><?php echo $model->vin_number; ?></td>
			</tr>
			<tr class="no-border">		
				<td colspan="4" style="padding: 5px;">&nbsp;</td>
			</tr>
			<tr>		
				<td colspan="4"><center><strong><?php echo $lbl['VEHICLE_CHECK_OUT']; ?></strong></center></td>
			</tr>
			<tr>
				<td><?php echo $lbl['PICKUP_PLACE']; ?></td>
				<td class="data-txt"><?php echo $model->pickup_location; ?></td>
				<td><?php echo $lbl['RETURN_PLACE']; ?></td>
				<td class="data-txt"><?php echo $model->return_location; ?></td>
			</tr>
			<tr>
				<td><?php echo $lbl['PICKUP_DATE']; ?></td>
				<td class="data-txt"><?php echo $model->pickup_date; ?></td>
				<td><?php echo $lbl['RETURN_DATE']; ?></td>
				<td class="data-txt"><?php echo $model->return_date; ?></td>
			</tr>
			<tr>
				<td><?php echo $lbl['PICKUP_TIME']; ?></td>
				<td class="data-txt"><?php echo $model->pickup_time; ?></td>
				<td><?php echo $lbl['RETURN_TIME']; ?></td>
				<td class="data-txt"><?php echo $model->return_time; ?></td>
			</tr>
			<tr>
				<td><?php echo $lbl['NOTES']; ?></td>
				<td colspan="3">&nbsp;<br></td>
			</tr>
			<tr>
				<td colspan="2">
					<center>
					<strong><?php echo $lbl['LESSOR_SIGN']; ?></strong><br><br><br>
					<hr class="thin-line" style="width: 90%;"/>
					</center>
				</td>
				<td colspan="2">
					<center>
					<strong><?php echo $lbl['RENTER_SIGN']; ?></strong><br><br><br>
					<hr class="thin-line" style="width: 90%;"/>
					</center>
				</td>
			</tr>
			<tr class="no-border">		
				<td colspan="4" style="padding: 5px;">&nbsp;</td>
			</tr>
			<tr>	
				<td colspan="4"><center><strong><?php echo $lbl['VEHICLE_CHECK_IN']; ?></strong></center></td>
			</tr>
			<tr>
				<td><strong><?php echo $lbl['RETURN_DATE']; ?></strong></td>
				<td style="padding: 5px;">&nbsp;</td>
				<td><strong><?php echo $lbl['RETURN_TIME']; ?></strong></td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td><strong><?php echo $lbl['NOTES']; ?></strong></td>
				<td colspan="3">&nbsp;<br><br><br></td>
			</tr>
			<tr>
				<td colspan="2">
					<center>
					<strong><?php echo $lbl['LESSOR_SIGN']; ?></strong><br><br><br>
					<hr class="thin-line" style="width: 90%;"/>
					</center>
				</td>
				<td colspan="2">
					<center>
					<strong><?php echo $lbl['RENTER_SIGN']; ?></strong><br><br><br>
					<hr class="thin-line" style="width: 90%;"/>
					</center>
				</td>
			</tr>
		</table>
	</div>
	<div class="col no-page-break" style="width: 44%; margin-left: 2px;">				
		<table class="no-page-break">
			<tr>
				<td><?php echo $lbl['CHECK_OUT']; ?> (<span style="color: blue;"><?php echo $lbl['BLUE']; ?></span>)</td>
				<td><?php echo $lbl['CHECK_IN']; ?> (<span style="color: #ee0000;"><?php echo $lbl['RED']; ?></span>)</td>
				<td><strong><?php echo $lbl['EQUIPMENTS_FUEL']; ?></strong></td>
			</tr>
			<tr>				
				<td colspan="2" style="padding: 3px;"><?php echo $lbl['NOTES']; ?></td>
				<td>Dokumentacioni</td>
			</tr>
			<tr>
				<td colspan="2" style="padding: 3px;">1.</td>
				<td>Boria</td>
			</tr>
			<tr>
				<td colspan="2" style="padding: 3px;">2.</td>
				<td>Celesi me pult</td>
			</tr>
			<tr>
				<td colspan="2" style="padding: 3px;">3.</td>
				<td>Goma rezerve</td>
			</tr>
			<tr>
				<td colspan="2" rowspan="13" style="padding: 0">
					<center><img src="<?php echo config_path().'/pdf/vehicle_check.png'; ?>" style="max-width: 204px; margin: 0px 0; padding: 0;"/></center>
				</td>
				<td>Kriku</td>
			</tr>
			<tr>
				<td>Celesi i gomes</td>
			</tr>
			<tr>
				<td>Trekendeshi</td>
			</tr>
			<tr>
				<td>Kutia e ndihmes se shpejte</td>
			</tr>
			<tr>
				<td>Tapetet</td>
			</tr>
			<tr>
				<td>Tasat</td>
			</tr>
			<tr>
				<td>Ganxha terheqese</td>
			</tr>
			<tr>
				<td>Literatura e bordit</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
		</table>

	</div>
</div>

</div>