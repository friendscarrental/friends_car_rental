﻿<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<style>
	@page { margin: 10px; }
	body { margin: 10px; }

	.page-break {
		page-break-after: always;
	}

	table {
	    border-collapse: collapse;
	}
	
	table, th, td {
	    border: 1px solid black;
	}
</style>
<div style="font-size: 11px">
<table>
	<tr>
		<td colspan="3">&nbsp;</td>
		<td colspan="3">ZYRA TIRANE</td>
		<td colspan="3">ZYRA FIER</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
		<td colspan="3">Tel : +355 4 481 9333</td>
		<td colspan="3">Tel: +355 34 22 1666</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
		<td colspan="3">Mob : +355 69 99 11 010</td>
		<td colspan="3">Mob: +355 69 25 52 723</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
		<td colspan="3">Email : explore@friendstravel.al</td>
		<td colspan="4"><a href="mailto:explore@friendstravel.al">Email: friends_travel@yahoo.com</a></td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
		<td colspan="3">Web : www.friendstravel.al</td>
		<td colspan="3"><a href="http://www.friendstravel.al/">Web: www.friendstravel.al</a></td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td colspan="10"> <hr> </td>
	</tr>
	<tr>
		<td colspan="9"><strong>FRIENDS TRAVEL AGENCY Sh.p.k. KONTRATE QIRAJE</strong></td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td colspan="4"><strong>NIPT: L32522402A </strong></td>
		<td colspan="6">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="4"><strong>Lagja "15 Tetori ", Rruga "Kastriot Muco ", Fier, Shqiperi </strong></td>
		<td>&nbsp;</td>
		<td><strong>NR</strong></td>
		<td><strong>17</strong></td>
		<td><strong>Data</strong></td>
		<td colspan="2"><strong>16-12-15</strong></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2"><strong>QIRAMARRESI</strong></td>
		<td colspan="3"><strong>KOHEZGJATJA E QIRASE</strong></td>
		<td colspan="5"><strong>PAGESA DHE DEPOZITA</strong></td>
	</tr>
	<tr>
		<td>EMRI</td>
		<td>ALEKSANDER</td>
		<td>KOHEZGJATJA</td>
		<td colspan="2">6 Dite</td>
		<td colspan="2">&nbsp;</td>
		<td>&nbsp;</td>
		<td>MARRJE</td>
		<td>KTHIM</td>
	</tr>
	<tr>
		<td>MBIEMRI</td>
		<td>BOGDANI</td>
		<td>DATA E KTHIMIT</td>
		<td colspan="2">22-Dec-15</td>
		<td colspan="2">&Ccedil;MIMI I QIRASE</td>
		<td>EUR</td>
		<td> 180.00</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>PASAPORTA/ID</td>
		<td>BI9759951</td>
		<td>ORA E KTHIMI</td>
		<td colspan="2">15:00</td>
		<td colspan="2">TRANSFERIM SHERBIMI</td>
		<td>EUR</td>
		<td> - </td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>DATELINDJA</td>
		<td>9-Apr-79</td>
		<td>VENDI I KTHIMIT</td>
		<td colspan="2">Zyre Fier</td>
		<td colspan="2">TVSH (20%)</td>
		<td>EUR</td>
		<td> 30.00</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>VENDLINDJA</td>
		<td>FIER</td>
		<td colspan="3">DEKLARIM I QIRAMARRESIT</td>
		<td colspan="2">KARBURANT</td>
		<td>EUR</td>
		<td>PLOT</td>
		<td>PLOT</td>
	</tr>
	<tr>
		<td>ADRESA E PERHERSHME</td>
		<td>Kryengritja e Fierit</td>
		<td colspan="3" rowspan="7">
			Pasi lexova me kujdes Percaktimet dhe Kushtet e Pergjithshme te kesaj Kontrate Qiraje, deklaroj se bie plotesisht dakord me to dhe duke pranuar te marr me qira automjetin e kontraktuar, marr persiper ne pergjegjesine time, ta perdor me kujdes dhe te respektoj Percaktimet dhe Kushtet e Pergjithshme te kesaj Kontrate
		</td>
		<td colspan="2">DEMTIME</td>
		<td>EUR</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>ADRESA E PERKOHSHME</td>
		<td>Kryengritja e Fierit</td>
		<td colspan="2">VONESA, SHTYRJE AFATI</td>
		<td>EUR</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>TEL/MOB</td>
		<td>0692507262</td>
		<td colspan="2">LARJE, PASTRIM</td>
		<td>EUR</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>EMAIL</td>
		<td>bogdanifier@hotmail.com</td>
		<td colspan="2">GJOBA</td>
		<td>EUR</td>
		<td>Nuk Ka</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>LEJE DREJTIMI</td>
		<td>06262180 43</td>
		<td colspan="2">GARANCI</td>
		<td>EUR</td>
		<td> 100.00</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>VLEFSHMERIA</td>
		<td>7-Jun-20</td>
		<td colspan="2"><strong>GJITHSEJ</strong></td>
		<td><strong>EUR</strong></td>
		<td><strong>GJITHSEJ</strong></td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td colspan="2"><strong>&nbsp;</strong></td>
		<td><strong>ALL</strong></td>
		<td><strong>&nbsp;</strong></td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2"><strong>DREJTUES AUTOMJETI SHTESE</strong></td>
		<td colspan="8">
			<strong>PER PAGESAT ME KARTE KREDITI</strong></td>
	</tr>
	<tr>
		<td>EMRI</td>
		<td>AFRON</td>
		<td colspan="2" rowspan="5">
			Autorizoj perdorimin e te dhenave te kesaj karte krediti pEr te gjitha pagesat qe kane lidhje me marrjen me qira te automjetit. Nenshkrimi im vlen si dorezani dhe detyrim pagese.
		</td>
		<td colspan="2"><strong>NR</strong></td>
		<td colspan="4"><strong>0</strong></td>
	</tr>
	<tr>
		<td>MBIEMRI</td>
		<td>VRUSHA</td>
		<td colspan="2"><strong>VLEFSHMERIA</strong></td>
		<td colspan="2"><strong>Jan-00</strong></td>
		<td><strong>CVV</strong></td>
		<td><strong>0</strong></td>
	</tr>
	<tr>
		<td>LEJE DREJTIMI</td>
		<td>032538134</td>
		<td colspan="6" rowspan="3">
			<strong>0</strong></td>
	</tr>
	<tr>
		<td>VLEFSHMERIA</td>
		<td>45420</td>
	</tr>
	<tr>
		<td>TEL/MOB</td>
		<td>0692507262</td>
	</tr>
	<tr>
		<td colspan="10">
			<strong>AUTORIZIM</strong>
		</td>
	</tr>
	<tr>
		<td colspan="10">
			Friends Travel Agency Sh.p.k. autorizon <strong>Z. ALEKSANDER BOGDANI / Z.AFRON VRUSHA </strong>ta perdore automjetin me te dhenat e meposhtme, nga data <strong>16/12/2015</strong> deri ne daten <strong>22/12/2015</strong> . Ky autorizim eshte i vlefshem vetem brenda territorit te Republikes se Shqiperise.
		</td>
	</tr>
	<tr>
		<td colspan="10">
			<strong>PROCES VERBAL MARRJE NE DOREZIM AUTOMJETI</strong>
		</td>
	</tr>
	<tr>
		<td colspan="4"><strong>TE DHENAT E AUTOMJETIT</strong></td>
		<td colspan="2">Marrje (me blu)</td>
		<td>Kthim (me te kuqe)</td>
		<td colspan="3"><strong>PAJISJE DHE KARBURANTI</strong></td>
	</tr>
	<tr>
		<td>MARKA</td>
		<td><strong>CITROEN C4</strong></td>
		<td>TARGA</td>
		<td><strong>AA 409 HT</strong></td>
		<td colspan="3">Shenime</td>
		<td colspan="3">Dokumentacioni</td>
	</tr>
	<tr>
		<td>SHASIA/ KARBURANTI</td>
		<td colspan="2"><strong>VF7LC9HZH74821726</strong></td>
		<td><strong>NAFTE</strong></td>
		<td colspan="3">1.___________________</td>
		<td colspan="3">Boria</td>
	</tr>
	<tr>
		<td colspan="4"><strong>MARRJA E AUTOMJETIT</strong></td>
		<td colspan="3">2.___________________</td>
		<td colspan="3">Celesi me pult</td>
	</tr>
	<tr>
		<td>VENDI MARRJES</td>
		<td><strong>Zyra Fier</strong></td>
		<td>VENDI KTHIMI</td>
		<td><strong>Zyra Fier</strong></td>
		<td colspan="3" rowspan="10">
			&nbsp;</td>
		<td colspan="3">Goma rezerve</td>
	</tr>
	<tr>
		<td>DATA MARRJES</td>
		<td><strong>16-Dec-15</strong></td>
		<td>DATA E KTHIMIT</td>
		<td><strong>22-Dec-15</strong></td>
		<td colspan="3">Kriku</td>
	</tr>
	<tr>
		<td>ORA MARRJES</td>
		<td><strong>14:20</strong></td>
		<td>ORA KTHIMI</td>
		<td><strong>15:00</strong></td>
		<td colspan="3">Celesi i gomes</td>
	</tr>
	<tr>
		<td colspan="2" rowspan="2">
			<strong>Qiradhenesi <br /> __________________________</strong>
		</td>
		<td colspan="2" rowspan="2">
			<strong>Qiramarresi <br /> ________________________</strong>
		</td>
		<td colspan="3">Trekendeshi</td>
	</tr>
	<tr>
		<td colspan="3">Kutia e ndihmes se shpejte</td>
	</tr>
	<tr>
		<td colspan="4"><strong>KTHIMI I AUTOMJETIT</strong></td>
		<td colspan="3">Tapetet</td>
	</tr>
	<tr>
		<td><strong>DATA KTHIMIT</strong></td>
		<td>&nbsp;</td>
		<td><strong>ORA</strong></td>
		<td>&nbsp;</td>
		<td colspan="3">Tasat</td>
	</tr>
	<tr>
		<td><strong>SHENIME</strong></td>
		<td colspan="3">&nbsp;</td>
		<td colspan="3">Ganxha terheqese</td>
	</tr>
	<tr>
		<td colspan="2" rowspan="2">
			<strong>Qiradhenesi <br /> <br /> __________________________</strong></td>
		<td colspan="2" rowspan="2">
			<strong>Qiramarresi <br /> <br /> _______________________</strong></td>
		<td colspan="3">Literatura e bordit</td>
	</tr>
	<tr>
		<td colspan="3">_________________</td>
	</tr>
</table>
</div>