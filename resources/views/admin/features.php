<?php echo View::make('partials.header') ?>
<br>
<div class="row">
	<h2 class="col-md-10 pull-left">Features</h2>
	<br>
	<a href="/<?php echo $resourceName; ?>/create" class="btn btn-success pull-right">Create New</a>
</div>
<br>
	
<table class="table table-striped table-hover">
    <thead>
        <tr>
			<th>#</th>
			<th>Name</th>
			<th>Description</th>
			<th>Icon Type</th>
			<th>Icon Value</th>
			<th>Status</th>
			<th>&nbsp;</th>
        </tr>
    </thead>
    <tbody>
	<?php 
	$recordsCount = count($records);	
	for($i=0; $i<$recordsCount; $i++) { ?>	
        <tr>
			<th scope="row"><?php echo $i+1; ?></th>
            <td><a href="<?php echo "/".$resourceName."/".$records[$i]->id."/edit"; ?>"><?php echo $records[$i]->name; ?></a></td>
		    <td><?php echo $records[$i]->description; ?></td>
		    <td><?php echo $records[$i]->icon_type==0?'Label':($records[$i]->icon_type==1?'Code':'Image'); ?></td>
		    <td><?php if($records[$i]->icon_type==1){
				echo '<i class="fa '.$records[$i]->icon_value.'"></i> '.$records[$i]->icon_value;
			} else if($records[$i]->icon_type==2) {
				echo '<img src="/images/uploads/'.$records[$i]->icon_value.'" class="thumb sm" ></img> ';
			} else {
				echo $records[$i]->icon_value;
			}
			?></td>
			<td><?php echo $records[$i]->active==1?'Active':'Disabled'; ?></td>
			<td><button class="btn btn-danger btn-sm pull-right" 
						onclick="Utils.confirmDeletion('<?php echo $resourceName."','".$records[$i]->id."','".$records[$i]->name; ?>');">
					<span class="glyphicon glyphicon-trash"></span>
				</button>
			</td>          
        </tr>
	<?php } ?>
    </tbody>
</table>

<?php echo View::make('partials.footer') ?>