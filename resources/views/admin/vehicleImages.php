<?php echo View::make('partials.header') ?>
<br>
<div class="row">
	<h2 class="col-md-8 pull-left">Images for <b><a href="/vehicles/<?php echo $vehicle->id; ?>"><?php echo $vehicle->title; ?></a></b></h2>
	<br>
		
	<div class="col-md-4">
		<a href="/vehicles/<?php echo $vehicle->id; ?>/edit" class="btn btn-info pull-right">Edit Car Details</a>
		<a href="/vehicles" class="btn btn-default pull-right" style="margin-right: 20px;">Vehicles List</a>
	</div>
</div>
<br>
<table class="table table-striped table-hover table-bordered">
    <thead>
        <tr>
			<th class="col-md-1">#</th>
			<th class="col-md-5">Image</th>
			<th class="col-md-4">Description</th>
			<th class="col-md-2">&nbsp;</th>
        </tr>
    </thead>
    <tbody>
	<?php 
	$recordsCount = count($records);	
	for($i=0; $i<$recordsCount; $i++) { ?>	
        <tr>
			<th scope="row"><?php echo $i+1; ?></th>			
			<td><?php echo '<img src="/images/uploads/'.$records[$i]->img_url.'" class="thumb md"></img> '; ?></td>
			<td><?php echo $records[$i]->description; ?></td>
			<td><button class="btn btn-danger btn-sm pull-right" 
						onclick="Utils.confirmDeletion('vehicles', 'images/<?php echo $records[$i]->id; ?>', 'this image');">
					<span class="glyphicon glyphicon-trash"></span>
				</button>
			</td>          
        </tr>
	<?php } ?>
	<form class="form-inline" action="/vehicles/images" method="post" enctype="multipart/form-data">
		<tr class="success">
			<th scope="row">NEW</th>			
			<td>Select Image:
				<div class="input-group">
					<span class="input-group-btn">
						<span class="btn btn-info btn-file upload-file-inputs">
							Browse&hellip; <input type="file" name="image" id="image" class="upload-file-inputs">
						</span>
					</span>
					<input type="text" class="form-control upload-file-inputs" value="<?php echo isset($model['icon_image_path'])?$model['icon_image_path']:''; ?>" readonly>
				</div>
			</td>
			<td>Description:<input type="description" name="description" class="form-control" placeholder="Description"></td>
			<td>
				<input type="hidden" name="vehicle_id" value="<?php echo $vehicle->id; ?>">
				<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
				<button type="submit" class="btn btn-success btn-md" style="margin-top: 18px; width: 120px;">Add New</button>
			</td>          
        </tr>
	</form>
    </tbody>
</table>

		<div class="col-md-offset-3 col-sm-6" id="missingUploadMsg"></div>
		
<?php if (count($errors) > 0) { ?>
	<div class="alert alert-danger">
		<ul>
			<?php foreach ($errors->all() as $error){ ?>
				<li><?php echo $error; ?></li>
			<?php } ?>
		</ul>
	</div>
<?php } ?>

<script>

	$(document).on('change', '.btn-file :file', function() {
		var input = $(this);
		var numFiles = input.get(0).files ? input.get(0).files.length : 1;
		var label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
		
		input.trigger('fileselect', [numFiles, label]);
	});

	$(document).ready(function ()
	{
		$('.btn-file :file').on('fileselect', function(event, numFiles, label) {        
			var input = $(this).parents('.input-group').find(':text'),
				log = numFiles > 1 ? numFiles + ' files selected' : label;
			
			if( input.length ) {
				input.val(log);
			} else {
				if( log ) alert(log);
			}        
		});
				
		$('form').on('submit', function()
		{
			if ($('input:text.upload-file-inputs').val() == "")
			{
				$("#missingUploadMsg")
					.addClass("alert alert-warning")
					.html("The IMAGE field is required, please select a file to upload!");
				
				return false;
			}
			else
				return true;
		});
		
	});

</script>
<?php echo View::make('partials.footer') ?>