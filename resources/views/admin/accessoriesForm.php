<?php echo View::make('partials.header') ?>

<div class="row">
	<form class="form-horizontal col-md-offset-2 col-md-8" action="/accessories" method="post" enctype="multipart/form-data">
		<?php $model = isset($model)?$model:[]; ?>
		<h2>Create/Edit Accessorie </h2></br>
		
		<?php if (count($errors) > 0) { ?>
			<div class="alert alert-danger">
				<ul>
					<?php foreach ($errors->all() as $error){ ?>
						<li><?php echo $error; ?></li>
					<?php } ?>
				</ul>
			</div>
		<?php } ?>
		
		<!-- FORM INPUTS start -->
		
		<div class="form-group required">
			<label for="name" class="col-md-3 control-label">Name</label>
			<div class="col-md-5">
				<input type="input" name="name" id="name" value="<?php echo isset($model['name'])?$model['name']:''; ?>" class="form-control" placeholder="Name">
			</div>
		</div>
		<!--div class="form-group">
			<label for="img_url" class="col-sm-3 control-label">Image Url</label>
			<div class="col-sm-9">
				<input type="input" name="img_url" id="img_url" value="<?php //echo isset($model['img_url'])?$model['img_url']:''; ?>" class="form-control" placeholder="Image Url">
			</div>
		</div-->
		<div class="form-group required">
			<label for="price" class="col-md-3 control-label">Price</label>
			<div class="col-md-5">
				<input type="input" name="price" id="price" value="<?php echo isset($model['price'])?$model['price']:''; ?>" class="form-control" placeholder="Price">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-3 control-label">Fee Frequency</label>
			<div class="col-sm-9">
				<label class="radio-inline">
				  <input type="radio" name="one_time_fee" value="1" <?php if( !isset($model['id']) || (isset($model['one_time_fee']) && $model['one_time_fee']==1)) echo 'checked="checked"'; ?>> Charged Once
				</label>
				<label class="radio-inline">
				  <input type="radio" name="one_time_fee" value="0" <?php if(isset($model['id']) && $model['one_time_fee']==0) echo 'checked="checked"'; ?>> Charged Daily
				</label>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-3 control-label">Availability Type</label>
			<div class="col-sm-9">
				<?php foreach ($availabilities as $availability){ ?>						
					<div class="radio">
						<label>
							<input type="radio" name="availability" value="<?php echo $availability->id; ?>" <?php if( (!isset($model['id']) && $availability->id == 1) || (isset($model['availability']) && $availability->id == $model['availability'])) echo 'checked="checked"'; ?> ><?php echo $availability->name; ?>
						</label>
					</div>
				<?php } ?>
			</div>
		</div>
		<div class="form-group">
			<label for="img_url" class="col-md-3 control-label">Image</label>
			<div class="col-md-7">
				<div class="input-group">
					<span class="input-group-btn">
						<span class="btn btn-info btn-file upload-file-inputs">
							Browse&hellip; <input type="file" name="img_url" id="img_url" class="upload-file-inputs">
						</span>
					</span>
					<input type="text" class="form-control upload-file-inputs" value="<?php echo isset($model['img_url'])?$model['img_url']:''; ?>" readonly>
				</div>
			</div>
		</div>
		<div class="form-group">
			<label for="description" class="col-sm-3 control-label">Description</label>
			<div class="col-sm-7">
				<textarea name="description" id="description" class="form-control" rows="2" placeholder="Description"><?php echo isset($model['description'])?$model['description']:''; ?></textarea>
			</div>
		</div>
		<?php if(!isset($model['id'])): ?>
		<div class="form-group">
			<label class="col-sm-3 control-label">Usability/Fit</label>
			<div class="col-sm-9">
				<label class="radio-inline">
				  <input type="radio" name="apply_to_all" value="1" checked="checked"> Usable on EVERY Car
				</label>
				<label class="radio-inline">
				  <input type="radio" name="apply_to_all" value="0"> Specific ONLY to some Cars
				</label>
				<div style="color: #777; font-style: italic;">You can change Usability later from the Vehicles Center</div>
			</div>
		</div>
		<?php endif; ?>
		<div class="form-group">
			<label class="col-sm-3 control-label">Status</label>
			<div class="col-sm-9">
				<label class="radio-inline">
				  <input type="radio" name="active" value="1" <?php if( !isset($model['id']) || (isset($model['active']) && $model['active']==1)) echo 'checked="checked"'; ?>> Active
				</label>
				<label class="radio-inline">
				  <input type="radio" name="active" value="0" <?php if(isset($model['id']) && $model['active']==0) echo 'checked="checked"'; ?>> Disabled
				</label>
			</div>
		</div>
		
		<!-- FORM INPUTS end -->
		
		
		<?php if(isset($model['id'])): ?>
			<input type="hidden" name="id" value="<?php echo $model['id']; ?>">		
		<?php endif; ?>
		<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">	
		<hr>
		<div class="col-md-offset-3 col-sm-6" id="missingUploadMsg"></div>
		<div class="form-group">
			<div class="col-md-offset-3 col-sm-8">
				<button type="submit" class="btn btn-primary">Save Changes</button>
			</div>
		</div>
	</form>
</div>
<script>

	$(document).on('change', '.btn-file :file', function() {
		var input = $(this);
		var numFiles = input.get(0).files ? input.get(0).files.length : 1;
		var label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
		
		input.trigger('fileselect', [numFiles, label]);
	});

	$(document).ready(function ()
	{
		$('.btn-file :file').on('fileselect', function(event, numFiles, label) {        
			var input = $(this).parents('.input-group').find(':text'),
				log = numFiles > 1 ? numFiles + ' files selected' : label;
			
			if( input.length ) {
				input.val(log);
			} else {
				if( log ) alert(log);
			}        
		});
		
		$('form').on('submit', function()
		{
			if ($('input:text.upload-file-inputs').val() == "")
			{
				console.warn('The IMAGE field is required, please select a file to upload!');
				$("#missingUploadMsg")
					.addClass("alert alert-warning")
					.html("The IMAGE field is required, please select a file to upload!");
				
				return false;
			}
			else
				return true;
		});
		
	});

</script>


<?php echo View::make('partials.footer') ?>