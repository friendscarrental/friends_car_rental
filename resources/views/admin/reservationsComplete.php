<?php echo View::make('partials.public_header') ?>

<style>
	.driver-details .form-group {
		margin-bottom: 0;
	}
	
	.ac-item {
		margin: 5px -5px 5px -8px;
	}
	
	.acmarem .ac-heading {
		font-size: 14px;
		font-weight: 600;
		margin-bottom: 0;
	}
	.ac-item .ac-email {
		text-align: right;
	}
	
	.driver-details {
		border: 1px solid #eee;	
		border-radius: 3px;
		background-color: #f6f6f6;
		margin: 10px 10px 20px 0px;
		padding-top: 10px;
		padding-bottom: 10px;
	}
	
</style>

<div class="container">
<br>

<div class="row">
	<div class="col-md-12" style="background-color: #fff; border: 1px solid #ccc; padding: 0 20px 30px;">
		<div class="row">
			<a href="/home" class="btn btn-link pull-right" style="margin: 10px 0 0; fonts-weight: 900;">
				<span class="glyphicon glyphicon-user"></span> Hello, <?php echo Auth::user()->firstname; ?>
			</a>
		</div>
		<hr>
		<div class="row">
			<form class="col-md-8" action="/reservations/save" method="post">
				
				<div class="row">
					<center><h2>Complete your Reservation</h2></center>
				</div>
						
				<?php if (count($errors) > 0) { ?>
					<div class="alert alert-danger">
						<ul>
							<?php foreach ($errors->all() as $error){ ?>
								<li><?php echo $error; ?></li>
							<?php } ?>
						</ul>
					</div>
				<?php } ?>
				
				<hr>				
				<h4>Primary Diver</h4>
				<div class="row">
					<div class="col-md-8">
						<select id="primary_driver" name="primary_driver" class="form-control input-lg">					
							<?php if(isset($reservation_data['driver'])){ ?>
								<option value="<?php echo $reservation_data['driver']->id; ?>" selected="selected"><?php echo $reservation_data['driver']->text; ?></option>		
							<?php } ?>
						</select>
					</div>
					<div class="col-md-4">
						<a href="/drivers/create/-101" class="btn btn-success" style="margin: 0px 0;">Create New Driver</a>
					</div>
				</div>
				<div id="primaryDriverDetails"></div>
				
				<!--
				<hr>
				<h4>Secondary Diver</h4>
				<div class="row">
					<div class="col-md-8">
						<select id="secondary_driver" name="secondary_driver" class="form-control input-lg">
						</select>
					</div>
					<div class="col-md-4">
						<a href="#" class="btn btn-link" style="margin: 0px 0;">New Driver</a>
					</div>
				</div>
				<div id="secondaryDriverDetails"></div>
				-->
				<h5 style="margin-top: 20px;">You can add <b>Additional Drivers</b> later from your account.</h5>
				
				<div class="row" style="font-size: 18px; font-weight: 900; border: 5px solid #CCE1F2; margin: 50px 0 20px 0; background-color: #F7F7F7; padding: 10px 35px 0 35px;">				
					<div class="row">
						<div class="col-md-6">
							<p style="color: #666666;">TOTAL PRICE</p>
						</div>
						<div class="col-md-6 text-right">
							<span id="receipt_total_price2"></span>
						</div>
					</div>
					<div class="row">
						<div class="col-md-7" style="padding-bottom: 10px;" >
							<a href="javascript:safetyDepositTooltip()" data-toggle="tooltip" data-placement="right" 
								title="A payment  that secures any damages made to the vehicle. It is refunded back to you on drop-off. Click for More.">
								Refundable SECURITY DEPOSIT <span aria-hidden="true" class="glyphicon glyphicon-info-sign"></span>
							</a>
						</div>
						<div class="col-md-5 text-right">
							<span id="security_deposit"></span>
						</div>
					</div>
				</div>
				
				<div class="row" style="font-size: 22px; font-weight: 900; border: 5px solid #038C00; margin: 0 0 50px 0; background-color: #f0fff0; padding: 10px 20px 0 20px;">				
					<div class="col-md-6">
						<p style="color: #888;">Final Payment</p>
					</div>
					<div class="col-md-6 text-right">
						<span id="final_total_price"></span>
					</div>
				</div>
				
				<h4>Special Requests</h4>
				<div class="row" style="margin-bottom: 30px;">
					<div class="col-md-12">
						<textarea name="request_description" class="form-control" rows="2" placeholder="Special Requests or Reservation Notes"></textarea>
					</div>
				</div>
				
				<?php if (count($errors) > 0) { ?>
					<div class="alert alert-danger">
						<ul>
							<?php foreach ($errors->all() as $error){ ?>
								<li><?php echo $error; ?></li>
							<?php } ?>
						</ul>
					</div>
				<?php } ?>
				
				<?php if(Auth::user()->id_role == 1){ ?>
					<div class="well form-inline">
						<div class="row">
							<div class="col-md-6">							
								<p style="font-size: 18px; font-weight: bold;">Admin Section</p>
							</div>
							<div class="form-group col-md-6" style="margin-bottom: 50px;">
								<label for="special_discount" class="control-label">Special Discount</label> &nbsp; 
								<input id="percentageStepper" name="special_discount" class="form-control" value="0" style="max-width: 80px;">
								
								<label class="form-control-static" style="font-size: 18px; margin-left: 8px;">%</label>						
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">							
								<button type="submit" name="approved_reservation" value="approved_no_payment" class="btn btn-default btn-lg" style="width: 100%;">Save Reservation <small style="margin-left: 10px;"> (no payment)</small></button>
							</div>
							<div class="col-md-6">
								<button type="submit" name="approved_reservation" value="approved_with_payment" class="btn btn-default btn-lg" style="width: 100%;">Save with Payment <small style="margin-left: 10px;"> (confirmed)</small></button>
							</div>
						</div>
					</div>
				<?php } ?>
				
				<?php if(Auth::user()->id_role == 5){ ?>
					<div class="row" style="margin: 0 0 20px 0">
						<button type="submit" name="approved_reservation" value="approved_with_payment" class="btn btn-default btn-block btn-lg">Complete Reservation <small style="margin-left: 10px;"> (payment confirmed)</small></button>
					</div>
				<?php } ?>
				
				
				<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
				
				<div class="row">
					<div class="col-md-6">
						<div style="color: #777; font-size: 13px; margin: 4px 0;">Complete your Reservation</div>
						<button type="submit" name="pay_now_with_paypal" value="paypal" class="btn btn-yellow btn-lg" style="padding-top: 5px;">
							<span>Pay Now with</span> 
							<i class="fa fa-cc-paypal" style="font-size: 2em; vertical-align: -15%; margin: 0 -3px -5px 5px"></i>
						</button>
					</div>
					<div class="col-md-6">
						<div style="color: #777; font-size: 13px; margin: 4px 0;">Pending Approval, describe your Special Requests above</div>
						<button type="submit" name="pay_later" value="approval" class="btn btn-default btn-lg" style="width: 100%;">Pay Later <small style="margin-left: 10px;"> by requesting Admin approval</small></button>
					</div>
				</div>
			</form>
			
			<div id="reservationDetailsPanel" class="col-md-4 reservation-details-panel"></div>
		</div>
		
	</div>
</div>

<script type="text/template" id="reservationDetailsTemplate">

	<div class="row">
		<div class="col-md-6">
			<h4 style="padding-top: 10px;">Your selection</h4>
		</div>
		<div class="col-md-6">
			<a href="/reservations/cancel" class="btn btn-default pull-right" style="margin: 10px 0;">Cancel
				<span class="glyphicon glyphicon-chevron-right"></span>
			</a>
		</div>
	</div>
	<div class="row" style="border: 4px solid #ffc547; margin: 10px 0; background-color: #fff;">
		<div class="col-md-5" style="padding: 7px;">
			<img src="/images/uploads/small/<%- data.vehicle.icon_image_path %>" style="max-width: 100%;">
		</div>
		<div class="col-md-7">
			<h5 style="min-height: 35px;"><%= data.vehicle.title %></h5>			
		</div>
	</div>
	<h4>Rental Locations</h4>
	<address style="font-size: 1.2em;">
		Pickup:<br>
		<strong><%= data.pickup_location.name %></strong><br>
		<%= data.pickup_location.datetime %><br><br>
		Return:<br>
		<strong><%= data.return_location.name %></strong><br>
		<%= data.return_location.datetime %>
	</address>
	
	<div id="receipt_entries_table"></div>
	
	<div class="row" style="font-size: 22px; font-weight: 900; border: 5px solid #038C00; margin: 10px 0; background-color: #fff; padding: 10px 15px 0 15px;">				
		<p style="color: #888;">Total price <span style="font-size: 16px; font-weight: 500;">for <%= data.duration %></span></p>
		<p class="text-right"><span id="receipt_total_price"></span></br>
		<span style="font-size: 14px; font-style: italic; font-weight: 400;">(tax included, excluding fuel costs)</span></p>
	</div>
	
</script>

<script type="text/template" id="driverDetailsTemplate">

	<div class="row form-horizontal driver-details">
		<div class="col-md-6">
			<div class="form-group">
				<label class="col-sm-3 control-label">Firstname</label>
				<div class="col-sm-9">
					<p class="form-control-static"><%= data.firstname %> </p>
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 15px;">
				<label class="col-sm-3 control-label">Lastname</label>
				<div class="col-sm-9">
					<p class="form-control-static"><%= data.lastname %></p>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">Birthday</label>
				<div class="col-sm-9">
					<p class="form-control-static"><%= data.birthdate %></p>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">License</label>
				<div class="col-sm-9">
					<p class="form-control-static"><%= data.license_number %></p>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">Expiration</label>
				<div class="col-sm-9">
					<p class="form-control-static"><%= data.license_expiration %></p>
				</div>
			</div>							
		</div>
		<div class="col-md-6">
			<div class="form-group" style="margin-bottom: 15px;">
				<label class="col-sm-3 control-label">Email</label>
				<div class="col-sm-9">
					<p class="form-control-static"><%= data.email %></p>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">Address</label>
				<div class="col-sm-9">
					<p class="form-control-static"><%= data.address %></p>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">Postcode</label>
				<div class="col-sm-9">
					<p class="form-control-static"><%= data.zip_code %></p>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">City</label>
				<div class="col-sm-9">
					<p class="form-control-static"><%= data.city %></p>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">Country</label>
				<div class="col-sm-9">
					<p class="form-control-static"><%= data.address_country %></p>
				</div>
			</div>
			
		</div>
	</div>
	
</script>

<script>
	var reservationData = jQuery.parseJSON(<?php echo var_export(json_encode($reservation_data)); ?>);
	var adminSpecialDiscount = 0;
	
    function safetyDepositTooltip() 
	{
		bootbox.dialog({
			title: "Refundable SAFETY DEPOSIT",
			message: "<br>The SAFETY DEPOSIT is a payment that secures the rentee for any damages made to the vehicle during your reservation period. <br>If there are no damages the payment is <b>fully refunded</b> back to you within a few days after the end of your reservation.<br><br>",
			buttons: { ok: {label: "OK, close", className: "btn-primary"} },
			backdrop: true
		});
	}
    function formatData (data) 
	{
		if (data.loading) 
			return data.text;

		var markup = "<div class='row ac-item'>"
						+"<div class='col-md-6 ac-heading'>" + data.firstname +' '+ data.lastname + "</div>"
						+"<div class='col-md-6 ac-email'>" + data.email + "</div>"
				    +"</div>";

		return markup;
    }
	
	$(document).ready(function ()
	{
		$('[data-toggle="tooltip"]').tooltip({container: 'body'});
		
		$(document).on("keypress", "form :input:not(textarea)", function(event) { 			
			return event.keyCode != 13;
		});

		<?php if(isset($reservation_data['driver'])){ ?>
			setTimeout(function(){
				onDriverChange( <?php echo $reservation_data['driver']->id; ?>, 'primary');
			}, 500);
		<?php } ?>
		
		$("#primary_driver").select2({
			placeholder: {
				id: '-1', // the value of the option
				text: 'Select an Existing Driver'
			},
			theme: "bootstrap",
			ajax: {
				url: "/drivers/search",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						term: params.term, // search term
						page: params.page
					};
				},
				processResults: function (data, params) {
					return {
						results: data
					};
				},
				cache: true
			},
			escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
			minimumInputLength: 2,
			templateResult: formatData,
			templateSelection: function (data) {
				return data.text || data.firstname +' '+ data.lastname;
			},
			allowClear: true
		})
		.on("change", function(e) {
			onDriverChange($(this).val(), 'primary');
        });
		/*
		$("#secondary_driver").select2({
			placeholder: {
				id: '-1', // the value of the option
				text: 'Select a Secondary Driver (optional)'
			},
			theme: "bootstrap",
			ajax: {
				url: "/drivers/search",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						term: params.term, // search term
						page: params.page
					};
				},
				processResults: function (data, params) {
					return {
						results: data
					};
				},
				cache: true
			},
			escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
			minimumInputLength: 2,
			templateResult: formatData,
			templateSelection: function (data) {
				return data.text || data.firstname +' '+ data.lastname;
			},
			allowClear: true
		})
		.on("change", function(e) {
			onDriverChange($(this).val(), 'secondary');
        });
		*/
		var stepperConfig = {
			min: 0,
			max: 100,
			step: 1,
			buttondown_class: "btn btn-yellow",
			buttonup_class: "btn btn-yellow",
			buttondown_txt: '<i class="fa fa-minus"></i>',
			buttonup_txt: '<i class="fa fa-plus"></i>'
		};
		
		$(".extras-panel").ready(function()
		{
			$("#percentageStepper").TouchSpin(stepperConfig);
			$("#percentageStepper").on("change", function(){
				adminSpecialDiscount = $(this).val();
				updateReceiptEntries();
			});			
		});
		
		var sidebarTemplate = _.template($("#reservationDetailsTemplate").html());
		
		var durationDays = (reservationData.duration==1?"1 day":reservationData.duration+" days");
		var templateData = {
			vehicle: reservationData.vehicle,
			duration: durationDays,
			pickup_location: {
				name: reservationData.pickup_location.name,
				datetime: moment(reservationData['pickup_date']).format('dddd, MMM D, YYYY - hh:mm A')
			},
			return_location: {
				name: reservationData.return_location.name,
				datetime: moment(reservationData['return_date']).format('dddd, MMM D, YYYY - hh:mm A')
			}
		};
		var sideBarContent = sidebarTemplate(templateData);
		
		$("#reservationDetailsPanel").html(sideBarContent);
		
		updateReceiptEntries();
	});
	
	
	function onDriverChange(driverID, whichDriver)//primary secondary
	{
		if(driverID && driverID > -1)
		{
			$.ajax({
				type: "GET", 
				url: '/drivers/'+driverID+'/details',
				dataType: 'json', 
				success: function(response)
				{	
					var driverTemplate = _.template($("#driverDetailsTemplate").html());
					var driverContent = driverTemplate(response);
					$("#"+whichDriver+"DriverDetails").html(driverContent);				
				}
			});
		}
		else
		{
			$("#"+whichDriver+"DriverDetails").html("");
		}
	}
	
	function updateReceiptEntries()
	{
		var i = 0;
		var receiptTotalPrice = reservationData.vehicle.final_price;
		
		var tableContent = '<table class="table table-hover" style="background-color: #fff; border: 1px solid #ccc;">';
		
		var entries = reservationData.vehicle.receipt_entries;			
		for(i=0; i < entries.length; i++)
		{
			tableContent += '<tr><td>'+entries[i].entry_name+
							(entries[i].quantity != null && entries[i].quantity > 1?'<br><small>Duration: &nbsp; '+entries[i].quantity+' days</small>':'')+
							'</td><td class="text-right">EUR '+Utils.priceFormat(entries[i].price)+'</td></tr>';
			
			//tableContent += '<tr><td>'+entries[i].entry_name+'</td><td class="text-right">EUR '+Utils.priceFormat(entries[i].price)+'</td></tr>';
		}
		tableContent += '<tr><td>Mileage unlimited</td><td class="text-right" style="min-width: 100px;">included</td></tr>';
		
		entries = reservationData.reservation_receipt_entries;			
		for(i=0; i < entries.length; i++)
		{
			receiptTotalPrice += parseFloat(entries[i].price);
			tableContent += '<tr><td>'+entries[i].entry_name+'</td><td class="text-right">EUR '+Utils.priceFormat(entries[i].price)+'</td></tr>';
		}
		
		if(reservationData.accessories_receipt_entries)
		{
			entries = reservationData.accessories_receipt_entries;
			
			for(i=0; i < entries.length; i++)
			{
				receiptTotalPrice += parseFloat(entries[i].price);
				tableContent += '<tr><td>'+entries[i].entry_name+
								(entries[i].quantity > 1 ?'<br><small>Quantity: &nbsp; '+entries[i].quantity+'</small>':'')+
								'</td><td class="text-right">EUR '+Utils.priceFormat(entries[i].price)+'</td></tr>';
			}
		}
		
		if(adminSpecialDiscount > 0)
		{
			var discountValue = parseFloat(receiptTotalPrice * adminSpecialDiscount/100);
			
			tableContent += '<tr style="font-style: italic; font-weight:700; background-color: #ffff00;"><td>SPECIAL DISCOUNT'+
							'<br><small> - '+adminSpecialDiscount+' % &nbsp; of &nbsp; '+Utils.priceFormat(receiptTotalPrice)+'</small></td>'+
							'<td class="text-right">EUR '+Utils.priceFormat(discountValue)+'</td></tr>';
			
			receiptTotalPrice -= discountValue;
		}
		
		$("#receipt_entries_table").html( tableContent+'</table>' );
		$("#receipt_total_price").html( 'EUR '+Utils.priceFormat(receiptTotalPrice) );
		
		$("#receipt_total_price2").html( 'EUR '+Utils.priceFormat(receiptTotalPrice) );
		$("#security_deposit").html( 'EUR '+Utils.priceFormat(reservationData.vehicle.security_deposit) );
		$("#final_total_price").html( 'EUR '+Utils.priceFormat(receiptTotalPrice + reservationData.vehicle.security_deposit) );
	}
	
</script>



<?php echo View::make('partials.footer')  ?>