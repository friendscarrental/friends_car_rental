<?php echo View::make('partials.header') ?>
<br>
<div class="row">
	<h2 class="col-md-8 pull-left">Location Extra Cost for <b><a href="/vehicles/<?php echo $vehicle->id; ?>"><?php echo $vehicle->title; ?></a></b></h2>
	<br>
		
	<div class="col-md-4">
		<a href="/vehicles/<?php echo $vehicle->id; ?>/edit" class="btn btn-info pull-right">Edit Car Details</a>
		<a href="/vehicles" class="btn btn-default pull-right" style="margin-right: 20px;">Vehicles List</a>
	</div>
</div>
<br>
<hr>
<div class="row">
<form class="form-horizontal col-md-offset-2 col-md-8" action="/vehicles/location-costs" method="post">
	
	<?php if (count($errors) > 0) { ?>
		<div class="alert alert-danger">
			<ul>
				<?php foreach ($errors->all() as $error){ ?>
					<li><?php echo $error; ?></li>
				<?php } ?>
			</ul>
		</div>
	<?php } ?>		
			
	<table class="table table-striped table-hover">
		<thead>
			<tr>
				<th class="col-md-1">#</th>
				<th class="col-md-4">Location</th>
				<th class="col-md-4">Extra Cost</th>
			</tr>
		</thead>
		<tbody>
		<?php 
		$recordsCount = count($records);	
		for($i=0; $i<$recordsCount; $i++) { ?>	
			<tr>
				<th scope="row"><?php echo $i+1; ?></th>			
				<td><b><?php echo $records[$i]->name; ?></b></td>
				<td>
				<div class="input-group">
					<input type="input" name="<?php echo $records[$i]->id; ?>" value="<?php echo $records[$i]->cost; ?>" class="form-control" placeholder="Cost" >
					<span class="input-group-addon">Euro</span>
				</div>
				</td>
			</tr>
		<?php } ?>
		</tbody>
	</table>
	<hr>
	<input type="hidden" name="vehicle_id" value="<?php echo $vehicle->id; ?>">
	<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">		
	<div class="form-group">
		<div class="col-md-offset-4 col-sm-8">
			<button type="submit" class="btn btn-primary">Save Changes</button>
		</div>
	</div>
</form>
</div>

<?php echo View::make('partials.footer') ?>