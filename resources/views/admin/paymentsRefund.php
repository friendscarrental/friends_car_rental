<?php echo View::make('partials.header') ?>

<div class="row">
	<form class="form-horizontal col-md-offset-3 col-md-6" action="/payments/refund" method="post">
		<?php $model = isset($model)?$model:[]; ?>
		<h2><?php echo isset($security_deposit)?'Security Deposit':'Payment'; ?>  Refund</h2>
		<hr>
		<?php if (count($errors) > 0) { ?>
			<div class="alert alert-danger">
				<ul>
					<?php foreach ($errors->all() as $error){ ?>
						<li><?php echo $error; ?></li>
					<?php } ?>
				</ul>
			</div>
		<?php } ?>
		
		<!-- FORM INPUTS start -->
		<div class="well">
			<div class="form-group" style="margin-top: 10px; margin-bottom: 30px;">
				<label class="col-sm-4 control-label">Refund Type:</label>
				<div class="col-sm-8">
					<div class="radio">
						<label>
							<input type="radio" name="refund_type" value="6" checked="checked"> Full Refund
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="refund_type" value="7"> Partial Refund
						</label>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label for="amount" class="col-md-4 control-label">Amount</label>
				<div class="col-md-3">
					<input type="input" name="amount" id="amount" disabled="disabled" value="<?php echo $model['amount']; ?>" class="form-control" placeholder="Vehicle Type">
				</div>	
				<div class="col-md-2">
					<p class="form-control-static" style="margin-left: -12px; padding-left: 0px;">Euro</p>
				</div>
			</div>
		</div>
		
		<input type="hidden" name="id" value="<?php echo $model['id']; ?>">
		
		<?php if(isset($security_deposit)): ?>
			<input type="hidden" name="security_deposit" value="true">		
		<?php endif; ?>
		<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
		<hr>
		<div class="form-group">
			<div class="col-md-offset-4 col-md-8">				
				<button type="submit" class="btn btn-yellow" style="padding-top: 5px;">
					<span>Refund Payment</span>
					<i class="fa fa-cc-paypal" style="font-size: 2em; vertical-align: -15%; margin: 0 -3px -5px 5px"></i>
				</button>
			</div>
		</div>
		
	</form>
</div>

<script>

	$(document).ready(function ()
	{	
		$("input[name='refund_type']").change(function(){
			checkRadioInputs(this);
		});			
	});

	function checkRadioInputs(that)
	{
		var radioSelected = $(that);
		if (radioSelected.val() == 6) {
			$('#amount').attr("disabled","disabled");
		} else {
			$('#amount').removeAttr('disabled');
		}
	}
	
</script>

<?php echo View::make('partials.footer') ?>