<?php echo View::make('partials.header') ?>
<br>
<div class="row">
	<h2 class="col-md-5 pull-left"><?php echo $model->title; ?></h2>
	<br>
		
	<div class="col-md-7">
		<a href="/vehicles/<?php echo $model->id; ?>/edit" class="btn btn-info pull-right">Edit Car Details</a>
		<a href="/vehicles" class="btn btn-default pull-right" style="margin-right: 20px;">Vehicles List</a>
	</div>
</div>
<hr>	
<div class="row">
	<div class="col-md-3">
		<img src="/images/uploads/<?php echo $model->icon_image_path; ?>" class="thumb lg">
	</div>
	<div class="col-md-9">
		<div class="row form-inline">
			<div class="form-group">
				<label style="margin-left: 15px;">Year:</label>
				<p class="form-control-static" style="margin-right: 40px;"><?php echo $model->year; ?></p>
			</div>
			<div class="form-group">
				<label style="margin-left: 15px;">Type:</label>
				<p class="form-control-static" style="margin-right: 40px;"><?php echo $model->vehicletypes_name; ?></p>
			</div>
			<div class="form-group">
				<label style="margin-left: 15px;">Transmission:</label>
				<p class="form-control-static" style="margin-right: 40px;"><?php echo $model->transmission==1?'Manual':'Automatic'; ?></p>
			</div>
			<div class="form-group">
				<label style="margin-left: 15px;">Fuel:</label>
				<p class="form-control-static" style="margin-right: 40px;"><?php echo $model->fuel_type; ?></p>
			</div>
			<div class="form-group">
				<label style="margin-left: 15px;">Price:</label>
				<p class="form-control-static" style="margin-right: 40px;"><?php echo $model->standard_price; ?> Euro</p>
			</div>
			<br>
			<div class="form-group">
				<label style="margin-left: 15px;">Plate No:</label>
				<p class="form-control-static" style="margin-right: 40px;"><?php echo $model->plate_number; ?></p>
			</div>
			<div class="form-group">
				<label style="margin-left: 15px;">Vin No:</label>
				<p class="form-control-static" style="margin-right: 40px;"><?php echo $model->vin_number; ?></p>
			</div>
		</div>
		<hr>	
		<div class="row">
			<a href="/vehicles/<?php echo $model->id; ?>/images" class="btn btn-info btn-md" style="margin-left: 15px;">Images/Galery</a>		
			<a href="/accessories/<?php echo $model->id; ?>/per-vehicle" class="btn btn-info btn-md" style="margin-left: 10px;">Accessories</a>
			<a href="/features/<?php echo $model->id; ?>/per-vehicle" class="btn btn-info btn-md" style="margin-left: 10px;">Features</a>
			
			<a href="/vehicles/<?php echo $model->id; ?>/seasonal-prices" class="btn btn-info btn-md pull-right" style="margin-right: 15px;">Seasonal Prices</a>
			<a href="/vehicles/<?php echo $model->id; ?>/discounts" class="btn btn-info btn-md pull-right" style="margin-right: 10px;">Duration Discounts</a>
			<a href="/vehicles/<?php echo $model->id; ?>/location-costs" class="btn btn-info btn-md pull-right" style="margin-right: 10px;">Location Costs</a>
		</div>
	</div>
</div>

<hr>
<br>
<div class="row">
	<h2 class="col-md-8 pull-left">Upcoming Reservations</h2>
	<br>
	<a href="/reservations/past/<?php echo $model->id; ?>" class="btn btn-warning pull-right">Older/Past Reservations</a>
</div>
<br>

<table class="table table-striped table-hover">
    <thead>
        <tr>
			<th>Code</th>
			<th>Driver</th>
			<th>Duration</th>
			<th>Locations</th>
			<th>Pickup Date</th>
			<th>Return Date</th>
			<th>Payment</th>
			<th style="text-align: right;">Price</th>
        </tr>
    </thead>
    <tbody>
	<?php 
	$recordsCount = count($records);
	for($i=0; $i<$recordsCount; $i++) { ?>
        <tr>
			<th scope="row"><?php echo $records[$i]->id; ?></th>
			<th><a href="<?php echo "/reservations/".$records[$i]->id; ?>"><?php echo $records[$i]->primary_driver_name; ?></a></td>
			<td><?php echo $records[$i]->duration_days; ?> Days</td>
			<td><div style="max-width: 130px;"> <?php echo $records[$i]->pickup_location.($records[$i]->id_location_pickup != $records[$i]->id_location_return?' - '.$records[$i]->return_location:''); ?></div></td>
			<td><?php echo $startDateYear = date("j-M-y <b>H:i</b>", strtotime($records[$i]->pickup_datetime)); ?></td>
			<td><?php echo $startDateYear = date("j-M-y <b>H:i</b>", strtotime($records[$i]->return_datetime)); ?></td>
			<td><?php echo $records[$i]->payment_completed == 1?"Completed":"Pending"; ?></td>
			<td style="text-align: right;"><?php echo number_format($records[$i]->total_price, 2, '.', ' '); ?></td>
        </tr>
	<?php } ?>
    </tbody>
</table>
<?php echo $records->render(); ?>
<br>

<?php echo View::make('partials.footer') ?>