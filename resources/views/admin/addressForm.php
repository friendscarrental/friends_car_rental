<?php echo View::make('partials.header') ?>

<div class="row">
	<form class="form-horizontal col-md-offset-2 col-md-8" action="/address" method="post">
		<?php $model = isset($model)?$model:[]; ?>
		<h2>Create/Edit Address</h2>
		<hr>
		<?php if (count($errors) > 0) { ?>
			<div class="alert alert-danger">
				<ul>
					<?php foreach ($errors->all() as $error){ ?>
						<li><?php echo $error; ?></li>
					<?php } ?>
				</ul>
			</div>
		<?php } ?>
		
		<!-- FORM INPUTS start -->
		
		<div class="form-group required">
			<label for="firstname" class="col-md-3 control-label">Firstname</label>
			<div class="col-md-5">
				<input type="input" name="firstname" id="firstname" value="<?php echo isset($model['firstname'])?$model['firstname']:''; ?>" class="form-control" placeholder="Firstname">
			</div>
		</div>
		<div class="form-group required">
			<label for="lastname" class="col-md-3 control-label">Lastname</label>
			<div class="col-md-5">
				<input type="input" name="lastname" id="lastname" value="<?php echo isset($model['lastname'])?$model['lastname']:''; ?>" class="form-control" placeholder="Lastname">
			</div>
		</div>
		<div class="form-group required">
			<label for="address" class="col-md-3 control-label">Address</label>
			<div class="col-md-8">
				<input type="input" name="address" id="address" value="<?php echo isset($model['address'])?$model['address']:''; ?>" class="form-control" placeholder="address">
			</div>
		</div>
		<div class="form-group">
			<label for="zip_code" class="col-md-3 control-label">Postal</label>
			<div class="col-md-3">
				<input type="input" name="zip_code" id="zip_code" value="<?php echo isset($model['zip_code'])?$model['zip_code']:''; ?>" class="form-control" placeholder="Postal Code">
			</div>
		</div>
		<div class="form-group required">
			<label for="city" class="col-md-3 control-label">City</label>
			<div class="col-md-5">
				<input type="input" name="city" id="city" value="<?php echo isset($model['city'])?$model['city']:''; ?>" class="form-control" placeholder="City">
			</div>
		</div>
		<div class="form-group required">
			<label for="id_country" class="col-md-3 control-label">Country</label>
			<div class="col-md-5">
				<select name="id_country" id="id_country" class="form-control">
					<option value="" <?php if(!isset($model['id_country'])) echo'selected="selected"'; ?> >Select a Country</option>
					<?php foreach ($countries as $country){ ?>
						<option value="<?php echo $country->id; ?>" <?php if($country->id == $model['id_country']) echo'selected="selected"'; ?> ><?php echo $country->name; ?></option>
					<?php } ?>
				</select>
			</div>
		</div>
		
		<div class="form-group required">
			<label for="phone_1" class="col-md-3 control-label">Phone 1</label>
			<div class="col-md-5">
				<input type="input" name="phone_1" id="phone_1" value="<?php echo isset($model['phone_1'])?$model['phone_1']:''; ?>" class="form-control" placeholder="Primary Phone Number">
			</div>
		</div>
		<div class="form-group">
			<label for="phone_2" class="col-md-3 control-label">Phone 2</label>
			<div class="col-md-5">
				<input type="input" name="phone_2" id="phone_2" value="<?php echo isset($model['phone_2'])?$model['phone_2']:''; ?>" class="form-control" placeholder="Other Phone Number">
			</div>
		</div>
		<!-- FORM INPUTS end -->
		
		<?php if(isset($model['id'])): ?>
			<input type="hidden" name="id" value="<?php echo $model['id']; ?>">		
		<?php endif; ?>
		<?php if(isset($model['id_driver'])): ?>
			<input type="hidden" name="id_driver" value="<?php echo $model['id_driver']; ?>">		
		<?php endif; ?>
		<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
		<hr>
		<div class="form-group">
			<div class="col-md-offset-3 col-sm-8">
				<button type="submit" class="btn btn-primary">Save Changes</button>
			</div>
		</div>
	</form>
</div>


<?php echo View::make('partials.footer') ?>