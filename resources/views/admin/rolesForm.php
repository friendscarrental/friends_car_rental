<?php echo View::make('partials.header') ?>

<div class="row">
	<form class="form-horizontal col-md-offset-2 col-md-8" action="/roles" method="post">
		<?php $model = isset($model)?$model:[]; ?>
		<h2>Create/Edit Role</h2></br>
		
		<?php if (count($errors) > 0) { ?>
			<div class="alert alert-danger">
				<ul>
					<?php foreach ($errors->all() as $error){ ?>
						<li><?php echo $error; ?></li>
					<?php } ?>
				</ul>
			</div>
		<?php } ?>
		
		<!-- FORM INPUTS start -->
		
		<div class="form-group">
			<label for="role_name" class="col-md-3 control-label">Role Name</label>
			<div class="col-md-5">
				<input type="input" name="role_name" id="role_name" value="<?php echo isset($model['role_name'])?$model['role_name']:''; ?>" class="form-control" placeholder="Role Name">
			</div>
		</div>
		<div class="form-group">
			<label for="description" class="col-sm-3 control-label">Description</label>
			<div class="col-sm-9">
				<input type="input" name="description" id="description" value="<?php echo isset($model['description'])?$model['description']:''; ?>" class="form-control" placeholder="Description">
			</div>
		</div>
		
		<!-- FORM INPUTS end -->
		
		<?php if(isset($model['id'])): ?>
			<input type="hidden" name="id" value="<?php echo $model['id']; ?>">		
		<?php endif; ?>
		<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
		<hr>
		<div class="form-group">
			<div class="col-md-offset-3 col-sm-8">
				<button type="submit" class="btn btn-primary">Save Changes</button>
			</div>
		</div>
	</form>
</div>


<?php echo View::make('partials.footer') ?>