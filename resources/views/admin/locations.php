<?php echo View::make('partials.header') ?>
<br>
<div class="row">
	<h2 class="col-md-5 pull-left">Locations</h2>
	<br>
	<div class="col-md-7">
		<a href="/<?php echo $resourceName; ?>/create" class="btn btn-success pull-right">Create New</a>
		<a href="/locationtypes" class="btn btn-info pull-right" style="margin-right: 20px;">Location Types List</a>
	</div>
</div>
<br>
	
<table class="table table-striped table-hover">
    <thead>
        <tr>
			<th>#</th>
			<th>Name</th>
			<th>Location Type</th>
			<th>City</th>
			<th>Address</th>
			<th>Phone</th>
			<th>Fax</th>
			<th>Status</th>
			<th>&nbsp;</th>
        </tr>
    </thead>
    <tbody>
	<?php 
	$recordsCount = count($records);	
	for($i=0; $i<$recordsCount; $i++) { ?>	
        <tr>
			<th scope="row"><?php echo $i+1; ?></th>
            <td><a href="<?php echo "/".$resourceName."/".$records[$i]->id."/edit"; ?>"><?php echo $records[$i]->name; ?></a></td>
		    <td><?php echo $records[$i]->locationtypes_name; ?></td>
			<td><?php echo $records[$i]->city; ?></td>
		    <td><?php echo $records[$i]->address; ?></td>
			<td><?php echo $records[$i]->tel.($records[$i]->tel2!=''?'<br>'.$records[$i]->tel2:''); ?></td>
			<td><?php echo $records[$i]->fax; ?></td>
			<td><?php echo $records[$i]->active==1?'Active':'Disabled'; ?></td>
			<td><button class="btn btn-danger btn-sm pull-right" 
						onclick="Utils.confirmDeletion('<?php echo $resourceName."','".$records[$i]->id."','".$records[$i]->name; ?>');">
					<span class="glyphicon glyphicon-trash"></span>
				</button>
			</td>          
        </tr>
	<?php } ?>
    </tbody>
</table>

<?php echo View::make('partials.footer') ?>