<?php echo View::make('partials.header') ?>
<br>
<div class="row">
	<h2 class="col-md-5 pull-left">Create/Edit Vehicles</h2>
	<?php if(isset($model['id'])): ?>
	<br>
	<a href="/vehicles/<?php echo $model['id']; ?>" class="btn btn-info pull-right">Vehicle Details/Tools</a>
	<?php endif; ?>
</div>
<hr>
<br>

	<form class="form-horizontal" action="/vehicles" method="post" enctype="multipart/form-data">
		<div class="row">
			<?php $model = isset($model)?$model:[]; ?>
			<?php if (count($errors) > 0) { ?>
				<div class="alert alert-danger">
					<ul>
						<?php foreach ($errors->all() as $error){ ?>
							<li><?php echo $error; ?></li>
						<?php } ?>
					</ul>
				</div>
			<?php } ?>
			
			<div class="col-md-7">
				<div class="form-group required">
					<label for="title" class="col-md-4 control-label">Title</label>
					<div class="col-md-7">
						<input type="input" name="title" id="title" value="<?php echo isset($model['title'])?$model['title']:''; ?>" class="form-control" placeholder="Display Title">
					</div>
				</div>
				<div class="form-group required">
					<label for="make" class="col-md-4 control-label">Make</label>
					<div class="col-md-7">
						<input type="input" name="make" id="make" value="<?php echo isset($model['make'])?$model['make']:''; ?>" class="form-control" placeholder="Make">
					</div>
				</div>
				<div class="form-group required">
					<label for="model" class="col-md-4 control-label">Model</label>
					<div class="col-md-7">
						<input type="input" name="model" id="model" value="<?php echo isset($model['model'])?$model['model']:''; ?>" class="form-control" placeholder="Model">
					</div>
				</div>					
				<div class="form-group required">
					<label for="year" class="col-md-4 control-label">Year</label>
					<div class="col-md-4">
						<input type="input" name="year" id="year" value="<?php echo isset($model['year'])?$model['year']:''; ?>" class="form-control" placeholder="Year">
					</div>
				</div>
				
				<div class="form-group">
					<label for="icon_image_path" class="col-md-4 control-label">Image</label>
					<div class="col-md-7">
						<div class="input-group">
							<span class="input-group-btn">
								<span class="btn btn-info btn-file upload-file-inputs">
									Browse&hellip; <input type="file" name="icon_image_path" id="icon_image_path" class="upload-file-inputs">
								</span>
							</span>
							<input type="text" class="form-control upload-file-inputs" value="<?php echo isset($model['icon_image_path'])?$model['icon_image_path']:''; ?>" readonly>
						</div>
					</div>
				</div>
						
				<div class="form-group">
					<label for="vin_number" class="col-md-4 control-label">VIN No.</label>
					<div class="col-md-7">
						<input type="input" name="vin_number" id="vin_number" value="<?php echo isset($model['vin_number'])?$model['vin_number']:''; ?>" class="form-control" placeholder="vin_number Number">
					</div>
				</div>
				<div class="form-group">
					<label for="plate_number" class="col-md-4 control-label">Plate No.</label>
					<div class="col-md-7">
						<input type="input" name="plate_number" id="plate_number" value="<?php echo isset($model['plate_number'])?$model['plate_number']:''; ?>" class="form-control" placeholder="plate_number Number">
					</div>
				</div>
				<div class="form-group">
					<label for="description" class="col-md-4 control-label">Description</label>
					<div class="col-md-7">
						<textarea name="description" id="description" class="form-control" rows="2" placeholder="Description"><?php echo isset($model['description'])?$model['description']:''; ?></textarea>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Status</label>
					<div class="col-md-8">
						<label class="radio-inline">
						  <input type="radio" name="active" value="1" <?php if( !isset($model['id']) || (isset($model['active']) && $model['active']==1)) echo 'checked="checked"'; ?>> Active
						</label>
						<label class="radio-inline">
						  <input type="radio" name="active" value="0" <?php if(isset($model['id']) && $model['active']==0) echo 'checked="checked"'; ?>> Disabled
						</label>
					</div>
				</div>
			</div>
			
			<div class="col-md-5">
				
				<div class="form-group required">
					<label for="id_vehicletype" class="col-md-4 control-label">Vehicle Type</label>
					<div class="col-md-5">		
						<select name="id_vehicletype" id="id_vehicletype" class="form-control">
							<?php foreach ($vehicletypes as $vehicletype){ ?>
								<option value="<?php echo $vehicletype->id; ?>" <?php if(isset($model['id_vehicletype']) && $vehicletype->id == $model['id_vehicletype']) echo 'selected="selected"'; ?> ><?php echo $vehicletype->name; ?></option>
							<?php } ?>
						</select>
					</div>
				</div>
				<div class="form-group required">
					<label class="col-md-4 control-label">Transmission</label>
					<div class="col-md-7">
						<label class="radio-inline">
						  <input type="radio" name="transmission" value="1" <?php if( !isset($model['id']) || (isset($model['transmission']) && $model['transmission']==1)) echo 'checked="checked"'; ?>> Automatic
						</label>
						<label class="radio-inline">
						  <input type="radio" name="transmission" value="0" <?php if(isset($model['id']) && $model['transmission']==0) echo 'checked="checked"'; ?>> Manual
						</label>
					</div>
				</div>
				
				<div class="form-group required">
					<label for="id_vehiclefueltype" class="col-md-4 control-label">Fuel Type</label>
					<div class="col-md-5">		
						<select name="id_vehiclefueltype" id="id_vehiclefueltype" class="form-control">
							<?php foreach ($fueltypes as $fueltype){ ?>
								<option value="<?php echo $fueltype->id; ?>" <?php if(isset($model['id_vehiclefueltype']) && $fueltype->id == $model['id_vehiclefueltype']) echo 'selected="selected"'; ?> ><?php echo $fueltype->name; ?></option>
							<?php } ?>
						</select>
					</div>
				</div>
				<div class="form-group required">
					<label for="seats" class="col-md-4 control-label">Seats</label>
					<div class="col-md-5">
						<input type="input" name="seats" id="seats" value="<?php echo isset($model['seats'])?$model['seats']:''; ?>" class="form-control" placeholder="Seats">
					</div>
				</div>
				<div class="form-group required">
					<label for="doors" class="col-md-4 control-label">Doors</label>
					<div class="col-md-5">
						<input type="input" name="doors" id="doors" value="<?php echo isset($model['doors'])?$model['doors']:''; ?>" class="form-control" placeholder="Doors">
					</div>
				</div>
				<div class="form-group required">
					<label for="baggage_size" class="col-md-4 control-label">Baggage Size</label>
					<div class="col-md-5">
						<input type="input" name="baggage_size" id="baggage_size" value="<?php echo isset($model['baggage_size'])?$model['baggage_size']:''; ?>" class="form-control" placeholder="Bagage size">
					</div>
				</div>				
				<div class="form-group">
					<label class="col-md-4 control-label">Air Conditioning</label>
					<div class="col-md-7">
						<label class="radio-inline">
						  <input type="radio" name="airconditioning" value="1" <?php if( !isset($model['id']) || (isset($model['airconditioning']) && $model['airconditioning']==1)) echo 'checked="checked"'; ?>> Included
						</label>
						<label class="radio-inline">
						  <input type="radio" name="airconditioning" value="0" <?php if(isset($model['id']) && $model['airconditioning']==0) echo 'checked="checked"'; ?>> Missing
						</label>
					</div>
				</div>
				<hr>
				<div class="form-group required">
					<label for="security_deposit" class="col-md-4 control-label">Security Deposit</label>
					<div class="col-md-4">
						<input type="input" name="security_deposit" id="security_deposit" value="<?php echo isset($model['security_deposit'])?$model['security_deposit']:''; ?>" class="form-control" placeholder="Security Deposit">
					</div>
					<div class="col-md-3">
						<p class="form-control-static">Euro</p>
					</div>
				</div>	
				<div class="form-group required">
					<label for="standard_price" class="col-md-4 control-label">Standard Price</label>
					<div class="col-md-4">
						<input type="input" name="standard_price" id="standard_price" value="<?php echo isset($model['standard_price'])?$model['standard_price']:''; ?>" class="form-control" placeholder="Standard price">
					</div>
					<div class="col-md-3">
						<p class="form-control-static">Euro</p>
					</div>
				</div>					
				<?php if(isset($model['id'])): ?>
				<div class="form-group">
					<label for="password" class="col-md-4 control-label">Price Ranges</label>
					<div class="col-md-8">
						<a href="/vehicles/<?php echo $model['id']; ?>/seasonal-prices" class="btn btn-warning">Edit Seasonal Prices</a>
					</div>
				</div>
				<?php endif; ?>
			</div>
			<!-- FORM INPUTS end -->
			
			<?php if(isset($model['id'])): ?>
				<input type="hidden" name="id" value="<?php echo $model['id']; ?>">		
			<?php endif; ?>
			<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
		</div>
		<hr>
		<div class="col-md-offset-3 col-md-6" id="missingUploadMsg"></div>
		<div class="row">
			<div class="col-md-offset-5 col-md-7">
				<button type="submit" class="btn btn-primary">Save Changes</button>
			</div>
		</div>
	</form>

<script>

	$(document).on('change', '.btn-file :file', function() {
		var input = $(this);
		var numFiles = input.get(0).files ? input.get(0).files.length : 1;
		var label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
		
		input.trigger('fileselect', [numFiles, label]);
	});

	$(document).ready(function ()
	{
		$('.btn-file :file').on('fileselect', function(event, numFiles, label) {        
			var input = $(this).parents('.input-group').find(':text'),
				log = numFiles > 1 ? numFiles + ' files selected' : label;
			
			if( input.length ) {
				input.val(log);
			} else {
				if( log ) alert(log);
			}        
		});
		
		$('form').on('submit', function()
		{
			if ($('input:text.upload-file-inputs').val() == "")
			{
				console.warn('The IMAGE field is required, please select a file to upload!');
				$("#missingUploadMsg")
					.addClass("alert alert-warning")
					.html("The IMAGE field is required, please select a file to upload!");
				
				return false;
			}
			else
				return true;
		});
		
	});

</script>

<?php echo View::make('partials.footer') ?>