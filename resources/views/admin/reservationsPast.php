<?php echo View::make('partials.header') ?>
<br>
<div class="row">
	<h2 class="col-md-8 pull-left">Older/Past Reservations</h2>
	<br>
	<a href="/<?php echo $resourceName; ?>" class="btn btn-success pull-right">New Reservations</a>
	<?php if($filtered == true) { ?>
		<a href="/<?php echo $resourceName; ?>/past" class="btn btn-warning pull-right" style="margin-right: 20px;">All Past Reservations</a>
	<?php } ?>
</div>
<br>
	
<table class="table table-striped table-hover">
    <thead>
        <tr>
			<th>Code</th>
			<th class="col-md-2">Vehicle</th>
			<th>Duration</th>
			<th>Locations</th>
			<th>Pickup Date</th>
			<th>Return Date</th>
			<th>Driver</th>
			<th>Price</th>
			<th>Approved</th>
        </tr>
    </thead>
    <tbody>
	<?php 
	$recordsCount = count($records);	
	for($i=0; $i<$recordsCount; $i++) { ?>	
        <tr>
			<th scope="row"><?php echo $records[$i]->id; ?></th>
			<th><a href="<?php echo "/".$resourceName."/".$records[$i]->id; ?>"><?php echo $records[$i]->vehicle_title; ?></a></td>
			<td><?php echo $records[$i]->duration_days; ?> Days</td>
			<td><div style="max-width: 130px;"> <?php echo $records[$i]->pickup_location.($records[$i]->id_location_pickup != $records[$i]->id_location_return?' - '.$records[$i]->return_location:''); ?></div></td>
			<td><?php echo date("j-M-y <b>H:i</b>", strtotime($records[$i]->pickup_datetime)); ?></td>
			<td><?php echo date("j-M-y <b>H:i</b>", strtotime($records[$i]->return_datetime)); ?></td>
			<td><?php echo $records[$i]->primary_driver_name; ?></td>
			<td style="text-align: right;"><?php echo number_format($records[$i]->total_price, 2, '.', ' '); ?></td>
			<td><?php echo '<em>by '.$records[$i]->approved_user_name.'</em>'; ?></td>
        </tr>
	<?php } ?>
    </tbody>
</table>
<?php echo $records->render(); ?>
<br>

<?php echo View::make('partials.footer') ?>