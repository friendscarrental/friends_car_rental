<?php echo View::make('partials.header') ?>
<br>

<?php if (count($errors) > 0) { ?>
	<div class="alert alert-danger">
		<ul>
			<?php foreach ($errors->all() as $error){ ?>
				<li><?php echo $error; ?></li>
			<?php } ?>
		</ul>
	</div>
	<br>
<?php } ?>
		
<div class="row" <?php if($model->status==0){ ?> style="background-color: #eeaaaa;"<?php } ?> >
	<h3 class="col-md-8 pull-left">Reservation No. <strong><?php echo $model->id; ?></strong>
		<strong style="margin: 0 20px 0 50px;"><a href="/vehicles/<?php echo $model->id_vehicle; ?>"><?php echo $model->vehicle_title; ?></a></strong> 
		<small>( <?php echo $model->duration_days; ?> Days )</small>
	</h3>
	<br>
	<div class="col-md-4" >		
		<?php if($model->status==0){ ?>			
			<a href="/reservations/<?php echo $model->id; ?>/status/2" class="btn btn-primary pull-right" style="margin-bottom: 15px;">Try Approving</a>
			<label style="font-size: 20px; background-color: #ffe3e3; padding: 4px 10px; border-radius: 5px;"><?php echo $reservation_status; ?></label>
		<?php } else { ?>
			<a href="/reservations/<?php echo $model->id; ?>/status/0" class="btn btn-danger pull-right">Cancel</a>
			<?php if($model->approved!=1){ ?>
				<a href="/reservations/<?php echo $model->id; ?>/status/1" class="btn btn-primary pull-right" style="margin-right: 10px;">Approve</a>
			<?php } ?>
		<?php } ?>
	</div>
</div>	

<style>
	.reservations-view {
		padding-bottom: 5px;
	}
	
	.reservations-view .form-group{
		margin-bottom: 5px;
	}
	.reservations-view .control-label{
		padding-right: 0;
	}
	
	#pickup_return_box {
		margin: 0; 
		padding: 10px 0; 
		background-color: #f3f3f3; 
		border: 1px solid #ccc; 
		border-radius: 5px; 
	}
		
	#pickup_return_box .form-control-static {
		font-size: 16px;
		margin-top: -2px;
	}
	
</style>

<?php if($model->pay_later_request == 1) { ?>
<div class="row" style="padding: 10px 25px 25px; background-color: #ffdd55;">
	<h4>Special Request to Pay Later</h4>
	<div>
		<?php echo $model->request_description; ?>
	</div>
</div>
<?php } ?>
<hr>
<div class="row" style="margin: 0;">
	<div class="col-md-7 well form-horizontal reservations-view">
		<div id="pickup_return_box" class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="col-md-5 control-label">Pickup Date:</label>
					<div class="col-md-7">
						<p class="form-control-static"><?php echo date("j-M-y <b>H:i</b>", strtotime($model->pickup_datetime)); ?></p>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-5 control-label">Pickup From:</label>
					<div class="col-md-7">
						<p class="form-control-static"><?php echo $model->pickup_location; ?></p>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="col-md-5 control-label">Return Date:</label>
					<div class="col-md-7">
						<p class="form-control-static"><?php echo date("j-M-y <b>H:i</b>", strtotime($model->return_datetime)); ?></p>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-5 control-label">Return To:</label>
					<div class="col-md-7">
						<p class="form-control-static"><?php echo $model->return_location; ?></p>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row" style="margin-top: 20px;">
			<div class="col-md-6">
				<div class="form-group">
					<label class="col-md-5 control-label">Payment:</label>
					<div class="col-md-7">
						<p class="form-control-static"><?php echo ($model->payment_completed == 1?"Completed":"Pending"); ?></p>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-5 control-label">Reserved By:</label>
					<div class="col-md-7">
						<p class="form-control-static"><?php echo $model->user_fullname; ?></p>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-5 control-label">Primary Driver:</label>
					<div class="col-md-7">
						<p class="form-control-static"><?php echo $model->primary_driver_name; ?></p>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="col-md-6 control-label">Status:</label>
					<div class="col-md-6">
					  <p class="form-control-static"><?php echo $reservation_status; ?></p>
					</div>
				</div>
				<?php if($model->approved == 1) { ?>				
				<div class="form-group">
					<label class="col-md-6 control-label">Approved By:</label>
					<div class="col-md-6">
						<p class="form-control-static"><?php echo $model->approved_user_name; ?></p>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-6 control-label">Approved At:</label>
					<div class="col-md-6">
					  <p class="form-control-static"><?php echo date("j-M-y H:i", strtotime($model->approved_at)); ?></p>
					</div>
				</div>
				<?php } ?>
				
				<div class="form-group">
					<label class="col-md-6 control-label">Created:</label>
					<div class="col-md-6">
						<p class="form-control-static"><?php echo date("j-M-y", strtotime($model->created_at)); ?></p>
					</div>
				</div>
			</div>
		</div>		
	</div>

	<div class="col-md-5" style="padding-right: 0px;">	
		<div class="row" >
			<div class="col-md-7">
				<div class="btn-group"> 
					<button aria-expanded="false" aria-haspopup="true" data-toggle="dropdown" class="btn btn-success dropdown-toggle" type="button"> 
						Download Docs &nbsp; <i class="fa fa-download"></i>
					</button> 
					<ul class="dropdown-menu"> 
						<li><a href="/reservations/<?php echo $model->id; ?>/docs/agreement/en" target="_blank">Rental Agreement [EN]</a></li>
						<li><a href="/reservations/<?php echo $model->id; ?>/docs/terms/en" target="_blank">Rental Terms [EN]</a></li>
						<li class="divider" role="separator"></li> 
						<li><a href="/reservations/<?php echo $model->id; ?>/docs/agreement/sq" target="_blank">Kontrate Qiraje [SQ]</a></li>
						<li><a href="/reservations/<?php echo $model->id; ?>/docs/terms/sq" target="_blank">Kushtet e Kontrates [SQ]</a></li>
					</ul> 
				</div>
			</div>
			<div class="col-md-5" style="text-align: right;">
				<!--a href="/reservations/<?php echo $model->id; ?>/dropoff" class="btn btn-info pull-right">DropOff - END &nbsp; <span class="glyphicon glyphicon-check"></span></a-->
			</div>
		</div>
		<hr>
		<h4>Entries/Details</h4>
		<table class="table table-striped table-bordered">
			<thead>
				<tr>
					<th>Name</th>
					<th>Quantity</th>
					<th style="text-align: right;">Price</th>
				</tr>
			</thead>
			<tbody>
			<?php 
			$recordsCount = count($entries);	
			for($i=0; $i<$recordsCount; $i++) { ?>	
				<tr>
					<td><?php echo $entries[$i]->entry_name; ?></td>
					<td><?php echo $entries[$i]->quantity; ?></td>
					<td style="text-align: right;"><?php echo number_format($entries[$i]->price, 2, '.', ' '); ?></td>
				</tr>
			<?php } ?>
			</tbody>
			<tfoot>				
				<tr>
					<td colspan="3" style="font-size: 18px; text-align: right;">
						<span style="font-size: 16px; font-weight: 600; margin-right: 40px;">Total Price: </span> 
						EUR &nbsp; <span style="min-width: 70px; float: right;font-weight: 600; " ><?php echo number_format($model->total_price, 2, '.', ' '); ?></span> 
					</td>
				</tr>
				<tr>
					<td colspan="3" style="font-size: 18px; text-align: right;">
					  <?php if($model->approved == 1 && $model->id_user_approved == 0 && $model->payment_completed == 1 && $model->security_deposit_refunded == 0) { ?>	
						<a class="btn btn-warning btn-sm" style="margin-right: 15px;" href="/payments/<?php echo $model->id_payment; ?>/refund/deposit">Refund</a>
					  <?php } ?>
						<span style="font-size: 16px; font-weight: 600; margin-right: 40px;">Security Deposit: </span>
						EUR &nbsp; <span style="min-width: 70px; float: right;font-weight: 600; " ><?php echo number_format($model->security_deposit, 2, '.', ' '); ?></span> 
					</td>
				</tr>
			</tfoot>
		</table>
	</div>
	
</div>	

<hr>

<div class="row">
	<h4 class="col-md-6 pull-left">Drivers</h4>
	<div class="col-md-6">
		<a href="/drivers/create/<?php echo $model->id; ?>" class="btn btn-success pull-right"><span class="glyphicon glyphicon-plus"></span> Add New Driver</a>
		<a href="/drivers/select/<?php echo $model->id; ?>" class="btn btn-default pull-right" style="margin-right: 20px;">Add Existing Driver</a>
	</div>
</div>
<table class="table table-striped table-bordered">
    <thead>
        <tr>
			<th>Driver Name</th>
			<th>License</th>
			<th>Passport/ID</th>
			<th>Born</th>
			<th>Email</th>
			<th>Phone</th>
			<th>Address</th>
			<th>&nbsp;</th>
        </tr>
    </thead>
    <tbody>
	<?php 
	$recordsCount = count($drivers);	
	for($i=0; $i<$recordsCount; $i++) { ?>	
        <tr>
            <td style="text-align: center;" >
				<a class="btn btn-link btn-block" style="padding: 3px 3px 8px 3px;" href="/drivers/<?php echo $drivers[$i]->id; ?>/edit">
					<?php echo $drivers[$i]->firstname.' '.$drivers[$i]->lastname; ?> &nbsp; <span class="glyphicon glyphicon-edit"></span>
				</a>
				<?php if($drivers[$i]->primary == 0) { ?>	
					<a class="btn btn-default btn-sm" href="/drivers/<?php echo $drivers[$i]->id.'/primary/'.$model->id; ?>">
						Set As Primary
					</a>
				<?php } else {?>
					<label>Primary Driver</label>
				<?php } ?>	
			</td>
		    <td><?php echo $drivers[$i]->license_number; ?><br><small>Exp: <?php echo $drivers[$i]->license_expiration; ?></small></td>
		    <td><?php 
					$str = "";
					if($drivers[$i]->passport != "")
						$str = 'Pass: '.$drivers[$i]->passport;
					
					if($drivers[$i]->idcard != ""){
						if($str != "")
							$str .= '<br>'; 					
						$str .= 'ID: '.$drivers[$i]->idcard;
					}
					echo $str;
			?></td>
		    <td><?php echo $drivers[$i]->birthdate; ?><br><small><?php echo $drivers[$i]->birthplace.', '.$drivers[$i]->birth_country; ?></small></td>
			<td><?php echo $drivers[$i]->email; ?></td>
		    <td><?php echo $drivers[$i]->phone_1.($drivers[$i]->phone_2!=''?'<br>'.$drivers[$i]->phone_2:''); ?></td>
			<td><?php echo $drivers[$i]->address.'<br>'.$drivers[$i]->zip_code.' '.$drivers[$i]->city.', '.$drivers[$i]->address_country; ?></td>
			<td>
				<?php if($drivers[$i]->primary == 0) { ?>	
					<button class="btn btn-warning btn-sm btn-block" style="margin-bottom: 5px; min-width: 80px;"
							onclick="Utils.confirmDeletion('drivers','<?php echo $drivers[$i]->id."/remove/".$model->id."','You are about to remove <b>".$drivers[$i]->firstname." ".$drivers[$i]->lastname; ?></b> from this reservation which you still may added it later. It will not be deleted from the system. Do you want to continue', '', 'Yes, Remove Driver');">
						<span class="glyphicon glyphicon-remove"></span> Remove
					</button>
					<button class="btn btn-danger btn-sm btn-block" 
							onclick="Utils.confirmDeletion('drivers','<?php echo $drivers[$i]->id."','Are you sure you want to PERMANENTLY delete <b>".$drivers[$i]->firstname." ".$drivers[$i]->lastname; ?></b> from the system', '');">
						Delete <span class="glyphicon glyphicon-trash"></span>
					</button>
				<?php } else {?>
					<em>Primary<br>Driver</em>
				<?php } ?>	
			</td>
        </tr>
	<?php } ?>
    </tbody>
</table>

<hr>

<div class="row">
	<h4 class="col-md-6 pull-left">Payments</h4>
	<div class="col-md-6">
		<?php if($model->status==1){ ?>
			<a href="/payments/create/<?php echo $model->id; ?>" class="btn btn-default pull-right">Create New Payment</a>
		<?php } ?>
	</div>
</div>
<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th>Price</th>
			<th>Method</th>
			<th>Type</th>
			<th>Status</th>
			<th>Verified By</th>
			<th>Details</th>
			<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>
	<?php 
	$paymentsCount = count($payments);	
	for($i=0; $i<$paymentsCount; $i++) { ?>	
		<tr>
			<td>EUR <?php echo number_format($payments[$i]->amount, 2, '.', ' '); ?></td>
			<td><?php echo $payments[$i]->paymentmethod; ?></td>
			<td><?php echo $payments[$i]->paymenttype; ?></td>
			<td><?php echo $payments[$i]->status == 1?'Verified':'Pending'; ?></td>
			<td><?php echo $payments[$i]->verified_by; ?></td>
			<td><?php echo $payments[$i]->details.(trim($payments[$i]->details)==""?'':'<br>').'Date: '.date("j-M-y H:i", strtotime($payments[$i]->paid_at)); ?></td>
			<td>
				<?php 
				if($payments[$i]->status >= 1 && $payments[$i]->id_paymentmethod==1 && $payments[$i]->id_user_verified==0) 
				{
					if($payments[$i]->status == 1) { ?>
					<a class="btn btn-warning btn-sm btn-block" style="margin-bottom: 5px;"
					   href="/payments/<?php echo $payments[$i]->id; ?>/refund">Refund</a>
				<?php }
				} else { ?>
					<a class="btn btn-info btn-sm btn-block" href="/payments/<?php echo $payments[$i]->id; ?>/edit">Edit</a>
				<?php } ?>		
			</td>          
		</tr>
	<?php } ?>
	</tbody>
</table>

<?php echo View::make('partials.footer') ?>