<?php echo View::make('partials.header') ?>

<div class="row">
	<form class="form-horizontal col-md-6" action="/rolerights" method="post">
		<?php $model = isset($model)?$model:[]; ?>
		<h2>Create/Edit Role Rights </h2></br>
		
		<?php if (count($errors) > 0) { ?>
			<div class="alert alert-danger">
				<ul>
					<?php foreach ($errors->all() as $error){ ?>
						<li><?php echo $error; ?></li>
					<?php } ?>
				</ul>
			</div>
		<?php } ?>
		
		<!-- FORM INPUTS start -->
		
		<div class="form-group">
			<label for="id_role" class="col-md-3 control-label">Id Role</label>
			<div class="col-md-5">
				<input type="input" name="id_role" id="id_role" value="<?php echo isset($model['id_role'])?$model['id_role']:''; ?>" class="form-control" placeholder="Id role">
			</div>
		</div>
		<div class="form-group">
			<label for="route_url" class="col-sm-3 control-label">Route Url</label>
			<div class="col-sm-9">
				<input type="input" name="route_url" id="route_url" value="<?php echo isset($model['route_url'])?$model['route_url']:''; ?>" class="form-control" placeholder="Route url">
			</div>
		</div>
		<div class="form-group">
			<label for="access" class="col-sm-3 control-label">Access</label>
			<div class="col-sm-9">
				<input type="input" name="access" id="access" value="<?php echo isset($model['access'])?$model['access']:''; ?>" class="form-control" placeholder="Access">
			</div>
		</div>
		
		<!-- FORM INPUTS end -->
		
		
		<?php if(isset($model['id'])): ?>
			<input type="hidden" name="id" value="<?php echo $model['id']; ?>">		
		<?php endif; ?>
		<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">		
		<div class="form-group">
			<div class="col-md-offset-3 col-sm-8">
				<button type="submit" class="btn btn-primary">Save Changes</button>
			</div>
		</div>
	</form>
</div>


<?php echo View::make('partials.footer') ?>