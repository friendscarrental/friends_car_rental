<?php echo View::make('partials.header') ?>

<div class="row">
	<form class="form-horizontal col-md-offset-2 col-md-8" action="/payments" method="post">
		<?php $model = isset($model)?$model:[]; ?>
		<h2>Create/Edit Payment</h2>
		<hr>
		</br>
		<?php if (count($errors) > 0) { ?>
			<div class="alert alert-danger">
				<ul>
					<?php foreach ($errors->all() as $error){ ?>
						<li><?php echo $error; ?></li>
					<?php } ?>
				</ul>
			</div>
		<?php } ?>
		
		<!-- FORM INPUTS start -->
		
		<div class="form-group required">
			<label for="amount" class="col-md-3 control-label">Amount</label>
			<div class="col-md-3">
				<input type="input" name="amount" id="amount" value="<?php echo isset($model['amount'])?$model['amount']:''; ?>" class="form-control" placeholder="Vehicle Type">
			</div>	
			<div class="col-md-2">
				<p class="form-control-static" style="margin-left: -12px; padding-left: 0px;">Euro</p>
			</div>
		</div>
		<div class="form-group">
			<label for="id_paymentmethod" class="col-sm-3 control-label">Method</label>
			<div class="col-sm-4">				
				<select name="id_paymentmethod" id="id_paymentmethod" class="form-control">
					<?php foreach ($paymentmethods as $paymentmethod){ ?>
						<option value="<?php echo $paymentmethod->id; ?>" <?php if(isset($model['id_paymentmethod']) && $paymentmethod->id == $model['id_paymentmethod']) echo 'selected="selected"'; ?> ><?php echo $paymentmethod->name; ?></option>
					<?php } ?>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label for="id_paymenttype" class="col-sm-3 control-label">Type</label>
			<div class="col-sm-4">				
				<select name="id_paymenttype" id="id_paymenttype" class="form-control">
					<?php foreach ($paymenttypes as $paymenttype){ ?>
						<option value="<?php echo $paymenttype->id; ?>" <?php if(isset($model['id_paymenttype']) && $paymenttype->id == $model['id_paymenttype']) echo 'selected="selected"'; ?> ><?php echo $paymenttype->name; ?></option>
					<?php } ?>
				</select>
			</div>
		</div>
		<div class="form-group" style="margin-top: 30px; margin-bottom: 30px;">
			<label class="col-sm-3 control-label">Status:</label>
			<div class="col-sm-9">				
				<?php if(isset($model['id']) && $model->id_user_verified >= 0){ ?>
					<p class="form-control-static">Verified by <strong><em><?php echo $model->verified_by; ?></em></strong></p>
					<input type="hidden" name="status" value="1">
				<?php } else { ?>
					<label class="radio-inline">
						<input type="radio" name="status" value="0" <?php if( !isset($model['id']) || (isset($model['status']) && $model['status']==0)) echo 'checked="checked"'; ?>> Pending
					</label>
					<label class="radio-inline">
						<input type="radio" name="status" value="1" <?php if(isset($model['id']) && $model['status']==1) echo 'checked="checked"'; ?>> Verified Payment
					</label>
				<?php } ?>
			</div>
		</div>
		
		<div class="form-group">
			<label for="details" class="col-md-3 control-label">Details</label>
			<div class="col-md-8">
				<textarea name="details" id="details" class="form-control" rows="2" placeholder="Payment Details"><?php echo isset($model['details'])?$model['details']:''; ?></textarea>
			</div>
		</div>
		
				
		<!-- FORM INPUTS end 
		
		id_paymentmethod == 1
		
		id_user_verified == 0 
		status == 1 
		-->
		
		<?php if(isset($model['id'])): ?>
			<input type="hidden" name="id" value="<?php echo $model['id']; ?>">		
		<?php endif; ?>
		<input type="hidden" name="reservation_id" value="<?php echo $reservation_id; ?>">	
		<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
		<hr>
		<div class="form-group">
			<div class="col-md-offset-3 col-md-9">
				<?php 
				if(isset($model['id']) && $model['status']>=1 && $model['id_paymentmethod']==1 && $model['id_user_verified']==0) 
				{
					if($model['status'] == 1) { ?>
						<a href="/payments/<?php echo $model['id']; ?>/refund" class="btn btn-warning" style="padding-top: 5px;">
							<span>Refund Payment</span>
							<i class="fa fa-cc-paypal" style="font-size: 2em; vertical-align: -15%; margin: 0 -3px -5px 5px"></i>
						</a>
				<?php }
				} else { ?>
					<button type="submit" name="save" value="save" class="btn btn-primary">Save Changes</button>
					<button type="submit" name="pay_now" value="paypal" class="btn btn-yellow" style="padding-top: 5px; margin-left: 30px;">
						<span>Pay Now with</span> 
						<i class="fa fa-cc-paypal" style="font-size: 2em; vertical-align: -15%; margin: 0 -3px -5px 5px"></i>
					</button>
				<?php } ?>
			</div>
		</div>
		
	</form>
</div>


<?php echo View::make('partials.footer') ?>