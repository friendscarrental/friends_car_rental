<?php echo View::make('partials.header') ?>
<br>

<div class="row">
	<form class="form-horizontal" action="/drivers" method="post">
		<?php if (count($errors) > 0) { ?>
			<div class="alert alert-danger">
				<ul>
					<?php foreach ($errors->all() as $error){ ?>
						<li><?php echo $error; ?></li>
					<?php } ?>
				</ul>
			</div>
		<?php } ?>
		
		<!-- FORM INPUTS start -->
		<div class="row">
		  <div class="col-md-7">
		    <h2>Edit Driver Details</h2>
			<hr>
			<div class="form-group required">
				<label for="firstname" class="col-md-3 control-label">Firstname</label>
				<div class="col-md-8">
					<input type="input" name="firstname" id="firstname" value="<?php echo $model['firstname']; ?>" class="form-control" placeholder="Firstname">
				</div>
			</div>
			<div class="form-group required">
				<label for="lastname" class="col-md-3 control-label">Lastname</label>
				<div class="col-md-8">
					<input type="input" name="lastname" id="lastname" value="<?php echo $model['lastname']; ?>" class="form-control" placeholder="Lastname">
				</div>
			</div>
			<div class="form-group required">
				<label for="email" class="col-md-3 control-label">Email</label>
				<div class="col-md-8">
					<input type="input" name="email" id="email" value="<?php echo $model['email']; ?>" class="form-control" placeholder="Email">
				</div>
			</div>
			<div class="form-group required">
				<label for="birthdate" class="col-md-3 control-label">Birth Date</label>
				<div class="col-md-8 form-inline">
					<!-- input type="input" name="birthdate" id="birthdate" value="< ?php echo $model['birthdate']; ?>" class="form-control" placeholder="Birthdate" -->
					<select name="birth_day" id="birth_day" class="form-control">
						<option value="" selected="selected">Day</option>
						<?php for($i=1; $i <=31; $i++){ 
								$value = str_pad($i, 2, '0', STR_PAD_LEFT); ?>
							<option value="<?php echo $value; ?>" <?php if($value == $model['birth_day']) echo'selected="selected"'; ?> ><?php echo $i; ?></option>
						<?php } ?>
					</select>
					<select name="birth_month" id="birth_month" class="form-control">
						<option value="" selected="selected">Month</option>
						<?php for($i=1; $i <=12; $i++){ 
								$value = str_pad($i, 2, '0', STR_PAD_LEFT); ?>
							<option value="<?php echo $value; ?>" <?php if($value == $model['birth_month']) echo'selected="selected"'; ?> ><?php echo $i; ?></option>
						<?php } ?>
					</select>							
					<input type="input" name="birth_year" id="birth_year" value="<?php echo $model['birth_year']; ?>" class="form-control" style="max-width: 150px;" placeholder="Year">
				</div>
			</div>
			<div class="form-group required">
				<label for="birthplace" class="col-md-3 control-label">Birth Place</label>
				<div class="col-md-8">
					<input type="input" name="birthplace" id="birthplace" value="<?php echo $model['birthplace']; ?>" class="form-control" placeholder="Birth Place">
				</div>
			</div>
			<div class="form-group required">
				<label for="id_country_birth" class="col-md-3 control-label">Country of Birth</label>
				<div class="col-md-8">
					<select name="id_country_birth" id="id_country_birth" class="form-control">
						<option value="" <?php if(!isset($model['id_country_birth'])) echo'selected="selected"'; ?> >Select a Country</option>
						<?php foreach ($countries as $country){ ?>
							<option value="<?php echo $country->id; ?>" <?php if($country->id == $model['id_country_birth']) echo'selected="selected"'; ?> ><?php echo $country->name; ?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<hr>
			<div class="form-group required">
				<label for="license_number" class="col-md-3 control-label">License No</label>
				<div class="col-md-8">
					<input type="input" name="license_number" id="license_number" value="<?php echo $model['license_number']; ?>" class="form-control" placeholder="License Number">
				</div>
			</div>
			<div class="form-group required">
				<label for="license_expiration" class="col-md-3 control-label">License Exp</label>
				<div class="col-md-8 form-inline">
					<!-- input type="input" name="license_expiration" id="license_expiration" value="< ?php echo $model['license_expiration']; ?>" class="form-control" placeholder="Expiry Date" -->
					<select name="license_exp_day" id="license_exp_day" class="form-control">
						<option value="" selected="selected">Day</option>
						<?php for($i=1; $i <=31; $i++){ 
								$value = str_pad($i, 2, '0', STR_PAD_LEFT); ?>
							<option value="<?php echo $value; ?>" <?php if($value == $model['license_exp_day']) echo'selected="selected"'; ?> ><?php echo $i; ?></option>
						<?php } ?>
					</select>
					<select name="license_exp_month" id="license_exp_month" class="form-control">
						<option value="" selected="selected">Month</option>
						<?php for($i=1; $i <=12; $i++){ 
								$value = str_pad($i, 2, '0', STR_PAD_LEFT); ?>
							<option value="<?php echo $value; ?>" <?php if($value == $model['license_exp_month']) echo'selected="selected"'; ?> ><?php echo $i; ?></option>
						<?php } ?>
					</select>
					
					<input type="input" name="license_exp_year" id="license_exp_year" value="<?php echo $model['license_exp_year']; ?>" class="form-control" style="max-width: 150px;" placeholder="Year">					
				</div>
			</div>
			
			<div class="form-group required">
				<label for="id_country_license" class="col-md-3 control-label">Country</label>
				<div class="col-md-8">
					<select name="id_country_license" id="id_country_license" class="form-control">
						<option value="" <?php if(!isset($model['id_country_license'])) echo'selected="selected"'; ?> >Select a Country</option>
						<?php foreach ($countries as $country){ ?>
							<option value="<?php echo $country->id; ?>" <?php if($country->id == $model['id_country_license']) echo'selected="selected"'; ?> ><?php echo $country->name; ?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group required">
				<label for="passport" class="col-md-3 control-label">Passport</label>
				<div class="col-md-8">
					<input type="input" name="passport" id="passport" value="<?php echo $model['passport']; ?>" class="form-control" placeholder="Passport Number">
				</div>
			</div>
			<div class="form-group required">
				<label for="idcard" class="col-md-3 control-label">ID Card</label>
				<div class="col-md-8">
					<input type="input" name="idcard" id="idcard" value="<?php echo $model['idcard']; ?>" class="form-control" placeholder="ID Card Number">
				</div>
			</div>
			
			<hr>
			<!-- FORM INPUTS end -->
			
			<input type="hidden" name="id" value="<?php echo $model['id']; ?>">	
			<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
			
			<div class="form-group">
				<div class="col-md-offset-4 col-sm-7">
					<button type="submit" class="btn btn-primary">Save Changes</button>
				</div>
			</div>
		  </div>

		  <div class="col-md-5">
			<div class="row" style="margin-top: 10px; margin-bottom: 5px;">
				<h3 class="col-md-7 pull-left">Driver Addresses</h3>				
				<a href="/address/create/<?php echo $model['id']; ?>" class="btn btn-success pull-right"  style="margin-top: 10px;">New Address</a>
			</div>
			<?php foreach($addresses as $address) { ?>
				<div class="row well">
					<div class="col-md-offset-2 col-md-10" style="margin-bottom: 10px;">
						<h4 class="pull-left"><?php echo $address->type_home==1?'Primary':'Other'; ?> Address</h4>
						<?php if($address->type_home==0) { ?>
							<a href="/address/<?php echo $address->id; ?>/primary" class="btn btn-primary pull-right">Set As Primary</a>
						<?php } ?>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label">Name:</label>
						<div class="col-md-8">
						  <p class="form-control-static"><?php echo $address->name; ?></p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label">Address:</label>
						<div class="col-md-8">
						  <p class="form-control-static">
								<?php echo $address->address; ?><br>
								<?php echo $address->zip_code.' '.$address->city.' '.$address->address_country; ?></p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label">Phone:</label>
						<div class="col-md-8">
						  <p class="form-control-static"><?php echo $address->phone_1.($address->phone_2!=''?'<br>'.$address->phone_2:''); ?></p>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-offset-4 col-sm-8">
							<a href="/address/<?php echo $address->id; ?>/edit" class="btn btn-warning">Edit Address <span class="glyphicon glyphicon-edit"></span></a>
							<?php if($address->type_home==0) { ?>
								<button class="btn btn-danger" style="margin-left: 20px;" type="button"
									onclick="Utils.confirmDeletion('address','<?php echo $address->id."','".$address->address; ?>');">
									Remove <span class="glyphicon glyphicon-trash"></span>
								</button>
							<?php } ?>
						</div>
					</div>
				</div>	
			<?php } ?>
		  </div>	
		</div>	
	</form>
</div>


<?php echo View::make('partials.footer') ?>