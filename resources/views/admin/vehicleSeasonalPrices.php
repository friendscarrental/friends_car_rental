<?php echo View::make('partials.header') ?>
<br>
<div class="row">
	<h2 class="col-md-8 pull-left">Seasonal/Range Prices for <b><a href="/vehicles/<?php echo $vehicle->id; ?>"><?php echo $vehicle->title; ?></a></b></h2>
	<br>
		
	<div class="col-md-4">
		<a href="/vehicles/<?php echo $vehicle->id; ?>/edit" class="btn btn-info pull-right">Edit Car Details</a>
		<a href="/vehicles" class="btn btn-default pull-right" style="margin-right: 20px;">Vehicles List</a>
	</div>
</div>
<br>
<div class="well">
	<h3>Standard Price: <b><?php echo $vehicle->standard_price; ?> &euro;/day</b></h3>
</div>
<table class="table table-striped table-hover table-bordered">
    <thead>
        <tr>
			<th>#</th>
			<th class="col-md-3">Season Name</th>
			<th class="col-md-3" colspan="2">Start Date</th>
			<th class="col-md-3" colspan="2">End Date</th>
			<th class="col-md-2">Price</th>
			<th>&nbsp;</th>
        </tr>
    </thead>
    <tbody>
	<?php 
	$months = ['01'=>'Jan.','02'=>'Feb.','03'=>'Mar.','04'=>'Apr.','05'=>'May','06'=>'Jun.','07'=>'Jul.','08'=>'Aug.','09'=>'Sep.','10'=>'Oct.','11'=>'Nov.','12'=>'Dec.'];

	$recordsCount = count($records);	
	for($i=0; $i<$recordsCount; $i++) { ?>	
        <tr>
			<th scope="row"><?php echo $i+1; ?></th>
			<td><?php echo $records[$i]->name; ?></td>
			<td colspan="2"><?php echo $months[$records[$i]->start_season_month].' '.$records[$i]->start_season_day; ?></td>
			<td colspan="2"><?php echo $months[$records[$i]->end_season_month].' '.$records[$i]->end_season_day; ?></td>
			<td><?php echo $records[$i]->price; ?> &euro;/day</td>
			<td><button class="btn btn-danger btn-sm pull-right" 
						onclick="Utils.confirmDeletion('vehicles', 'seasonal-prices/<?php echo $records[$i]->id; ?>', 'this price range');">
					<span class="glyphicon glyphicon-trash"></span>
				</button>
			</td>
        </tr>
	<?php } ?>
	<form class="form-inline" action="/vehicles/seasonal-prices" method="post">
		<tr class="success">
			<th scope="row">NEW</th>
			<td>Season Name:<input type="input" name="name" class="form-control" placeholder="Name"></td>
			<td>Month: <select name="start_date_month" class="form-control">
					<?php foreach($months as $key=>$value){ ?>
						<option value="<?php echo $key; ?>" ><?php echo $value; ?></option>
					<?php } ?>
				</select>
			</td>
			<td>Day: <select name="start_date_day" class="form-control">
					<?php for($day=1; $day <=31; $day++){ ?>
						<option value="<?php echo str_pad($day, 2, '0', STR_PAD_LEFT); ?>" ><?php echo $day; ?></option>
					<?php } ?>
				</select>
			</td>			
			<td>Month: <select name="end_date_month" class="form-control">
					<?php foreach($months as $key=>$value){ ?>
						<option value="<?php echo $key; ?>" ><?php echo $value; ?></option>
					<?php } ?>
				</select>
			</td>			
			<td>Day: <select name="end_date_day"class="form-control">
					<?php for($day=1; $day <=31; $day++){ ?>
						<option value="<?php echo str_pad($day, 2, '0', STR_PAD_LEFT); ?>" ><?php echo $day; ?></option>
					<?php } ?>
				</select>
			</td>
			<td>Range Price:<input type="input" name="price" class="form-control" placeholder="Price"></td>
			<td>
				<input type="hidden" name="vehicle_id" value="<?php echo $vehicle->id; ?>">
				<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
				<button type="submit" class="btn btn-success btn-md" style="margin-top: 18px; width: 120px;">Add New</button>
			</td>          
        </tr>
	</form>
    </tbody>
</table>

<?php if (count($errors) > 0) { ?>
	<div class="alert alert-danger">
		<ul>
			<?php foreach ($errors->all() as $error){ ?>
				<li><?php echo $error; ?></li>
			<?php } ?>
		</ul>
	</div>
<?php } ?>

<?php echo View::make('partials.footer') ?>