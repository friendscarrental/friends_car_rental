<?php echo View::make('partials.header') ?>

<div class="row">
<form class="form-horizontal col-md-offset-2 col-md-8" action="/locations/save-open-hours" method="post">
	<h2>Operational Hours for <b><?php echo $location->name; ?></b></h2></br>
	
	<?php if (count($errors) > 0) { ?>
		<div class="alert alert-danger">
			<ul>
				<?php foreach ($errors->all() as $error){ ?>
					<li><?php echo $error; ?></li>
				<?php } ?>
			</ul>
		</div>
	<?php } ?>
	
	
	<table class="table table-striped table-hover">
		<thead>
			<tr>
				<th>#</th>
				<th>Week Day</th>
				<th>Open Time</th>
				<th>Close Time</th>
				<th>Closed</th>
			</tr>
		</thead>
		<tbody>
		<?php foreach($data as $day) { ?>	
			<tr>
				<th scope="row"><?php echo $day->week_day; ?></th>
				<th scope="row"><?php echo $weekDaysArray[$day->week_day]; ?></th>
				<td><input type="input" name="<?php echo $day->id.'-open_time-'.$day->week_day; ?>" value="<?php echo isset($day->open_time)?$day->open_time:'8'; ?>" class="form-control" placeholder="Open"></td>
				<td><input type="input" name="<?php echo $day->id.'-close_time-'.$day->week_day; ?>" value="<?php echo isset($day->close_time)?$day->close_time:'20'; ?>" class="form-control" placeholder="Close"></td>
				<td><input type="checkbox" name="<?php echo $day->id; ?>-not_operational" value="1" <?php if( isset($day->not_operational) && $day->not_operational==1) echo 'checked="checked"'; ?> class="form-control"></td>
			</tr>
		<?php } ?>
		</tbody>
	</table>
	<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">	
	<hr>	
	<div class="form-group">
		<div class="col-md-offset-4 col-sm-8">
			<button type="submit" class="btn btn-primary">Save Changes</button>
		</div>
	</div>
</form>
</div>


<?php echo View::make('partials.footer') ?>