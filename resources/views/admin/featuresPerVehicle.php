<?php echo View::make('partials.header') ?>
<br>
<div class="row">
	<h2 class="col-md-7 pull-left">Features for <b><a href="/vehicles/<?php echo $vehicle->id; ?>"><?php echo $vehicle->title; ?></a></b></h2>
	<br>
		
	<div class="col-md-5">
		<a href="/vehicles/<?php echo $vehicle->id; ?>/edit" class="btn btn-info pull-right">Edit Car Details</a>
		<a href="/vehicles" class="btn btn-default pull-right" style="margin-right: 20px;">Vehicles List</a>
		<a href="/features" class="btn btn-default pull-right" style="margin-right: 20px;">Features List</a>
	</div>
</div>
<br>
<hr>
<div class="row">
<form class="form-horizontal col-md-offset-2 col-md-8" action="/features/per-vehicle" method="post">
	
	<?php if (count($errors) > 0) { ?>
		<div class="alert alert-danger">
			<ul>
				<?php foreach ($errors->all() as $error){ ?>
					<li><?php echo $error; ?></li>
				<?php } ?>
			</ul>
		</div>
	<?php } ?>		
			
	<table class="table table-striped table-hover">
		<thead>
			<tr>
				<th class="col-md-1">#</th>
				<th class="col-md-3">Feature Name</th>
				<th class="col-md-3" colspan="2" style="text-align: center;">Icon Type</th>
				<th class="col-md-3">Value/Amount</th>
				<th class="col-md-1">Applicable</th>
			</tr>
		</thead>
		<tbody>
		<?php 
		$recordsCount = count($records);	
		for($i=0; $i<$recordsCount; $i++) { ?>	
        <tr class="<?php echo $records[$i]->applicable==0?'active':'success'; ?>">
			<th scope="row"><?php echo $i+1; ?></th>			
		    <td><?php echo $records[$i]->name; ?></td>		    
			<td style="text-align:right"><?php echo $records[$i]->icon_type==0?'Label':($records[$i]->icon_type==1?'Code':'Image'); ?></td>
			<td><?php if($records[$i]->icon_type==1){
						echo '<i class="fa '.$records[$i]->icon_value.'"></i> '.$records[$i]->icon_value;
					} else if($records[$i]->icon_type==2) {
						echo '<img src="/images/uploads/'.$records[$i]->icon_value.'" class="thumb sm"></img> ';
					} else {
						echo $records[$i]->icon_value;
					}
					?></td>
			<td><input type="text" name="<?php echo $records[$i]->id.'-value'; ?>" value="<?php echo $records[$i]->value; ?>" class="form-control" placeholder="Value" <?php echo $records[$i]->applicable==0?'disabled':''; ?>></td>
			<td><input class="form-control applicable-chbox" type="checkbox" name="<?php echo $records[$i]->id; ?>-applicable" value="1" <?php echo $records[$i]->applicable==1?'checked="checked"':''; ?>></td>
		</tr>			
		<?php } ?>
		</tbody>
	</table>
	<hr>
	<input type="hidden" name="vehicle_id" value="<?php echo $vehicle->id; ?>">
	<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">		
	<div class="form-group">
		<div class="col-md-offset-4 col-sm-8">
			<button type="submit" class="btn btn-primary">Save Changes</button>
		</div>
	</div>
</form>
</div>

<script type='text/javascript'>
	$(document).ready(function(){
		$('input.applicable-chbox').click(function(){
			if($(this).is(":checked")){
				$(this).parent().parent().removeClass('active').addClass('success');
				$(this).parent().parent().find(':text').removeAttr('disabled');
			} else {
				$(this).parent().parent().removeClass('success').addClass('active');
				$(this).parent().parent().find(':text').attr("disabled","disabled");
			}
		});
	});
</script>


<?php echo View::make('partials.footer') ?>