<?php echo View::make('partials.header') ?>

<style>
	.driver-details .form-group {
		margin-bottom: 0;
	}
	
	.ac-item {
		margin: 5px -5px 5px -8px;
	}
	
	.acmarem .ac-heading {
		font-size: 14px;
		font-weight: 600;
		margin-bottom: 0;
	}
	.ac-item .ac-email {
		text-align: right;
	}
	
	.driver-details {
		border: 1px solid #eee;	
		border-radius: 3px;
		background-color: #f6f6f6;
		margin: 10px 10px 20px 0px;
		padding-top: 10px;
		padding-bottom: 10px;
	}
	
</style>

<div class="row">
	<form action="/drivers/select" method="post">		
		<div class="row">
			<center><h2>Select Driver</h2></center>
		</div>
		
		<?php if (count($errors) > 0) { ?>
			<div class="alert alert-danger">
				<ul>
					<?php foreach ($errors->all() as $error){ ?>
						<li><?php echo $error; ?></li>
					<?php } ?>
				</ul>
			</div>
		<?php } ?>
		
		<hr>
		<div class="row">
			<div class="col-md-8">
				<select id="driver" name="driver" class="form-control input-lg">					
					<?php if(Auth::user()->id_role >= 5 && isset($reservation_data)){ ?>
						<option value="<?php echo $reservation_data['driver']->id; ?>" selected="selected"><?php echo $reservation_data['driver']->text; ?></option>		
					<?php } ?>
				</select>
			</div>
			<div class="col-md-4">
				<a href="/drivers/create/<?php echo $reservation_id; ?>" class="btn btn-link" style="margin: 0px 0;">Create New Driver</a>
			</div>
		</div>
		<div id="primaryDriverDetails"></div>
		
		<input type="hidden" name="reservation_id" value="<?php echo $reservation_id; ?>">
		
		<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
		<hr>
		<div class="form-group">
			<div class="col-md-offset-3 col-md-8">
				<button type="submit" class="btn btn-primary" style="padding-top: 5px;">Add Driver to Reservation</button>
			</div>
		</div>
		
	</form>
</div>

<script type="text/template" id="driverDetailsTemplate">

	<div class="col-md-8 form-horizontal driver-details">
		<div class="col-md-6">
			<div class="form-group">
				<label class="col-sm-3 control-label">Firstname</label>
				<div class="col-sm-9">
					<p class="form-control-static"><%= data.firstname %> </p>
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 15px;">
				<label class="col-sm-3 control-label">Lastname</label>
				<div class="col-sm-9">
					<p class="form-control-static"><%= data.lastname %></p>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">Birthday</label>
				<div class="col-sm-9">
					<p class="form-control-static"><%= data.birthdate %></p>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">License</label>
				<div class="col-sm-9">
					<p class="form-control-static"><%= data.license_number %></p>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">Expiration</label>
				<div class="col-sm-9">
					<p class="form-control-static"><%= data.license_expiration %></p>
				</div>
			</div>							
		</div>
		<div class="col-md-6">
			<div class="form-group" style="margin-bottom: 15px;">
				<label class="col-sm-3 control-label">Email</label>
				<div class="col-sm-9">
					<p class="form-control-static"><%= data.email %></p>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">Address</label>
				<div class="col-sm-9">
					<p class="form-control-static"><%= data.address %></p>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">Postcode</label>
				<div class="col-sm-9">
					<p class="form-control-static"><%= data.zip_code %></p>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">City</label>
				<div class="col-sm-9">
					<p class="form-control-static"><%= data.city %></p>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">Country</label>
				<div class="col-sm-9">
					<p class="form-control-static"><%= data.address_country %></p>
				</div>
			</div>
			
		</div>
	</div>
	
</script>

<script>

    function formatData (data) 
	{
		if (data.loading) 
			return data.text;

		var markup = "<div class='row ac-item'>"
						+"<div class='col-md-6 ac-heading'>" + data.firstname +' '+ data.lastname + "</div>"
						+"<div class='col-md-6 ac-email'>" + data.email + "</div>"
				    +"</div>";

		return markup;
    }
	
	$(document).ready(function ()
	{
		console.log("ready");
		$("#driver").select2({
			placeholder: {
				id: '-1', // the value of the option
				text: 'Select an Existing Driver'
			},
			theme: "bootstrap",
			ajax: {
				url: "/drivers/search",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						term: params.term, // search term
						page: params.page
					};
				},
				processResults: function (data, params) {
					return {
						results: data
					};
				},
				cache: true
			},
			escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
			minimumInputLength: 2,
			templateResult: formatData,
			templateSelection: function (data) {
				return data.text || data.firstname +' '+ data.lastname;
			},
			allowClear: true
		})
		.on("change", function(e) {
			onDriverChange($(this).val(), 'primary');
        });
	});
	
	
	function onDriverChange(driverID, whichDriver)//primary secondary
	{
		if(driverID && driverID > -1)
		{
			$.ajax({
				type: "GET", 
				url: '/drivers/'+driverID+'/details',
				dataType: 'json', 
				success: function(response)
				{	
					var driverTemplate = _.template($("#driverDetailsTemplate").html());
					var driverContent = driverTemplate(response);
					$("#"+whichDriver+"DriverDetails").html(driverContent);				
				}
			});
		}
		else
		{
			$("#"+whichDriver+"DriverDetails").html("");
		}
	}
	
	
</script>

<?php echo View::make('partials.footer') ?>