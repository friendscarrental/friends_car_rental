<?php echo View::make('partials.header') ?>
<br>
<div class="row">
	<?php if(isset($user)){ ?>
		<h2>Drivers List of <b><a href="/users/<?php echo $user->id; ?>/edit"><?php echo $user->firstname.' '.$user->lastname; ?></a></b></h2>
	<?php }else{ ?>
		<h2 class="col-md-8 pull-left">All Drivers List</h2>
		<a href="/<?php echo $resourceName; ?>/create" class="btn btn-success pull-right">Create New</a>
	<?php } ?>
	<br>
</div>
<br>

<table class="table table-striped table-hover">
    <thead>
        <tr>
			<th>&nbsp;</th>
			<th>Name</th>
			<th>License</th>
			<th>Passport/ID</th>
			<th>Born</th>
			<th>Email</th>
			<th>Phone</th>
			<th>Address</th>
			<th>&nbsp;</th>
        </tr>
    </thead>
    <tbody>
	<?php 
	$recordsCount = count($records);	
	for($i=0; $i<$recordsCount; $i++) { ?>	
        <tr>
			<th scope="row">&nbsp;</th>
            <td><a href="<?php echo "/".$resourceName."/".$records[$i]->id."/edit"; ?>"><?php echo $records[$i]->firstname.' '.$records[$i]->lastname; ?></a></td>
		    <td><?php echo $records[$i]->license_number; ?><br><small>Exp: <?php echo $records[$i]->license_expiration; ?></small></td>
		    <td><?php 
					$str = "";
					if($records[$i]->passport != "")
						$str = 'Pass: '.$records[$i]->passport;
					
					if($records[$i]->idcard != ""){
						if($str != "")
							$str .= '<br>'; 					
						$str .= 'ID: '.$records[$i]->idcard;
					}
					echo $str;
			?></td>
		    <td><?php echo $records[$i]->birthdate; ?><br><small><?php echo $records[$i]->birthplace.', '.$records[$i]->birth_country; ?></small></td>
			<td><?php echo $records[$i]->email; ?></td>
		    <td><?php echo $records[$i]->phone_1.($records[$i]->phone_2!=''?'<br>'.$records[$i]->phone_2:''); ?></td>
			<td><?php echo $records[$i]->address.'<br>'.$records[$i]->zip_code.' '.$records[$i]->city.', '.$records[$i]->address_country; ?></td>
			<td><button class="btn btn-danger btn-sm pull-right" 
						onclick="Utils.confirmDeletion('<?php echo $resourceName."','".$records[$i]->id."','".$records[$i]->firstname." ".$records[$i]->lastname; ?>');">
					<span class="glyphicon glyphicon-trash"></span>
				</button>
			</td>
        </tr>
	<?php } ?>
    </tbody>
</table>
<?php echo $records->render(); ?>
<br>

<?php echo View::make('partials.footer') ?>