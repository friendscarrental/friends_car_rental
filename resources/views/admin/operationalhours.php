<?php echo View::make('partials.header') ?>
<br>

<div class="row">
	<h2 class="col-md-8 pull-left">Operational Hours for <b><?php echo $location->name; ?></b></h2>
	<br>
		
	<div class="col-md-4">
		<a href="/locations/<?php echo $location->id; ?>/edit" class="btn btn-info pull-right">Edit Location Details</a>
		<a href="/locations" class="btn btn-default pull-right" style="margin-right: 20px;">Locations List</a>
	</div>
</div>
<br>

<table class="table table-striped table-hover table-bordered">
    <thead>
        <tr>
			<th>#</th>
			<th>From</th>
			<th>To</th>
			<th>Open/Closed</th>
			<th>Open Time</th>
			<th>Close Time</th>
			<th>&nbsp;</th>
        </tr>
    </thead>
    <tbody>
	<?php 
	$recordsCount = count($records);	
	for($i=0; $i<$recordsCount; $i++) { ?>	
        <tr>
			<th scope="row"><?php echo $i+1; ?></th>
			<td><?php echo $weekDaysArray[$records[$i]->start_day]; ?></th>
			<td><?php echo $records[$i]->start_day==$records[$i]->end_day?'':$weekDaysArray[$records[$i]->end_day]; ?></th>
			<td><?php echo $records[$i]->not_operational==1?'Closed':'Open'; ?></td>
			<td><?php echo $records[$i]->not_operational==1?'Closed':$records[$i]->open_time; ?></td>
			<td><?php echo $records[$i]->not_operational==1?'Closed':$records[$i]->close_time; ?></td>
			<td><button class="btn btn-danger btn-sm pull-right" 
						onclick="Utils.confirmDeletion('locations', 'open-hours/<?php echo $records[$i]->id; ?>', 'this range');">
					<span class="glyphicon glyphicon-trash"></span>
				</button>
			</td>          
        </tr>
	<?php } ?>
	<form class="form-inline" action="/locations/open-hours" method="post">
		<tr class="success">
			<th scope="row">NEW</th>	
			<td>Start Day: <select name="start_day" class="form-control">
					<?php for($day=1; $day <=7; $day++){ ?>
						<option value="<?php echo $day; ?>" ><?php echo $weekDaysArray[$day]; ?></option>
					<?php } ?>
				</select>
			</td>	
			<td>End Day: <select name="end_day" class="form-control">
					<?php for($day=1; $day <=7; $day++){ ?>
						<option value="<?php echo $day; ?>" ><?php echo $weekDaysArray[$day]; ?></option>
					<?php } ?>
				</select>
			</td>
			<td>
				<div style="margin-top: 12px;" class="radio">
					<label>
					  <input type="radio" name="not_operational" value="0" checked="checked"> Open
					</label>
					</div>
				<div class="radio">
					<label>
					  <input type="radio" name="not_operational" value="1"> Closed
					</label>
				</div>
			</td>
			<td>Opening:<input type="input" name="open_time" class="form-control" placeholder="Open Time"></td>
			<td>Closing:<input type="input" name="close_time" class="form-control" placeholder="Close Time"></td>
			<td>
				<input type="hidden" name="location_id" value="<?php echo $location->id; ?>">
				<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
				<button type="submit" class="btn btn-success btn-md" style="margin-top: 18px; width: 120px;">Add New</button>
			</td>          
        </tr>
	</form>
    </tbody>
</table>

<?php if (count($errors) > 0) { ?>
	<div class="alert alert-danger">
		<ul>
			<?php foreach ($errors->all() as $error){ ?>
				<li><?php echo $error; ?></li>
			<?php } ?>
		</ul>
	</div>
<?php } ?>

<?php echo View::make('partials.footer') ?>