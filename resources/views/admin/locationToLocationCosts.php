<?php echo View::make('partials.header') ?>
<br>
<div class="row">
	<h2 class="col-md-8 pull-left">Return Costs from Different Pickup Location <br> for <b><a href="/locations/<?php echo $location->id; ?>/edit"><?php echo $location->name; ?></a></b></h2>
	<br>
		
	<div class="col-md-4">
		<a href="/locations/<?php echo $location->id; ?>/edit" class="btn btn-info pull-right">Edit Location Details</a>
		<a href="/locations" class="btn btn-default pull-right" style="margin-right: 20px;">Location List</a>
	</div>
</div>
<br>
<hr>
<div class="row">
<form class="form-horizontal col-md-offset-2 col-md-8" action="/locations/to-location-costs" method="post">
	
	<?php if (count($errors) > 0) { ?>
		<div class="alert alert-danger">
			<ul>
				<?php foreach ($errors->all() as $error){ ?>
					<li><?php echo $error; ?></li>
				<?php } ?>
			</ul>
		</div>
	<?php } ?>
	
	<table class="table table-striped table-hover">
		<thead>
			<tr>
				<th class="col-md-1">#</th>
				<th class="col-md-6">Picked Up From</th>
				<th class="col-md-5">Return Cost to <?php echo $location->name; ?></th>
			</tr>
		</thead>
		<tbody>
		<?php 
		$recordsCount = count($records);	
		for($i=0; $i<$recordsCount; $i++) { ?>	
			<tr>
				<th scope="row"><?php echo $i+1; ?></th>			
				<td><b><?php echo $records[$i]->name; ?></b></td>
				<td>
				<div class="input-group">
					<input type="input" name="<?php echo $records[$i]->id; ?>" value="<?php echo $records[$i]->cost; ?>" class="form-control" placeholder="Cost" >
					<span class="input-group-addon">Euro</span>
				</div>
				</td>
			</tr>
		<?php } ?>
		</tbody>
	</table>
	<hr>
	<input type="hidden" name="location_id" value="<?php echo $location->id; ?>">
	<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">		
	<div class="form-group">
		<div class="col-md-offset-4 col-sm-8">
			<button type="submit" class="btn btn-primary">Save Changes</button>
		</div>
	</div>
</form>
</div>

<?php echo View::make('partials.footer') ?>