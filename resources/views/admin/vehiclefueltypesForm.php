<?php echo View::make('partials.header') ?>

<div class="row">
	<form class="form-horizontal col-md-offset-2 col-md-8" action="/vehiclefueltypes" method="post">
		<?php $model = isset($model)?$model:[]; ?>
		<h2>Create/Edit Fuel Type</h2></br>
		
		<?php if (count($errors) > 0) { ?>
			<div class="alert alert-danger">
				<ul>
					<?php foreach ($errors->all() as $error){ ?>
						<li><?php echo $error; ?></li>
					<?php } ?>
				</ul>
			</div>
		<?php } ?>
		
		<!-- FORM INPUTS start -->
		<br>
		<div class="form-group required">
			<label for="name" class="col-md-2 control-label">Fuel Type</label>
			<div class="col-md-7">
				<input type="input" name="name" id="name" value="<?php echo isset($model['name'])?$model['name']:''; ?>" class="form-control" placeholder="Name">
			</div>
		</div>
		
		
		<!-- FORM INPUTS end -->
		
		<?php if(isset($model['id'])): ?>
			<input type="hidden" name="id" value="<?php echo $model['id']; ?>">		
		<?php endif; ?>
		<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
		<hr>
		<div class="form-group">
			<div class="col-md-offset-3 col-sm-8">
				<button type="submit" class="btn btn-primary">Save Changes</button>
			</div>
		</div>
	</form>
</div>


<?php echo View::make('partials.footer') ?>