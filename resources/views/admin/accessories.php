<?php echo View::make('partials.header') ?>
<br>
<div class="row">
	<h2 class="col-md-10 pull-left">Accessories</h2>
	<br>
	<a href="/<?php echo $resourceName; ?>/create" class="btn btn-success pull-right">Create New</a>
</div>
<br>
	
<table class="table table-striped table-hover">
    <thead>
        <tr>
			<th>#</th>
			<th>Name</th>
			<th>Image</th>
			<th>Description</th>
			<th>Price</th>
			<th>Fee Frequency</th>
			<th>Reservation</th>
			<th>Status</th>
			<th>&nbsp;</th>
        </tr>
    </thead>
    <tbody>
	<?php 
	$recordsCount = count($records);	
	for($i=0; $i<$recordsCount; $i++) { ?>	
        <tr>
			<th scope="row"><?php echo $i+1; ?></th>
            <td><a href="<?php echo "/".$resourceName."/".$records[$i]->id."/edit"; ?>"><?php echo $records[$i]->name; ?></a></td>
		    <td><?php echo '<img src="/images/uploads/'.$records[$i]->img_url.'" class="thumb sm"></img> ';?></td>
			<td><?php echo $records[$i]->description; ?></td>
		    <td><?php echo $records[$i]->price; ?></td>
			<td><?php echo $records[$i]->one_time_fee==1?'Charged Once':'Charged Daily'; ?></td>
			<td><?php echo $records[$i]->availability==1?'Trackable':'Non-trackable'; ?></td>
			<td><?php echo $records[$i]->active==1?'Active':'Disabled'; ?></td>
			<td><button class="btn btn-danger btn-sm pull-right" 
						onclick="Utils.confirmDeletion('<?php echo $resourceName."','".$records[$i]->id."','".$records[$i]->name; ?>');">
					<span class="glyphicon glyphicon-trash"></span>
				</button>
			</td>          
        </tr>
	<?php } ?>
    </tbody>
</table>

<?php echo View::make('partials.footer') ?>