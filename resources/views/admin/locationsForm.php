<?php echo View::make('partials.header') ?>

<form class="form-horizontal" action="/locations" method="post">
	<div class="row">
		<?php $model = isset($model)?$model:[]; ?>
		<h2>Create/Edit Location </h2></br>
		
		<?php if (count($errors) > 0) { ?>
			<div class="alert alert-danger">
				<ul>
					<?php foreach ($errors->all() as $error){ ?>
						<li><?php echo $error; ?></li>
					<?php } ?>
				</ul>
			</div>
		<?php } ?>
		
		<!-- FORM INPUTS start -->
		<div class="col-md-6">
			<div class="form-group required">
				<label for="name" class="col-md-3 control-label">Location Name</label>
				<div class="col-md-7">
					<input type="input" name="name" id="name" value="<?php echo isset($model['name'])?$model['name']:''; ?>" class="form-control" placeholder="Name">
				</div>
			</div>	
			<div class="form-group required">
				<label for="id_locationtype" class="col-md-3 control-label">Location Type</label>
				<div class="col-md-7">		
					<select name="id_locationtype" id="id_locationtype" class="form-control">
						<?php foreach ($locationtypes as $locationtype){ ?>
							<option value="<?php echo $locationtype->id; ?>" <?php if(isset($model['id_locationtype']) && $locationtype->id == $model['id_locationtype']) echo 'selected="selected"'; ?> ><?php echo $locationtype->name; ?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group required">
				<label for="tel" class="col-md-3 control-label">Phone 1</label>
				<div class="col-md-7">
					<input type="input" name="tel" id="tel" value="<?php echo isset($model['tel'])?$model['tel']:''; ?>" class="form-control" placeholder="Office Phone">
				</div>
			</div>
			<div class="form-group">
				<label for="tel2" class="col-md-3 control-label">Phone 2</label>
				<div class="col-md-7">
					<input type="input" name="tel2" id="tel2" value="<?php echo isset($model['tel2'])?$model['tel2']:''; ?>" class="form-control" placeholder="Secondary Phone">
				</div>
			</div>
			<div class="form-group">
				<label for="fax" class="col-md-3 control-label">Fax</label>
				<div class="col-md-7">
					<input type="input" name="fax" id="fax" value="<?php echo isset($model['fax'])?$model['fax']:''; ?>" class="form-control" placeholder="Fax">
				</div>
			</div>
			<div class="form-group">
				<label for="email" class="col-md-3 control-label">Email</label>
				<div class="col-md-7">
					<input type="input" name="email" id="email" value="<?php echo isset($model['email'])?$model['email']:''; ?>" class="form-control" placeholder="Email">
				</div>
			</div>
			<div class="form-group required">
				<label for="after_hours_price" class="col-md-3 control-label" style="margin-top: -2px; padding-top: 0px;">After Hours Extra Costs</label>
				<div class="col-md-6">
					<input type="input" name="after_hours_price" id="after_hours_price" value="<?php echo isset($model['after_hours_price'])?$model['after_hours_price']:''; ?>" class="form-control" placeholder="Extra Charge on Closed Hours">
				</div>
				<div class="col-md-2">
					<p class="form-control-static" style="margin-left: -12px; padding-left: 0px;">Euro</p>
				</div>
			</div>
			<?php if(!isset($model['id'])){ ?>
				<div class="form-group required">
					<label for="open_time" class="col-md-3 control-label">Open Hours</label>
					<div class="col-md-4">
						<input type="input" name="open_time" id="open_time" class="form-control" placeholder="Open Time 08:15">
					</div>				
					<div class="col-md-4">
						<input type="input" name="close_time" id="close_time" class="form-control" placeholder="Close Time 20:30">
					</div>
					<div class="col-md-offset-3 col-md-8">
						<p class="form-control-static" style="font-style: italic; color:#ccc;">Required Time Format => HH:MM (Example: 08:15)</p>
					</div>
				</div>
			<?php } else { ?>
				<div class="form-group">
					<label for="open_time" class="col-md-3 control-label">Open Hours</label>
					<div class="col-md-4">
						<a href="/locations/<?php echo $model['id']; ?>/open-hours" class="btn btn-warning">
							Edit Operation Hours  &nbsp; <i class="fa fa-clock-o"></i>
						</a>
					</div>
				</div>					
				<div class="form-group">
					<label for="email" class="col-md-3 control-label" style="margin-top: -2px; padding-top: 0px;">Extra Costs <br>per Vehicle</label>
					<div class="col-md-4">
						<a href="/locations/<?php echo $model['id']; ?>/per-vehicle-costs" class="btn btn-success">
							Location Extra Costs per Vehicle &nbsp; &nbsp;<i class="fa fa-car"></i>
						</a>
					</div>
				</div>
				<div class="form-group">
					<label for="email" class="col-md-3 control-label" style="margin-top: -2px; padding-top: 0px;">Diff. Location Extra Costs</label>
					<div class="col-md-4">
						<a href="/locations/<?php echo $model['id']; ?>/to-location-costs" class="btn btn-success">						
							Return Costs from Different Pickup &nbsp;<i class="fa fa-map-marker"></i>
						</a>
					</div>
				</div>
			<?php } ?>
		</div>
		
		<div class="col-md-6">
			
			<div class="form-group required">
				<label for="city" class="col-md-2 control-label">City</label>
				<div class="col-md-5">
					<input type="input" name="city" id="city" value="<?php echo isset($model['city'])?$model['city']:''; ?>" class="form-control" placeholder="City">
				</div>
			</div>
			<div class="form-group required">
				<label for="address" class="col-md-2 control-label">Address</label>
				<div class="col-md-10">
					<textarea name="address" id="address" class="form-control" rows="2" placeholder="address"><?php echo isset($model['address'])?$model['address']:''; ?></textarea>
				</div>
			</div>
			<div class="form-group required">
				<label for="lat" class="col-md-2 control-label">Marker</label>
				<div class="col-md-10" >
					<div class="form-control" id="map" style="height: 300px; background-color: background-color: #CCCCCC;">
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label">Status</label>
				<div class="col-sm-10">
					<label class="radio-inline">
					  <input type="radio" name="active" value="1" <?php if( !isset($model['id']) || (isset($model['active']) && $model['active']==1)) echo 'checked="checked"'; ?>> Active
					</label>
					<label class="radio-inline">
					  <input type="radio" name="active" value="0" <?php if(isset($model['id']) && $model['active']==0) echo 'checked="checked"'; ?>> Disabled
					</label>
				</div>
			</div>
		</div>
		<!-- FORM INPUTS end -->
		
		<?php if(isset($model['id'])): ?>
			<input type="hidden" name="id" value="<?php echo $model['id']; ?>">
		<?php endif; ?>		
		<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
		
		<!-- MAP Coordinates -->
		<input type="hidden" id="map_lat"  name="lat" value="<?php echo isset($model['lat'])?$model['lat']:''; ?>">
		<input type="hidden" id="map_lng" name="lng" value="<?php echo isset($model['lng'])?$model['lng']:''; ?>">
		
	</div>
	<hr>
	<div class="row">
		<div class="col-md-offset-5 col-sm-7">
			<button type="submit" class="btn btn-primary">Save Changes</button>
		</div>
	</div>
</form>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC9OmqRIYeF1EW2OO0BgHc8XnQVOHfBPgc&callback=initMap" async defer></script>

<script>

	// global "map" variable
	var map = null;
	var marker = null;
	var infowindow;

	function initMap() 
	{
		infowindow = new google.maps.InfoWindow(
		{ 
			size: new google.maps.Size(150,50)
		});
		var mapOptions = {	zoom: 8,
							center: {lat: 41.32913102117346, lng: 19.817962646484375},
							mapTypeControl: true,
							zoomControl: true,
							scaleControl: true,
							streetViewControl: false,
							mapTypeId: google.maps.MapTypeId.ROADMAP
						}
						
		<?php if(isset($model['id'])): ?>
			mapOptions['zoom'] = 15;
			mapOptions['center'] = {lat: parseFloat(<?php echo $model['lat']; ?>), lng: parseFloat(<?php echo $model['lng']; ?>)};
		<?php endif; ?>

		map = new google.maps.Map(document.getElementById('map'), mapOptions);
									
		<?php if(isset($model['id'])): ?>
		marker = new google.maps.Marker({
			position: mapOptions['center'],
			map: map
		});	
		<?php endif; ?>
		
		
		google.maps.event.addListener(map, 'click', function() {
			infowindow.close();
		});

		google.maps.event.addListener(map, 'click', function(event) {
			if (marker) {
				marker.setMap(null);
				marker = null;
			}
			marker = createMarker(event.latLng, "name", "<b>Coordinates</b><br>"+event.latLng);
		});
	}

	// A function to create the marker and set up the event window function 
	function createMarker(latlng, name, html) {
		var contentString = html;
		var marker = new google.maps.Marker({ position: latlng,
											  map: map,
											  zIndex: Math.round(latlng.lat()*-100000)<<5
											});

		google.maps.event.addListener(marker, 'click', function() {
			infowindow.setContent(contentString); 
			infowindow.open(map,marker);
		});
		
		google.maps.event.trigger(marker, 'click');
		//set HIDDEN input value
		$("#map_lat").val(latlng.lat());
		$("#map_lng").val(latlng.lng());
		
		return marker;
	}
</script>
	
<?php echo View::make('partials.footer') ?>