<?php echo View::make('partials.header') ?>

<div class="row">
	<form class="form-horizontal col-md-offset-2 col-md-8" action="/features" method="post" enctype="multipart/form-data">
		<?php $model = isset($model)?$model:[]; ?>
		<h2>Create/Edit Feature </h2></br>
		
		<?php if (count($errors) > 0) { ?>
			<div class="alert alert-danger">
				<ul>
					<?php foreach ($errors->all() as $error){ ?>
						<li><?php echo $error; ?></li>
					<?php } ?>
				</ul>
			</div>
		<?php } ?>
		
		<!-- FORM INPUTS start -->
		
		<div class="form-group required">
			<label for="name" class="col-md-3 control-label">Name</label>
			<div class="col-md-5">
				<input type="input" name="name" id="name" value="<?php echo isset($model['name'])?$model['name']:''; ?>" class="form-control" placeholder="Name">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-3 control-label">Image/Icon Type</label>
			<div class="col-sm-7 well">
			
				<div class="radio" style="margin-bottom: 10px; ">
					<label>
					  <input type="radio" name="icon_type" value="0" <?php if( !isset($model['id']) || (isset($model['icon_type']) && $model['icon_type']==0)) echo 'checked="checked"'; ?>> Use Short <b>Label</b>
					</label>
				</div>
				<input type="input" name="icon_label" id="icon_label" style="margin-top: 7px; margin-bottom: 12px; width: 150px;" value="<?php echo isset($model['icon_type']) && $model['icon_type']==0 && isset($model['icon_value'])?$model['icon_value']:''; ?>" class="form-control" placeholder="Label">
				<div class="radio">
					<label>
					  <input type="radio" name="icon_type" value="1" <?php if( isset($model['icon_type']) && $model['icon_type']==1) echo 'checked="checked"'; ?>> Use <b>Icon Code</b> from <i>Fontawesome 4.5 </i>
					</label>
				</div>
				<div class="input-group" style="margin-top: 7px; margin-bottom: 12px;" >
					<input type="input" name="icon_code" id="icon_code" value="<?php echo isset($model['icon_type']) && $model['icon_type']==1 && isset($model['icon_value'])?$model['icon_value']:''; ?>" class="form-control" placeholder="Icon Code">
					<span class="input-group-btn">
						<a href="https://fortawesome.github.io/Font-Awesome/icons/#web-application" target="_blank" class="btn btn-default">ex:&nbsp;&nbsp; <b>fa-car</b> &nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-car"></i></a>
					</span>
				</div>

				<div class="radio">
					<label>
					  <input type="radio" name="icon_type" value="2" <?php if(isset($model['icon_type']) && $model['icon_type']==2) echo 'checked="checked"'; ?>> Use <b>Image</b> file upload
					</label>
				</div>
				<div class="input-group" style="margin-top: 7px; margin-bottom: 10px; ">
					<span class="input-group-btn">
						<span class="btn btn-info btn-file upload-file-inputs">
							Browse&hellip; <input type="file" name="icon_image" id="icon_image" class="upload-file-inputs">
						</span>
					</span>
					<input type="text" class="form-control upload-file-inputs" value="<?php echo isset($model['icon_type']) && $model['icon_type']==2 && isset($model['icon_value'])?$model['icon_value']:''; ?>" readonly>
				</div>
						
				<div id="missingUploadMsg"></div>
				
			</div>
		</div>

		<div class="form-group">
			<label for="description" class="col-sm-3 control-label">Description</label>
			<div class="col-sm-7">
				<textarea name="description" id="description" class="form-control" rows="2" placeholder="Description"><?php echo isset($model['description'])?$model['description']:''; ?></textarea>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-3 control-label">Status</label>
			<div class="col-sm-9">
				<label class="radio-inline">
				  <input type="radio" name="active" value="1" <?php if( !isset($model['id']) || (isset($model['active']) && $model['active']==1)) echo 'checked="checked"'; ?>> Active
				</label>
				<label class="radio-inline">
				  <input type="radio" name="active" value="0" <?php if(isset($model['id']) && $model['active']==0) echo 'checked="checked"'; ?>> Disabled
				</label>
			</div>
		</div>
		<!-- FORM INPUTS end -->
		
		
		<?php if(isset($model['id'])): ?>
			<input type="hidden" name="id" value="<?php echo $model['id']; ?>">		
		<?php endif; ?>
		<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">		
		<hr>
		<div class="form-group">
			<div class="col-md-offset-3 col-sm-8">
				<button type="submit" class="btn btn-primary">Save Changes</button>
			</div>
		</div>
	</form>
</div>

<script>

	$(document).on('change', '.btn-file :file', function() {
		var input = $(this);
		var numFiles = input.get(0).files ? input.get(0).files.length : 1;
		var label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
		
		input.trigger('fileselect', [numFiles, label]);
	});

	$(document).ready(function ()
	{
		//upload select file event
		$('.btn-file :file').on('fileselect', function(event, numFiles, label) {        
			var input = $(this).parents('.input-group').find(':text'),
				log = numFiles > 1 ? numFiles + ' files selected' : label;
			
			if( input.length ) {
				input.val(log);
			} else {
				if( log ) alert(log);
			}        
		});
		
		
		$('form').on('submit', function(){
		
			if ($("input[name='icon_type']:checked").val() == '0' && $('#icon_label').val() == "")
			{
				$("#missingUploadMsg")
					.addClass("alert alert-warning")
					.html("You have choosen the LABEL option but there is no value entered!");
				
				return false;
			}
			else if ($("input[name='icon_type']:checked").val() == '1' && $('#icon_code').val() == "")
			{
				$("#missingUploadMsg")
					.addClass("alert alert-warning")
					.html("You have choosen the Font-Awesome CODE option but there is no value entered!");
				
				return false;
			}
			else if ($("input[name='icon_type']:checked").val() == '2' && $('input:text.upload-file-inputs').val() == "")
			{
				$("#missingUploadMsg")
					.addClass("alert alert-warning")
					.html("You have choosen the IMAGE option but there is no file selected to upload!");
				
				return false;
			}
			else
				return true;
		});
		
		//check on initializaion
		checkRadioInputs($("input[name='icon_type']:checked"));
		
		$("input[name='icon_type']").change(function(){
			checkRadioInputs(this);
		});			
	});

	function checkRadioInputs(that)
	{
		var radioSelected = $(that);
		if (radioSelected.val() == 0) {//label
			$('#icon_code').attr("disabled","disabled").val('');
			$('.upload-file-inputs').attr("disabled","disabled");
			$('#icon_label').removeAttr('disabled');
		} else if (radioSelected.val() == 1) {//code
			$('.upload-file-inputs').attr("disabled","disabled");
			$('#icon_label').attr("disabled","disabled").val('');
			$('#icon_code').removeAttr('disabled');
		} else if (radioSelected.val() == 2)  {//image
			$('#icon_code').attr("disabled","disabled").val('');
			$('#icon_label').attr("disabled","disabled").val('');
			$('.upload-file-inputs').removeAttr('disabled');
		}
	}

</script>

<?php echo View::make('partials.footer') ?>