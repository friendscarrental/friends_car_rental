<?php echo View::make('partials.header') ?>
<br>
<div class="row">
	<h2 class="col-md-10 pull-left">Users List</h2>
	<br>
	<a href="/<?php echo $resourceName; ?>/create" class="btn btn-success pull-right">Create New</a>
</div>
<br>

<table class="table table-striped table-hover">
    <thead>
        <tr>
          <th>&nbsp;</th>
          <th>Email</th>
          <th>First Name</th>
          <th>Last Name</th>
          <th>Role</th>
          <th>Status</th>
          <th>Drivers</th>
          <th>&nbsp;</th>
        </tr>
    </thead>
    <tbody>
	<?php 
	$usersCount = count($records);
	
	for($i=0; $i < $usersCount; $i++) {
		$statusLabel = '';
		$statusClass = '';
		if($records[$i]->status==0)
		{
			$statusLabel = 'Disabled';
			$statusClass = 'warning';
		}
		else if($records[$i]->status==1)
		{
			$statusLabel = 'Active';
			$statusClass = '';		
		}
		else// if($records[$i]->status==2)//default
		{
			$statusLabel = 'Blocked';
			$statusClass = 'danger';
		}
	?>
        <tr class="<?php echo $statusClass; ?>">
			<th scope="row">&nbsp;</th>
            <td><a href="<?php echo "/".$resourceName."/".$records[$i]->id."/edit"; ?>"><?php echo $records[$i]->email; ?></a></td>
			<td><?php echo $records[$i]->firstname; ?></td>
			<td><?php echo $records[$i]->lastname; ?></td>
			<td><?php echo $records[$i]->role_name; ?></td>
			<td><?php echo $statusLabel; ?></td>
			<td><a href="<?php echo "/drivers".($records[$i]->id_role < 5? '':'/'.$records[$i]->id.'/list'); ?>"><?php echo $records[$i]->drivers; ?></a></td>
			<td><button class="btn btn-danger btn-sm pull-right" 
						onclick="Utils.confirmDeletion('<?php echo $resourceName."','".$records[$i]->id."','".$records[$i]->firstname.' '.$records[$i]->lastname; ?>');">
					<span class="glyphicon glyphicon-trash"></span>
				</button>
			</td>
        </tr>
	<?php } ?>
    </tbody>
</table>
<?php echo $records->render(); ?>
<br>

<?php echo View::make('partials.footer') ?>