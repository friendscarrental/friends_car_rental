<?php echo View::make('partials.header') ?>

<div class="row">
	<form class="form-horizontal col-md-offset-2 col-md-8" action="/rights" method="post">
		<?php $model = isset($model)?$model:[]; ?>
		<h2>Create/Edit Rights </h2></br>
		
		<?php if (count($errors) > 0) { ?>
			<div class="alert alert-danger">
				<ul>
					<?php foreach ($errors->all() as $error){ ?>
						<li><?php echo $error; ?></li>
					<?php } ?>
				</ul>
			</div>
		<?php } ?>
		
		<!-- FORM INPUTS start -->
		
		<div class="form-group">
			<label for="id_rightsection" class="col-md-3 control-label">Id Rights Section</label>
			<div class="col-md-6">
				<input type="input" name="id_rightsection" id="id_rightsection" value="<?php echo isset($model['id_rightsection'])?$model['id_rightsection']:''; ?>" class="form-control" placeholder="Id rights section">
			</div>
		</div>
		<div class="form-group">
			<label for="shadow_rights" class="col-md-3 control-label">Shadow Rights</label>
			<div class="col-md-7">
				<input type="input" name="shadow_rights" id="shadow_rights" value="<?php echo isset($model['shadow_rights'])?$model['shadow_rights']:''; ?>" class="form-control" placeholder="Shadow rights">
			</div>
		</div>
		<div class="form-group">
			<label for="right_label" class="col-md-3 control-label">Right label</label>
			<div class="col-md-7">
				<input type="input" name="right_label" id="right_label" value="<?php echo isset($model['right_label'])?$model['right_label']:''; ?>" class="form-control" placeholder="Right label">
			</div>
		</div>
		<div class="form-group">
			<label for="virtual" class="col-md-3 control-label">Virtual</label>
			<div class="col-md-5">
				<input type="input" name="virtual" id="virtual" value="<?php echo isset($model['virtual'])?$model['virtual']:''; ?>" class="form-control" placeholder="Virtual">
			</div>
		</div>
		
		<!-- FORM INPUTS end -->
		
		
		<?php if(isset($model['id'])): ?>
			<input type="hidden" name="id" value="<?php echo $model['id']; ?>">		
		<?php endif; ?>
		<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">		
		<div class="form-group">
			<div class="col-md-offset-3 col-sm-8">
				<button type="submit" class="btn btn-primary">Save Changes</button>
			</div>
		</div>
	</form>
</div>


<?php echo View::make('partials.footer') ?>