<?php echo View::make('partials.header') ?>
<br>
<div class="row">
	<h2 class="col-md-5 pull-left">Vehicles List</h2>
	<br>
	<a href="/<?php echo $resourceName; ?>/create" class="btn btn-success pull-right">Create New</a>
	
</div>
<br>
	
<table class="table table-striped table-hover">
    <thead>
        <tr>
			<th>#</th>
			<th>Vehicle</th>
			<th>Type</th>
			<th>Year</th>
			<th>Transmission</th>
			<th>Fuel Type</th>
			<th>Seats</th>
			<th>Doors</th>
			<th>Baggage</th>
			<th>Price</th>
			<th>Status</th>
			<th colspan="2">&nbsp;</th>
        </tr>
    </thead>
    <tbody>
	<?php 
	$recordsCount = count($records);	
	for($i=0; $i<$recordsCount; $i++) { ?>	
        <tr>
			<th scope="row"><?php echo $i+1; ?></th>
            <td><a href="<?php echo "/".$resourceName."/".$records[$i]->id; ?>"><?php echo $records[$i]->title; ?></a></td>
		    <td><?php echo $records[$i]->vehicletypes_name; ?></td>
		    <td><?php echo $records[$i]->year; ?></td>
			<td><?php echo $records[$i]->transmission==1?'Manual':'Auto'; ?></td>
			<td><?php echo $records[$i]->fuel_type; ?></td>
			<td><?php echo $records[$i]->seats; ?></td>
			<td><?php echo $records[$i]->doors; ?></td>
			<td><?php echo $records[$i]->baggage_size; ?></td>
			<td><?php echo $records[$i]->standard_price; ?></td>
		    <td><?php echo $records[$i]->active==1?'Active':'Disabled'; ?></td>
			<td><a href="<?php echo "/".$resourceName."/".$records[$i]->id."/edit"; ?>" class="btn btn-info btn-sm pull-right" >Edit</a></td> 
			<td><button class="btn btn-danger btn-sm pull-right" 
						onclick="Utils.confirmDeletion('<?php echo $resourceName."','".$records[$i]->id."','".$records[$i]->id_vehicletype; ?>');">
					<span class="glyphicon glyphicon-trash"></span>
				</button>
			</td>          
        </tr>
	<?php } ?>
    </tbody>
</table>

<?php echo View::make('partials.footer') ?>