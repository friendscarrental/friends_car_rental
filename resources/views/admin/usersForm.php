<?php echo View::make('partials.header') ?>

<div class="row">
	<form class="form-horizontal col-md-offset-2 col-md-8" action="/<?php echo isset($client)?'edit-account':'users'; ?>" method="post">
		<?php $model = isset($model)?$model:[]; ?>
		
		<h2><?php echo isset($client)?'Edit Personal Information':'Register/Edit User'; ?></h2></br>
		
		<?php if (count($errors) > 0) { ?>
			<div class="alert alert-danger">
				<ul>
					<?php foreach ($errors->all() as $error){ ?>
						<li><?php echo $error; ?></li>
					<?php } ?>
				</ul>
			</div>
		<?php } ?>
		
		<div class="form-group required">
			<label for="email" class="col-sm-2 control-label">Email</label>
			<div class="col-sm-8">
				<input type="email" name="email" id="email" value="<?php echo isset($model['email'])?$model['email']:''; ?>" class="form-control" placeholder="Email">
			</div>
		</div>
		
		<?php if(!isset($model['id'])): ?>
		<div class="form-group required">
			<label for="password" class="col-sm-2 control-label">Password</label>
			<div class="col-sm-8">
				<input type="password" name="password" id="password" class="form-control" placeholder="Password">
			</div>
		</div>
		<?php endif; ?>
		
		<?php if(isset($roles)): ?>
		<div class="form-group required">
			<label for="id_role" class="col-sm-2 control-label">Role</label>
			<div class="col-sm-8">				
				<select name="id_role" id="id_role" class="form-control">
					<?php foreach ($roles as $role){ ?>
						<option value="<?php echo $role->id; ?>" <?php if(isset($model['id_role']) && $role->id == $model['id_role']) echo 'selected="selected"'; ?> ><?php echo $role->role_name; ?></option>
					<?php } ?>
				</select>
			</div>
		</div>
		<?php endif; ?>
		<div class="form-group required">
			<label for="firstname" class="col-sm-2 control-label">Firstname</label>
			<div class="col-sm-8">
				<input type="input" name="firstname" id="firstname" value="<?php echo isset($model['firstname'])?$model['firstname']:''; ?>" class="form-control" placeholder="Firstname">
			</div>
		</div>
		<div class="form-group required">
			<label for="lastname" class="col-sm-2 control-label">Lastname</label>
			<div class="col-sm-8">
				<input type="input" name="lastname" id="lastname" value="<?php echo isset($model['lastname'])?$model['lastname']:''; ?>" class="form-control" placeholder="Lastname">
			</div>
		</div>
		<div class="form-group">
			<label for="tel" class="col-sm-2 control-label">Phone</label>
			<div class="col-sm-8">
				<input type="input" name="tel" id="tel" value="<?php echo isset($model['tel'])?$model['tel']:''; ?>" class="form-control" placeholder="Phone">
			</div>
		</div>
		
		<?php if(isset($model['id'])): ?>
			<input type="hidden" name="id" value="<?php echo $model['id']; ?>">		
		<?php endif; ?>
		<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">		
		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-8">
				<button type="submit" class="btn btn-primary" style="margin-right: 30px;">Save Changes</button>
				<?php if(isset($client)){ ?>
					<a href="/change-pass" type="button" class="btn btn-warning">Change Password</a>
				<?php }else if(isset($model['id'])){ ?>
					<a href="/users/change-pass/<?php echo $model['id']; ?>" type="button" class="btn btn-warning">Change Password</a>
				<?php } ?>
			</div>
		</div>
	</form>
</div>

<script>

	$(document).ready(function ()
	{
		
	});

</script>

<?php echo View::make('partials.footer') ?>