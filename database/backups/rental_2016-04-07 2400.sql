/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.6.25 : Database - friends_car_rental
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`friends_car_rental` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `friends_car_rental`;

/*Table structure for table `accessories` */

DROP TABLE IF EXISTS `accessories`;

CREATE TABLE `accessories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `img_url` varchar(500) DEFAULT NULL,
  `price` double(8,2) DEFAULT NULL,
  `availability` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '1=trackable, 2=one unit, 3=seat related, 4=unlimited',
  `one_time_fee` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '0=charged daily 1=charged once',
  `created_at` datetime DEFAULT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

/*Data for the table `accessories` */

insert  into `accessories`(`id`,`name`,`description`,`img_url`,`price`,`availability`,`one_time_fee`,`created_at`,`active`,`deleted`) values (7,'Additional Driver','treuityoiyjdfmyurutrit','Ytbnr1454509582.png',12.00,3,0,'2015-12-03 14:49:27',1,0),(8,'Child booster seat','For children 8-12 years(CBS)','IpOT11454509614.jpg',10.00,1,0,'2015-12-03 14:51:16',1,0),(9,'Infant safety seat','Infant safety seats protect children. ','wsmNn1454509701.jpg',12.00,1,0,'2015-12-03 16:34:08',1,0),(10,'Cross border fee','qwreeriotity','M6Lyz1454509724.jpg',12.00,2,1,'2015-12-03 16:39:58',1,0),(11,'Toddler safety seat Toddler safety seat Toddler sa','Infant safety seats protect children. Infant safety seats protect children. Infant safety seats protect children. Infant safety seats protect children. Infant safety seats protect children. ','fGB2H1454509744.jpg',12.00,1,0,'2015-12-03 16:45:25',1,0),(12,'Child safety seat','For children 8-12 years(CBS)','4Hb5V1454509800.jpg',12.00,1,0,'2015-12-03 16:47:01',1,0),(13,'Winter snow tires for five to six people(1 set)','To handle the challenges of driving. ','qGElk1454509824.jpg',5.00,2,0,'2015-12-03 16:48:59',1,0),(14,'Navigational system','Stay on the right track book a GPS! Stay on the right track book a GPS! Stay on the right track book a GPS! Stay on the right track book a GPS! Stay on the right track book a GPS! ','QiZvi1454509846.jpg',10.00,1,0,'2015-12-03 16:50:00',1,0);

/*Table structure for table `accessory_availability` */

DROP TABLE IF EXISTS `accessory_availability`;

CREATE TABLE `accessory_availability` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `accessory_availability` */

insert  into `accessory_availability`(`id`,`name`) values (1,'Trackable, limited by Reservations'),(2,'Single Unit, non-trackable'),(3,'Multi non-trackable');

/*Table structure for table `accessory_reservations` */

DROP TABLE IF EXISTS `accessory_reservations`;

CREATE TABLE `accessory_reservations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_reservation` int(10) unsigned NOT NULL,
  `id_accessory` int(10) unsigned NOT NULL,
  `pickup_datetime` datetime DEFAULT NULL,
  `return_datetime` datetime DEFAULT NULL,
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `accessory_reservations` */

/*Table structure for table `address` */

DROP TABLE IF EXISTS `address`;

CREATE TABLE `address` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_driver` int(10) unsigned DEFAULT NULL,
  `type_home` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '0=professionale 1=home',
  `firstname` varchar(50) DEFAULT NULL,
  `lastname` varchar(100) DEFAULT NULL,
  `address` varchar(500) DEFAULT NULL,
  `zip_code` varchar(20) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `id_country` int(10) unsigned DEFAULT NULL,
  `phone_1` varchar(50) DEFAULT NULL,
  `phone_2` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `address` */

insert  into `address`(`id`,`id_driver`,`type_home`,`firstname`,`lastname`,`address`,`zip_code`,`city`,`id_country`,`phone_1`,`phone_2`,`created_at`,`deleted`) values (1,7,1,'Seven7','Eleven','main st','39440','Elbasan',2,'29340234903294','','2016-02-19 12:01:29',0),(2,8,1,'Shate','Tete','main st','39440','Elbasan',2,'29340234903294','','2016-02-19 12:11:00',0),(3,12,1,'Fifteen','Twenty','Rruga e Dibres','40001','Tirane',2,'29049320423','','2016-03-25 08:45:51',0),(4,13,1,'Pese','Fifty','Rruga e Dibres','40001','Tirane',2,'29049320423','','2016-03-25 10:03:25',0),(5,14,1,'Gjashte','Six','Rruga e Dibres','40001','Tirane',2,'29049320423','','2016-03-25 10:05:19',0),(6,15,1,'Shtate','Seven','Rruga e Dibres','','Tirane',2,'29049320423','','2016-03-25 15:13:47',0);

/*Table structure for table `countries` */

DROP TABLE IF EXISTS `countries`;

CREATE TABLE `countries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(2) NOT NULL DEFAULT '',
  `name` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=247 DEFAULT CHARSET=utf8;

/*Data for the table `countries` */

insert  into `countries`(`id`,`code`,`name`) values (1,'AF','Afghanistan'),(2,'AL','Albania'),(3,'DZ','Algeria'),(4,'DS','American Samoa'),(5,'AD','Andorra'),(6,'AO','Angola'),(7,'AI','Anguilla'),(8,'AQ','Antarctica'),(9,'AG','Antigua and/or Barbuda'),(10,'AR','Argentina'),(11,'AM','Armenia'),(12,'AW','Aruba'),(13,'AU','Australia'),(14,'AT','Austria'),(15,'AZ','Azerbaijan'),(16,'BS','Bahamas'),(17,'BH','Bahrain'),(18,'BD','Bangladesh'),(19,'BB','Barbados'),(20,'BY','Belarus'),(21,'BE','Belgium'),(22,'BZ','Belize'),(23,'BJ','Benin'),(24,'BM','Bermuda'),(25,'BT','Bhutan'),(26,'BO','Bolivia'),(27,'BA','Bosnia and Herzegovina'),(28,'BW','Botswana'),(29,'BV','Bouvet Island'),(30,'BR','Brazil'),(31,'IO','British Indian Ocean Territory'),(32,'BN','Brunei Darussalam'),(33,'BG','Bulgaria'),(34,'BF','Burkina Faso'),(35,'BI','Burundi'),(36,'KH','Cambodia'),(37,'CM','Cameroon'),(38,'CA','Canada'),(39,'CV','Cape Verde'),(40,'KY','Cayman Islands'),(41,'CF','Central African Republic'),(42,'TD','Chad'),(43,'CL','Chile'),(44,'CN','China'),(45,'CX','Christmas Island'),(46,'CC','Cocos (Keeling) Islands'),(47,'CO','Colombia'),(48,'KM','Comoros'),(49,'CG','Congo'),(50,'CK','Cook Islands'),(51,'CR','Costa Rica'),(52,'HR','Croatia (Hrvatska)'),(53,'CU','Cuba'),(54,'CY','Cyprus'),(55,'CZ','Czech Republic'),(56,'DK','Denmark'),(57,'DJ','Djibouti'),(58,'DM','Dominica'),(59,'DO','Dominican Republic'),(60,'TP','East Timor'),(61,'EC','Ecuador'),(62,'EG','Egypt'),(63,'SV','El Salvador'),(64,'GQ','Equatorial Guinea'),(65,'ER','Eritrea'),(66,'EE','Estonia'),(67,'ET','Ethiopia'),(68,'FK','Falkland Islands (Malvinas)'),(69,'FO','Faroe Islands'),(70,'FJ','Fiji'),(71,'FI','Finland'),(72,'FR','France'),(73,'FX','France, Metropolitan'),(74,'GF','French Guiana'),(75,'PF','French Polynesia'),(76,'TF','French Southern Territories'),(77,'GA','Gabon'),(78,'GM','Gambia'),(79,'GE','Georgia'),(80,'DE','Germany'),(81,'GH','Ghana'),(82,'GI','Gibraltar'),(83,'GK','Guernsey'),(84,'GR','Greece'),(85,'GL','Greenland'),(86,'GD','Grenada'),(87,'GP','Guadeloupe'),(88,'GU','Guam'),(89,'GT','Guatemala'),(90,'GN','Guinea'),(91,'GW','Guinea-Bissau'),(92,'GY','Guyana'),(93,'HT','Haiti'),(94,'HM','Heard and Mc Donald Islands'),(95,'HN','Honduras'),(96,'HK','Hong Kong'),(97,'HU','Hungary'),(98,'IS','Iceland'),(99,'IN','India'),(100,'IM','Isle of Man'),(101,'ID','Indonesia'),(102,'IR','Iran (Islamic Republic of)'),(103,'IQ','Iraq'),(104,'IE','Ireland'),(105,'IL','Israel'),(106,'IT','Italy'),(107,'CI','Ivory Coast'),(108,'JE','Jersey'),(109,'JM','Jamaica'),(110,'JP','Japan'),(111,'JO','Jordan'),(112,'KZ','Kazakhstan'),(113,'KE','Kenya'),(114,'KI','Kiribati'),(115,'KP','Korea, Democratic People\'s Republic of'),(116,'KR','Korea, Republic of'),(117,'XK','Kosovo'),(118,'KW','Kuwait'),(119,'KG','Kyrgyzstan'),(120,'LA','Lao People\'s Democratic Republic'),(121,'LV','Latvia'),(122,'LB','Lebanon'),(123,'LS','Lesotho'),(124,'LR','Liberia'),(125,'LY','Libyan Arab Jamahiriya'),(126,'LI','Liechtenstein'),(127,'LT','Lithuania'),(128,'LU','Luxembourg'),(129,'MO','Macau'),(130,'MK','Macedonia'),(131,'MG','Madagascar'),(132,'MW','Malawi'),(133,'MY','Malaysia'),(134,'MV','Maldives'),(135,'ML','Mali'),(136,'MT','Malta'),(137,'MH','Marshall Islands'),(138,'MQ','Martinique'),(139,'MR','Mauritania'),(140,'MU','Mauritius'),(141,'TY','Mayotte'),(142,'MX','Mexico'),(143,'FM','Micronesia, Federated States of'),(144,'MD','Moldova, Republic of'),(145,'MC','Monaco'),(146,'MN','Mongolia'),(147,'ME','Montenegro'),(148,'MS','Montserrat'),(149,'MA','Morocco'),(150,'MZ','Mozambique'),(151,'MM','Myanmar'),(152,'NA','Namibia'),(153,'NR','Nauru'),(154,'NP','Nepal'),(155,'NL','Netherlands'),(156,'AN','Netherlands Antilles'),(157,'NC','New Caledonia'),(158,'NZ','New Zealand'),(159,'NI','Nicaragua'),(160,'NE','Niger'),(161,'NG','Nigeria'),(162,'NU','Niue'),(163,'NF','Norfolk Island'),(164,'MP','Northern Mariana Islands'),(165,'NO','Norway'),(166,'OM','Oman'),(167,'PK','Pakistan'),(168,'PW','Palau'),(169,'PS','Palestine'),(170,'PA','Panama'),(171,'PG','Papua New Guinea'),(172,'PY','Paraguay'),(173,'PE','Peru'),(174,'PH','Philippines'),(175,'PN','Pitcairn'),(176,'PL','Poland'),(177,'PT','Portugal'),(178,'PR','Puerto Rico'),(179,'QA','Qatar'),(180,'RE','Reunion'),(181,'RO','Romania'),(182,'RU','Russian Federation'),(183,'RW','Rwanda'),(184,'KN','Saint Kitts and Nevis'),(185,'LC','Saint Lucia'),(186,'VC','Saint Vincent and the Grenadines'),(187,'WS','Samoa'),(188,'SM','San Marino'),(189,'ST','Sao Tome and Principe'),(190,'SA','Saudi Arabia'),(191,'SN','Senegal'),(192,'RS','Serbia'),(193,'SC','Seychelles'),(194,'SL','Sierra Leone'),(195,'SG','Singapore'),(196,'SK','Slovakia'),(197,'SI','Slovenia'),(198,'SB','Solomon Islands'),(199,'SO','Somalia'),(200,'ZA','South Africa'),(201,'GS','South Georgia South Sandwich Islands'),(202,'ES','Spain'),(203,'LK','Sri Lanka'),(204,'SH','St. Helena'),(205,'PM','St. Pierre and Miquelon'),(206,'SD','Sudan'),(207,'SR','Suriname'),(208,'SJ','Svalbard and Jan Mayen Islands'),(209,'SZ','Swaziland'),(210,'SE','Sweden'),(211,'CH','Switzerland'),(212,'SY','Syrian Arab Republic'),(213,'TW','Taiwan'),(214,'TJ','Tajikistan'),(215,'TZ','Tanzania, United Republic of'),(216,'TH','Thailand'),(217,'TG','Togo'),(218,'TK','Tokelau'),(219,'TO','Tonga'),(220,'TT','Trinidad and Tobago'),(221,'TN','Tunisia'),(222,'TR','Turkey'),(223,'TM','Turkmenistan'),(224,'TC','Turks and Caicos Islands'),(225,'TV','Tuvalu'),(226,'UG','Uganda'),(227,'UA','Ukraine'),(228,'AE','United Arab Emirates'),(229,'GB','United Kingdom'),(230,'US','United States'),(231,'UM','United States minor outlying islands'),(232,'UY','Uruguay'),(233,'UZ','Uzbekistan'),(234,'VU','Vanuatu'),(235,'VA','Vatican City State'),(236,'VE','Venezuela'),(237,'VN','Vietnam'),(238,'VG','Virgin Islands (British)'),(239,'VI','Virgin Islands (U.S.)'),(240,'WF','Wallis and Futuna Islands'),(241,'EH','Western Sahara'),(242,'YE','Yemen'),(243,'YU','Yugoslavia'),(244,'ZR','Zaire'),(245,'ZM','Zambia'),(246,'ZW','Zimbabwe');

/*Table structure for table `discountpervehicle` */

DROP TABLE IF EXISTS `discountpervehicle`;

CREATE TABLE `discountpervehicle` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_vehicle` int(10) unsigned DEFAULT NULL,
  `duration` smallint(5) unsigned DEFAULT '1' COMMENT 'in days',
  `discount` double(8,4) DEFAULT NULL COMMENT 'per day',
  `percentage` tinyint(1) unsigned DEFAULT '0' COMMENT '0=fixed amount per day 1=percentage discount',
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `discountpervehicle` */

insert  into `discountpervehicle`(`id`,`id_vehicle`,`duration`,`discount`,`percentage`,`active`) values (1,1,2,5.0000,0,1),(2,2,2,3.0000,0,1),(3,3,2,0.2000,1,1);

/*Table structure for table `discountranges` */

DROP TABLE IF EXISTS `discountranges`;

CREATE TABLE `discountranges` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `duration` smallint(5) unsigned DEFAULT '1',
  `last_range` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '1=and above (plus)',
  `description` varchar(200) DEFAULT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `discountranges` */

insert  into `discountranges`(`id`,`duration`,`last_range`,`description`,`active`) values (1,2,0,'Two Days Discount',1),(2,5,0,'Five Days Discount',1);

/*Table structure for table `drivers` */

DROP TABLE IF EXISTS `drivers`;

CREATE TABLE `drivers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL COMMENT '0=system (created by admin to be used by all admins)',
  `email` varchar(200) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `birthdate` date NOT NULL,
  `birthplace` varchar(100) NOT NULL,
  `id_country_birth` int(10) unsigned NOT NULL,
  `license_number` varchar(100) NOT NULL,
  `license_expiration` date NOT NULL,
  `id_country_license` int(10) unsigned DEFAULT NULL,
  `passport` varchar(100) DEFAULT NULL,
  `idcard` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

/*Data for the table `drivers` */

insert  into `drivers`(`id`,`id_user`,`email`,`firstname`,`lastname`,`birthdate`,`birthplace`,`id_country_birth`,`license_number`,`license_expiration`,`id_country_license`,`passport`,`idcard`,`created_at`,`active`,`deleted`) values (1,0,'','One','nje','0000-00-00','',0,'','0000-00-00',NULL,'',NULL,NULL,1,0),(2,2,'','Two','dy','0000-00-00','',0,'','0000-00-00',NULL,'',NULL,NULL,1,0),(3,2,'','Three','tre','0000-00-00','',0,'','0000-00-00',NULL,'',NULL,NULL,1,0),(4,2,'','four','kater','0000-00-00','',0,'','0000-00-00',NULL,'',NULL,NULL,1,0),(5,2,'','five','pese','0000-00-00','',0,'','0000-00-00',NULL,'',NULL,NULL,1,0),(6,2,'','six','gjashte','0000-00-00','',0,'','0000-00-00',NULL,'',NULL,NULL,1,0),(7,3,'seven@mail.com','Seven7','Shtate','1985-09-12','farminton hills',2,'M49230493V','1985-09-12',NULL,'23424343',NULL,'2016-01-27 18:14:21',1,0),(8,3,'eight@hotmail.com','Eight','Tete','1975-07-18','',230,'N24382094D','1975-07-18',NULL,'45345676',NULL,'2016-01-27 18:14:23',1,0),(10,5,'avni@mail.com','Seven7','Eleven','1985-09-12','Shkoder',2,'P290293402L','2017-04-03',2,'','','2016-02-19 12:01:29',1,0),(11,6,'kater@mail.com','Shate','Tete','1985-09-12','Shkoder',2,'P290293402L','2017-04-03',2,'','','2016-02-19 12:11:00',1,0),(12,7,'fifteen@mail.com','Fifteen','Twenty','1985-09-12','Shkoder',2,'23490234932040','2006-10-04',2,'','','2016-03-25 08:45:51',1,0),(13,8,'pese@mail.com','Pese','Fifty','1985-11-11','Tirane',2,'23490234932040','2006-05-18',2,'','','2016-03-25 10:03:25',1,0),(14,9,'gjashte@mail.com','Gjashte','Six','1985-07-04','Tirane',2,'23490234932040','2006-05-03',2,'','','2016-03-25 10:05:19',1,0),(15,10,'shtate@mail.com','Shtate','Seven','1985-06-03','Tirane',2,'23490234932040','2006-09-03',2,'','','2016-03-25 15:13:47',1,0);

/*Table structure for table `durationdiscounts` */

DROP TABLE IF EXISTS `durationdiscounts`;

CREATE TABLE `durationdiscounts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_vehicle` int(10) unsigned DEFAULT NULL,
  `duration` smallint(5) unsigned DEFAULT '1' COMMENT 'in days',
  `discount` double(8,4) DEFAULT NULL COMMENT 'per day',
  `percentage` tinyint(1) unsigned DEFAULT '0' COMMENT '0=fixed amount per day 1=percentage discount',
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `durationdiscounts` */

insert  into `durationdiscounts`(`id`,`id_vehicle`,`duration`,`discount`,`percentage`,`created_at`) values (2,1,3,5.0000,0,NULL),(5,1,15,8.0000,0,NULL),(6,1,28,10.0000,0,NULL);

/*Table structure for table `features` */

DROP TABLE IF EXISTS `features`;

CREATE TABLE `features` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `icon_type` tinyint(1) DEFAULT '0' COMMENT '0=label 1=icon 3=img',
  `icon_value` varchar(500) DEFAULT NULL COMMENT 'label, fontawesome icon code or img path',
  `created_at` datetime DEFAULT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

/*Data for the table `features` */

insert  into `features`(`id`,`name`,`description`,`icon_type`,`icon_value`,`created_at`,`active`,`deleted`) values (1,'ert','weryoyi',1,'fa-bug','2015-11-27 12:52:04',1,0),(2,'ih','guo',1,'fa-car','2015-11-27 12:52:04',1,0),(3,'nje','one',0,'nje','2015-11-27 12:52:04',1,0),(4,'dy bug','',1,'fa-bug','2015-11-27 13:03:39',0,0),(5,'tre','joins',0,NULL,'2015-11-27 13:09:10',1,1),(6,'newlbl','new img',0,'lbl','2015-11-27 13:11:14',1,0),(7,'test','',0,NULL,'2015-11-27 13:12:55',1,1),(8,'wo','',0,NULL,'2015-11-27 13:16:35',1,1),(9,'treeee','',2,'bjwoT1454511572.png','2015-11-27 13:35:11',1,0),(10,'asdf','',2,'agxvY1454511584.jpg','2015-11-27 13:38:24',0,0),(11,'Bluetooth','',1,'fa-bluetooth','2015-12-17 16:56:25',1,0);

/*Table structure for table `location_to_location_costs` */

DROP TABLE IF EXISTS `location_to_location_costs`;

CREATE TABLE `location_to_location_costs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_location_from` int(10) unsigned NOT NULL,
  `id_location_to` int(10) unsigned NOT NULL,
  `cost` double(8,2) DEFAULT '0.00',
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

/*Data for the table `location_to_location_costs` */

insert  into `location_to_location_costs`(`id`,`id_location_from`,`id_location_to`,`cost`,`created_at`) values (8,8,6,0.00,'2016-02-09 21:21:35'),(10,3,6,0.00,'2016-02-09 21:21:35'),(11,7,6,0.00,'2016-02-09 21:21:35'),(12,6,6,0.00,'2016-02-09 21:21:35'),(13,1,6,0.00,'2016-02-09 21:21:35'),(14,5,6,0.00,'2016-02-09 21:21:35'),(15,0,6,6.00,'2016-02-09 21:21:35'),(16,8,2,0.00,'2016-02-09 21:21:58'),(17,2,2,50.00,'2016-02-09 21:21:58'),(18,3,2,0.00,'2016-02-09 21:21:58'),(19,7,2,0.00,'2016-02-09 21:21:58'),(20,6,2,30.00,'2016-02-09 21:21:58'),(21,1,2,0.00,'2016-02-09 21:21:58'),(22,5,2,0.00,'2016-02-09 21:21:58'),(23,0,2,2.00,'2016-02-09 21:21:58');

/*Table structure for table `locations` */

DROP TABLE IF EXISTS `locations`;

CREATE TABLE `locations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `code` varchar(20) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `address` varchar(500) DEFAULT NULL,
  `lat` varchar(50) DEFAULT NULL,
  `lng` varchar(50) DEFAULT NULL,
  `tel` varchar(50) DEFAULT NULL,
  `tel2` varchar(50) DEFAULT NULL,
  `fax` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `id_locationtype` tinyint(3) unsigned DEFAULT NULL,
  `after_hours_price` double(6,2) DEFAULT NULL COMMENT 'after operational hour extra charge',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `locations` */

insert  into `locations`(`id`,`name`,`code`,`city`,`address`,`lat`,`lng`,`tel`,`tel2`,`fax`,`email`,`id_locationtype`,`after_hours_price`,`created_at`,`updated_at`,`active`,`deleted`) values (1,'Tirane','01','Tirane','Bulevardi Zogu I','41.318124791867035','19.809674620628357','NYT','','','',4,30.00,NULL,'2015-12-14 13:18:15',1,0),(2,'Durres','02','Durres','Rr. Teuta','41.317141714635824','19.45479154586792','219713','','12','',2,30.00,NULL,'2015-12-14 13:22:31',1,0),(3,'Fier','03','Fier','Fier','40.730575957147444','19.55469846725464','234','','12','',3,30.00,NULL,'2015-12-14 13:25:05',1,0),(5,'Vlore','04','Vlore','Bulevardi Vlore-Skele','40.4537397627549','19.48871612548828','57700','485845-9','','',2,30.00,'2015-11-24 10:10:22','2015-11-24 10:10:22',1,0),(6,'Tirana TIA (Rinas)',NULL,'Tirane','Rinas Airport Terminal','41.420155127079006','19.712133407592773','+35578899090','','+35578899090','one@mail.com',1,30.00,'2015-12-14 13:21:10','2015-12-15 09:49:22',1,0),(7,'Komuna e Parisit',NULL,'Tirana','Komuna e parisit','41.31914008557364','19.80757713317871','09900','','','',4,30.00,'2015-12-17 17:04:35','2015-12-17 17:04:35',1,0),(8,'Agjensia \"FriensTravel\" Elbasan',NULL,'Elbasan','Rruga Cerciz Topulli ne hyrje te qytetit','41.10900903575741','20.07178544998169','06920000943','','','',4,30.00,'2016-01-22 09:31:01','2016-01-22 09:31:01',1,0);

/*Table structure for table `locationtypes` */

DROP TABLE IF EXISTS `locationtypes`;

CREATE TABLE `locationtypes` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `flg_delete` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `locationtypes` */

insert  into `locationtypes`(`id`,`name`,`flg_delete`,`deleted`) values (1,'Airport',0,0),(2,'Port',0,0),(3,'Train Station',0,0),(4,'Downtown',0,0);

/*Table structure for table `operationalhours` */

DROP TABLE IF EXISTS `operationalhours`;

CREATE TABLE `operationalhours` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_location` int(10) unsigned DEFAULT NULL,
  `start_day` tinyint(4) DEFAULT NULL COMMENT '1=Monday 7=Sunday',
  `end_day` tinyint(4) DEFAULT NULL,
  `open_time` time DEFAULT NULL,
  `close_time` time DEFAULT NULL,
  `not_operational` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

/*Data for the table `operationalhours` */

insert  into `operationalhours`(`id`,`id_location`,`start_day`,`end_day`,`open_time`,`close_time`,`not_operational`) values (1,2,1,5,'08:00:00','20:15:00',0),(5,2,3,6,'09:30:00','16:30:00',0),(6,2,7,7,'00:00:00','00:00:00',1),(7,6,1,7,'07:00:00','24:00:00',0),(9,7,1,5,'08:00:00','20:00:00',0),(10,7,3,6,'09:00:00','17:30:00',0),(11,7,7,7,'00:00:00','00:00:00',1),(12,8,1,7,'09:00:00','17:30:00',0),(13,5,1,6,'02:30:00','00:00:00',0);

/*Table structure for table `options` */

DROP TABLE IF EXISTS `options`;

CREATE TABLE `options` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `icon_type` tinyint(1) DEFAULT '0' COMMENT '0=label 1=icon 3=img',
  `icon_value` varchar(500) DEFAULT NULL COMMENT 'label, fontawesome icon code or img path',
  `created_at` datetime DEFAULT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `options` */

insert  into `options`(`id`,`name`,`description`,`icon_type`,`icon_value`,`created_at`,`active`,`deleted`) values (1,'ert','weryoyi',1,'fa-bug','2015-11-27 12:52:04',1,0),(2,'ih','guo',1,'fa-car','2015-11-27 12:52:04',1,0),(3,'nje','one',0,'nje','2015-11-27 12:52:04',1,0),(4,'dy bug','',1,'fa-bug','2015-11-27 13:03:39',0,0),(5,'tre','joins',0,NULL,'2015-11-27 13:09:10',1,1),(6,'newlbl','new img',0,'lbl','2015-11-27 13:11:14',1,0),(7,'test','',0,NULL,'2015-11-27 13:12:55',1,1),(8,'wo','',0,NULL,'2015-11-27 13:16:35',1,1),(9,'treeee','',2,'N82QJ1448631311.png','2015-11-27 13:35:11',1,0),(10,'asdf','',2,'ITGce1448631504.png','2015-11-27 13:38:24',0,0);

/*Table structure for table `paymentmethod` */

DROP TABLE IF EXISTS `paymentmethod`;

CREATE TABLE `paymentmethod` (
  `id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `paymentmethod` */

insert  into `paymentmethod`(`id`,`name`) values (0,'None'),(1,'Paypal'),(2,'Cash'),(3,'Bank'),(4,'Check'),(5,'Other');

/*Table structure for table `payments` */

DROP TABLE IF EXISTS `payments`;

CREATE TABLE `payments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_reservation` int(10) unsigned DEFAULT NULL,
  `id_user` int(10) unsigned DEFAULT NULL COMMENT 'client',
  `amount` double(10,2) DEFAULT NULL,
  `id_paymentmethod` tinyint(4) DEFAULT '1',
  `id_paymenttype` tinyint(4) DEFAULT '1',
  `id_user_verified` int(11) DEFAULT '-1' COMMENT 'by paypal=0 else user_id',
  `verified_by` varchar(100) DEFAULT NULL COMMENT 'by paypal or user_fullname',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0=pending 1=verified 2=canceled by paypal',
  `paypal_transaction_id` varchar(500) DEFAULT NULL,
  `details` text,
  `paid_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

/*Data for the table `payments` */

insert  into `payments`(`id`,`id_reservation`,`id_user`,`amount`,`id_paymentmethod`,`id_paymenttype`,`id_user_verified`,`verified_by`,`status`,`paypal_transaction_id`,`details`,`paid_at`,`created_at`) values (1,1,3,101.00,1,1,1,'Admin Admin',1,NULL,'','2016-04-06 09:22:18','2016-03-14 19:20:15'),(2,2,3,235.00,1,5,-1,'',0,'PAY-3KX301932K733154JK3TVKKQ',NULL,NULL,'2016-03-15 00:19:52'),(3,3,3,235.00,1,1,0,'Paypal',1,'PAY-3MN3466654359833HK3TVLLA','{}',NULL,'2016-03-15 00:22:02'),(4,4,3,279.00,1,1,-1,'',0,NULL,NULL,NULL,'2016-03-15 13:35:49'),(5,5,3,279.00,1,1,-1,'',0,NULL,NULL,NULL,'2016-03-15 13:36:29'),(6,6,3,279.00,1,1,-1,'',0,NULL,NULL,NULL,'2016-03-15 13:39:02'),(7,7,3,279.00,1,1,-1,'',0,'PAY-5XR91054R7394683YK3UBF2A',NULL,NULL,'2016-03-15 13:49:26'),(8,8,3,279.00,1,1,-1,'',0,'PAY-1RJ44122MM532884AK3UBHFY',NULL,NULL,'2016-03-15 13:52:21'),(9,9,3,279.00,1,1,-1,'',0,NULL,NULL,NULL,'2016-03-15 13:59:06'),(10,10,3,279.00,1,1,0,'Paypal',1,'PAY-4UU12574V9293351EK3UBLYA','{\"id\":\"PAY-4UU12574V9293351EK3UBLYA\",\"intent\":\"sale\",\"state\":\"approved\",\"cart\":\"4FH823314D662700S\",\"payer\":{\"payment_method\":\"paypal\",\"status\":\"VERIFIED\",\"payer_info\":{\"email\":\"friends.travel.rental-buyer@gmail.com\",\"first_name\":\"test\",\"last_name\":\"buyer\",\"payer_id\":\"D2CHY6BJ73QG6\",\"shipping_address\":{\"recipient_name\":\"test buyer\",\"line1\":\"1 Main St\",\"city\":\"San Jose\",\"state\":\"CA\",\"postal_code\":\"95131\",\"country_code\":\"US\"},\"phone\":\"4083227882\",\"country_code\":\"US\",\"billing_address\":{\"line1\":\"1 Main St\",\"line2\":\"\",\"city\":\"San Jose\",\"state\":\"CA\",\"postal_code\":\"95131\",\"country_code\":\"US\"}}},\"transactions\":[{\"amount\":{\"total\":\"279.00\",\"currency\":\"EUR\",\"details\":{}},\"payee\":{\"merchant_id\":\"MZ884JBRWHKLW\"},\"description\":\" days for Mercedes Benz - E 320\",\"item_list\":{\"items\":[{\"name\":\"Base Rate\",\"price\":\"45.25\",\"currency\":\"EUR\",\"quantity\":4},{\"name\":\"Discount\",\"price\":\"-20.00\",\"currency\":\"EUR\",\"quantity\":1},{\"name\":\"Different Return Location\",\"price\":\"30.00\",\"currency\":\"EUR\",\"quantity\":1},{\"name\":\"Additional Driver\",\"price\":\"48.00\",\"currency\":\"EUR\",\"quantity\":1},{\"name\":\"Child booster seat\",\"price\":\"40.00\",\"currency\":\"EUR\",\"quantity\":1}],\"shipping_address\":{\"recipient_name\":\"test buyer\",\"line1\":\"1 Main St\",\"city\":\"San Jose\",\"state\":\"CA\",\"postal_code\":\"95131\",\"country_code\":\"US\"}},\"related_resources\":[{\"sale\":{\"id\":\"80C22674JJ278642D\",\"state\":\"pending\",\"amount\":{\"total\":\"279.00\",\"currency\":\"EUR\",\"details\":{}},\"payment_mode\":\"INSTANT_TRANSFER\",\"reason_code\":\"RECEIVING_PREFERENCE_MANDATES_MANUAL_ACTION\",\"protection_eligibility\":\"INELIGIBLE\",\"parent_payment\":\"PAY-4UU12574V9293351EK3UBLYA\",\"create_time\":\"2016-03-15T14:04:52Z\",\"update_time\":\"2016-03-15T14:04:53Z\",\"links\":[{\"href\":\"https://api.sandbox.paypal.com/v1/payments/sale/80C22674JJ278642D\",\"rel\":\"self\",\"method\":\"GET\"},{\"href\":\"https://api.sandbox.paypal.com/v1/payments/sale/80C22674JJ278642D/refund\",\"rel\":\"refund\",\"method\":\"POST\"},{\"href\":\"https://api.sandbox.paypal.com/v1/payments/payment/PAY-4UU12574V9293351EK3UBLYA\",\"rel\":\"parent_payment\",\"method\":\"GET\"}]}}]}],\"redirect_urls\":{\"return_url\":\"http://rental.arcuus.com/paypal-response/10/ok?paymentId=PAY-4UU12574V9293351EK3UBLYA\",\"cancel_url\":\"http://rental.arcuus.com/paypal-response/10/canceled\"},\"create_time\":\"2016-03-15T14:04:53Z\",\"update_time\":\"2016-03-15T14:02:08Z\",\"links\":[{\"href\":\"https://api.sandbox.paypal.com/v1/payments/payment/PAY-4UU12574V9293351EK3UBLYA\",\"rel\":\"self\",\"method\":\"GET\"}]}',NULL,'2016-03-15 14:02:06'),(11,11,3,30.00,1,1,1,'Admin Admin',1,NULL,'','2016-04-06 09:39:20','2016-03-15 14:58:52'),(12,12,3,35.00,1,1,-1,'',0,'PAY-1M5682252D655542CK3UCIUI',NULL,NULL,'2016-03-15 15:03:43'),(13,13,3,35.00,1,1,-1,'',0,'PAY-1TK85268WL0963442K3UCMNY',NULL,NULL,'2016-03-15 15:11:49'),(14,14,3,35.00,1,1,-1,'',0,'PAY-08M167108C4758033K3UCYBI',NULL,NULL,'2016-03-15 15:36:35'),(15,15,3,37.00,1,1,-1,'',0,'PAY-1NA0146262576923HK3UK5YA',NULL,NULL,'2016-03-16 00:54:54'),(16,16,7,212.00,1,1,-1,NULL,0,NULL,NULL,NULL,'2016-03-25 09:12:26'),(17,17,7,212.00,1,1,-1,NULL,0,NULL,NULL,NULL,'2016-03-25 09:20:59'),(18,18,7,55.00,1,1,-1,NULL,0,NULL,NULL,NULL,'2016-03-25 11:54:07'),(19,19,7,55.00,0,1,-1,NULL,0,NULL,NULL,NULL,'2016-03-25 12:07:33'),(20,20,10,224.00,1,1,-1,NULL,0,'PAY-1AA29405EE132353EK32VLQQ',NULL,NULL,'2016-03-25 15:13:59'),(21,21,10,224.00,1,1,-1,NULL,0,'PAY-0LF66734RN9302918K32VO4Y',NULL,NULL,'2016-03-25 15:21:15'),(22,22,10,224.00,1,1,-1,NULL,0,'PAY-7F205881AC307330NK32VQXQ',NULL,NULL,'2016-03-25 15:25:10'),(23,23,10,224.00,1,1,-1,NULL,0,'PAY-2HM68838238997251K32VR2Y',NULL,NULL,'2016-03-25 15:27:31'),(24,24,10,224.00,1,1,-1,NULL,0,'PAY-3X3251800M858992CK32VUIQ',NULL,NULL,'2016-03-25 15:32:42'),(25,25,10,224.00,1,1,-1,NULL,0,'PAY-82336967LH560513CK32VXTQ',NULL,NULL,'2016-03-25 15:39:50'),(26,26,1,56.00,1,1,-1,NULL,0,'PAY-70528039EY052641KK32VZ5A',NULL,NULL,'2016-03-25 15:44:44'),(27,27,1,39.00,1,1,-1,NULL,0,'PAY-4X675228027674045K32V2XA',NULL,NULL,'2016-03-25 15:46:29'),(28,28,7,25.00,1,1,-1,NULL,0,'PAY-1DA08025S6706872BK32V3LA',NULL,NULL,'2016-03-25 15:47:48'),(29,29,7,30.00,1,1,-1,NULL,0,'PAY-55945952LB215850AK32V5AQ',NULL,NULL,'2016-03-25 15:51:23'),(30,30,7,30.00,1,1,-1,NULL,0,'PAY-0SX79295LN277584WK32V5OI',NULL,NULL,'2016-03-25 15:52:18'),(31,1,1,20.00,2,0,1,'Admin Admin',1,NULL,'Paguar Filanit prane agjensise Tirane','2016-04-06 09:27:33','2016-04-06 09:26:10'),(32,11,1,5.00,2,0,1,'Admin Admin',1,NULL,'','2016-04-06 09:39:12','2016-04-06 09:39:12'),(33,1,1,30.00,3,0,1,'Admin Admin',1,NULL,'Bank: Credins\r\nAcc: 2949304\r\n','2016-04-06 11:40:24','2016-04-06 11:40:24'),(34,31,1,138.00,1,1,-1,NULL,0,'PAY-2FM89707H19382512K4DOSCI',NULL,NULL,'2016-04-07 23:10:56');

/*Table structure for table `paymenttype` */

DROP TABLE IF EXISTS `paymenttype`;

CREATE TABLE `paymenttype` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `paymenttype` */

insert  into `paymenttype`(`id`,`name`) values (0,'Partial Payment'),(1,'Full Payment'),(5,'Safety Deposit');

/*Table structure for table `reservation_drivers` */

DROP TABLE IF EXISTS `reservation_drivers`;

CREATE TABLE `reservation_drivers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_reservation` int(10) unsigned DEFAULT NULL,
  `id_driver` int(10) unsigned DEFAULT NULL,
  `driver_name` varchar(200) DEFAULT NULL,
  `primary` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;

/*Data for the table `reservation_drivers` */

insert  into `reservation_drivers`(`id`,`id_reservation`,`id_driver`,`driver_name`,`primary`) values (1,1,7,'Seven7 Shtate',1),(2,2,7,'Seven7 Shtate',1),(3,3,7,'Seven7 Shtate',1),(4,4,7,'Seven7 Shtate',1),(5,4,8,'Eight Tete',0),(6,5,7,'Seven7 Shtate',1),(7,5,8,'Eight Tete',0),(8,6,7,'Seven7 Shtate',1),(9,6,8,'Eight Tete',0),(10,7,7,'Seven7 Shtate',1),(11,7,8,'Eight Tete',0),(12,8,7,'Seven7 Shtate',1),(13,9,7,'Seven7 Shtate',1),(14,10,7,'Seven7 Shtate',1),(15,11,7,'Seven7 Shtate',1),(16,12,7,'Seven7 Shtate',1),(17,13,7,'Seven7 Shtate',1),(18,14,7,'Seven7 Shtate',1),(19,15,7,'Seven7 Shtate',1),(20,16,12,'Fifteen Twenty',1),(21,17,12,'Fifteen Twenty',1),(22,18,12,'Fifteen Twenty',1),(23,19,12,'Fifteen Twenty',1),(24,20,15,'Shtate Seven',1),(25,21,15,'Shtate Seven',1),(26,22,15,'Shtate Seven',1),(27,23,15,'Shtate Seven',1),(28,24,15,'Shtate Seven',1),(29,25,15,'Shtate Seven',1),(30,26,12,'Fifteen Twenty',1),(31,27,13,'Pese Fifty',1),(32,28,12,'Fifteen Twenty',1),(33,29,12,'Fifteen Twenty',1),(34,30,12,'Fifteen Twenty',1),(35,31,8,'Eight Tete',1);

/*Table structure for table `reservation_entries` */

DROP TABLE IF EXISTS `reservation_entries`;

CREATE TABLE `reservation_entries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_reservation` int(10) unsigned DEFAULT NULL,
  `entry_name` varchar(500) DEFAULT NULL,
  `quantity` double(10,2) DEFAULT '1.00',
  `price` double(10,4) DEFAULT NULL,
  `id_accessory` int(10) unsigned DEFAULT '0' COMMENT '0=no accesory',
  `details` varchar(2000) DEFAULT NULL,
  `deleted` tinyint(1) unsigned DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=108 DEFAULT CHARSET=latin1;

/*Data for the table `reservation_entries` */

insert  into `reservation_entries`(`id`,`id_reservation`,`entry_name`,`quantity`,`price`,`id_accessory`,`details`,`deleted`) values (1,1,'Base Rate',1.00,56.0000,0,'[{\"type\":\"standard\",\"name\":\"Standard Price\",\"rate\":56,\"amount\":1,\"total\":56}]',0),(2,1,'Different Return Location',1.00,30.0000,0,'\"<b>EUR 30<\\/b> extra charge for the <b>different<\\/b> RETURN LOCATION.\"',0),(3,1,'Navigational system',1.00,10.0000,14,'',0),(4,1,'Winter snow tires for five to six people(1 set)',1.00,5.0000,13,'',0),(5,2,'Base Rate',3.00,142.0000,0,'[{\"type\":\"standard\",\"name\":\"Standard Price\",\"rate\":\"39.00\",\"amount\":\"3\",\"total\":117},{\"type\":\"return_location_cost\",\"name\":\"Return Location Cost\",\"rate\":\"25.00\",\"amount\":1,\"total\":\"25.00\"}]',0),(6,2,'Discount',1.00,-15.0000,0,'{\"type\":\"discount_fixed\",\"name\":\"3 Days Discount\",\"rate\":15,\"amount\":\"3\",\"total\":45}',0),(7,2,'Different Return Location',1.00,30.0000,0,'\"<b>EUR 30.00<\\/b> extra charge for the <b>different<\\/b> RETURN LOCATION.\"',0),(8,2,'Additional Driver',1.00,36.0000,7,'',0),(9,2,'Child booster seat',1.00,30.0000,8,'',0),(10,2,'Cross border fee',1.00,12.0000,10,'',0),(11,3,'Base Rate',3.00,142.0000,0,'[{\"type\":\"standard\",\"name\":\"Standard Price\",\"rate\":\"39.00\",\"amount\":\"3\",\"total\":117},{\"type\":\"return_location_cost\",\"name\":\"Return Location Cost\",\"rate\":\"25.00\",\"amount\":1,\"total\":\"25.00\"}]',0),(12,3,'Discount',1.00,-15.0000,0,'{\"type\":\"discount_fixed\",\"name\":\"3 Days Discount\",\"rate\":15,\"amount\":\"3\",\"total\":45}',0),(13,3,'Different Return Location',1.00,30.0000,0,'\"<b>EUR 30.00<\\/b> extra charge for the <b>different<\\/b> RETURN LOCATION.\"',0),(14,3,'Additional Driver',1.00,36.0000,7,'',0),(15,3,'Child booster seat',1.00,30.0000,8,'',0),(16,3,'Cross border fee',1.00,12.0000,10,'',0),(17,4,'Base Rate',4.00,181.0000,0,'[{\"type\":\"standard\",\"name\":\"Standard Price\",\"rate\":\"39.00\",\"amount\":\"4\",\"total\":156},{\"type\":\"return_location_cost\",\"name\":\"Return Location Cost\",\"rate\":\"25.00\",\"amount\":1,\"total\":\"25.00\"}]',0),(18,4,'Discount',1.00,-20.0000,0,'{\"type\":\"discount_fixed\",\"name\":\"4 Days Discount\",\"rate\":20,\"amount\":\"4\",\"total\":80}',0),(19,4,'Different Return Location',1.00,30.0000,0,'\"<b>EUR 30.00<\\/b> extra charge for the <b>different<\\/b> RETURN LOCATION.\"',0),(20,4,'Additional Driver',1.00,48.0000,7,'',0),(21,4,'Child booster seat',1.00,40.0000,8,'',0),(22,5,'Base Rate',4.00,181.0000,0,'[{\"type\":\"standard\",\"name\":\"Standard Price\",\"rate\":\"39.00\",\"amount\":\"4\",\"total\":156},{\"type\":\"return_location_cost\",\"name\":\"Return Location Cost\",\"rate\":\"25.00\",\"amount\":1,\"total\":\"25.00\"}]',0),(23,5,'Discount',1.00,-20.0000,0,'{\"type\":\"discount_fixed\",\"name\":\"4 Days Discount\",\"rate\":20,\"amount\":\"4\",\"total\":80}',0),(24,5,'Different Return Location',1.00,30.0000,0,'\"<b>EUR 30.00<\\/b> extra charge for the <b>different<\\/b> RETURN LOCATION.\"',0),(25,5,'Additional Driver',1.00,48.0000,7,'',0),(26,5,'Child booster seat',1.00,40.0000,8,'',0),(27,6,'Base Rate',4.00,181.0000,0,'[{\"type\":\"standard\",\"name\":\"Standard Price\",\"rate\":\"39.00\",\"amount\":\"4\",\"total\":156},{\"type\":\"return_location_cost\",\"name\":\"Return Location Cost\",\"rate\":\"25.00\",\"amount\":1,\"total\":\"25.00\"}]',0),(28,6,'Discount',1.00,-20.0000,0,'{\"type\":\"discount_fixed\",\"name\":\"4 Days Discount\",\"rate\":20,\"amount\":\"4\",\"total\":80}',0),(29,6,'Different Return Location',1.00,30.0000,0,'\"<b>EUR 30.00<\\/b> extra charge for the <b>different<\\/b> RETURN LOCATION.\"',0),(30,6,'Additional Driver',1.00,48.0000,7,'',0),(31,6,'Child booster seat',1.00,40.0000,8,'',0),(32,7,'Base Rate',4.00,181.0000,0,'[{\"type\":\"standard\",\"name\":\"Standard Price\",\"rate\":\"39.00\",\"amount\":\"4\",\"total\":156},{\"type\":\"return_location_cost\",\"name\":\"Return Location Cost\",\"rate\":\"25.00\",\"amount\":1,\"total\":\"25.00\"}]',0),(33,7,'Discount',1.00,-20.0000,0,'{\"type\":\"discount_fixed\",\"name\":\"4 Days Discount\",\"rate\":20,\"amount\":\"4\",\"total\":80}',0),(34,7,'Different Return Location',1.00,30.0000,0,'\"<b>EUR 30.00<\\/b> extra charge for the <b>different<\\/b> RETURN LOCATION.\"',0),(35,7,'Additional Driver',1.00,48.0000,7,'',0),(36,7,'Child booster seat',1.00,40.0000,8,'',0),(37,8,'Base Rate',4.00,181.0000,0,'[{\"type\":\"standard\",\"name\":\"Standard Price\",\"rate\":\"39.00\",\"amount\":\"4\",\"total\":156},{\"type\":\"return_location_cost\",\"name\":\"Return Location Cost\",\"rate\":\"25.00\",\"amount\":1,\"total\":\"25.00\"}]',0),(38,8,'Discount',1.00,-20.0000,0,'{\"type\":\"discount_fixed\",\"name\":\"4 Days Discount\",\"rate\":20,\"amount\":\"4\",\"total\":80}',0),(39,8,'Different Return Location',1.00,30.0000,0,'\"<b>EUR 30.00<\\/b> extra charge for the <b>different<\\/b> RETURN LOCATION.\"',0),(40,8,'Additional Driver',1.00,48.0000,7,'',0),(41,8,'Child booster seat',1.00,40.0000,8,'',0),(42,9,'Base Rate',4.00,181.0000,0,'[{\"type\":\"standard\",\"name\":\"Standard Price\",\"rate\":\"39.00\",\"amount\":\"4\",\"total\":156},{\"type\":\"return_location_cost\",\"name\":\"Return Location Cost\",\"rate\":\"25.00\",\"amount\":1,\"total\":\"25.00\"}]',0),(43,9,'Discount',1.00,-20.0000,0,'{\"type\":\"discount_fixed\",\"name\":\"4 Days Discount\",\"rate\":20,\"amount\":\"4\",\"total\":80}',0),(44,9,'Different Return Location',1.00,30.0000,0,'\"<b>EUR 30.00<\\/b> extra charge for the <b>different<\\/b> RETURN LOCATION.\"',0),(45,9,'Additional Driver',1.00,48.0000,7,'',0),(46,9,'Child booster seat',1.00,40.0000,8,'',0),(47,10,'Base Rate',4.00,181.0000,0,'[{\"type\":\"standard\",\"name\":\"Standard Price\",\"rate\":\"39.00\",\"amount\":\"4\",\"total\":156},{\"type\":\"return_location_cost\",\"name\":\"Return Location Cost\",\"rate\":\"25.00\",\"amount\":1,\"total\":\"25.00\"}]',0),(48,10,'Discount',1.00,-20.0000,0,'{\"type\":\"discount_fixed\",\"name\":\"4 Days Discount\",\"rate\":20,\"amount\":\"4\",\"total\":80}',0),(49,10,'Different Return Location',1.00,30.0000,0,'\"<b>EUR 30.00<\\/b> extra charge for the <b>different<\\/b> RETURN LOCATION.\"',0),(50,10,'Additional Driver',1.00,48.0000,7,'',0),(51,10,'Child booster seat',1.00,40.0000,8,'',0),(52,11,'Base Rate',1.00,25.0000,0,'[{\"type\":\"standard\",\"name\":\"Standard Price\",\"rate\":\"25.00\",\"amount\":\"1\",\"total\":25}]',0),(53,11,'Child booster seat',1.00,10.0000,8,'',0),(54,12,'Base Rate',1.00,25.0000,0,'[{\"type\":\"standard\",\"name\":\"Standard Price\",\"rate\":\"25.00\",\"amount\":\"1\",\"total\":25}]',0),(55,12,'Child booster seat',1.00,10.0000,8,'',0),(56,13,'Base Rate',1.00,25.0000,0,'[{\"type\":\"standard\",\"name\":\"Standard Price\",\"rate\":\"25.00\",\"amount\":\"1\",\"total\":25}]',0),(57,13,'Navigational system',1.00,10.0000,14,'',0),(58,14,'Base Rate',1.00,25.0000,0,'[{\"type\":\"standard\",\"name\":\"Standard Price\",\"rate\":\"25.00\",\"amount\":\"1\",\"total\":25}]',0),(59,14,'Navigational system',1.00,10.0000,14,'',0),(60,15,'Base Rate',1.00,25.0000,0,'[{\"type\":\"standard\",\"name\":\"Standard Price\",\"rate\":\"25.00\",\"amount\":\"1\",\"total\":25}]',0),(61,15,'Infant safety seat',1.00,12.0000,9,'',0),(62,16,'Base Rate',3.00,125.0000,0,'[{\"type\":\"standard\",\"name\":\"Standard Price\",\"rate\":30,\"amount\":3,\"total\":90},{\"type\":\"return_location_cost\",\"name\":\"Return Location Cost\",\"rate\":35,\"amount\":1,\"total\":35}]',0),(63,16,'Discount',1.00,-9.0000,0,'{\"type\":\"discount_fixed\",\"name\":\"3 Days Discount\",\"rate\":9,\"amount\":3,\"total\":27}',0),(64,16,'Different Return Location',1.00,30.0000,0,'\"<b>EUR 30<\\/b> extra charge for the <b>different<\\/b> RETURN LOCATION.\"',0),(65,16,'Child booster seat',1.00,30.0000,8,'',0),(66,16,'Child safety seat',1.00,36.0000,12,'',0),(67,17,'Base Rate',3.00,125.0000,0,'[{\"type\":\"standard\",\"name\":\"Standard Price\",\"rate\":30,\"amount\":3,\"total\":90},{\"type\":\"return_location_cost\",\"name\":\"Return Location Cost\",\"rate\":35,\"amount\":1,\"total\":35}]',0),(68,17,'Discount',1.00,-9.0000,0,'{\"type\":\"discount_fixed\",\"name\":\"3 Days Discount\",\"rate\":9,\"amount\":3,\"total\":27}',0),(69,17,'Different Return Location',1.00,30.0000,0,'\"<b>EUR 30<\\/b> extra charge for the <b>different<\\/b> RETURN LOCATION.\"',0),(70,17,'Child booster seat',1.00,30.0000,8,'',0),(71,17,'Child safety seat',1.00,36.0000,12,'',0),(72,18,'Base Rate',1.00,45.0000,0,'[{\"type\":\"standard\",\"name\":\"Standard Price\",\"rate\":45,\"amount\":1,\"total\":45}]',0),(73,18,'Child booster seat',1.00,10.0000,8,'',0),(74,19,'Base Rate',1.00,45.0000,0,'[{\"type\":\"standard\",\"name\":\"Standard Price\",\"rate\":45,\"amount\":1,\"total\":45}]',0),(75,19,'Child booster seat',1.00,10.0000,8,'',0),(76,20,'Base Rate',4.00,156.0000,0,'[{\"type\":\"standard\",\"name\":\"Standard Price\",\"rate\":39,\"amount\":4,\"total\":156}]',0),(77,20,'Discount',1.00,-20.0000,0,'{\"type\":\"discount_fixed\",\"name\":\"4 Days Discount\",\"rate\":20,\"amount\":4,\"total\":80}',0),(78,20,'Additional Driver',1.00,48.0000,7,'',0),(79,20,'Child booster seat',1.00,40.0000,8,'',0),(80,21,'Base Rate',4.00,156.0000,0,'[{\"type\":\"standard\",\"name\":\"Standard Price\",\"rate\":39,\"amount\":4,\"total\":156}]',0),(81,21,'Discount',1.00,-20.0000,0,'{\"type\":\"discount_fixed\",\"name\":\"4 Days Discount\",\"rate\":20,\"amount\":4,\"total\":80}',0),(82,21,'Additional Driver',1.00,48.0000,7,'',0),(83,21,'Child booster seat',1.00,40.0000,8,'',0),(84,22,'Base Rate',4.00,156.0000,0,'[{\"type\":\"standard\",\"name\":\"Standard Price\",\"rate\":39,\"amount\":4,\"total\":156}]',0),(85,22,'Discount',1.00,-20.0000,0,'{\"type\":\"discount_fixed\",\"name\":\"4 Days Discount\",\"rate\":20,\"amount\":4,\"total\":80}',0),(86,22,'Additional Driver',1.00,48.0000,7,'',0),(87,22,'Child booster seat',1.00,40.0000,8,'',0),(88,23,'Base Rate',4.00,156.0000,0,'[{\"type\":\"standard\",\"name\":\"Standard Price\",\"rate\":39,\"amount\":4,\"total\":156}]',0),(89,23,'Discount',1.00,-20.0000,0,'{\"type\":\"discount_fixed\",\"name\":\"4 Days Discount\",\"rate\":20,\"amount\":4,\"total\":80}',0),(90,23,'Additional Driver',1.00,48.0000,7,'',0),(91,23,'Child booster seat',1.00,40.0000,8,'',0),(92,24,'Base Rate',4.00,156.0000,0,'[{\"type\":\"standard\",\"name\":\"Standard Price\",\"rate\":39,\"amount\":4,\"total\":156}]',0),(93,24,'Discount',1.00,-20.0000,0,'{\"type\":\"discount_fixed\",\"name\":\"4 Days Discount\",\"rate\":20,\"amount\":4,\"total\":80}',0),(94,24,'Additional Driver',1.00,48.0000,7,'',0),(95,24,'Child booster seat',1.00,40.0000,8,'',0),(96,25,'Base Rate',4.00,156.0000,0,'[{\"type\":\"standard\",\"name\":\"Standard Price\",\"rate\":39,\"amount\":4,\"total\":156}]',0),(97,25,'Discount',1.00,-20.0000,0,'{\"type\":\"discount_fixed\",\"name\":\"4 Days Discount\",\"rate\":20,\"amount\":4,\"total\":80}',0),(98,25,'Additional Driver',1.00,48.0000,7,'',0),(99,25,'Child booster seat',1.00,40.0000,8,'',0),(100,26,'Base Rate',1.00,56.0000,0,'[{\"type\":\"standard\",\"name\":\"Standard Price\",\"rate\":56,\"amount\":1,\"total\":56}]',0),(101,27,'Base Rate',1.00,39.0000,0,'[{\"type\":\"standard\",\"name\":\"Standard Price\",\"rate\":39,\"amount\":1,\"total\":39}]',0),(102,28,'Base Rate',1.00,25.0000,0,'[{\"type\":\"standard\",\"name\":\"Standard Price\",\"rate\":25,\"amount\":1,\"total\":25}]',0),(103,29,'Base Rate',1.00,30.0000,0,'[{\"type\":\"standard\",\"name\":\"Standard Price\",\"rate\":30,\"amount\":1,\"total\":30}]',0),(104,30,'Base Rate',1.00,30.0000,0,'[{\"type\":\"standard\",\"name\":\"Standard Price\",\"rate\":30,\"amount\":1,\"total\":30}]',0),(105,31,'Base Rate',3.00,117.0000,0,'[{\"type\":\"standard\",\"name\":\"Standard Price\",\"rate\":39,\"amount\":3,\"total\":117}]',0),(106,31,'Discount',1.00,-15.0000,0,'{\"type\":\"discount_fixed\",\"name\":\"3 Days Discount\",\"rate\":15,\"amount\":3,\"total\":45}',0),(107,31,'Additional Driver',1.00,36.0000,7,'',0);

/*Table structure for table `reservation_subjects` */

DROP TABLE IF EXISTS `reservation_subjects`;

CREATE TABLE `reservation_subjects` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `subject` varchar(100) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `reservation_subjects` */

insert  into `reservation_subjects`(`id`,`subject`,`description`) values (1,'base_rate','Base Rate'),(2,'duration_discount','Discount based on the duration of your reservation.'),(3,'different_return_location','Extra charge for the different RETURN LOCATION'),(4,'after_hours_pickup_time','Extra charge for the after hours PICKUP TIME'),(5,'after_hours_return_time','Extra charge for the after hours RETURN TIME');

/*Table structure for table `reservations` */

DROP TABLE IF EXISTS `reservations`;

CREATE TABLE `reservations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_vehicle` int(10) unsigned DEFAULT NULL,
  `vehicle_title` varchar(200) DEFAULT NULL,
  `id_user` int(10) unsigned DEFAULT NULL,
  `primary_driver_name` varchar(200) DEFAULT NULL,
  `agreement_url` varchar(500) DEFAULT NULL COMMENT 'pdf url path',
  `pickup_datetime` datetime DEFAULT NULL,
  `id_location_pickup` int(10) unsigned DEFAULT NULL,
  `pickup_location` varchar(100) DEFAULT NULL,
  `return_datetime` datetime DEFAULT NULL,
  `id_location_return` int(10) unsigned DEFAULT NULL,
  `return_location` varchar(100) DEFAULT NULL,
  `duration_days` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `approved` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0=pending 1=approved 2=expired 3=canceled',
  `id_user_approved` int(11) DEFAULT '-1' COMMENT '-1=pending 0=paypal payment completed >1 user approved',
  `approved_user_name` varchar(100) DEFAULT NULL,
  `approved_at` datetime DEFAULT NULL,
  `total_price` double(10,2) DEFAULT NULL,
  `payment_completed` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0=not completed 1=completed 2=temp canceled by paypal',
  `safety_deposit` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `pay_later_request` tinyint(1) unsigned DEFAULT '0' COMMENT '0=no/direct payment 1=request pending 2=request approved 3=request refused (status=0 => canceled)',
  `request_description` varchar(2000) DEFAULT NULL,
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '1=pending/reserved, 0=canceled 2=completed (old)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

/*Data for the table `reservations` */

insert  into `reservations`(`id`,`id_vehicle`,`vehicle_title`,`id_user`,`primary_driver_name`,`agreement_url`,`pickup_datetime`,`id_location_pickup`,`pickup_location`,`return_datetime`,`id_location_return`,`return_location`,`duration_days`,`created_at`,`approved`,`id_user_approved`,`approved_user_name`,`approved_at`,`total_price`,`payment_completed`,`safety_deposit`,`pay_later_request`,`request_description`,`status`) values (1,4,'Daimler AG',3,'Seven7 Shtate',NULL,'2016-03-14 10:00:00',6,'Tirana TIA (Rinas)','2016-03-15 10:00:00',2,'Durres',1,'2016-03-14 19:20:15',1,1,'Admin Admin','2016-04-07 23:06:11',101.00,1,0,0,'',1),(2,1,'Mercedes Benz - E 320',3,'Seven7 Shtate',NULL,'2016-03-22 10:00:00',6,'Tirana TIA (Rinas)','2016-03-25 10:00:00',2,'Durres',3,'2016-03-15 00:19:52',2,-1,NULL,NULL,235.00,0,0,0,'',0),(3,1,'Mercedes Benz - E 320',3,'Seven7 Shtate',NULL,'2016-03-22 10:00:00',6,'Tirana TIA (Rinas)','2016-03-25 10:00:00',2,'Durres',3,'2016-03-15 00:22:02',1,0,'PayPal','2016-03-15 00:22:38',235.00,1,0,0,'',1),(4,1,'Mercedes Benz - E 320',3,'Seven7 Shtate',NULL,'2016-03-29 10:00:00',6,'Tirana TIA (Rinas)','2016-04-02 10:00:00',2,'Durres',4,'2016-03-15 13:35:49',2,-1,NULL,NULL,279.00,0,0,0,'Some special request',0),(5,1,'Mercedes Benz - E 320',3,'Seven7 Shtate',NULL,'2016-03-29 10:00:00',6,'Tirana TIA (Rinas)','2016-04-02 10:00:00',2,'Durres',4,'2016-03-15 13:36:29',2,-1,NULL,NULL,279.00,0,0,0,'Some special request',0),(6,1,'Mercedes Benz - E 320',3,'Seven7 Shtate',NULL,'2016-03-29 10:00:00',6,'Tirana TIA (Rinas)','2016-04-02 10:00:00',2,'Durres',4,'2016-03-15 13:39:02',2,-1,NULL,NULL,279.00,0,0,0,'Some special request',0),(7,1,'Mercedes Benz - E 320',3,'Seven7 Shtate',NULL,'2016-03-29 10:00:00',6,'Tirana TIA (Rinas)','2016-04-02 10:00:00',2,'Durres',4,'2016-03-15 13:49:26',2,-1,NULL,NULL,279.00,0,0,0,'Some special request',0),(8,1,'Mercedes Benz - E 320',3,'Seven7 Shtate',NULL,'2016-03-29 10:00:00',6,'Tirana TIA (Rinas)','2016-04-02 10:00:00',2,'Durres',4,'2016-03-15 13:52:21',2,-1,NULL,NULL,279.00,0,0,0,'Some special request',0),(9,1,'Mercedes Benz - E 320',3,'Seven7 Shtate',NULL,'2016-03-29 10:00:00',6,'Tirana TIA (Rinas)','2016-04-02 10:00:00',2,'Durres',4,'2016-03-15 13:59:06',2,-1,NULL,NULL,279.00,0,0,0,'Some special request',0),(10,1,'Mercedes Benz - E 320',3,'Seven7 Shtate',NULL,'2016-03-29 10:00:00',6,'Tirana TIA (Rinas)','2016-04-02 10:00:00',2,'Durres',4,'2016-03-15 14:02:06',1,0,'PayPal','2016-03-15 14:04:53',279.00,1,0,0,'Some special request',1),(11,5,'Ford Fiesta',3,'Seven7 Shtate',NULL,'2016-03-15 10:00:00',6,'Tirana TIA (Rinas)','2016-03-16 10:00:00',6,'Tirana TIA (Rinas)',1,'2016-03-15 14:58:52',2,-1,NULL,NULL,35.00,1,0,0,'',0),(12,5,'Ford Fiesta',3,'Seven7 Shtate',NULL,'2016-03-15 10:00:00',6,'Tirana TIA (Rinas)','2016-03-16 10:00:00',6,'Tirana TIA (Rinas)',1,'2016-03-15 15:03:43',2,-1,NULL,NULL,35.00,0,0,0,'',0),(13,5,'Ford Fiesta',3,'Seven7 Shtate',NULL,'2016-03-30 10:00:00',6,'Tirana TIA (Rinas)','2016-03-31 10:00:00',6,'Tirana TIA (Rinas)',1,'2016-03-15 15:11:49',2,-1,NULL,NULL,35.00,0,0,0,'',0),(14,5,'Ford Fiesta',3,'Seven7 Shtate',NULL,'2016-03-30 10:00:00',6,'Tirana TIA (Rinas)','2016-03-31 10:00:00',6,'Tirana TIA (Rinas)',1,'2016-03-15 15:36:35',2,-1,NULL,NULL,35.00,0,0,0,'',0),(15,5,'Ford Fiesta',3,'Seven7 Shtate',NULL,'2016-03-16 10:00:00',6,'Tirana TIA (Rinas)','2016-03-17 10:00:00',6,'Tirana TIA (Rinas)',1,'2016-03-16 00:54:54',2,-1,NULL,NULL,37.00,0,0,0,'',0),(16,2,'Renault',7,'Fifteen Twenty',NULL,'2016-03-30 10:00:00',6,'Tirana TIA (Rinas)','2016-04-02 10:00:00',2,'Durres',3,'2016-03-25 09:12:25',2,-1,NULL,NULL,212.00,0,0,0,'',0),(17,2,'Renault',7,'Fifteen Twenty',NULL,'2016-03-30 10:00:00',6,'Tirana TIA (Rinas)','2016-04-02 10:00:00',2,'Durres',3,'2016-03-25 09:20:59',2,-1,NULL,NULL,212.00,0,0,1,'asldkfjsalf',0),(18,3,'BMW X5',7,'Fifteen Twenty',NULL,'2016-03-25 10:00:00',6,'Tirana TIA (Rinas)','2016-03-26 10:00:00',6,'Tirana TIA (Rinas)',1,'2016-03-25 11:54:07',2,-1,NULL,NULL,55.00,0,0,1,'i have a special request',0),(19,3,'BMW X5',7,'Fifteen Twenty',NULL,'2016-03-25 10:00:00',6,'Tirana TIA (Rinas)','2016-03-26 10:00:00',6,'Tirana TIA (Rinas)',1,'2016-03-25 12:07:33',2,-1,NULL,NULL,55.00,0,0,1,'I have another request',0),(20,1,'Mercedes Benz - E 320',10,'Shtate Seven',NULL,'2016-04-11 10:00:00',6,'Tirana TIA (Rinas)','2016-04-15 10:00:00',6,'Tirana TIA (Rinas)',4,'2016-03-25 15:13:59',2,-1,NULL,NULL,224.00,0,0,0,'',0),(21,1,'Mercedes Benz - E 320',10,'Shtate Seven',NULL,'2016-04-11 10:00:00',6,'Tirana TIA (Rinas)','2016-04-15 10:00:00',6,'Tirana TIA (Rinas)',4,'2016-03-25 15:21:15',2,-1,NULL,NULL,224.00,0,0,0,'',0),(22,1,'Mercedes Benz - E 320',10,'Shtate Seven',NULL,'2016-04-11 10:00:00',6,'Tirana TIA (Rinas)','2016-04-15 10:00:00',6,'Tirana TIA (Rinas)',4,'2016-03-25 15:25:10',2,-1,NULL,NULL,224.00,0,0,0,'',0),(23,1,'Mercedes Benz - E 320',10,'Shtate Seven',NULL,'2016-04-11 10:00:00',6,'Tirana TIA (Rinas)','2016-04-15 10:00:00',6,'Tirana TIA (Rinas)',4,'2016-03-25 15:27:31',2,-1,NULL,NULL,224.00,0,0,0,'',0),(24,1,'Mercedes Benz - E 320',10,'Shtate Seven',NULL,'2016-04-11 10:00:00',6,'Tirana TIA (Rinas)','2016-04-15 10:00:00',6,'Tirana TIA (Rinas)',4,'2016-03-25 15:32:42',2,-1,NULL,NULL,224.00,0,0,0,'',0),(25,1,'Mercedes Benz - E 320',10,'Shtate Seven',NULL,'2016-04-11 10:00:00',6,'Tirana TIA (Rinas)','2016-04-15 10:00:00',6,'Tirana TIA (Rinas)',4,'2016-03-25 15:39:50',1,1,'Admin Admin','2016-04-07 23:12:13',224.00,0,0,0,'',1),(26,4,'Daimler AG',1,'Fifteen Twenty',NULL,'2016-03-25 10:00:00',6,'Tirana TIA (Rinas)','2016-03-26 10:00:00',6,'Tirana TIA (Rinas)',1,'2016-03-25 15:44:44',2,-1,NULL,NULL,56.00,0,0,0,'',0),(27,1,'Mercedes Benz - E 320',1,'Pese Fifty',NULL,'2016-03-25 10:00:00',6,'Tirana TIA (Rinas)','2016-03-26 10:00:00',6,'Tirana TIA (Rinas)',1,'2016-03-25 15:46:29',2,-1,NULL,NULL,39.00,0,0,0,'',0),(28,5,'Ford Fiesta',7,'Fifteen Twenty',NULL,'2016-03-25 10:00:00',6,'Tirana TIA (Rinas)','2016-03-26 10:00:00',6,'Tirana TIA (Rinas)',1,'2016-03-25 15:47:48',2,-1,NULL,NULL,25.00,0,0,0,'',0),(29,2,'Renault',7,'Fifteen Twenty',NULL,'2016-03-25 10:00:00',6,'Tirana TIA (Rinas)','2016-03-26 10:00:00',6,'Tirana TIA (Rinas)',1,'2016-03-25 15:51:23',2,-1,NULL,NULL,30.00,0,0,0,'',0),(30,2,'Renault',7,'Fifteen Twenty',NULL,'2016-03-25 10:00:00',6,'Tirana TIA (Rinas)','2016-03-26 10:00:00',6,'Tirana TIA (Rinas)',1,'2016-03-25 15:52:18',1,1,'Admin Admin','2016-04-07 23:25:52',30.00,0,0,0,'',1),(31,1,'Mercedes Benz - E 320',1,'Eight Tete',NULL,'2016-04-12 10:00:00',6,'Tirana TIA (Rinas)','2016-04-15 10:00:00',6,'Tirana TIA (Rinas)',3,'2016-04-07 23:10:55',0,-1,NULL,NULL,138.00,0,0,0,'',1);

/*Table structure for table `rights` */

DROP TABLE IF EXISTS `rights`;

CREATE TABLE `rights` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_rightsection` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `route_url` varchar(200) NOT NULL,
  `shadow_rights` varchar(2000) DEFAULT '0' COMMENT '0=none else is a CSV of routes of the dependant rights',
  `right_label` varchar(100) DEFAULT NULL,
  `virtual` tinyint(1) unsigned DEFAULT '0' COMMENT 'not an actual route used for dispaly',
  `active` tinyint(1) unsigned DEFAULT '1',
  PRIMARY KEY (`id`,`route_url`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `rights` */

/*Table structure for table `rightsections` */

DROP TABLE IF EXISTS `rightsections`;

CREATE TABLE `rightsections` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `section_name` varchar(100) DEFAULT NULL,
  `description` varchar(400) DEFAULT NULL,
  `id_module` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '1=system',
  PRIMARY KEY (`id`),
  KEY `id_module` (`id_module`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

/*Data for the table `rightsections` */

insert  into `rightsections`(`id`,`section_name`,`description`,`id_module`) values (1,'Users','Users list, create or edit',1),(2,'Cars','Cars list, create or edit',1),(3,'Prices','Periodic prices',1),(4,'Locations','Create pickup locations',1),(5,'Accessories','Extra accessories',1),(6,'Approvals','Pending approvals',1),(7,'Payments','Payment history',1),(20,'Client Account','The account of self-registers users',2);

/*Table structure for table `rolerights` */

DROP TABLE IF EXISTS `rolerights`;

CREATE TABLE `rolerights` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_role` int(10) unsigned NOT NULL,
  `route_url` varchar(200) DEFAULT NULL,
  `access` tinyint(1) unsigned DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `rolerights` */

/*Table structure for table `roles` */

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_name` varchar(100) DEFAULT NULL,
  `description` varchar(300) DEFAULT NULL,
  `flg_delete` tinyint(1) unsigned DEFAULT '1' COMMENT 'can be deleted or not',
  `deleted` tinyint(1) unsigned DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

/*Data for the table `roles` */

insert  into `roles`(`id`,`role_name`,`description`,`flg_delete`,`deleted`) values (1,'Admin','Has permissions to access everything by default.',0,0),(2,'Operator','Approve Reservations and Open new ones',0,1),(5,'Agency','Reseller Agency, Makes reservations',0,1),(9,'Client/Visitor','The public that registers as client to make an order/reservation',0,0);

/*Table structure for table `user_failed_logins` */

DROP TABLE IF EXISTS `user_failed_logins`;

CREATE TABLE `user_failed_logins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_users` int(10) unsigned DEFAULT '0',
  `ip_address` char(15) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `attempted` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `usersId` (`id_users`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

/*Data for the table `user_failed_logins` */

insert  into `user_failed_logins`(`id`,`id_users`,`ip_address`,`attempted`) values (1,0,'127.0.0.1',1449587190),(2,0,'127.0.0.1',1449587261),(3,0,'127.0.0.1',1449587286),(4,0,'127.0.0.1',1449778436),(5,3,'127.0.0.1',1449778609),(6,3,'127.0.0.1',1449778694),(7,3,'127.0.0.1',1449778944),(8,1,'127.0.0.1',1450203898),(9,1,'127.0.0.1',1455717135),(10,1,'127.0.0.1',1455786663),(11,1,'127.0.0.1',1458483959);

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `email_code` varchar(100) DEFAULT NULL,
  `firstname` varchar(50) DEFAULT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `id_role` tinyint(4) unsigned DEFAULT '2',
  `description` varchar(800) DEFAULT NULL,
  `tel` varchar(20) DEFAULT NULL,
  `created_at` datetime DEFAULT '0000-00-00 00:00:00',
  `updated_at` datetime DEFAULT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `status` tinyint(1) unsigned DEFAULT '1' COMMENT '0=not active 1=active 2=Blocked',
  `deleted` tinyint(1) unsigned DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `users` */

insert  into `users`(`id`,`username`,`password`,`email`,`email_code`,`firstname`,`lastname`,`id_role`,`description`,`tel`,`created_at`,`updated_at`,`remember_token`,`status`,`deleted`) values (1,'','$2y$10$ch50LN92wteXe/H0pb8aAe2i/gn9om9R3WNRnuOfIB.molviaxUKS','admin@mail.com',NULL,'Admin','Admin',1,NULL,'2483204','2015-11-16 10:23:04',NULL,'dnUCiOnIhGBFarkNZaN81mik8pwFXFTRdbXoAhElUJXprEJ0sasIpqd7nb7z',1,0),(2,'','$2y$10$CMHS6NAV.iRYv8CsQvWXSO02C83gxjRyTxVvpSxhQAp2V9aYtJOBS','one@mail.com',NULL,'One','Black',5,NULL,'20493204','2015-11-17 16:19:57',NULL,NULL,1,0),(3,'','$2y$10$gCE4ijCxJwyevvEe8sktP.fJOS9Tm7bHljLz8WZycNcGlTLLFYZha','two@mail.com',NULL,'Two','Green',9,NULL,'29043902','2015-11-17 16:20:26',NULL,'ESlQtihle3MkUVe3ujsLnKTj8ran2QwpeARmGOKPtqMmAmRz53ZJqqlFboZ0',1,0),(5,'','$2y$10$X7yyNtSbzEOk24IujMNs0emH3hiEKhZYYIajrdq.uWcVQX0zdJ.Z.','avni@mail.com',NULL,'Seven7','Eleven',9,NULL,NULL,'2016-02-19 12:01:29',NULL,NULL,1,0),(6,'','$2y$10$bPZ38STFt8Jvu7ASSBT7r.3QMvi1YSS3685HW5RSdAEbTjb6jDcP.','kater@mail.com',NULL,'Seven7','Eleven',9,NULL,NULL,'2016-02-19 12:11:00',NULL,NULL,1,0),(7,'','$2y$10$qoBCnJGfgVFLR6I5aXs0ZuMH.mvhUZ7wGhXmt1EJtbbCsNhzjeZ/W','fifteen@mail.com',NULL,'Fifteen','Twenty',9,NULL,NULL,'2016-03-25 08:45:51',NULL,'AVLuLL3Xz396xfMOXEewxLrgrMVeRuKalueKcBco56ZoXwMF8aR6r5DwQbbN',1,0),(8,'','$2y$10$d5FTRgI6b/IZiqagXtqNZudgNjpO0sjLB7tXdg1f0gNL6jyYIAEkG','pese@mail.com',NULL,'Pese','Fifty',9,NULL,NULL,'2016-03-25 10:03:25',NULL,'6i4vXoJR3WE14r8130LvoZMFIrtS5Im11G8BNlUgQsPHALJTQqpb33aVjACW',1,0),(9,'','$2y$10$HfwatKVFYJdkZVqo2m3B.e77E/gV505j0RLpkNV9TEfda2fq.jYFK','gjashte@mail.com',NULL,'Gjashte','Six',9,NULL,NULL,'2016-03-25 10:05:19',NULL,'5GxQj4X7llbi4QDA1N5eNzgfCp8ANfgA54xYLGmZrNcRo6xgUNGFhy8jiooH',1,0),(10,'','$2y$10$RSTUOeUEm/VRt2bQ0nT84.0wthVbJOxfe.F0/er6aw85npJobepn2','shtate@mail.com',NULL,'Shtate','Seven',9,NULL,NULL,'2016-03-25 15:13:46',NULL,NULL,1,0);

/*Table structure for table `vehicleaccessories` */

DROP TABLE IF EXISTS `vehicleaccessories`;

CREATE TABLE `vehicleaccessories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_vehicle` int(11) DEFAULT NULL,
  `id_accessory` int(11) DEFAULT NULL,
  `price` double(8,2) DEFAULT '0.00',
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=latin1;

/*Data for the table `vehicleaccessories` */

insert  into `vehicleaccessories`(`id`,`id_vehicle`,`id_accessory`,`price`,`created_at`) values (19,1,7,12.00,'2015-12-05 13:58:18'),(20,1,8,10.00,'2015-12-05 13:58:18'),(21,1,12,12.00,'2015-12-05 13:58:18'),(22,1,10,12.00,'2015-12-05 13:58:18'),(23,1,9,12.00,'2015-12-05 13:58:18'),(24,1,14,10.00,'2015-12-05 13:58:18'),(25,1,11,12.00,'2015-12-05 13:58:18'),(26,1,13,5.00,'2015-12-05 13:58:18'),(27,2,7,12.00,'2015-12-05 13:58:40'),(28,2,8,10.00,'2015-12-05 13:58:40'),(29,2,12,12.00,'2015-12-05 13:58:40'),(30,2,10,12.00,'2015-12-05 13:58:40'),(31,2,9,12.00,'2015-12-05 13:58:40'),(32,2,14,10.00,'2015-12-05 13:58:40'),(33,2,11,12.00,'2015-12-05 13:58:40'),(34,2,13,5.00,'2015-12-05 13:58:40'),(35,3,7,12.00,'2015-12-05 13:58:59'),(36,3,8,10.00,'2015-12-05 13:58:59'),(37,3,12,12.00,'2015-12-05 13:58:59'),(38,3,10,12.00,'2015-12-05 13:58:59'),(39,3,9,12.00,'2015-12-05 13:58:59'),(40,4,9,12.00,'2015-12-05 13:59:15'),(41,4,14,10.00,'2015-12-05 13:59:15'),(42,4,11,12.00,'2015-12-05 13:59:15'),(43,4,13,5.00,'2015-12-05 13:59:15'),(44,5,8,10.00,'2015-12-05 13:59:31'),(45,5,12,12.00,'2015-12-05 13:59:31'),(46,5,10,12.00,'2015-12-05 13:59:31'),(47,5,9,12.00,'2015-12-05 13:59:31'),(48,5,14,10.00,'2015-12-05 13:59:31'),(49,6,7,12.00,'2015-12-17 16:54:00'),(50,6,8,10.00,'2015-12-17 16:54:00'),(51,6,10,12.00,'2015-12-17 16:54:00'),(52,6,9,12.00,'2015-12-17 16:54:00'),(53,6,14,20.00,'2015-12-17 16:54:00');

/*Table structure for table `vehiclefeatures` */

DROP TABLE IF EXISTS `vehiclefeatures`;

CREATE TABLE `vehiclefeatures` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `id_vehicle` int(10) unsigned NOT NULL,
  `id_feature` int(10) unsigned NOT NULL,
  `show_in_result` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT 'display in search list/result',
  `value` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `vehiclefeatures` */

insert  into `vehiclefeatures`(`id`,`id_vehicle`,`id_feature`,`show_in_result`,`value`,`created_at`) values (5,1,4,0,'12','2015-12-02 10:06:16'),(6,1,6,0,'dy','2015-12-02 10:06:16'),(7,1,3,0,'22','2015-12-02 10:06:16');

/*Table structure for table `vehiclefueltypes` */

DROP TABLE IF EXISTS `vehiclefueltypes`;

CREATE TABLE `vehiclefueltypes` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `vehiclefueltypes` */

insert  into `vehiclefueltypes`(`id`,`name`,`deleted`) values (1,'Diesel',0),(2,'Gasoline',0),(3,'Electric',0),(4,'Gas',0);

/*Table structure for table `vehicleimages` */

DROP TABLE IF EXISTS `vehicleimages`;

CREATE TABLE `vehicleimages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_vehicle` int(10) unsigned DEFAULT NULL,
  `img_url` varchar(200) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `vehicleimages` */

insert  into `vehicleimages`(`id`,`id_vehicle`,`img_url`,`description`,`created_at`,`deleted`) values (1,1,'xRwLn1454502155.jpg','Mercedes Benz - E 320','2016-02-03 12:22:35',0),(3,1,'OI7Dz1454503505.jpg','Front view','2016-02-03 12:45:05',0),(4,2,'4EbQz1454509385.jpg','','2016-02-03 14:23:05',0),(5,3,'DlmWb1454509397.jpg','','2016-02-03 14:23:18',0),(6,4,'lRpHI1454509412.jpg','','2016-02-03 14:23:33',0),(7,5,'Df0Dq1454509432.jpg','','2016-02-03 14:23:53',0),(8,6,'cmlCk1454509452.jpg','','2016-02-03 14:24:12',0);

/*Table structure for table `vehiclelocationcosts` */

DROP TABLE IF EXISTS `vehiclelocationcosts`;

CREATE TABLE `vehiclelocationcosts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_vehicle` int(11) DEFAULT NULL,
  `id_location` int(11) DEFAULT NULL,
  `cost` double(8,2) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=latin1;

/*Data for the table `vehiclelocationcosts` */

insert  into `vehiclelocationcosts`(`id`,`id_vehicle`,`id_location`,`cost`,`created_at`) values (4,3,1,25.00,'2015-11-26 10:39:13'),(5,3,5,30.00,'2015-11-26 10:39:13'),(26,3,2,20.00,'2016-01-21 13:41:57'),(27,4,2,0.00,'2016-01-21 13:41:57'),(28,6,2,35.00,'2016-01-21 13:41:57'),(29,5,2,0.00,'2016-01-21 13:41:57'),(31,2,2,35.00,'2016-01-21 13:41:57'),(32,0,2,2.00,'2016-01-21 13:41:57'),(40,1,2,25.00,'2016-01-21 13:46:17'),(41,1,3,20.00,'2016-01-21 13:46:17'),(42,1,7,30.00,'2016-01-21 13:46:17'),(43,1,6,0.00,'2016-01-21 13:46:17'),(44,1,1,0.00,'2016-01-21 13:46:17'),(45,1,5,0.00,'2016-01-21 13:46:17'),(46,1,0,1.00,'2016-01-21 13:46:17');

/*Table structure for table `vehicleoptions` */

DROP TABLE IF EXISTS `vehicleoptions`;

CREATE TABLE `vehicleoptions` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `id_vehicle` int(10) unsigned NOT NULL,
  `id_option` int(10) unsigned NOT NULL,
  `show_in_result` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT 'display in search list/result',
  `value` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `vehicleoptions` */

insert  into `vehicleoptions`(`id`,`id_vehicle`,`id_option`,`show_in_result`,`value`,`created_at`) values (5,1,4,0,'12','2015-12-02 10:06:16'),(6,1,6,0,'dy','2015-12-02 10:06:16'),(7,1,3,0,'22','2015-12-02 10:06:16');

/*Table structure for table `vehicles` */

DROP TABLE IF EXISTS `vehicles`;

CREATE TABLE `vehicles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_vehicletype` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `chassis` varchar(200) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `make` varchar(100) DEFAULT NULL,
  `model` varchar(100) DEFAULT NULL,
  `year` smallint(6) DEFAULT NULL,
  `transmission` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '0=automatic 1=manual',
  `seats` tinyint(4) DEFAULT NULL,
  `doors` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `id_vehiclefueltype` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `baggage_size` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT 'bagage carries space',
  `airconditioning` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `standard_price` double(6,2) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `icon_image_path` varchar(500) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `vehicles` */

insert  into `vehicles`(`id`,`id_vehicletype`,`chassis`,`title`,`make`,`model`,`year`,`transmission`,`seats`,`doors`,`id_vehiclefueltype`,`baggage_size`,`airconditioning`,`standard_price`,`description`,`icon_image_path`,`created_at`,`updated_at`,`active`,`deleted`) values (1,6,NULL,'Mercedes Benz - E 320','Mercedes Benz','E 320',2015,1,5,4,1,2,0,39.00,'Mercedes Benz - E 320','xRwLn1454502155.jpg','2015-11-25 10:16:19','2016-02-08 09:04:14',1,0),(2,1,NULL,'Renault','Renault','Clio',2003,0,2,2,2,6,1,30.00,'','4EbQz1454509385.jpg','2015-11-25 10:16:19','2016-02-03 14:23:05',1,0),(3,1,NULL,'BMW X5','BMW','X5',2008,1,4,2,1,3,1,45.00,'','DlmWb1454509397.jpg','2015-11-25 10:16:19','2016-02-03 14:23:17',1,0),(4,1,NULL,'Daimler AG','Daimler AG','Mustang',2004,0,5,4,1,4,1,56.00,'','lRpHI1454509412.jpg','2015-11-25 10:16:19','2016-02-03 14:23:32',1,0),(5,1,NULL,'Ford Fiesta','Ford','Fiesta',2002,0,5,2,2,6,1,25.00,'','Df0Dq1454509432.jpg','2015-11-25 10:16:19','2016-02-03 15:12:57',1,0),(6,1,NULL,'Fiat Punto','Fiat','Punto',2002,0,5,4,1,255,1,40.00,'','cmlCk1454509452.jpg','2015-12-17 16:52:33','2016-02-03 14:24:12',1,0);

/*Table structure for table `vehicleseasonalprices` */

DROP TABLE IF EXISTS `vehicleseasonalprices`;

CREATE TABLE `vehicleseasonalprices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_vehicle` int(11) unsigned DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `start_season_month` char(2) DEFAULT NULL,
  `start_season_day` char(2) DEFAULT NULL,
  `end_season_month` char(2) DEFAULT NULL,
  `end_season_day` char(2) DEFAULT NULL,
  `price` double(8,2) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `vehicleseasonalprices` */

insert  into `vehicleseasonalprices`(`id`,`id_vehicle`,`name`,`start_season_month`,`start_season_day`,`end_season_month`,`end_season_day`,`price`,`created_at`,`deleted`) values (1,1,'Autumn/Fall','11','01','11','30',45.00,'2015-12-09 11:35:00',0),(3,1,'Winter','12','01','12','20',55.00,'2015-12-09 11:35:00',0),(4,1,'Summer','05','15','08','31',60.00,NULL,0);

/*Table structure for table `vehicletypes` */

DROP TABLE IF EXISTS `vehicletypes`;

CREATE TABLE `vehicletypes` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `size` char(2) NOT NULL DEFAULT 'SM' COMMENT 'sm=sedan/coupe md=minivan/SUV lg=van/truck',
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

/*Data for the table `vehicletypes` */

insert  into `vehicletypes`(`id`,`name`,`size`,`deleted`) values (1,'Mini','SM',0),(2,'Economy','SM',0),(3,'Compact','SM',0),(4,'Midsize','MD',0),(5,'Standard','MD',0),(6,'Premium','MD',0),(7,'Luxury','MD',0),(8,'Sports Car','MD',0),(9,'Minivan','LG',0),(10,'SUV','LG',0),(11,'Van','LG',0),(12,'Pickup','LG',0);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
