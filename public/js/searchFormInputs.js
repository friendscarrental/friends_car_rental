
$(function () {
	
	var d = new Date();//now
	var pickupDate = new Date(d.getFullYear(), d.getMonth(), d.getDate(), 10, 0);	
	var returnDate = new Date(d.getFullYear(), d.getMonth(), d.getDate(), 10, 0);
	returnDate.setDate(returnDate.getDate() + 1);
	
	$('#pickup_datepicker').datetimepicker({
		stepping: 15,
		allowInputToggle: true,
		defaultDate: pickupDate,
		format: 'lll',//format: 'MMM D, YYYY      HH:mm',//24hour format
        sideBySide: true
	});
	$('#return_datepicker').datetimepicker({
		stepping: 15,
		allowInputToggle: true,
		defaultDate: returnDate,
		format: 'lll',
        sideBySide: true,
		useCurrent: false
	});
	
	//DISABLE PREVIOUS DATES ON THE RETURN DATEPICKER ON CHANGE EVENT
	$("#pickup_datepicker").on("dp.change", function (e) {
		$('#return_datepicker').data("DateTimePicker").minDate(e.date);
		resetOnChange();
	});
	//INCREASE BY ONE DAY THE RETURN DATEPICKER ON DROPDOWN CLOSE EVENT
	$("#pickup_datepicker").on("dp.hide", function (e) {
		var sd = e.date.toDate();
		returnDate = new Date(sd.getFullYear(), sd.getMonth(), sd.getDate(), sd.getHours(), sd.getMinutes());
		returnDate.setDate(returnDate.getDate() + 1);
		$('#return_datepicker').data("DateTimePicker").date(returnDate);
	});
	
	$("#return_datepicker").on("dp.change", function (e) {
		//$('#pickup_datepicker').data("DateTimePicker").maxDate(e.date);
		resetOnChange();
	});	
	/*$('#pickup_timepicker').datetimepicker({format: 'LT'});//$('#return_timepicker')*/
});

function buildLocationsLists()
{
	var content = '';
	var listLength = locationsListData.length;
	for(var i=0; i<listLength; i++)
	{
		content += '<button type="button" class="list-group-item" data-row_index="'+i+'">'+
						'<h4 class="list-group-item-heading">'+locationsListData[i].name+'</h4>'+
						'<p class="list-group-item-text">'+locationsListData[i].locationtypes_name+' - <em>'+locationsListData[i].city+'</em></p>'+
				   '</button>';
	}
	
	$("#pickuplocationsList").html(content);
	$("#returnlocationsList").html(content);
}
	
function locationDetails(direction, index)
{
	var openHoursContent = '';
	var openhours = locationsListData[index].openhours;
	for(var i = 0; i < openhours.length; i++)
	{
		var daysRange = weekdays[openhours[i].start_day];
		if(openhours[i].start_day != openhours[i].end_day)
			daysRange += ' - '+weekdays[openhours[i].end_day];
		var hoursRange = 'Closed';
		if(openhours[i].not_operational == 0)
			hoursRange = openhours[i].open_time+' - '+openhours[i].close_time;
		
		openHoursContent += '<tr><th scope="row">'+daysRange+'</th><td scope="row">'+hoursRange+'</td></tr>';
	}
	openHoursContent += '<tr><th scope="row" colspan="2" style="font-size: .9em"><em>* After Hours Extra Charge: &nbsp; &nbsp;'+locationsListData[index].after_hours_price+' Euro</em></th></tr>';
	
	var title = '<strong>'+locationsListData[index].name+'</strong> <em class="sub-title">('+locationsListData[index].locationtypes_name+')</em>';

	var content = '<div class="row"><h4 class="col-md-7 pull-left">'+title+'</h4><div class="col-md-5"><button type="button" class="btn btn-warning pull-right" onclick="closeDropdown();">Select This Location <span class="glyphicon glyphicon-chevron-right"></span></button></div></div><hr style="margin: 5px 0 10px;">'+
				'<div class="row">'+
					'<div class="col-md-5"><address><strong>Address:</strong><br>'+locationsListData[index].address+
						'<br><strong>'+locationsListData[index].city+', Albania</strong><br>'+
						'<br><strong>Phone: </strong> '+locationsListData[index].tel;
	if(locationsListData[index].tel2 != "")
		content +=		'<br><strong>Phone 2: </strong> '+locationsListData[index].tel2;
	if(locationsListData[index].fax != "")
		content +=		'<br><strong>Fax: </strong> '+locationsListData[index].fax;
	if(locationsListData[index].email != "")
		content +=		'<br><strong>Email: </strong> <a href="'+locationsListData[index].email+'" target="_top">'+locationsListData[index].email+'</a>';					
	content += 		'</address></div>'+					
					'<div class="col-md-6 col-md-offset-1"><strong>Open Hours:</strong><br><table class="table">'+openHoursContent+'</table></div>'+					
				'</div>';
	
	setDetailsContent(direction, index, title, content);
	locationMarker({lat: parseFloat(locationsListData[index].lat), lng: parseFloat(locationsListData[index].lng)}, direction);
	
	//SET THE RETURN LOCATION TO BE THE SAME AS THE PICKUP ONE UNTIL IT IS SET MANUALLY
	if(direction == 'pickup' && returnLocationManuallyChanged==false)
		setDetailsContent('return', index, title, content);
	else 
		returnLocationManuallyChanged = true;
}

var returnLocationManuallyChanged = false;

function setDetailsContent(direction, index, title, content)
{	
	if(selectedLocations[direction+'Index'] !== index)
		resetOnChange();
	
	selectedLocations[direction+'Index'] = index;
	
	$("#"+direction+"locationDetailsPanel").html(content);
	var dropdownButtonLabel = title+' <span class="sub-title"> - '+locationsListData[index].address+', '+locationsListData[index].city+'</span>';
	$("#"+direction+"location > .dropdown-btn-label").html(dropdownButtonLabel);	
}

function closeDropdown()
{
	$('.btn-group.btn-block.open > .dropdown-toggle').dropdown('toggle');
}

var mapVars = {pickupMap: null, pickupMarker: null, returnMap: null, returnMarker: null};

function locationMarker(location, direction) 
{	
	if(mapVars[direction+'Map'] == null)
	{
		mapVars[direction+'Map'] = new google.maps.Map(document.getElementById(direction+'Map'), {
										zoom: 14,
										center: location,
										mapTypeControl: true,
										zoomControl: true,
										scaleControl: true,
										streetViewControl: true,
										mapTypeId: google.maps.MapTypeId.ROADMAP
									});
	}
	
	addMarker(location, direction);
}

// Adds a marker to the map and push to the array.
function addMarker(location, direction) 
{
	if(mapVars[direction+'Map'] == null)
	{
		setTimeout( function(){ addMarker(location, direction);} , 400);
		return;
	}
	
	if (mapVars[direction+'Marker']) {
		mapVars[direction+'Marker'].setMap(null);
		mapVars[direction+'Marker'] = null;
	}
	
	mapVars[direction+'Marker'] = new google.maps.Marker({
		position: location,
		map: mapVars[direction+'Map']
	});
	
	mapVars[direction+'Map'].setCenter(location);
}
