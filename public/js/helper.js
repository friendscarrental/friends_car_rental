/**********************************************
*	GENERAL HELPER FUNCTIONS
**********************************************/
var Utils = Utils || {};

Utils.CSRF_TOKEN = '';
	
(function ($, bootbox, window) {

	Utils.confirmDeletion = function(resource, recordID, recordNameLabel, customMessage, btnLabel)
	{
		if(customMessage == null)
			customMessage = "Are you sure you want to delete ";
			
		if(btnLabel == null)
			btnLabel = "Yes, Delete";
			
		bootbox.dialog({
		  title: "Confirm Deletion",
		  message: customMessage+recordNameLabel+"?",
		  buttons: {
			cancel: {label: "Cancel", className: "btn-default"},
			confirm: { label: btnLabel,  className: "btn-danger", callback: function() {
				$.ajax({ type: 'post',
					url: '/'+resource+'/'+recordID,
					data: {_method: 'delete', _token: Utils.CSRF_TOKEN},
					success: function(result) {
						if(result['message'])
						{
							bootbox.dialog({
								title: "Response",
								message: "<br>"+result['message']+"<br><br>",
								buttons: { ok: {label: "OK, close", className: "btn-primary"} },
								backdrop: true
							});
						}
						else 
							location.reload();
					}
				});
			}}
		  }
		});
	}

})(jQuery, bootbox, window);

Utils.priceFormat = function(value)
{
	return parseFloat(value).toFixed(2);
}

/**********************************************
*	DATE CUSTOM HELPER FUNCTIONS 
**********************************************/
var Date = Date || {};

Date.convertToMysqlNoTimeZone = function(date, includeTime)
{
	var mysql_date = "";
    if(date)
    {
		mysql_date = date.getUTCFullYear() + '-' +
					('00' + (date.getUTCMonth()+1)).slice(-2) + '-' +
					('00' + date.getDate()).slice(-2);//not used getUTCDate because of the timezone

	    if(includeTime!==false)
	    {
			mysql_date += ' '+('00' + date.getHours()).slice(-2) + ':' +
							('00' + date.getMinutes()).slice(-2) + ':00';
	    }
	    else
	        mysql_date += ' 00:00:00';
    }

    return mysql_date;
};


function redirectRequest(url, data, target)
{
	var postInput = ' method="get"> ';
	if(data != undefined)
	{
		postInput = ' method="post"> ';
		for (var key in data){
			postInput += '<input type="hidden" name="'+key+'" value="'+data[key]+'" />';
		}
	}

	var formTarget = '';//'target="_self"'
	if(target != undefined)
	{
		formTarget = 'target="'+target+'"';
	}

	var form = $('<form action="'+url+'" '+formTarget+' '+postInput+'</form>');
	$('body').append(form);
	$(form).submit();
}